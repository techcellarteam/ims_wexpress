$(document).ready(function () {
    load_datalist({action: adminURL + 'document_types/method/list_document_type'});
    //Link
    $('.add_document_type').on('click', {
        'action': adminURL + "document_types/add_document_type",
        'type': 1,
        'redirect': adminURL + 'document_types/method/list_document_type'
    }, load_dfltpopform);

    //Link
    $('#containerList').on('click', '.delete_document_type', {
        'action': adminURL + 'document_types/method/delete_document_type',
        'conMessage': "You are about to delete this document types.",
        'redirect': adminURL + 'document_types/list_document_type',
    }, dfltaction_item);
});

jQuery(function ()
{
    jQuery(document).on('click', '.edit_document_type', edit_document_type);
    jQuery(document).on('click', '.view_document_type', view_document_type);
});

function edit_document_type() {
    var id = $(this).attr('id');
    var data = {
        'data': {'action': adminURL + 'document_types/edit_document_type/' + id,
            'type': 2,
            'redirect': adminURL + 'document_types/list_document_type'
        }
    };
    load_dfltpopform(data);
}

function view_document_type() {
    var id = $(this).attr('id');
    var data = {
        'data': {'action': adminURL + 'document_types/view_document_type/' + id,
            'type': 2,
            'redirect': adminURL + 'document_types/list_document_type'
        }
    };
    load_dfltpopform(data);
}
