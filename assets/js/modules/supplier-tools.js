/*
 |--------------------------------------------------------------------------
 | jQuery Tools for supplier tools
 |--------------------------------------------------------------------------
 */
$(document).ready(function () {
    $('#formdata-save').on('click', {
        'action': adminURL + 'suppliers/method/add_supplier',
        'conMessage': "You are about to add new supplier.",
        'clear': true,
        'redirect': adminURL + 'suppliers/list_supplier'
    }, save_form_v1);

});

