/*
 |--------------------------------------------------------------------------
 | jQuery Tools for item tools
 |--------------------------------------------------------------------------
 */
$(document).ready(function () {
    load_datalist({action: adminURL + 'items/method/list_item'});
    //Link
    $('#containerList').on('click', '.delete_item', {
        'action': adminURL + 'items/method/delete_item',
        'conMessage': "You are about to delete this item.",
        'redirect': currentURL,
    }, dfltaction_item);

    $('#formdata-save').on('click', {
        'action': adminURL + 'items/method/add_item',
        'conMessage': "You are about to add new item.",
        'clear': true,
        'redirect': adminURL + 'items/add_item',
    }, save_form_v1);

    $('#formdata-edit').on('click', {
        'action': adminURL + 'items/method/edit_item',
        'id': $('#formdata-item_id').val(),
        'conMessage': "You are about to edit item.",
        'redirect': adminURL + 'items/view_item/' + $('#formdata-item_id').val(),
    }, save_form_v1);

    $('#popformdata-save').on('click', {
        'action': adminURL + 'items / method / add_item_suppliers',
        'conMessage': "You are about to add new item supplier.",
        'redirect': currentURL,
    }, save_form_v1);
});
jQuery(function ()
{
    jQuery(document).on('click', '#formdata-add_supplier', addSupplier);
    jQuery(document).on('click', '.formdata-remove_supplier', removeSupplier);
    jQuery(document).on('click', '.add_item_suppliers', addItemSupplier);
    jQuery(document).on('click', '#popformdata-add_supplier', addPopSupplier);
    jQuery(document).on('change', '#formdata-sku', checkItem);
    jQuery(document).on('change', '#formdata-description', checkItem);
    jQuery(document).on('change', '#formdata-item_type_id', checkItem);
    jQuery(document).on('change', '#formdata-item_brand_id', checkItem);
    jQuery(document).on('change', '#formdata-unit_measurement_id', checkItem);
});


/*  
 * Add Supplier
 */
function addSupplier() {
    var supplier_id = $('#formdata-supplier').val();
    var supplier_name = $('option:selected', '#formdata-supplier').attr('supplier_name');
//    var id = encode_id(supplier_id);
    var price = parseFloat($('#formdata-unit_cost').val());
    if (price && supplier_id) {
        var data = "<tr id='id_table_" + supplier_id + "'>\n\
            <td class='col-lg-1'>\n\
                <input type='hidden' class='form-control formdata' id='formdata-supplier_name[" + supplier_id + "]' value='" + supplier_name + "' />\n\
                <input type='hidden' class='form-control formdata' id='formdata-supplier_id[" + supplier_id + "]' value='" + supplier_id + "' />" + supplier_name + "</td>\n\
            <td class='col-lg-1'> <input type='text' class='form-control formdata' id='formdata-item_unit_cost[" + supplier_id + "]' value='" + price + "' /></td>\n\
            <td class='col-lg-1'>\n\
                <button class='icon16 entypo-icon-cancel formdata-remove_supplier' id='" + supplier_id + "'></button>\n\
            </td>\n\
            </tr>";
        $('.table tbody#supplier_body:last').append(data);
    } else {
        alert('Enter Valid Values');
    }
}

/*  
 * Add Supplier
 */
function addPopSupplier() {
    var supplier_id = $('#popformdata-supplier').val();
    var supplier_name = $('option:selected', '#popformdata-supplier').attr('supplier_name');
//    var id = encode_id(supplier_id);
    var price = parseFloat($('#popformdata-unit_cost').val());
    if (price && supplier_id) {
        var data = "<tr id='id_table_" + supplier_id + "'>\n\
            <td class='col-lg-1'>\n\
                <input type='hidden' class='form-control popformdata' id='popformdata-supplier_name[" + supplier_id + "]' value='" + supplier_name + "' />\n\
                <input type='hidden' class='form-control popformdata' id='popformdata-supplier_id[" + supplier_id + "]' value='" + supplier_id + "' />" + supplier_name + "</td>\n\
            <td class='col-lg-1'> <input type='hidden' class='form-control popformdata' id='popformdata-item_unit_cost[" + supplier_id + "]' value='" + price + "' />" + price + "</td>\n\
            <td class='col-lg-1'>\n\
            <button class='icon16 icomoon-icon-remove formdata-remove_supplier' id='" + supplier_id + "'></button>\n\
            </td>\n\
            </tr>";
        $('.table tbody#supplier_body:last').append(data);
    } else {
        alert('Enter Valid Values');
    }
}

/*  
 * Add Item Supplier
 */
function addItemSupplier() {
    var data = {'data': {
            'action': adminURL + 'items/add_item_suppliers',
            'id': $(this).attr('id'),
            'type': 1,
            'redirect': currentURL, }};
    load_dfltpopform(data);
}

/*  
 * Add Supplier
 */
function removeSupplier() {
    $('tr#id_table_' + $(this).attr("id")).remove();
}

/*  
 * Check if items inputted is existing
 */
function checkItem() {
    var description = $('#formdata-sku').val();
    var title = $('#formdata-description').val();
    var item_type_id = $('#formdata-item_type_id').val();
    var item_brand_id = $('#formdata-item_brand_id').val();
    var unit_measurement_id = $('#formdata-unit_measurement_id').val();
    if (description && title && item_type_id && item_brand_id && unit_measurement_id) {
        $.ajax({
            type: 'POST',
            url: adminURL + 'items/check_item',
            data: {
                'description': description,
                'title': title,
                'item_type_id': item_type_id,
                'item_brand_id': item_brand_id,
                'unit_measurement_id': unit_measurement_id
            },
            success: function (data)
            {
                var result = JSON.parse(data);
                $('.formdata_alert').html(result['message_alert']);
                if (parseInt(result['success']) === 0) { // existing
                    $('#formdata-save').addClass('disabled');
                } else {
                    $('#formdata-save').removeClass('disabled');
                }
            }
            ,
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(xhr.status);
                alert(thrownError);
            }
            ,
            beforeSend: function ()
            {
                $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
            }
        }
        );
    }
}