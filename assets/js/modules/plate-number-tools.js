$(document).ready(function () {
    load_datalist({action: adminURL + 'plate_numbers/method/list_plate_number'});
    //Link
    $('.add_plate_number').on('click', {
        'action': adminURL + "plate_numbers/add_plate_number",
        'type': 1,
        'redirect': currentURL
    }, load_dfltpopform);

    //Link
    $('#containerList').on('click', '.delete_plate_number', {
        'action': adminURL + 'plate_numbers/method/delete_plate_number',
        'conMessage': "You are about to delete this plate number.",
        'redirect': currentURL,
    }, dfltaction_item);
});

jQuery(function ()
{
    jQuery(document).on('click', '.edit_plate_number', edit_plate_number);
    jQuery(document).on('click', '.view_plate_number', view_plate_number);
});

function edit_plate_number() {
    var id = $(this).attr('id');
    var data = {
        'data': {'action': adminURL + 'plate_numbers/edit_plate_number/' + id,
            'type': 2,
            'redirect': adminURL + 'plate_numbers/list_plate_number'
        }
    };
    load_dfltpopform(data);
}

function view_plate_number() {
    var id = $(this).attr('id');
    var data = {
        'data': {'action': adminURL + 'plate_numbers/view_plate_number/' + id,
            'type': 2,
            'redirect': adminURL + 'plate_numbers/list_plate_number'
        }
    };
    load_dfltpopform(data);
}
