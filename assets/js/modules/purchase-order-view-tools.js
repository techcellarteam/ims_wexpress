/*
 |--------------------------------------------------------------------------
 | jQuery Tools for purchase order tools
 |--------------------------------------------------------------------------
 */
$(document).ready(function () {
    $('#formupload_save').on('click', {
        'action': adminURL + 'purchase_orders/method/add_attachment',
        'conMessage': "You are about to upload an attachment.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_order_id').val()
    }, save_upload);

    $('#formdata-approve').on('click', {
        'action': adminURL + 'purchase_orders/method/approve_purchase_order',
        'conMessage': "You are about to approve purchase order.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_order_id').val()
    }, save_form_v1);

    $('#formdata-decline').on('click', {
        'action': adminURL + 'purchase_orders/method/decline_purchase_order',
        'conMessage': "You are about to decline purchase order.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_order_id').val()
    }, save_form_v1);

    $('#formdata-add_comment').on('click', {
        'action': adminURL + 'purchase_orders/method/add_comment',
        'conMessage': "You are about to add comment for this purchase order.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_order_id').val()
    }, save_form_v1);

    $('.delete_attachment').on('click', {
        'action': adminURL + 'purchase_orders/method/delete_attachment',
        'conMessage': "You are about to delete this attachment.",
        'redirect': currentURL
    }, dfltaction_item);

    $('#formdata-close').on('click', {
        'action': adminURL + 'purchase_orders/method/close_purchase_order',
        'conMessage': "You are about to close Purchase Order (Transaction Complete).",
        'redirect': currentURL,
        'id': $('#formdata-purchase_order_id').val()
    }, save_form_v1);

    $('#formdata-receive').on('click', {
        'action': adminURL + 'purchase_orders/method/receive_purchase_order',
        'conMessage': "You are about to receive Purchase Order Items.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_order_id').val()
    }, save_form_v1);
    
    $('#formdata-re_po').on('click', {
        'action': adminURL + 'purchase_orders/method/re_po',
        'conMessage': "You are about to RE-PO this purchase order.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_order_id').val()
    }, save_form_v1);
    
    $('#formdata-cancel').on('click', {
        'action': adminURL + 'purchase_orders/method/cancel',
        'conMessage': "You are about to CANCEL this purchase order. Make sure to log your remarks int the History Tab.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_order_id').val()
    }, save_form_v1);
});
jQuery(function () {
    jQuery(document).on('change', '#formdata-item_type_id', itemSelect);
//    jQuery(document).on('change', '.item_check', indicateCheck);
    jQuery(document).on('change', '.received', checkReceived);//check received items
});

/*  
 * Get Items Based on Inventory Type
 */
function checkReceived() {
    var disable = 0;
    var total = 0;
    $('.received').each(function () {
        var max_to_be_received = parseInt($(this).attr('max'));
        var received = parseInt($(this).val());
        if (received > max_to_be_received) {
            $(this).css({'color': 'red'});
            disable++;
        } else {
            $(this).css({'color': 'black'});
            disable--;
        }
        total += +received;
    });
    //display total
    $('th.total_received').html(total);
    if (disable > 0) {
        $('#formdata-receive').addClass('disabled');
    }else{
        $('#formdata-receive').removeClass('disabled');
    }
}

/*  
 * Get Items Based on Inventory Type
 */
function itemSelect() {
    var item_type_id = $('option:selected', '#formdata-item_type_id').val();

    if (item_type_id) {
        $.ajax({
            type: 'POST',
            url: adminURL + 'items/method/select_item',
            data: {'item_type_id': item_type_id},
            success: function (data)
            {
                $('select#formdata-item').html(data).trigger('liszt:updated');
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(xhr.status);
                alert(thrownError);
            },
            beforeSend: function ()
            {
                $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
            }
        });
    } else {
        alert('Select Item Inventory Type');
    }
}

//function indicateCheck() {
//    var code = $(this).attr('code');
//    var val = $(this).val();
//    if (val == 1) {
//        $('i.' + code + '_' + 1).removeClass(' icomoon-icon-close');
//        $('i.' + code + '_' + 2).removeClass('icomoon-icon-checkmark-3');
//        $('i.' + code + '_' + 1).addClass('icomoon-icon-checkmark-3');
//        $('i.' + code + '_' + 2).addClass(' icomoon-icon-close');
//    } else {
//        $('i.' + code + '_' + 1).removeClass('icomoon-icon-checkmark-3');
//        $('i.' + code + '_' + 2).removeClass(' icomoon-icon-close');
//        $('i.' + code + '_' + 1).addClass(' icomoon-icon-close');
//        $('i.' + code + '_' + 2).addClass('icomoon-icon-checkmark-3');
//    }
//}

/*  
 * Add Supplier
 */
function removeItem() {
    $('tr#id_table_' + $(this).attr("id")).remove();
}