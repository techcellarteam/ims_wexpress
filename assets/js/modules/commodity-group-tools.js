$(document).ready(function () {
    load_datalist({action: adminURL + 'commodity_groups/method/list_commodity_group'});
    //Link
    $('.add_commodity_group').on('click', {
        'action': adminURL + "commodity_groups/add_commodity_group",
        'type': 1,
        'redirect': currentURL
    }, load_dfltpopform);

    //Link
    $('#containerList').on('click', '.delete_commodity_group', {
        'action': adminURL + 'commodity_groups/method/delete_commodity_group',
        'conMessage': "You are about to delete this commodity group.",
        'redirect': currentURL,
    }, dfltaction_item);
});

jQuery(function ()
{
    jQuery(document).on('click', '.edit_commodity_group', edit_commodity_group);
    jQuery(document).on('click', '.view_commodity_group', view_commodity_group);
});

function edit_commodity_group() {
    var id = $(this).attr('id');
    var data = {
        'data': {'action': adminURL + 'commodity_groups/edit_commodity_group/' + id,
            'type': 2,
            'redirect': adminURL + 'commodity_groups/list_commodity_group'
        }
    };
    load_dfltpopform(data);
}

function view_commodity_group() {
    var id = $(this).attr('id');
    var data = {
        'data': {'action': adminURL + 'commodity_groups/view_commodity_group/' + id,
            'type': 2,
            'redirect': adminURL + 'commodity_groups/list_commodity_group'
        }
    };
    load_dfltpopform(data);
}
