$(document).ready(function () {
    load_datalist({action: adminURL + 'business_units/method/list_business_unit'});
    //Link
    $('.add_business_unit').on('click', {
        'action': adminURL + "business_units/add_business_unit",
        'type': 1,
        'redirect': currentURL
    }, load_dfltpopform);

    //Link
    $('#containerList').on('click', '.delete_business_unit', {
        'action': adminURL + 'business_units/method/delete_business_unit',
        'conMessage': "You are about to delete this business_unit.",
        'redirect': adminURL + 'business_units/list_business_unit',
    }, dfltaction_item);
});

jQuery(function ()
{
    jQuery(document).on('click', '.edit_business_unit', edit_business_unit);
    jQuery(document).on('click', '.view_business_unit', view_business_unit);
});

function edit_business_unit() {
    var id = $(this).attr('id');
    var data = {
        'data': {'action': adminURL + 'business_units/edit_business_unit/' + id,
            'type': 2,
            'redirect': adminURL + 'business_units/list_business_unit'
        }
    };
    load_dfltpopform(data);
}

function view_business_unit() {
    var id = $(this).attr('id');
    var data = {
        'data': {'action': adminURL + 'business_units/view_business_unit/' + id,
            'type': 2,
            'redirect': adminURL + 'business_units/list_business_unit'
        }
    };
    load_dfltpopform(data);
}
