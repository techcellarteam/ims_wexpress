/*
 |--------------------------------------------------------------------------
 | jQuery Tools for payment request tools
 |--------------------------------------------------------------------------
 */
$(document).ready(function () {
    $(".datepicker").datepicker({changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", minDate: 0});
    load_datalist({action: adminURL + 'payment_requests/method/list_payment_request'});

    $('#containerList').on('click', '.delete_payment_request', {
        'action': adminURL + 'payment_requests/method/delete_payment_request',
        'conMessage': "You are about to delete this payment request.",
        'redirect': currentURL,
    }, dfltaction_item);

    $('#formdata-add_prf').on('click', {
        'action': adminURL + 'payment_requests/method/add_payment_request',
        'conMessage': "You are about to create RFP.",
        'redirect': adminURL + 'payment_requests/list_payment_request',
        'id': $('#formdata-receiving_report_id').val(),
    }, save_form_v1);

    $('#formdata-edit_prf').on('click', {
        'action': adminURL + 'payment_requests/method/edit_payment_request',
        'conMessage': "You are about to edit RFP.",
        'redirect': adminURL + 'payment_requests/view_payment_request/' + $('#formdata-payment_request_id').val(),
        'id': $('#formdata-payment_request_id').val(),
    }, save_form_v1);

    $('#formdata-add_comment').on('click', {
        'action': adminURL + 'payment_requests/method/add_comment',
        'conMessage': "You are about to add comment in this transaction.",
        'redirect': currentURL,
        'id': $('#formdata-payment_request_id').val()
    }, save_form_v1);
});

jQuery(function () {
    jQuery(document).on('change', '#formdata-receiving_report_id', checkData);
    jQuery(document).on('click', '.add_date_forwarded', addDateForwarded);
});

function addDateForwarded() {
    var id = $(this).attr('id');
    var data = {
        'data': {'action': adminURL + 'payment_requests/display_date_input',
            'id': id,
            'type': 2,
            'redirect': adminURL + 'payment_requests/list_payment_request'
        }
    };
    load_dfltpopform(data);
}

//function addDateForwarded() { 
//    var payment_request_id = $(this).attr('id');
//    if (payment_request_id) {
//        $.ajax({
//            type: 'POST',
//            url: adminURL + 'payment_requests/display_date_input',
//            data: {'payment_request_id': payment_request_id},
//            success: function (data)
//            {
//                $('div#custom_field').html('');
//                $('div#dfltmodal').html(data);
//            },
//            error: function (xhr, ajaxOptions, thrownError)
//            {
//                alert(xhr.status);
//                alert(thrownError);
//            },
//            beforeSend: function ()
//            {
//                $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
//            }
//        });
//    } else {
//        alert('Select Payment Request');
//    }
//}

function checkData() {
    var receiving_id = $('#formdata-receiving_report_id').val();
    if (receiving_id) {
        $.ajax({
            type: 'POST',
            url: adminURL + 'payment_requests/method/getPRFdetails',
            data: {'receiving_report_id': receiving_id},
            success: function (data)
            {
                $('.table tbody#particulars:last').append(data);
                $('div#custom_field').html('');
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(xhr.status);
                alert(thrownError);
            },
            beforeSend: function ()
            {
                $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
            }
        });
    } else {
        alert('Select Receiving Report');
    }
}