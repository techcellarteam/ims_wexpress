/*
 |--------------------------------------------------------------------------
 | jQuery Tools for issuance tools
 |--------------------------------------------------------------------------
 */
$(document).ready(function () {
    $(".datepicker").datepicker({changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", minDate: 0});
    load_datalist({action: adminURL + 'issuances/method/list_issuance'});

    $('#containerList').on('click', '.delete_issuance', {
        'action': adminURL + 'issuances/method/delete_issuance',
        'conMessage': "You are about to delete this issuance.",
        'redirect': currentURL,
    }, dfltaction_item);

    $('#formdata-add_issuance').on('click', {
        'action': adminURL + 'issuances/method/add_issuance',
        'conMessage': "You are about to create Issuance Transaction.",
        'redirect': adminURL + 'issuances/list_issuance'
    }, save_form_v1);

    $('#formdata-edit_issuance').on('click', {
        'action': adminURL + 'issuances/method/edit_issuance',
        'conMessage': "You are about to edit Issuance Transaction.",
        'redirect': adminURL + 'issuances/view_issuance/' + $('#formdata-issuance_id').val(),
        'id': $('#formdata-issuance_id').val(),
    }, save_form_v1);

    $('#formdata-release_issuance').on('click', {
        'action': adminURL + 'issuances/method/release_issuance',
        'conMessage': "You are about to release items in this transaction.",
        'redirect': adminURL + 'issuances/view_issuance/' + $('#formdata-issuance_id').val(),
        'id': $('#formdata-issuance_id').val(),
    }, save_form_v1);

    $('#formdata-add_comment').on('click', {
        'action': adminURL + 'issuances/method/add_comment',
        'conMessage': "You are about to add comment in this transaction.",
        'redirect': currentURL,
        'id': $('#formdata-issuance_id').val()
    }, save_form_v1);
});

jQuery(function () {
    jQuery(document).on('change', '#formdata-stock_classification_id', itemSelect);
    jQuery(document).on('click', '#formdata-add_item', addItem);
    jQuery(document).on('click', '.formdata-remove_item', removeItem);
    jQuery(document).on('change', '.quantities', total_amount);
});

/*  
 * Add Supplier
 */
function addItem() {
    var added_items = [];
    $('.items').each(function () {
        added_items[$(this).val()] = parseInt($(this).val());
    });
//        var id_item = parseInt($('#formdata-item').val());


//    if (id_item) {
//        if ($.inArray(id_item, added_items) <= -1) {
    $.ajax({
        type: 'POST',
        url: adminURL + 'issuances/method/get_item_detail',
        data: {'selected_items': $('select[id="formdata-item"]').val(),
//                    'id_item': id_item,
            'added_items': added_items
        },
        success: function (data)
        {
            $('.table tbody#particulars:last').append(data);
            total_amount();
        },
        error: function (xhr, ajaxOptions, thrownError)
        {
            alert(xhr.status);
            alert(thrownError);
        },
        beforeSend: function ()
        {
            $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
        }
    });
//        } else {
//            alert("Item Already Added");
//        }
//    }

}

/*  
 * Get Items Based on Inventory Type
 */
function itemSelect() {
    var stock_classification_id = $('option:selected', '#formdata-stock_classification_id').val();
    var added_items = [];
    $('.items').each(function () {
        added_items[$(this).val()] = parseInt($(this).val());
    });
    if (stock_classification_id) {
        $.ajax({
            type: 'POST',
            url: adminURL + 'issuances/method/select_location_item',
            data: {'stock_classification_id': stock_classification_id,
                'added_items': added_items},
            success: function (data)
            {
                $('select#formdata-item').html(data).trigger('chosen:updated');
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(xhr.status);
                alert(thrownError);
            },
            beforeSend: function ()
            {
                $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
            }
        });
    } else {
        alert('Select Item Inventory Type');
    }
}

function total_amount() {
    var total_quantity = 0;
    var disable = 0;
    $('.quantities').each(function () {
        var val = parseInt($(this).val());
        var max_val = parseInt($(this).attr('max'));
        total_quantity += +parseInt(val);
        if (val > max_val || val < 0) {
            $(this).css({'color': 'red'});
            disable++;
        } else {
            $(this).css({'color': 'black'});
        }
    });

    if (disable > 0) {
        $('#formdata-add_issuance').addClass('disabled');
    } else {
        $('#formdata-add_issuance').removeClass('disabled');
    }

//    var total_amount = 0;
//    $('.total').each(function () {
//        total_amount += +parseFloat($(this).html().replace(/,/g, ""));
//    });
    $('#formdata-total_quantity').html(total_quantity);
//    $('#formdata-total_amount').html(parseFloat(total_amount).toFixed(2));
}


/*  
 * Add Supplier
 */
function removeItem() {
    $('tr#id_table_' + $(this).attr("id")).remove();
    total_amount();
}