$(document).ready(function () {
    load_datalist({action: adminURL + 'unit_measurements/method/list_unit_measurement'});
    //Link
    $('.add_unit_measurement').on('click', {
        'action': adminURL + "unit_measurements/add_unit_measurement",
        'type': 1,
        'redirect': adminURL + 'unit_measurements/list_unit_measurement',
    }, load_dfltpopform);

    //Link
    $('#containerList').on('click', '.delete_unit_measurement', {
        'action': adminURL + 'unit_measurements/method/delete_unit_measurement',
        'conMessage': "You are about to delete this unit measurement.",
        'redirect': adminURL + 'unit_measurements/list_unit_measurement',
    }, dfltaction_item);
});

jQuery(function ()
{
    jQuery(document).on('click', '.edit_unit_measurement', edit_unit_measurement);
    jQuery(document).on('click', '.view_unit_measurement', view_unit_measurement);
});

function edit_unit_measurement() {
    var id = $(this).attr('id');
    var data = {
        'data': {'action': adminURL + 'unit_measurements/edit_unit_measurement/' + id,
            'type': 2,
            'redirect': adminURL + 'unit_measurements/list_unit_measurement'
        }
    };
    load_dfltpopform(data);
}

function view_unit_measurement() {
    var id = $(this).attr('id');
    var data = {
        'data': {'action': adminURL + 'unit_measurements/view_unit_measurement/' + id,
            'type': 2,
            'redirect': adminURL + 'unit_measurements/list_unit_measurement'
        }
    };
    load_dfltpopform(data);
}
