/*
 |--------------------------------------------------------------------------
 | jQuery Tools for purchase requisition tools
 |--------------------------------------------------------------------------
 */
$(document).ready(function () {
    $(".datepicker").datepicker({changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", minDate: 0});
    load_datalist({action: adminURL + 'purchase_requisitions/method/list_purchase_requisition'});
    //Link
    $('#containerList').on('click', '.delete_purchase_requisition', {
        'action': adminURL + 'purchase_requisitions/method/delete_purchase_requisition',
        'conMessage': "You are about to delete this purchase requisition.",
        'redirect': currentURL,
    }, dfltaction_item);
    $('#formdata-save').on('click', {
        'action': adminURL + 'purchase_requisitions/method/add_purchase_requisition',
        'conMessage': "You are about to add new purchase requisition.",
        'clear': true,
        'redirect': adminURL + 'purchase_requisitions/list_purchase_requisition'
    }, save_form_v1);
    $('#formdata-auto_approve').on('click', {
        'action': adminURL + 'purchase_requisitions/method/auto_approve_purchase_requisition',
        'conMessage': "You are about to add new purchase requisition.",
        'clear': true,
        'redirect': adminURL + 'purchase_requisitions/list_purchase_requisition'
    }, save_form_v1);
    $('#formdata-add_submit').on('click', {
        'action': adminURL + 'purchase_requisitions/method/add_submit_purchase_requisition',
        'conMessage': "You are about to submit purchase requisition.",
        'redirect': adminURL + 'purchase_requisitions/list_purchase_requisition',
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_form_v1);
    $('#formdata-edit').on('click', {
        'action': adminURL + 'purchase_requisitions/method/edit_purchase_requisition',
        'id': $('#formdata-purchase_requisition_id').val(),
        'conMessage': "You are about to edit purchase requisition.",
        'redirect': adminURL + 'purchase_requisitions/view_purchase_requisition/' + $('#formdata-purchase_requisition_id').val(),
    }, save_form_v1);

    check_fleet();
});
jQuery(function ()
{
    jQuery(document).on('change', '#formdata-business_unit_id', check_fleet);
    jQuery(document).on('change', '#formdata-stock_classification_id', itemSelect);
    jQuery(document).on('click', '#formdata-add_item', addItem);
    jQuery(document).on('click', '.formdata-remove_item', removeItem);
    jQuery(document).on('change', '.initiators_price', total);
});
function check_fleet() {
    var bu = $('#formdata-business_unit_id :selected').text();
    var string = 'leet'; //tfleet
    $('.chosen-container').css('width', '100%');
    if (bu.match(string)) {
        $('.tfleet_code').removeClass('hidden');
    } else {
        $('.tfleet_code').addClass('hidden');
    }
    $('#formdata-stock_classification_id').trigger('click');
}

function total() {
    var initiators_price = parseFloat($(this).val());
    var quantity = parseInt($(this).parent().prev().find('input').val());
    var amount = parseFloat(initiators_price * quantity);
    $(this).parent().next().html(amount.toFixed(2));
}

function total_amount() {
    var total_quantity = 0;
    $('.quantities').each(function () {
        total_quantity += +parseInt($(this).val());
    });
    var total_amount = 0;
    $('.total').each(function () {
        total_amount += +parseFloat($(this).html().replace(/,/g, ""));
    });
    $('#formdata-total_quantity').html(total_quantity);
    $('#formdata-total_amount').html(parseFloat(total_amount).toFixed(2));
}


/*  
 * Add Supplier
 */
function addItem() {
    var item_id = parseInt($('#formdata-item').val());
    var quantity = parseFloat($('#formdata-qty').val());
    var initiators_price = parseFloat($('#formdata-initiators_input_price').val());
    var remarks = $('textarea#formdata-init_remarks').val();
    var added_items = [];
    $('.items').each(function () {
        added_items[$(this).val()] = parseInt($(this).val());
    });
    if (item_id && (initiators_price > 0)) {
        if ($.inArray(item_id, added_items) <= -1) {
            $.ajax({
                type: 'POST',
                url: adminURL + 'items/method/get_item_detail',
                data: {'item_id': item_id,
                    'quantity': quantity,
                    'initiators_price': initiators_price,
                    'remarks': remarks,
                    'added_items': added_items
                },
                success: function (data)
                {
                    $('.table tbody#pr_items_body:last').append(data);
                    $('select#formdata-stock_classification_id').trigger('change');
                    $('#formdata-initiators_input_price').val('');
                    $('#formdata-qty').val('1');
                    $('textarea#formdata-init_remarks').val()
                    total_amount();
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    alert(xhr.status);
                    alert(thrownError);
                },
                beforeSend: function ()
                {
                    $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
                }
            });
        } else {
            alert("Item Already Added");
        }
    } else {
        if (initiators_price) {
            if ($.inArray(initiators_price.toLowerCase(), added_request) <= -1) {
                $.ajax({
                    type: 'POST',
                    url: adminURL + 'items/method/set_item_input',
                    data: {'initiators_price': initiators_price,
                        'quantity': quantity,
                        'added_request': added_request
                    },
                    success: function (data)
                    {

                        $('.table tbody#pr_items_body:last').append(data);
                        $('#formdata-initiators_input_price').val('');
                        $('#formdata-qty').val('1');
//                    $('select#formdata-item').html('<option>Select Product</option>').trigger('liszt:updated');
                    },
                    error: function (xhr, ajaxOptions, thrownError)
                    {
                        alert(xhr.status);
                        alert(thrownError);
                    },
                    beforeSend: function ()
                    {
                        $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
                    }
                });
            } else {
                alert("Item Already Added");
            }
        } else {
            alert("Select Item");
        }
    }

}

/*  
 * Get Items Based on Inventory Type
 */
function itemSelect() {
    var stock_classification_id = $('option:selected', '#formdata-stock_classification_id').val();
    var bu_id = $('option:selected', '#formdata-business_unit_id').val();
    //for edit page
    if (bu_id == undefined) {
        var bu_id = $('#formdata-business_unit_id').val();
    }
    if (stock_classification_id && bu_id) {
        $.ajax({
            type: 'POST',
            url: adminURL + 'items/method/select_item',
            data: {'stock_classification_id': stock_classification_id,
                'business_unit_id': bu_id,
            },
            success: function (data)
            {
                $('select#formdata-item').html(data).trigger('chosen:updated');
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(xhr.status);
                alert(thrownError);
            },
            beforeSend: function ()
            {
                $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
            }
        });
    } else {
        alert('Select Item Inventory Type / Business Unit for PR');
    }
}

/*  
 * Add Supplier
 */
function removeItem() {
    $('tr#id_table_' + $(this).attr("id")).remove();
    total_amount();
}