/*
 |--------------------------------------------------------------------------
 | jQuery Tools for purchase order tools
 |--------------------------------------------------------------------------
 */
$(document).ready(function () {
    $(".datepicker").datepicker({changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", maxDate: 1});
    load_datalist({action: adminURL + 'receiving_orders/method/list_receiving_order'});

    $('#formdata-add_comment').on('click', {
        'action': adminURL + 'receiving_orders/method/add_comment',
        'conMessage': "You are about to add comment in this transaction.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_order_id').val()
    }, save_form_v1);

    $('#formdata-receive').on('click', {
        'action': adminURL + 'receiving_orders/method/receive_purchase_order',
        'conMessage': "You are about to receive in this transaction.",
        'redirect': adminURL + 'receiving_orders/view_receiving_order/' + $('#formdata-purchase_order_id').val(),
        'id': $('#formdata-purchase_order_id').val()
    }, save_form_v1);

    $('#formdata-edit_receive').on('click', {
        'action': adminURL + 'receiving_orders/method/edit_receive_po',
        'conMessage': "You are about to edit received in this transaction.",
        'redirect': adminURL + 'receiving_orders/view_receiving_order/' + $('#formdata-purchase_order_id').val(),
        'id': $('#formdata-purchase_order_id').val()
    }, save_form_v1);

});
jQuery(function () {
    jQuery(document).on('change', '.received', checkReceived);//check received items
});

/*  
 * Get Items Based on Inventory Type
 */
function checkReceived() {
    var disable = 0;
    var total = 0;
    $('.received').each(function () {
        var max_to_be_received = parseInt($(this).attr('max'));
        var received = parseInt($(this).val());
        if (received > max_to_be_received) {
            $(this).css({'color': 'red'});
            disable++;
        } else {
            $(this).css({'color': 'black'});
            disable--;
        }
        total += +received;
    });
    if (total == 0) {
        $('#formdata-receive').addClass('disabled');
        $('#formdata-edit_receive').addClass('disabled');
    } else {
        $('#formdata-receive').removeClass('disabled');
        $('#formdata-edit_receive').removeClass('disabled');
    }
    
    //display total
    $('th.total_received').html(total);
    if (disable > 0) {
        $('#formdata-receive').addClass('disabled');
        $('#formdata-edit_receive').addClass('disabled');
    } else if (disable == 0) {
        $('#formdata-receive').addClass('disabled');
        $('#formdata-edit_receive').addClass('disabled');
    } else {
        $('#formdata-receive').removeClass('disabled');
        $('#formdata-edit_receive').removeClass('disabled');
    }
}

/*  
 * Show Items
 */
function view_rri(id) {
    $('table#table_' + id).removeClass('hidden');
    $('#close_rri_' + id).removeClass('hidden');
    $('#view_rri_' + id).addClass('hidden');
}
/*  
 * Hide Items
 */
function close_rri(id) {
    $('table#table_' + id).addClass('hidden');
    $('#close_rri_' + id).addClass('hidden');
    $('#view_rri_' + id).removeClass('hidden');
}