/*
 |--------------------------------------------------------------------------
 | jQuery Tools for purchase order tools
 |--------------------------------------------------------------------------
 */
$(document).ready(function () {
    $(".datepicker").datepicker({changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", minDate: 0});

    $('#formdata-add_comment').on('click', {
        'action': adminURL + 'full_deliveries/method/add_comment',
        'conMessage': "You are about to add comment in this transaction.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_order_id').val()
    }, save_form_v1);

});

/*  
 * Show Items
 */
function view_rri(id) {
    $('table#table_' + id).removeClass('hidden');
    $('#close_rri_' + id).removeClass('hidden');
    $('#view_rri_' + id).addClass('hidden');
}
/*  
 * Hide Items
 */
function close_rri(id) {
    $('table#table_' + id).addClass('hidden');
    $('#close_rri_' + id).addClass('hidden');
    $('#view_rri_' + id).removeClass('hidden');
}