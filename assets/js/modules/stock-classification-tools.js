$(document).ready(function () {
    load_datalist({action: adminURL + 'stock_classifications/method/list_stock_classification'});
    //Link
    $('.add_stock_classification').on('click', {
        'action': adminURL + "stock_classifications/add_stock_classification",
        'type': 1,
        'redirect': currentURL
    }, load_dfltpopform);

    //Link
    $('#containerList').on('click', '.delete_stock_classification', {
        'action': adminURL + 'stock_classifications/method/delete_stock_classification',
        'conMessage': "You are about to delete this commodity group.",
        'redirect': currentURL,
    }, dfltaction_item);
});

jQuery(function ()
{
    jQuery(document).on('click', '.edit_stock_classification', edit_stock_classification);
    jQuery(document).on('click', '.view_stock_classification', view_stock_classification);
});

function edit_stock_classification() {
    var id = $(this).attr('id');
    var data = {
        'data': {'action': adminURL + 'stock_classifications/edit_stock_classification/' + id,
            'type': 2,
            'redirect': adminURL + 'stock_classifications/list_stock_classification'
        }
    };
    load_dfltpopform(data);
}

function view_stock_classification() {
    var id = $(this).attr('id');
    var data = {
        'data': {'action': adminURL + 'stock_classifications/view_stock_classification/' + id,
            'type': 2,
            'redirect': adminURL + 'stock_classifications/list_stock_classification'
        }
    };
    load_dfltpopform(data);
}
