/*
 |--------------------------------------------------------------------------
 | jQuery Tools for purchase order tools
 |--------------------------------------------------------------------------
 */
$(document).ready(function () {
    $(".datepicker").datepicker({changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", minDate: 0});
    load_datalist({action: adminURL + 'purchase_orders/method/list_purchase_order'});
    //Link
    $('#containerList').on('click', '.delete_purchase_order', {
        'action': adminURL + 'purchase_orders/method/delete_purchase_order',
        'conMessage': "You are about to delete this purchase order.",
        'redirect': currentURL,
    }, dfltaction_item);

    $('#formdata-save').on('click', {
        'action': adminURL + 'purchase_orders/method/add_purchase_order',
        'conMessage': "You are about to add new purchase order.",
        'clear': true,
        'redirect': adminURL + 'purchase_orders/list_purchase_order'
    }, save_form_v1);

    $('#formdata-edit').on('click', {
        'action': adminURL + 'purchase_orders/method/edit_purchase_order',
        'id': $('#formdata-purchase_order_id').val(),
        'conMessage': "You are about to edit purchase order.",
        'redirect': adminURL + 'purchase_orders/view_purchase_order/' + $('#formdata-purchase_order_id').val(),
    }, save_form_v1);
});
jQuery(function ()
{
    jQuery(document).on('change', '#formdata-group_id', getDetails);
    jQuery(document).on('change', '#formdata-purchase_requisition_id', getCommodityGroup);
    jQuery(document).on('change', '#formdata-stock_classification_id', itemSelect);
    jQuery(document).on('change', '#formdata-edit_stock_classification_id', itemEditSelect);
    jQuery(document).on('click', '#formdata-add_item', addItem);
    jQuery(document).on('click', '.formdata-remove_item', removeItem);
    jQuery(document).on('keyup', '.quantity', total_by_quantity);
    jQuery(document).on('keyup', '.purchase_price', total_by_amount);
});

function getDetails() {
    var group_id = $('#formdata-group_id').val();
    if (group_id) {
        //get suppliers based on group ID
        $.ajax({
            type: 'POST',
            url: adminURL + 'suppliers/method/supplier_select',
            data: {'where': "(`general_group_id` = " + group_id + ")"},
            success: function (data)
            {
                $('select#formdata-supplier_id').html(data).trigger('chosen:updated');
                $('div#custom_field').html('');
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(xhr.status);
                alert(thrownError);
            },
            beforeSend: function ()
            {
                $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
            }
        });
        //get purchase requisitions based on group ID
        $.ajax({
            type: 'POST',
            url: adminURL + 'purchase_requisitions/method/pr_select',
            data: {'where': "(`dty`.`general_group_id` =" + group_id + " ) AND (`status` = 'Generate PO' OR `status` like '%Partially%')"},
            success: function (data)
            {
                $('select#formdata-purchase_requisition_id').html(data).trigger('chosen:updated');
                $('div#custom_field').html('');
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(xhr.status);
                alert(thrownError);
            },
            beforeSend: function ()
            {
                $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
            }
        });
    }
}

function getCommodityGroup() {
    var group_id = $('#formdata-group_id').val();
    var purchase_requisition_id = $('#formdata-purchase_requisition_id').val();
    $.ajax({
        type: 'POST',
        url: adminURL + 'purchase_requisitions/method/stock_classification_select',
        data: {'general_group_id': group_id,
            'purchase_requisition_id': purchase_requisition_id
        },
        success: function (data)
        {
            $('select#formdata-stock_classification_id').html(data).trigger('chosen:updated');
            $('div#custom_field').html('');
        },
        error: function (xhr, ajaxOptions, thrownError)
        {
            alert(xhr.status);
            alert(thrownError);
        },
        beforeSend: function ()
        {
            $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
        }
    });
}

function total_by_quantity() {
    var quantity = parseFloat($(this).val());
    var max_qty = parseFloat($(this).attr('max'));

    if (quantity > max_qty) {
        $(this).next().html('Invalid Input Quantity');
    } else {
        $(this).next().html('');
    }

    var purchasing_price = parseInt($(this).parent().prev().find('input').val());
    var amount = parseFloat(purchasing_price * quantity);
    $(this).parent().next().html(amount.toFixed(2));
    total_amount();

}

function total_by_amount() {
    var purchasing_price = parseFloat($(this).val());
    var quantity = parseInt($(this).parent().next().find('input').val());
    var amount = parseFloat(purchasing_price * quantity);
    $(this).parent().next().next().html(amount.toFixed(2));
    total_amount();
}

function total_amount() {
    var total_quantity = 0;
    $('.quantity').each(function () {
        total_quantity += +parseInt($(this).val());
    });

    var total_amount = 0;
    $('.po_amount').each(function () {
        total_amount += +parseFloat($(this).html().replace(/,/g, ""));
    });
    $('#total_quantity').html(total_quantity);
    $('#total_amount').html(parseFloat(total_amount).toFixed(2));
    $('#formdata-total_amount').val(parseFloat(total_amount));
}


/*  
 * Add Supplier
 */
function addItem() {
    var item_id = parseInt($('#formdata-item').val());
    var added_items = [];
    $('.items').each(function () {
        added_items[$(this).val()] = parseInt($(this).val());
    });
    if (item_id) {
        if ($.inArray(item_id, added_items) <= -1) {
            $.ajax({
                type: 'POST',
                url: adminURL + 'purchase_requisitions/method/get_item_detail',
                data: {'item_id': item_id,
                    'added_items': added_items,
                    'purchase_requisition_id': $('#formdata-purchase_requisition_id').val()
                },
                success: function (data)
                {
                    $('.table tbody#po_items_body:last').append(data);
                    $('select#formdata-item').html('<option>Select Product</option>').trigger('chosen:updated');
                    $('select#formdata-stock_classification_id').trigger('change');
                    $('div#custom_field').html('');
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    alert(xhr.status);
                    alert(thrownError);
                },
                beforeSend: function ()
                {
                    $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
                }
            });
        } else {
            alert("Item Already Added");
        }
    }

}

/*  
 * Get Items Based on Inventory Type
 */
function itemSelect() {
    var stock_classification_id = $('option:selected', '#formdata-stock_classification_id').val();
    var pr_id = $('#formdata-purchase_requisition_id').val();
    if (stock_classification_id && pr_id) {
        $.ajax({
            type: 'POST',
            url: adminURL + 'purchase_requisitions/method/get_pr_items',
            data: {'stock_classification_id': stock_classification_id,
                'pr_id': pr_id},
            success: function (data)
            {
                $('select#formdata-item').html(data).trigger('chosen:updated');
                $('div#custom_field').html('');
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(xhr.status);
                alert(thrownError);
            },
            beforeSend: function ()
            {
                $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
            }
        });
    } else {
        alert('Select Stock Classification');
    }
}
/*  
 * Get Items Based on Inventory Type
 */
function itemEditSelect() {
    var stock_classification_id = $('option:selected', '#formdata-edit_stock_classification_id').val();
    var pr_id = $('#formdata-purchase_requisition_id').val();
    var added_items = '';
    $('.items').each(function () {
        if (added_items != '') {
            added_items = added_items.concat(',' + $(this).val());
        } else {
            added_items = $(this).val();
        }
    });
    if (stock_classification_id && pr_id) {
        $.ajax({
            type: 'POST',
            url: adminURL + 'purchase_requisitions/method/edit_pr_items',
            data: {'stock_classification_id': stock_classification_id,
                'pr_id': pr_id,
                'added_items': added_items},
            success: function (data)
            {
                $('select#formdata-item').html(data).trigger('chosen:updated');
                $('div#custom_field').html('');
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(xhr.status);
                alert(thrownError);
            },
            beforeSend: function ()
            {
                $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
            }
        });
    } else {
        alert('Select Stock Classification');
    }
}

/*  
 * Add Supplier
 */
function removeItem() {
    $('tr#id_table_' + $(this).attr("id")).remove();
    itemEditSelect();
    total_amount();
}