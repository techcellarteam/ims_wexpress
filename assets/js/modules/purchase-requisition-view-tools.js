/*
 |--------------------------------------------------------------------------
 | jQuery Tools for purchase requisition tools
 |--------------------------------------------------------------------------
 */
$(document).ready(function () {
    $('#formupload_save').on('click', {
        'action': adminURL + 'purchase_requisitions/method/add_attachment',
        'conMessage': "You are about to upload an attachment.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_upload);

    $('#formdata-submit').on('click', {
        'action': adminURL + 'purchase_requisitions/method/submit_purchase_requisition',
        'conMessage': "You are about to submit purchase requisition.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_form_v1);

    $('#formdata-first_level_approval').on('click', {
        'action': adminURL + 'purchase_requisitions/method/first_level_approval',
        'conMessage': "You are about to approve purchase requisition.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_form_v1);

    $('#formdata-second_level_approval').on('click', {
        'action': adminURL + 'purchase_requisitions/method/second_level_approval',
        'conMessage': "You are about to approve purchase requisition.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_form_v1);

    $('#formdata-third_level_approval').on('click', {
        'action': adminURL + 'purchase_requisitions/method/third_level_approval',
        'conMessage': "You are about to approve purchase requisition.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_form_v1);

    $('#formdata-fourth_level_approval').on('click', {
        'action': adminURL + 'purchase_requisitions/method/fourth_level_approval',
        'conMessage': "You are about to approve purchase requisition.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_form_v1);

    $('#formdata-branch_level_approval').on('click', {
        'action': adminURL + 'purchase_requisitions/method/branch_level_approval',
        'conMessage': "You are about to approve purchase requisition.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_form_v1);
    
    $('#formdata-gtwy_level_approval').on('click', {
        'action': adminURL + 'purchase_requisitions/method/gtwy_level_approval',
        'conMessage': "You are about to approve purchase requisition.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_form_v1);

    $('#formdata-svp_level_approval').on('click', {
        'action': adminURL + 'purchase_requisitions/method/svp_level_approval',
        'conMessage': "You are about to approve purchase requisition.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_form_v1);

    $('#formdata-decline').on('click', {
        'action': adminURL + 'purchase_requisitions/method/decline_purchase_requisition',
        'conMessage': "You are about to decline purchase requisition.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_form_v1);

    $('#formdata-reQueue').on('click', {
        'action': adminURL + 'purchase_requisitions/method/requeue_purchase_requisition',
        'conMessage': "You are about to reQueue purchase requisition. Make sure to input your comment in the the History Tab (below).",
        'redirect': currentURL,
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_form_v1);

    $('#formdata-add_comment').on('click', {
        'action': adminURL + 'purchase_requisitions/method/add_comment',
        'conMessage': "You are about to add comment for this purchase requisition.",
        'redirect': currentURL,
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_form_v1);

    $('.delete_attachment').on('click', {
        'action': adminURL + 'purchase_requisitions/method/delete_attachment',
        'conMessage': "You are about to delete this attachment.",
        'redirect': currentURL
    }, dfltaction_item);

    $('#formdata-close').on('click', {
        'action': adminURL + 'purchase_requisitions/method/close_purchase_requisition',
        'conMessage': "You are about to close Purchase Requisition (Transaction Complete).",
        'redirect': currentURL,
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_form_v1);

    $('#formdata-on_hold').on('click', {
        'action': adminURL + 'purchase_requisitions/method/on_hold_pr',
        'conMessage': "You are about to HOLD this PR. Make sure to input your comment in the the History Tab (below).",
        'redirect': currentURL,
        'id': $('#formdata-purchase_requisition_id').val()
    }, save_form_v1);
});
jQuery(function () {
    jQuery(document).on('change', '#formdata-item_type_id', itemSelect);
    jQuery(document).on('change', '.item_check', indicateCheck);

});

/*  
 * Get Items Based on Inventory Type
 */
function itemSelect() {
    var item_type_id = $('option:selected', '#formdata-item_type_id').val();

    if (item_type_id) {
        $.ajax({
            type: 'POST',
            url: adminURL + 'items/method/select_item',
            data: {'item_type_id': item_type_id},
            success: function (data)
            {
                $('select#formdata-item').html(data).trigger('liszt:updated');
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(xhr.status);
                alert(thrownError);
            },
            beforeSend: function ()
            {
                $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
            }
        });
    } else {
        alert('Select Item Inventory Type');
    }
}

function indicateCheck() {
    var code = $(this).attr('code');
    var val = $(this).val();
    if (val == 1) {
        $('i.' + code + '_' + 1).removeClass(' icomoon-icon-close');
        $('i.' + code + '_' + 2).removeClass('icomoon-icon-checkmark-3');
        $('i.' + code + '_' + 1).addClass('icomoon-icon-checkmark-3');
        $('i.' + code + '_' + 2).addClass(' icomoon-icon-close');
    } else {
        $('i.' + code + '_' + 1).removeClass('icomoon-icon-checkmark-3');
        $('i.' + code + '_' + 2).removeClass(' icomoon-icon-close');
        $('i.' + code + '_' + 1).addClass(' icomoon-icon-close');
        $('i.' + code + '_' + 2).addClass('icomoon-icon-checkmark-3');
    }
}

/*  
 * Add Supplier
 */
function removeItem() {
    $('tr#id_table_' + $(this).attr("id")).remove();
}