$(document).ready(function () {
    load_datalist({action: adminURL + 'business_units_inventory/method/list_bu_inventory'});

    //Link
    $('#containerList').on('click', '.delete_bu_inventory', {
        'action': adminURL + 'business_units_inventory/method/delete_bu_inventory',
        'conMessage': "You are about to delete this business_unit.",
        'redirect': adminURL + 'business_units_inventory/list_bu_inventory',
    }, dfltaction_item);
    $('#formdata-edi_bu_inventory').on('click', {
        'action': adminURL + 'business_units_inventory/method/edit_bu_inventory',
        'conMessage': "You are about to save manual adjustments.",
        'redirect': adminURL + 'business_units_inventory/list_bu_inventory',
    }, save_form_v1);
});

jQuery(function ()
{
    jQuery(document).on('change', '.manual_edit', checkTotal);
});

function checkTotal() {
    var total_qty_edit = 0;
    $('.manual_edit').each(function () {
        total_qty_edit += +$(this).val();
    });

    $('.total_qty_edit').html(total_qty_edit);
}