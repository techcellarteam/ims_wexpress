$(document).ready(function () {
    $('#formupload_save').on('click', {
        'action': adminURL + 'data_import/method/import_file',
        'conMessage': "You are about to import data  from csv file.",
//        'redirect': currentURL,//adminURL + 'data_import/view_import',
        'business_unit_id': $('formdata-business_unit_id').val(),
    }, save_upload);
});

jQuery(function ()
{
    jQuery(document).on('click', '.items_radio', selectCommodity);
    jQuery(document).on('click', '.stock_classifications_radio', selectComGroup);
    jQuery(document).on('click', '.unit_measurements_radio', selectMeasurement);
    jQuery(document).on('click', '.plate_numbers_radio', selectPlateNum);
    jQuery(document).on('click', '.suppliers_radio', selectSupplierRadio);
    jQuery(document).on('click', '.beg_inv_radio', selectBegInv);
});

function selectCommodity() {
    $(this).val('items');
    $('.stock_classifications_radio').val('');
    $('.unit_measurements_radio').val('');
    $('.plate_numbers_radio').val('');
    $('.suppliers_radio').val('');
    $('.beg_inv_radio').val('');

    //templates
    $('.item_template').removeClass('hidden');
    $('.um_template').addClass('hidden');
    $('.cg_template').addClass('hidden');
    $('.pn_template').addClass('hidden');
    $('.supplier_template').addClass('hidden');
    $('.beg_inv_template').addClass('hidden');
}

function selectComGroup() {
    $(this).val('stock_classifications');
    $('.unit_measurements_radio').val('');
    $('.items_radio').val('');
    $('.plate_numbers_radio').val('');
    $('.suppliers_radio').val('');
    $('.beg_inv_radio').val('');

    //templates
    $('.item_template').addClass('hidden');
    $('.um_template').addClass('hidden');
    $('.cg_template').removeClass('hidden');
    $('.pn_template').addClass('hidden');
    $('.supplier_template').addClass('hidden');
    $('.beg_inv_template').addClass('hidden');
}

function selectMeasurement() {
    $(this).val('unit_measurements');
    $('.stock_classifications_radio').val('');
    $('.items_radio').val('');
    $('.plate_numbers_radio').val('');
    $('.suppliers_radio').val('');
    $('.beg_inv_radio').val('');

    //templates
    $('.item_template').addClass('hidden');
    $('.um_template').removeClass('hidden');
    $('.cg_template').addClass('hidden');
    $('.pn_template').addClass('hidden');
    $('.supplier_template').addClass('hidden');
    $('.beg_inv_template').addClass('hidden');
}

function selectPlateNum() {
    $(this).val('plate_numbers');
    $('.items_radio').val('');
    $('.stock_classifications_radio').val('');
    $('.unit_measurements_radio').val('');
    $('.suppliers_radio').val('');
    $('.unit_measurements_radio').val('');
    $('.beg_inv_radio').val('');

    //templates
    $('.item_template').addClass('hidden');
    $('.um_template').addClass('hidden');
    $('.cg_template').addClass('hidden');
    $('.pn_template').removeClass('hidden');
    $('.supplier_template').addClass('hidden');
    $('.beg_inv_template').addClass('hidden');
}

function selectSupplierRadio() {
    $(this).val('suppliers');
    $('.items_radio').val('');
    $('.stock_classifications_radio').val('');
    $('.plate_numbers_radio').val('');
    $('.unit_measurements_radio').val('');
    $('.beg_inv_radio').val('');

    //templates
    $('.item_template').addClass('hidden');
    $('.um_template').addClass('hidden');
    $('.cg_template').addClass('hidden');
    $('.pn_template').addClass('hidden');
    $('.supplier_template').removeClass('hidden');
    $('.beg_inv_template').addClass('hidden');
}

function selectBegInv() {
    $(this).val('beginning_inventory');
    $('.items_radio').val('');
    $('.stock_classifications_radio').val('');
    $('.plate_numbers_radio').val('');
    $('.unit_measurements_radio').val('');
    $('.suppliers_radio').val('');

    //templates
    $('.item_template').addClass('hidden');
    $('.um_template').addClass('hidden');
    $('.cg_template').addClass('hidden');
    $('.pn_template').addClass('hidden');
    $('.supplier_template').addClass('hidden');
    $('.beg_inv_template').removeClass('hidden');
}