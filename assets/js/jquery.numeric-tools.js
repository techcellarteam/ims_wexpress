/*
|--------------------------------------------------------------------------
| JQuery Tools for addons management
|--------------------------------------------------------------------------
*/
$(document).ready(
	function()
	{
		$('.price').on("blur",function() {
                        $(this).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
                })
                .on("keyup",function(e) {
                        var e = window.event || e;
                        var keyUnicode = e.charCode || e.keyCode;
                        if (e !== undefined) {
                                switch (keyUnicode) {
                                        case 16: break; // Shift
                                        case 17: break; // Ctrl
                                        case 18: break; // Alt
                                        case 27: this.value = ''; break; // Esc: clear entry
                                        case 35: break; // End
                                        case 36: break; // Home
                                        case 37: break; // cursor left
                                        case 38: break; // cursor up
                                        case 39: break; // cursor right
                                        case 40: break; // cursor down
                                        case 78: break; // N (Opera 9.63+ maps the "." from the number key section to the "N" key too!) (See: http://unixpapa.com/js/key.html search for ". Del")
                                        case 110: break; // . number block (Opera 9.63+ maps the "." from the number block to the "N" key (78) !!!)
                                        case 190: break; // .
                                        default: $(this).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: -1, eventOnDecimalsEntered: true });
                                }
                        }
                })
                .bind('decimalsEntered', function(e, cents) {
                        if (String(cents).length > 2) {
                                var errorMsg = 'Please do not enter any cents (0.' + cents + ')';
                                alert(errorMsg);
                        }
                })
                
                ;
            
                jQuery(".price").on("keypress",function(){
                    return numbersonly(this , event, ".");
                })
                .on("change",function(){
                    $(this).val(checkChangePrice($(this).val()));
                    //return checkChangePrice($(this).val());
                })
                ;
            
	}
);
    function checkChangePrice(value)
    {
       var val=parseFloat(value);
       
       if(isNaN(val))
        {
            val = 0;
        }
        return number_format(val,2,'.','');
    }
    
    function number_format (number, decimals, dec_point, thousands_sep) {
        // Strip all characters but numerical ones.
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }

    function numbersonly(myfield, e, dec){
        var key;
        var keychar;

        if (window.event){key = window.event.keyCode;}
        else if(e){key = e.which;}
        else{return true;}

        keychar = String.fromCharCode(key);

        // control keys
        if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) ){return true;}

        // numbers
        else if ((("0123456789").indexOf(keychar) > -1)){return true;}

        // decimal point jump
        else if (dec && (keychar == ".")){
                myfield.form.elements[dec].focus();
                return true;
        }
        else { return false;}
    }
