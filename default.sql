-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 25, 2016 at 06:42 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `default`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `ccode` varchar(2) NOT NULL DEFAULT '',
  `country` varchar(200) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`ccode`, `country`) VALUES
('AF', 'Afghanistan'),
('AX', 'Åland Islands'),
('AL', 'Albania'),
('DZ', 'Algeria'),
('AS', 'American Samoa'),
('AD', 'Andorra'),
('AO', 'Angola'),
('AI', 'Anguilla'),
('AQ', 'Antarctica'),
('AG', 'Antigua and Barbuda'),
('AR', 'Argentina'),
('AM', 'Armenia'),
('AW', 'Aruba'),
('AU', 'Australia'),
('AT', 'Austria'),
('AZ', 'Azerbaijan'),
('BS', 'Bahamas'),
('BH', 'Bahrain'),
('BD', 'Bangladesh'),
('BB', 'Barbados'),
('BY', 'Belarus'),
('BE', 'Belgium'),
('BZ', 'Belize'),
('BJ', 'Benin'),
('BM', 'Bermuda'),
('BT', 'Bhutan'),
('BO', 'Bolivia'),
('BA', 'Bosnia and Herzegovina'),
('BW', 'Botswana'),
('BV', 'Bouvet Island'),
('BR', 'Brazil'),
('IO', 'British Indian Ocean Territory'),
('BN', 'Brunei Darussalam'),
('BG', 'Bulgaria'),
('BF', 'Burkina Faso'),
('BI', 'Burundi'),
('KH', 'Cambodia'),
('CM', 'Cameroon'),
('CA', 'Canada'),
('CV', 'Cape Verde'),
('KY', 'Cayman Islands'),
('CF', 'Central African Republic'),
('TD', 'Chad'),
('CL', 'Chile'),
('CN', 'China'),
('CX', 'Christmas Island'),
('CC', 'Cocos (Keeling) Islands'),
('CO', 'Colombia'),
('KM', 'Comoros'),
('CG', 'Congo'),
('CD', 'Congo, The Democratic Republic of the'),
('CK', 'Cook Islands'),
('CR', 'Costa Rica'),
('CI', 'Côte D''Ivoire'),
('HR', 'Croatia'),
('CU', 'Cuba'),
('CY', 'Cyprus'),
('CZ', 'Czech Republic'),
('DK', 'Denmark'),
('DJ', 'Djibouti'),
('DM', 'Dominica'),
('DO', 'Dominican Republic'),
('EC', 'Ecuador'),
('EG', 'Egypt'),
('SV', 'El Salvador'),
('GQ', 'Equatorial Guinea'),
('ER', 'Eritrea'),
('EE', 'Estonia'),
('ET', 'Ethiopia'),
('FO', 'Faroe Islands'),
('FJ', 'Fiji'),
('FI', 'Finland'),
('FR', 'France'),
('GF', 'French Guiana'),
('PF', 'French Polynesia'),
('TF', 'French Southern Territories'),
('GA', 'Gabon'),
('GM', 'Gambia'),
('GE', 'Georgia'),
('DE', 'Germany'),
('GH', 'Ghana'),
('GI', 'Gibraltar'),
('GR', 'Greece'),
('GL', 'Greenland'),
('GD', 'Grenada'),
('GP', 'Guadeloupe'),
('GU', 'Guam'),
('GT', 'Guatemala'),
('GG', 'Guernsey'),
('GN', 'Guinea'),
('GW', 'Guinea-Bissau'),
('GY', 'Guyana'),
('HT', 'Haiti'),
('HM', 'Heard Island and McDonald Islands'),
('VA', 'Holy See (Vatican City State)'),
('HN', 'Honduras'),
('HK', 'Hong Kong'),
('HU', 'Hungary'),
('IS', 'Iceland'),
('IN', 'India'),
('ID', 'Indonesia'),
('IR', 'Iran, Islamic Republic of'),
('IQ', 'Iraq'),
('IE', 'Ireland'),
('IM', 'Isle of Man'),
('IL', 'Israel'),
('IT', 'Italy'),
('JM', 'Jamaica'),
('JP', 'Japan'),
('JE', 'Jersey'),
('JO', 'Jordan'),
('KZ', 'Kazakhstan'),
('KE', 'Kenya'),
('KI', 'Kiribati'),
('KP', 'Korea, Democratic People''s Republic of'),
('KR', 'Korea, Republic of'),
('KW', 'Kuwait'),
('KG', 'Kyrgyzstan'),
('LA', 'Lao People''s Democratic Republic'),
('LV', 'Latvia'),
('LB', 'Lebanon'),
('LS', 'Lesotho'),
('LR', 'Liberia'),
('LY', 'Libyan Arab Jamahiriya'),
('LI', 'Liechtenstein'),
('LT', 'Lithuania'),
('LU', 'Luxembourg'),
('MO', 'Macao'),
('MK', 'Macedonia, The Former Yugoslav Republic of'),
('MG', 'Madagascar'),
('MW', 'Malawi'),
('MY', 'Malaysia'),
('MV', 'Maldives'),
('ML', 'Mali'),
('MT', 'Malta'),
('MH', 'Marshall Islands'),
('MQ', 'Martinique'),
('MR', 'Mauritania'),
('MU', 'Mauritius'),
('YT', 'Mayotte'),
('MX', 'Mexico'),
('FM', 'Micronesia, Federated States of'),
('MD', 'Moldova, Republic of'),
('MC', 'Monaco'),
('MN', 'Mongolia'),
('ME', 'Montenegro'),
('MS', 'Montserrat'),
('MA', 'Morocco'),
('MZ', 'Mozambique'),
('MM', 'Myanmar'),
('NA', 'Namibia'),
('NR', 'Nauru'),
('NP', 'Nepal'),
('NL', 'Netherlands'),
('AN', 'Netherlands Antilles'),
('NC', 'New Caledonia'),
('NZ', 'New Zealand'),
('NI', 'Nicaragua'),
('NE', 'Niger'),
('NG', 'Nigeria'),
('NU', 'Niue'),
('NF', 'Norfolk Island'),
('MP', 'Northern Mariana Islands'),
('NO', 'Norway'),
('OM', 'Oman'),
('PK', 'Pakistan'),
('PW', 'Palau'),
('PS', 'Palestinian Territory, Occupied'),
('PA', 'Panama'),
('PG', 'Papua New Guinea'),
('PY', 'Paraguay'),
('PE', 'Peru'),
('PH', 'Philippines'),
('PN', 'Pitcairn'),
('PL', 'Poland'),
('PT', 'Portugal'),
('PR', 'Puerto Rico'),
('QA', 'Qatar'),
('RE', 'Reunion'),
('RO', 'Romania'),
('RU', 'Russian Federation'),
('RW', 'Rwanda'),
('BL', 'Saint Barthélemy'),
('SH', 'Saint Helena'),
('KN', 'Saint Kitts and Nevis'),
('LC', 'Saint Lucia'),
('MF', 'Saint Martin'),
('PM', 'Saint Pierre and Miquelon'),
('VC', 'Saint Vincent and the Grenadines'),
('WS', 'Samoa'),
('SM', 'San Marino'),
('ST', 'Sao Tome and Principe'),
('SA', 'Saudi Arabia'),
('SN', 'Senegal'),
('RS', 'Serbia'),
('SC', 'Seychelles'),
('SL', 'Sierra Leone'),
('SG', 'Singapore'),
('SK', 'Slovakia'),
('SI', 'Slovenia'),
('SB', 'Solomon Islands'),
('SO', 'Somalia'),
('ZA', 'South Africa'),
('GS', 'South Georgia and the South Sandwich Islands'),
('ES', 'Spain'),
('LK', 'Sri Lanka'),
('SD', 'Sudan'),
('SR', 'Suriname'),
('SJ', 'Svalbard and Jan Mayen'),
('SZ', 'Swaziland'),
('SE', 'Sweden'),
('CH', 'Switzerland'),
('SY', 'Syrian Arab Republic'),
('TW', 'Taiwan, Province Of China'),
('TJ', 'Tajikistan'),
('TZ', 'Tanzania, United Republic of'),
('TH', 'Thailand'),
('TL', 'Timor-Leste'),
('TG', 'Togo'),
('TK', 'Tokelau'),
('TO', 'Tonga'),
('TT', 'Trinidad and Tobago'),
('TN', 'Tunisia'),
('TR', 'Turkey'),
('TM', 'Turkmenistan'),
('TC', 'Turks and Caicos Islands'),
('TV', 'Tuvalu'),
('UG', 'Uganda'),
('UA', 'Ukraine'),
('AE', 'United Arab Emirates'),
('GB', 'United Kingdom'),
('US', 'United States'),
('UM', 'United States Minor Outlying Islands'),
('UY', 'Uruguay'),
('UZ', 'Uzbekistan'),
('VU', 'Vanuatu'),
('VE', 'Venezuela'),
('VN', 'Viet Nam'),
('VG', 'Virgin Islands, British'),
('VI', 'Virgin Islands, U.S.'),
('WF', 'Wallis And Futuna'),
('EH', 'Western Sahara'),
('YE', 'Yemen'),
('ZM', 'Zambia'),
('ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `default_classes`
--

CREATE TABLE `default_classes` (
  `id_class` int(11) NOT NULL,
  `class_name` varchar(128) NOT NULL,
  `class_title` varchar(128) NOT NULL,
  `enabled` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `default_class_functions`
--

CREATE TABLE `default_class_functions` (
  `id_class_function` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `class_function_title` varchar(128) NOT NULL,
  `class_function_name` varchar(128) NOT NULL,
  `class_function_type` int(11) NOT NULL,
  `class_function_order` int(11) NOT NULL,
  `enabled` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `default_configs`
--

CREATE TABLE `default_configs` (
  `id_config` int(11) NOT NULL,
  `config_name` varchar(128) NOT NULL,
  `config_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `default_configs`
--

INSERT INTO `default_configs` (`id_config`, `config_name`, `config_value`) VALUES
(1, 'EMAIL_PROTOCOL', 'mail'),
(2, 'SMTP_DOMAIN', ''),
(3, 'SMTP_HOST', 'mail.techcellar.net'),
(4, 'SMTP_USER', 'email@techcellar.ne'),
(5, 'SMTP_PASS', 'XC-3#-rc'),
(6, 'SMTP_PORT', '25'),
(7, 'SMTP_CC', 'melin@techcellar.com'),
(8, 'SMTP_BCC', ''),
(9, 'EMAIL_TYPE', 'html'),
(10, 'DEBUG_MODE', '0'),
(11, 'SITE_TITLE', 'PAMS'),
(12, 'COMP_LOGO', ''),
(13, 'COMP_NAME', 'Parkmall'),
(14, 'DEFAULT_COUNTRY', 'Philippines'),
(15, 'DEFAULT_TIME_ZONE', 'Asia/Manila'),
(16, 'COPY_RIGHT', 'www.techcellar.com');

-- --------------------------------------------------------

--
-- Table structure for table `business_units`
--

CREATE TABLE `business_units` (
  `id_business_unit` int(11) NOT NULL,
  `business_unit_head_id` int(11) NOT NULL,
  `business_unit_name` varchar(64) NOT NULL,
  `business_unit_code` varchar(16) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `default_links`
--

CREATE TABLE `default_links` (
  `id_link` int(11) NOT NULL,
  `parent_link_id` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `link_url` tinytext NOT NULL,
  `link_name` tinytext NOT NULL,
  `link_title` tinytext NOT NULL,
  `link_detail` tinytext NOT NULL,
  `link_icon` varchar(50) NOT NULL,
  `link_location` int(11) NOT NULL,
  `link_newtab` int(11) NOT NULL,
  `link_external` int(11) NOT NULL,
  `link_head` int(11) NOT NULL,
  `link_order` int(11) NOT NULL,
  `enabled` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `default_links`
--

INSERT INTO `default_links` (`id_link`, `parent_link_id`, `user_type_id`, `link_url`, `link_name`, `link_title`, `link_detail`, `link_icon`, `link_location`, `link_newtab`, `link_external`, `link_head`, `link_order`, `enabled`, `added_by`, `added_date`, `updated_by`, `updated_date`) VALUES
(1, 0, 1, 'profile', 'Profile', '', '', 'icomoon-icon-user', 1, 0, 0, 0, 1, 1, 1, '2015-07-07 14:01:43', 0, '0000-00-00 00:00:00'),
(2, 0, 1, '#', 'Settings', '', '', 'icomoon-icon-cog-2', 2, 0, 0, 1, 11, 1, 1, '2015-07-07 14:03:00', 1, '2015-11-06 13:37:06'),
(3, 2, 1, 'user/list_user_page', 'User', '', '', 'brocco-icon-user', 2, 0, 0, 0, 4, 1, 1, '2015-07-07 14:04:03', 1, '2015-11-06 08:42:24'),
(4, 2, 1, 'user/list_usertype_page', 'User Type', '', '', 'icomoon-icon-users', 2, 0, 0, 0, 2, 1, 1, '2015-07-07 14:07:25', 1, '2015-07-07 14:07:54'),
(5, 2, 1, 'myclass/list_class_page', 'Class', '', '', 'entypo-icon-globe', 2, 0, 0, 0, 3, 1, 1, '2015-07-07 14:13:34', 0, '0000-00-00 00:00:00'),
(6, 2, 1, 'menu/list_typemenu_page', 'Menu', '', '', 'icomoon-icon-menu-2', 2, 0, 0, 0, 1, 1, 1, '2015-07-07 14:14:22', 1, '2015-11-06 08:42:37'),
(7, 2, 1, 'user/access_user_page', 'Access', '', '', 'icomoon-icon-lock', 2, 0, 0, 0, 5, 1, 1, '2015-07-07 14:14:57', 0, '0000-00-00 00:00:00'),
(9, 0, 3, '#', 'Settings', '', '', 'icomoon-icon-cog-2', 2, 0, 0, 1, 11, 0, 1, '2016-03-29 12:16:26', 1, '2016-03-29 12:17:06'),
(10, 9, 3, 'user/list_user_page', 'User', '', '', 'brocco-icon-user', 2, 0, 0, 0, 4, 0, 1, '2016-03-29 12:16:26', 1, '2016-03-29 12:17:06'),
(11, 9, 3, 'user/list_usertype_page', 'User Type', '', '', 'icomoon-icon-users', 2, 0, 0, 0, 2, 0, 1, '2016-03-29 12:16:26', 1, '2016-03-29 12:17:06'),
(12, 9, 3, 'menu/list_typemenu_page', 'Menu', '', '', 'icomoon-icon-menu-2', 2, 0, 0, 0, 1, 0, 1, '2016-03-29 12:16:26', 1, '2016-03-29 12:17:06'),
(13, 9, 3, 'myclass/list_class_page', 'Class', '', '', 'entypo-icon-globe', 2, 0, 0, 0, 3, 0, 1, '2016-03-29 12:16:26', 1, '2016-03-29 12:17:06'),
(14, 9, 3, 'user/access_user_page', 'Access', '', '', 'icomoon-icon-lock', 2, 0, 0, 0, 5, 0, 1, '2016-03-29 12:16:26', 1, '2016-03-29 12:17:06');

-- --------------------------------------------------------

--
-- Table structure for table `default_notifications`
--

CREATE TABLE `default_notifications` (
  `id_notification` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `link` varchar(64) NOT NULL,
  `status` text NOT NULL,
  `todo` tinyint(1) NOT NULL,
  `seen` tinyint(1) NOT NULL,
  `done` tinyint(1) NOT NULL,
  `approved` int(11) NOT NULL,
  `seen_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `default_users`
--

CREATE TABLE `default_users` (
  `id_user` int(11) NOT NULL,
  `business_unit_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `sup_user_id` int(11) NOT NULL,
  `user_fname` varchar(128) NOT NULL,
  `user_mname` varchar(128) NOT NULL,
  `user_lname` varchar(128) NOT NULL,
  `user_code` varchar(16) NOT NULL,
  `user_email` varchar(64) NOT NULL,
  `user_contact` varchar(20) NOT NULL,
  `user_password` varchar(128) NOT NULL,
  `user_address` tinytext NOT NULL,
  `user_position` varchar(100) NOT NULL,
  `user_picture` tinytext NOT NULL,
  `user_idp` int(11) NOT NULL,
  `user_yt` int(11) NOT NULL,
  `user_hp` int(11) NOT NULL,
  `user_spn` int(11) NOT NULL,
  `user_mntr` int(11) NOT NULL,
  `user_state` int(11) NOT NULL,
  `enabled` int(11) NOT NULL,
  `user_resetkey` varchar(128) NOT NULL,
  `added_date` datetime NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `default_users`
--

INSERT INTO `default_users` (`id_user`, `business_unit_id`, `division_id`, `user_type_id`, `sup_user_id`, `user_fname`, `user_mname`, `user_lname`, `user_code`, `user_email`, `user_contact`, `user_password`, `user_address`, `user_position`, `user_picture`, `user_idp`, `user_yt`, `user_hp`, `user_spn`, `user_mntr`, `user_state`, `enabled`, `user_resetkey`, `added_date`, `added_by`, `updated_date`, `updated_by`) VALUES
(1, 0, 0, 1, 1, 'Melin', 'Kiong', 'Lonod', 'melin', 'melin@techcellar.com', '', '8044dc6da0fadb88d033a11f9fd4a6cf', 'Makati City', '', '35732187d8fad5cf2b80fd11d3cc255a.jpg', 1, 1, 1, 1, 1, 1, 1, '', '2016-06-21 00:00:00', 1, '2016-07-28 15:26:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `default_user_accesses`
--

CREATE TABLE `default_user_accesses` (
  `id_user_access` int(11) NOT NULL,
  `class_function_id` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `user_access_status` int(11) NOT NULL,
  `enabled` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `default_user_logs`
--

CREATE TABLE `default_user_logs` (
  `id_user_log` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `page_name` varchar(100) NOT NULL,
  `class_name` varchar(100) NOT NULL,
  `method_name` varchar(100) NOT NULL,
  `user_log_value` tinytext NOT NULL,
  `user_log_detail` tinytext NOT NULL,
  `user_log_table` varchar(50) NOT NULL,
  `user_log_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `default_user_logs`
--

INSERT INTO `default_user_logs` (`id_user_log`, `user_id`, `page_name`, `class_name`, `method_name`, `user_log_value`, `user_log_detail`, `user_log_table`, `user_log_date`) VALUES
(1, 1, 'login', 'log', '', '', '', '', '2016-11-25 13:25:20'),
(2, 1, 'logout', 'log', '', '', '', '', '2016-11-25 13:41:34');

-- --------------------------------------------------------

--
-- Table structure for table `default_user_types`
--

CREATE TABLE `default_user_types` (
  `id_user_type` int(11) NOT NULL,
  `user_type_name` varchar(128) NOT NULL,
  `user_type_code` varchar(16) NOT NULL,
  `enabled` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `default_user_types`
--

INSERT INTO `default_user_types` (`id_user_type`, `user_type_name`, `user_type_code`, `enabled`, `added_date`, `added_by`, `updated_date`, `updated_by`) VALUES
(1, 'Super Admin', 'super_admin', 1, '2016-11-25 00:00:00', 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `document_types`
--

CREATE TABLE `document_types` (
  `id_document_type` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `document_type_code` varchar(16) NOT NULL,
  `document_type_name` varchar(64) NOT NULL,
  `multiplier` varchar(8) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_types`
--

INSERT INTO `document_types` (`id_document_type`, `class_id`, `document_type_code`, `document_type_name`, `multiplier`, `enabled`, `added_by`, `added_date`, `updated_by`, `updated_date`) VALUES
(1, 11, 'Prod Req', 'Product Requisition', 'None', 1, 0, '0000-00-00 00:00:00', 1, '2016-07-25 16:22:18'),
(2, 11, 'SR', 'Service Requisition', 'None', 1, 1, '2016-07-26 17:43:58', 1, '2016-07-26 17:43:58'),
(3, 19, 'PRP', 'Payment Request for PO', 'None', 1, 1, '2016-10-25 20:35:51', 1, '2016-10-25 20:35:51'),
(4, 15, 'IPO', 'Items Purchase Ordered', 'Out', 1, 0, '0000-00-00 00:00:00', 1, '2016-11-03 17:51:21'),
(5, 15, 'IFS', 'Items From Stock', 'Out', 1, 1, '2016-11-03 17:51:48', 1, '2016-11-03 17:51:48'),
(6, 19, 'SPR', 'Special Payment Request', 'None', 1, 1, '2016-11-11 19:02:37', 1, '2016-11-11 19:02:37');

-- --------------------------------------------------------

--
-- Table structure for table `module_attachments`
--

CREATE TABLE `module_attachments` (
  `id_module_attachment` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `upload_title` varchar(64) NOT NULL,
  `upload_file_name` varchar(128) NOT NULL,
  `upload_file_type` varchar(32) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `module_signatories`
--

CREATE TABLE `module_signatories` (
  `id_module_signatory` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `signatory_user_id` int(11) NOT NULL,
  `signatory_level` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `added_by` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `months`
--

CREATE TABLE `months` (
  `id_month` int(11) NOT NULL,
  `month` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `months`
--

INSERT INTO `months` (`id_month`, `month`) VALUES
(1, 'January'),
(2, 'February'),
(3, 'March'),
(4, 'April'),
(5, 'May'),
(6, 'June'),
(7, 'July'),
(8, 'August'),
(9, 'September'),
(10, 'October'),
(11, 'November'),
(12, 'December');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`ccode`);

--
-- Indexes for table `default_classes`
--
ALTER TABLE `default_classes`
  ADD PRIMARY KEY (`id_class`);

--
-- Indexes for table `default_class_functions`
--
ALTER TABLE `default_class_functions`
  ADD PRIMARY KEY (`id_class_function`);

--
-- Indexes for table `default_configs`
--
ALTER TABLE `default_configs`
  ADD PRIMARY KEY (`id_config`);

--
-- Indexes for table `business_units`
--
ALTER TABLE `business_units`
  ADD PRIMARY KEY (`id_business_unit`);

--
-- Indexes for table `default_links`
--
ALTER TABLE `default_links`
  ADD PRIMARY KEY (`id_link`);

--
-- Indexes for table `default_notifications`
--
ALTER TABLE `default_notifications`
  ADD PRIMARY KEY (`id_notification`);

--
-- Indexes for table `default_users`
--
ALTER TABLE `default_users`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `default_user_accesses`
--
ALTER TABLE `default_user_accesses`
  ADD PRIMARY KEY (`id_user_access`);

--
-- Indexes for table `default_user_logs`
--
ALTER TABLE `default_user_logs`
  ADD PRIMARY KEY (`id_user_log`);

--
-- Indexes for table `default_user_types`
--
ALTER TABLE `default_user_types`
  ADD PRIMARY KEY (`id_user_type`);

--
-- Indexes for table `document_types`
--
ALTER TABLE `document_types`
  ADD PRIMARY KEY (`id_document_type`);

--
-- Indexes for table `module_attachments`
--
ALTER TABLE `module_attachments`
  ADD PRIMARY KEY (`id_module_attachment`);

--
-- Indexes for table `module_signatories`
--
ALTER TABLE `module_signatories`
  ADD PRIMARY KEY (`id_module_signatory`);

--
-- Indexes for table `months`
--
ALTER TABLE `months`
  ADD PRIMARY KEY (`id_month`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `default_classes`
--
ALTER TABLE `default_classes`
  MODIFY `id_class` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_class_functions`
--
ALTER TABLE `default_class_functions`
  MODIFY `id_class_function` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_configs`
--
ALTER TABLE `default_configs`
  MODIFY `id_config` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `business_units`
--
ALTER TABLE `business_units`
  MODIFY `id_business_unit` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_links`
--
ALTER TABLE `default_links`
  MODIFY `id_link` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `default_notifications`
--
ALTER TABLE `default_notifications`
  MODIFY `id_notification` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_users`
--
ALTER TABLE `default_users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `default_user_accesses`
--
ALTER TABLE `default_user_accesses`
  MODIFY `id_user_access` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_user_logs`
--
ALTER TABLE `default_user_logs`
  MODIFY `id_user_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `default_user_types`
--
ALTER TABLE `default_user_types`
  MODIFY `id_user_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `document_types`
--
ALTER TABLE `document_types`
  MODIFY `id_document_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `module_attachments`
--
ALTER TABLE `module_attachments`
  MODIFY `id_module_attachment` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `module_signatories`
--
ALTER TABLE `module_signatories`
  MODIFY `id_module_signatory` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `months`
--
ALTER TABLE `months`
  MODIFY `id_month` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
