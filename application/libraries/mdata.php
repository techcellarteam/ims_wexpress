<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
Class Mdata
{
	/*
	*	Query Data to Array
	*/
	function qrtoarr($data,$key_id){
		$layer='';
		foreach($data->result() as $q){
			$layer[$q->{$key_id}]=$q;
		}
		return $layer;
	}
	
	/*
	*	Query Data to Array
	*	Base Parent 
	*/
	
	function qrtoarr_baseparent($data,$key_id,$parent_key_id){
		$layer='';
		$layer2='';
		foreach($data->result() as $q){
			$layer[$q->{$key_id}]=$q;
			$layer2[$q->{$parent_key_id}][$q->{$key_id}]=$q;
		}
		
		return array('arr'=>$layer,'arr_baseparent'=>$layer2);
	}
	
	/*
	*	Query Data to Array
	*	Hierarchy Parent and Child
	*/
	
	function qrtoarr_hierarchy($data,$key_id,$parent_key_id){
		$result=self::qrtoarr_baseparent($data,$key_id,$parent_key_id);
		$arr=$result['arr_baseparent'];
		$arrParent= $arr[0];
		return self::set_hierarchy($arrParent,$arr);	
	}
	
	function set_hierarchy($parent,$list){
		$tree=array();
		foreach($parent as $var=>$val){
			$layer['id']=$var;
			$layer['val']=$val;
			if(!empty($list[$var])){
				$layer['child']=self::set_hierarchy($list[$var],$list);
			}
			$tree[]=$layer;
		}
		return $tree;
	}

	
	/*
	*	Query Data to Menu Data
	*	Hierarchy Menu
	*/	
	
	function qrtoarr_menudata($data){
		$layer='';
		foreach($data->result() as $q){
			$layer1['id']=$q->id_menutl;
			$layer1['name']=$q->menulist_dname;
			$layer1['parent_id']=$q->parentid_menutl;
			$layer1['activeparent']=$q->menutl_activeparent;
			$layer1['icon']=$q->menulist_icon;
			
			if($q->menulist_type==1){
				$varLink=$q->module_name.'/page/'.$q->modpage_name;
				if($q->menulist_pagevalue!=''){
					$varLink.="/".$q->menulist_pagevalue;
				}
				$layer1['link']=admin_url($varLink);
			}else{
				$layer1['link']=$q->menulist_link;
			}
			
			$layer[$q->parentid_menutl][$q->id_menutl]=$layer1;
		}
		
		$arr=$layer;
		$arrParent= $arr[0];
		return self::set_hierarchy($arrParent,$arr);	
	}
	
	/*
	*	Key to Form Data
	*/
	
	function keytoformdata($token){
		return $data=array(
			"prefix_idForm"=>"formfield_".$token."_",
			"classForm"=>"formfield",
			"prefix_idInput"=>"formdata_".$token."_",
			"classInput"=>"formdata formdata_".$token,
			"groupInput"=>"formdata_".$token,
			"prefix_idButton"=>"formbtn_".$token."_",
			"classButton"=>"formbtn formbtn_".$token,
			"classMessage"=>"formmessage_".$token,
		);
	}
	
	/*
	*	Encode Array
	*/
	
	function arrayencode($arr){
		return str_replace('=','#',base64_encode(base64_encode((json_encode($arr)))));
	}
	
	
	function arraydecode($string){
		return json_decode(base64_decode(base64_decode(str_replace('#','=',$string))));
	}
	
	
}