<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// ------------------------------------------------------------------------

/*
 * CodeIgniter URL Helpers Extension
 *
 * @category	Helpers
 * @author	-_-
 */

define('_ASSETS_DIR_', config_item('base_url') . 'assets/');
define('_AVATARS_DIR_', _ASSETS_DIR_ . 'avatars/');
define('_CSS_DIR_', _ASSETS_DIR_ . 'css/');
define('_FONT_DIR_', _ASSETS_DIR_ . 'font/');
define('_IMAGES_DIR_', _ASSETS_DIR_ . 'images/');
define('_IMG_DIR_', _ASSETS_DIR_ . 'img/');
define('_JS_DIR_', _ASSETS_DIR_ . 'js/');

define('_UPLOAD_DIR_', config_item('base_url') . 'upload/');
define('_UPLOAD_IMAGES_DIR_', _UPLOAD_DIR_ . 'images/');
define('_UPLOAD_USER_DIR_', _UPLOAD_DIR_ . 'user/');
define('_UPLOAD_COMPANY_DIR_', _UPLOAD_DIR_ . 'company/');
define('_UPLOAD_SURVEY_DIR_', _UPLOAD_DIR_ . 'survey/');

define('_ADMIN_URL_', config_item('base_url') . 'admin/');
define('_USER_URL_', config_item('base_url') . 'users/');
define('_ITEMS_URL_', config_item('base_url') . 'items/');
define('_CLIENTS_URL_', config_item('base_url') . 'clients/');
define('_UOM_URL_', config_item('base_url') . 'uom/');
define('_PRINCIPAL_URL_', config_item('base_url') . 'principal/');
define('_LOCATIONS_URL_', config_item('base_url') . 'locations/');
define('_CATEGORY_URL_', config_item('base_url') . 'categories/');
define('_INBOUND_URL_', config_item('base_url') . 'inbound/');
define('_INVENTORY_HISTORY_URL_', config_item('base_url') . 'inventory_history/');
/*
 * Admin
 */
if (!function_exists('admin_url')) {

    function admin_url($subdirectory = '') {
        $link = _ADMIN_URL_;
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('admin_dir')) {

    function admin_dir($subdirectory = '') {
        $link = 'admin/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('cron_dir')) {

    function cron_dir($subdirectory = '') {
        $link = 'cron/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('users_url')) {

    function users_url($subdirectory = '') {
        $link = _USER_URL_;
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}
if (!function_exists('users_dir')) {

    function users_dir($subdirectory = '') {
        $link = 'users/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('items_url')) {

    function items_url($subdirectory = '') {
        $link = _ITEMS_URL_;
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('items_dir')) {

    function items_dir($subdirectory = '') {
        $link = 'items/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('clients_dir')) {

    function clients_dir($subdirectory = '') {
        $link = 'clients/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('clients_url')) {

    function clients_url($subdirectory = '') {
        $link = _CLIENTS_URL_;
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('uom_dir')) {

    function uom_dir($subdirectory = '') {
        $link = 'uom/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('uom_url')) {

    function uom_url($subdirectory = '') {
        $link = _UOM_URL_;
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('locations_dir')) {

    function locations_dir($subdirectory = '') {
        $link = 'locations/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('locations_url')) {

    function locations_url($subdirectory = '') {
        $link = _LOCATIONS_URL_;
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('category_dir')) {

    function category_dir($subdirectory = '') {
        $link = 'categories/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('category_url')) {

    function category_url($subdirectory = '') {
        $link = _CATEGORY_URL_;
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('data_import_dir')) {

    function data_import_dir($subdirectory = '') {
        $link = 'data_import/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('principal_dir')) {

    function principal_dir($subdirectory = '') {
        $link = 'principal/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('principal_url')) {

    function principal_url($subdirectory = '') {
        $link = _PRINCIPAL_URL_;
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('inbound_dir')) {

    function inbound_dir($subdirectory = '') {
        $link = 'inbound/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('inbound_url')) {

    function inbound_url($subdirectory = '') {
        $link = _INBOUND_URL_;
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('inventory_history_dir')) {

    function inventory_history_dir($subdirectory = '') {
        $link = 'inventory_history/';
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}

if (!function_exists('inventory_history_url')) {

    function invenory_history_url($subdirectory = '') {
        $link = _INVENTORY_HISTORY_URL_;
        if (!empty($subdirectory))
            $link .= $subdirectory;
        return $link;
    }

}



/*
 * Uploads URL
 */
if (!function_exists('upload_dir')) {

    function upload_dir($subdirectory = '', $file = '') {
        $link = _UPLOAD_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('upload_images_dir')) {

    function upload_images_dir($subdirectory = '', $file = '') {
        $link = _UPLOAD_IMAGES_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}


if (!function_exists('upload_user_dir')) {

    function upload_user_dir($subdirectory = '', $file = '') {
        $link = _UPLOAD_USER_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('upload_company_dir')) {

    function upload_company_dir($subdirectory = '', $file = '') {
        $link = _UPLOAD_COMPANY_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

/*
 * Assets URL
 */
if (!function_exists('assets_dir')) {

    function assets_dir($subdirectory = '', $file = '') {
        $link = _ASSETS_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('avatars_dir')) {

    function avatars_dir($subdirectory = '', $file = '') {
        $link = _AVATARS_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('css_dir')) {

    function css_dir($subdirectory = '', $file = '') {
        $link = _CSS_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('font_dir')) {

    function font_dir($subdirectory = '', $file = '') {
        $link = _FONT_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('images_dir')) {

    function images_dir($subdirectory = '', $file = '') {
        $link = _IMAGES_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('img_dir')) {

    function img_dir($subdirectory = '', $file = '') {
        $link = _IMG_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

if (!function_exists('js_dir')) {

    function js_dir($subdirectory = '', $file = '') {
        $link = _JS_DIR_;

        if (!empty($subdirectory)) {
            $link .= $subdirectory;

            if (!empty($file))
                $link .= '/' . $file;
        }

        return $link;
    }

}

/* End of file MY_url_helper.php */
/* Location: ./application/helpers/MY_url_helper.php */