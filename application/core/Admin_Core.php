<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Admin_Core extends MY_Controller {

    public $my_session = '';
    public $class_functions_model = '';
    public $configs_model = '';
    public $user_logs_model = '';
    public $links_model = '';
    public $Misc = '';
    public $classname = '';
    public $pagename = '';
    public $methodname = '';
    public $access = '';

    public function __construct() {
        parent::__construct();
        $this->my_session = new my_session();
        if ($this->uri->rsegment(1) != 'log') {
            if (!$this->my_session->get('admin', 'login') and ! IS_AJAX) {
                $this->session->set_userdata('redirect', current_url());
                redirect(admin_url('log'));
            } else if (!$this->my_session->get('admin', 'login') and IS_AJAX) {
                die();
            }
        } else if ($this->my_session->get('admin', 'login') and $this->uri->rsegment(2) != 'logout') {
            redirect(admin_url('profile'));
        }

        //load library
        $this->load->library('Tools');

        //load models
        $this->load->model('admin/class_functions_model');
        $this->load->model('admin/configs_model');
        $this->load->model('admin/user_logs_model');
        $this->load->model('admin/links_model');
        $this->load->model('admin/notifications_model');


        $this->class_functions_model = new class_functions_model();
        $this->configs_model = new configs_model();
        $this->user_logs_model = new user_logs_model();
        $this->links_model = new links_model();
        $this->Misc = new Misc();
        $this->notifications_model = new Notifications_Model();

        self::_set_access();
    }

    /*
     * Check if User Allowed to Access Current Page 
     */

    function _remap() {
        $class_name = $this->classname;
        $page_name = $this->pagename;
        $method_name = $this->methodname;
        $user_type_id = $this->my_session->get('admin', 'user_type_id');
        switch ($class_name) {
            case "log":
                $access = 1;
                break;
            default:
                switch ($page_name) {
                    case "method":
                        $access = $this->class_functions_model->is_user_allowed($user_type_id, array("class_name" => $class_name, "class_function_name" => $method_name, "class_function_type" => 2));
                        break;
                    default: // page
                        $access = $this->class_functions_model->is_user_allowed($user_type_id, array("class_name" => $class_name, "class_function_name" => $page_name, "class_function_type" => 1));
                        break;
                }
                break;
        }

        /* Grant permission on the admin homepage for all administrator */
        if ($this->classname == admin_dir() OR $user_type_id == 1) {
            $access = 1;
        }

        if (isset($access) && $access) {
            if (is_callable($this->classname, $page_name) && method_exists($this->classname, $page_name)) {
                $this->$page_name();
            } else {
                redirect(admin_dir('profile/view_pagenotfound_page'));
            }
        } else {
            if (is_callable($this->classname, $page_name) && method_exists($this->classname, $page_name)) {
                
            } else {
                redirect(admin_dir('profile/view_pagenotfound_page'));
            }
        }
    }

    /*
     * Set Message User Not allowed
     */

    private function deny() {
        $data = array(
            'message' => "You don't have permission here."
        );
        $this->load->view(admin_dir('template/deny'), $data);
    }

    /*
     * Set Allowed Access
     */

    function _set_access() {
        $access = $this->class_functions_model->allowed_user_accesses($this->my_session->get('admin', 'user_type_id'));
        $access['user_type_id'] = $this->my_session->get('admin', 'user_type_id');
        $this->access = $access;
    }

    /*
     * For Sticky Message
     */

    function sticky_message($data = array()) {
        if (!empty($data['text'])) {
            $data['text'] = $this->Misc->oneline_string($data['text']);
            if (empty($data['type'])) {
                $data['type'] = 'info';
            }
            if (empty($data['title'])) {
                $data['title'] = $this->Misc->status_title($data['type']);
            }
            if (empty($data['icon'])) {
                $data['icon'] = $this->Misc->status_iconnoti($data['type']);
            }
            if (empty($data['hide'])) {
                $data['hide'] = false;
            }
            if (empty($data['history'])) {
                $data['history'] = false;
            }
            if (empty($data['sticker'])) {
                $data['sticker'] = false;
            }
            return $this->load->view(admin_dir('js/sticky_message'), array('data' => json_encode($data)), true);
        }
    }

    /*
     * Data Template for LogIn Page
     */

    function log_template() {
        $condition = "config_name like 'SITE_TITLE'";
        $condition.=" OR config_name like 'COMP_LOGO'";
        $condition.=" OR config_name like 'COMP_NAME'";
        $condition.=" OR config_name like 'COPY_RIGHT'";
        $result = $this->configs_model->getSearch($condition, "", "", true);
        $data = '';
        foreach ($result as $q) {
            $data[$q->config_name] = $q->config_value;
        }
        return $data;
    }

    /*
     * Data Template for Page
     */

    function main_template($datas = array()) {
        $define_breadcrumb = '';
        if (!empty($datas['breadcrumb'])) {
            $define_breadcrumb = $datas['breadcrumb'];
        }

        $user_picture = $this->my_session->get('admin', 'user_picture');
        $name = $this->my_session->get('admin', 'name');
        $user_id = $this->my_session->get('admin', 'user_id');
        $user_type_id = $this->my_session->get('admin', 'user_type_id');
        $user_type_name = $this->my_session->get('admin', 'user_type_name');

        $condition = "config_name like 'SITE_TITLE'"; //SITE_TITLE
        $condition.=" OR config_name like 'COMP_LOGO'"; //COMP_LOGO
        $condition.=" OR config_name like 'COMP_NAME'"; //COMP_NAME
        $condition.=" OR config_name like 'COPY_RIGHT'"; //COPY_RIGHT
        $result = $this->configs_model->getSearch($condition, "", "", true);
        $data = '';
        foreach ($result as $q) {
            $data[$q->config_name] = $q->config_value;
        }

        $data['PROFILE_PICTURE'] = $user_picture;
        $data['PROFILE_USER'] = $name;
        $data['PROFILE_ID'] = $user_id;
        $data['PROFILE_USERTYPE'] = $user_type_name;
        $data['CUS_MENU_TOP'] = self::_get_topmenu($user_type_id);

        $pagename = explode('_', $this->pagename);
        if (($pagename['0'] == 'add') OR ( $pagename['0'] == 'edit') OR ( $pagename['0'] == 'view')) {
            $menu_side = self::_get_leftmenu($user_type_id, $this->classname . '/list_' . str_replace(' ', '_', strtolower($this->contents['functionName'])));
        } else {
            $menu_side = self::_get_leftmenu($user_type_id);
        }
        $data['CUS_MENU_SIDE'] = $menu_side['data'];
        $data['CUS_MENU_SIDE_ACTIVE'] = $menu_side['active'];
        return $data;
    }

    /*
     * Get Top Menu
     */

    function _get_topmenu($user_type_id) {
        $layer = '';
        $result = $this->links_model->getSearch(array("id_user_type" => $user_type_id, "l.link_location" => 1), "", array("link_order" => "ASC"));
        foreach ($result->result() as $q) {
            $layer[] = $q;
        }
        return $layer;
    }

    /*
     * Get Side Menu
     */

    function _get_leftmenu($user_type_id, $url_name = '') {
        $layer = '';
        $active['parent'] = 0;
        $active['id'] = 0;
        $result = $this->links_model->getSearch(array("id_user_type" => $user_type_id, "l.link_location" => 2), "", array("link_order" => "ASC"));
        foreach ($result->result() as $q) {
            if (strpos($q->link_url, self::current_classurl()) !== false) {
                $active['parent'] = $q->parent_link_id;
                $active['id'] = $q->id_link;
            }

            if ($active['parent'] == 0) {
                if ($q->link_url == $url_name) {
                    $active['parent'] = $q->parent_link_id;
                    $active['id'] = $q->id_link;
                }
            }
        }
        return array('data' => self::_query_menuhierarchy($result), 'active' => $active);
    }

    /*
     * Arrange Order of Menu
     */

    function _query_menuhierarchy($result) {
        $layer = '';
        foreach ($result->result() as $q) {
            $layer1['id'] = $q->id_link;
            $layer1['name'] = $q->link_name;
            $layer1['parent_id'] = $q->parent_link_id;

            $layer1['data'] = $q;
            $layer[$q->parent_link_id][$q->id_link] = $layer1;
        }

        $arr = $layer;
        $arrParent = '';
        if ($layer != '') {
            $arrParent = $arr[0];
        }
        return self::_set_menuhierarchy($arrParent, $arr);
    }

    function _set_menuhierarchy($parent, $list) {
        $tree = array();
        if (!empty($parent))
            foreach ($parent as $var => $val) {
                $layer = '';
                $layer['id'] = $var;
                $layer['val'] = $val;
                if (!empty($list[$var])) {
                    $layer['child'] = self::_set_menuhierarchy($list[$var], $list);
                }
                $tree[] = $layer;
            }
        return $tree;
    }

    /*
     * Get Current Class URL
     */

    function current_classurl() {
        $class_name = $this->classname;
        $page_name = $this->pagename;
        $method_name = $this->methodname;

        $url = '';
        if ($class_name != '')
            $url.=$class_name;
        if ($page_name != '')
            $url.='/' . $page_name;
        if ($method_name != '')
            $url.='/' . $method_name;
        return $url;
    }

    function upload_company_path($company_id) {
        $path_name = "./upload";
        if (!is_dir($path_name)) {
            mkdir($path_name);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name.="/company";
        if (!is_dir($path_name)) {
            mkdir($path_name);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name.="/" . $company_id;
        if (!is_dir($path_name)) {
            mkdir($path_name);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name_logo = $path_name . "/logo";
        if (!is_dir($path_name_logo)) {
            mkdir($path_name_logo);
            copy('./upload/index.html', $path_name_logo . '/index.html');
        }

        $path_name_buttonlogo = $path_name . "/buttonlogo";
        if (!is_dir($path_name_buttonlogo)) {
            mkdir($path_name_buttonlogo);
            copy('./upload/index.html', $path_name_buttonlogo . '/index.html');
        }

        $data['logo'] = $path_name_logo . "/";
        $data['buttonlogo'] = $path_name_buttonlogo . "/";
        $data['logo_url'] = base_url() . "upload/company/" . $company_id . "/logo/";
        $data['buttonlogo_url'] = base_url() . "upload/company/" . $company_id . "/buttonlogo/";

        return $data;
    }

    function upload_user_path($user_id) {
        $path_name = "./upload";
        if (!is_dir($path_name)) {
            mkdir($path_name);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name.="/user";
        if (!is_dir($path_name)) {
            mkdir($path_name);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name.="/" . $user_id;
        if (!is_dir($path_name)) {
            mkdir($path_name);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name_profile = $path_name . "/profile";
        if (!is_dir($path_name_profile)) {
            mkdir($path_name_profile);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name_profile_thumb = $path_name_profile . '/thumbnail';
        if (!is_dir($path_name_profile_thumb)) {
            mkdir($path_name_profile_thumb);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name_attendance = $path_name . "/attendance";
        if (!is_dir($path_name_attendance)) {
            mkdir($path_name_attendance);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $data['profile'] = $path_name_profile . "/";
        $data['profile_thumb'] = $path_name_profile_thumb . "/";
        $data['attendance'] = $path_name_attendance . "/";
        $data['profile_url'] = base_url() . "upload/user/" . $user_id . "/profile/";
        $data['profile_thumb_url'] = base_url() . "upload/user/" . $user_id . "/profile/thumbnail/";
        $data['attendance_url'] = base_url() . "upload/user/" . $user_id . "/attendance/";

        return $data;
    }

    function upload_applicant_path($applicant_id) {
        $path_name = "./upload";
        if (!is_dir($path_name)) {
            mkdir($path_name);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name.="/applicant";
        if (!is_dir($path_name)) {
            mkdir($path_name);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name.="/" . $applicant_id;
        if (!is_dir($path_name)) {
            mkdir($path_name);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name_profile = $path_name . "/profile";
        if (!is_dir($path_name_profile)) {
            mkdir($path_name_profile);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name_profile_thumb = $path_name_profile . '/thumbnail';
        if (!is_dir($path_name_profile_thumb)) {
            mkdir($path_name_profile_thumb);
            copy('./upload/index.html', $path_name . '/index.html');
        }


        $data['profile'] = $path_name_profile . "/";
        $data['profile_thumb'] = $path_name_profile_thumb . "/";
        $data['profile_url'] = base_url() . "upload/applicant/" . $applicant_id . "/profile/";
        $data['profile_thumb_url'] = base_url() . "upload/applicant/" . $applicant_id . "/profile/thumbnail/";

        return $data;
    }

    function upload_client_path($client_id) {
        $path_name = "./upload";
        if (!is_dir($path_name)) {
            mkdir($path_name);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name.="/client";
        if (!is_dir($path_name)) {
            mkdir($path_name);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name.="/" . $client_id;
        if (!is_dir($path_name)) {
            mkdir($path_name);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name_profile = $path_name . "/profile";
        if (!is_dir($path_name_profile)) {
            mkdir($path_name_profile);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name_profile_thumb = $path_name_profile . '/thumbnail';
        if (!is_dir($path_name_profile_thumb)) {
            mkdir($path_name_profile_thumb);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $path_name_attendance = $path_name . "/attendance";
        if (!is_dir($path_name_attendance)) {
            mkdir($path_name_attendance);
            copy('./upload/index.html', $path_name . '/index.html');
        }

        $data['profile'] = $path_name_profile . "/";
        $data['profile_thumb'] = $path_name_profile_thumb . "/";
        $data['attendance'] = $path_name_attendance . "/";
        $data['profile_url'] = base_url() . "upload/client/" . $client_id . "/profile/";
        $data['profile_thumb_url'] = base_url() . "upload/client/" . $client_id . "/profile/thumbnail/";
        $data['attendance_url'] = base_url() . "upload/client/" . $client_id . "/attendance/";

        return $data;
    }

    function convert_image($img, $path_name, $path_name_thumb) {
        $data[0] = false;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $datas = base64_decode($img);
        $file_name = md5(date('Y-m-d H:i:s') . rand()) . '.png';
        $file = $path_name . '/' . $file_name;
        file_put_contents($file, $datas);
        if (self::generate_imagethumbname($file, $file_name, $path_name_thumb, 'png')) {
            $data['file_name'] = $file_name;
            $data[0] = true;
        } else {
            $data['error'] = "Error upload";
        }
        return $data;
    }

    function upload_file($path_name, $path_name_thumb, $file_name, $type, $max_size) {
        if (!is_dir($path_name)) {
            mkdir($path_name);
        }
        $data[0] = false;
        $config['upload_path'] = $path_name;
        $config['allowed_types'] = $type;
        $config['file_name'] = md5($file_name . date('Y-m-d H:i:s') . rand());
        $config['max_size'] = $max_size;
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($file_name)) {
            $data['error'] = $this->upload->display_errors();
            $data['error'] = str_replace("<p>", "", $data['error']);
            $data['error'] = str_replace("</p>", " ", $data['error']);
            return $data;
        } else {
            $data = $this->upload->data();
            if ($data['image_type'] != '') {
                if (self::generate_imagethumbname($data['full_path'], $data['orig_name'], $path_name_thumb, $data['image_type'])) {
                    $data[0] = true;
                    return $data;
                } else {
                    $data[0] = true;
                    return $data;
                }
            } else {
                $data[0] = true;
                return $data;
            }
        }
    }

    function generate_imagethumbname($file_path, $file_name, $path_name_thumb, $type) {
        if ($path_name_thumb) {
            $pass = 0;
            $thumb_size = '100';
            if ($type == 'jpg' || $type == 'jpeg' || $type == 'jpe') {
                $im = imagecreatefromjpeg($file_path);
                $pass = true;
            } else if ($type == 'gif') {
                $im = imagecreatefromgif($file_path);
                $pass = true;
            } else if ($type == 'png') {
                $im = imagecreatefrompng($file_path);
                $pass = true;
            }
            if ($pass) {
                $ox = imagesx($im);
                $oy = imagesy($im);

                $nx = $thumb_size;
                $ny = floor($oy * ($thumb_size / $ox));

                $nm = imagecreatetruecolor($nx, $ny);

                imagecopyresized($nm, $im, 0, 0, 0, 0, $nx, $ny, $ox, $oy);

                imagejpeg($nm, $path_name_thumb . $file_name);
                return true;
            } else {
                return true;
            }
        }
        return true;
    }

    function upload_temp_path() {
        $path_name = "./upload";
        if (!is_dir($path_name)) {
            mkdir($path_name);
            copy('./upload/index.html', $path_name . '/index.html');
        }
        $path_name.="/temp";
        if (!is_dir($path_name)) {
            mkdir($path_name);
            copy('./upload/index.html', $path_name . '/index.html');
        }
        return $path_name;
    }

    function validation_alert($alert_type, $alert_message) {
        $result = '';
        if ($alert_message != '') {
            $data['alert_type'] = $alert_type;
            $data['alert_message'] = $alert_message;
            $result = $this->load->view(admin_dir('template/validation_alert'), $data, true);
        }
        return $result;
    }

    function save_log($detail = '', $table = '', $value = '', $page = '', $classname = '') {
        if ($page == '') {
            $page = $this->pagename;
        }

        $method = $this->methodname;
        if ($this->pagename != 'method') {
            $method = '';
        }
        if ($classname == '') {
            $classname = $this->classname;
        }

        $datas = array(
            'user_log_date' => date('Y-m-d H:i:s'),
            'user_log_detail' => $detail,
            'user_log_table' => $table,
            'user_log_value' => $value,
            'page_name' => $page,
            'method_name' => $method,
            'class_name' => $classname,
            'user_id' => $this->my_session->get('admin', 'user_id')
        );
        $this->user_logs_model->insert_table($datas);
    }

    function save_notification($data) {
        self::manipulate_emailsend($data);
    }

    function manipulate_emailsend($data) {
        foreach ($data as $var => $val) {
            $datas = array(
                'template' => !empty($val['template']) ? $val['template'] : "",
                'subject' => !empty($val['subject']) ? $val['subject'] : "",
                'to' => !empty($val['to']) ? $val['to'] : "",
                'toName' => !empty($val['toName']) ? $val['toName'] : "",
                'from' => !empty($val['from']) ? $val['from'] : $this->my_session->get('admin', 'user_email'),
                'fromName' => !empty($val['fromName']) ? $val['fromName'] : $this->my_session->get('admin', 'name'),
                'cc' => !empty($val['cc']) ? $val['cc'] : "",
                'bcc' => !empty($val['bcc']) ? $val['bcc'] : ""
            );

            if (!empty($val['type']))
                $datas = self::_generate_emaildata($val['type'], $val, $datas);

            if (!empty($datas['template']) and ! empty($datas['subject']) and ! empty($datas['to']) and ! empty($datas['from']))
                parent::send_email($datas['template'], $datas['subject'], $datas['to'], $datas['toName'], $datas['from'], $datas['fromName'], $datas['cc'], $datas['bcc']);
        }
    }

    //inserting array
    function array_insert(&$array, $position, $insert_array) {
        $first_array = array_splice($array, 0, $position);
        $array = array_merge($first_array, $insert_array, $array);
    }

    //search value in Array
    function searchArrayValues($value, $array) {

        foreach ($array as $key => $arr) {
            foreach ($arr as $key => $val) {
                if ($val == $value) {
                    return $key;
                    break;
                }
            }
        }
    }

}
