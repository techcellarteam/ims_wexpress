<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//require '/var/www/html/inventory/vendor/autoload.php';

class MY_Controller extends MX_Controller {

    private $configs_model = '';

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/configs_model');
        $this->configs_model = new configs_model();
        date_default_timezone_set($this->configs_model->getValue(15, 'config_value')); //DEFAULT_TIME_ZONE
    }

    public function send_email($template = '', $subject = '', $to = '', $toName = '', $from = 'no-reply@techcellar.com', $fromName = '', $cc = '', $bcc = '') {
        $this->load->model('admin/configs_model');
        $condition = "(";
        $condition.="config_name like 'EMAIL_PROTOCOL'"; //EMAIL_PROTOCOL
        $condition.="or config_name like 'EMAIL_TYPE'"; //EMAIL_TYPE
        $condition.="or config_name like 'SMTP_HOST'"; //SMTP_HOST
        $condition.="or config_name like 'SMTP_USER'"; //SMTP_USER
        $condition.="or config_name like 'SMTP_PASS'"; //SMTP_PASS
        $condition.="or config_name like 'SMTP_PORT'"; //SMTP_PORT
        $condition.=")";
        $result = $this->configs_model->getSearch(trim($condition), "", "", true);
        $data = '';
        foreach ($result as $q) {
            $data[$q->config_name] = $q->config_value;
        }
//
//        $config['protocol'] = $data['EMAIL_PROTOCOL'];
//        $config['mailtype'] = $data['EMAIL_TYPE'];
//        $config['newline'] = "\r\n";
//        $config['crlf'] = "\r\n";
//
//        if ($config['protocol'] == 'smtp') {
//            $config['smtp_host'] = $data['SMTP_HOST'];
//            $config['smtp_user'] = $data['SMTP_USER'];
//            $config['smtp_pass'] = $data['SMTP_PASS'];
//            $config['smtp_port'] = $data['SMTP_PORT'];
//        }
//
//        // Load E-mail Library
//        $this->load->library('email');
//
//        $this->email->initialize($config);
//
//        $this->email->from($from, $fromName);
//        $this->email->to($to);
//        $this->email->cc($cc);
//        $this->email->bcc($bcc);
//        $this->email->subject($subject);
//        $this->email->set_newline("\r\n");
//        $this->email->message($template);
//
//        if ($this->email->send()) {
//            echo $this->email->print_debugger();
//        } else {
//            echo $this->email->print_debugger();
//        }
//
//        $this->email->clear();

        $mail = new PHPMailer(true);

        // Passing `true` enables exceptions
//        try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                       // Set mailer to use SMTP
        $mail->Host = $data['SMTP_HOST']; //'mail.techcellar.net';              // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication

        $mail->Username = $data['SMTP_USER']; //'smtp@techcellar.net';                 // SMTP username
        $mail->Password = $data['SMTP_PASS']; //'Tbs112233';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $data['SMTP_PORT']; //587;                                    // TCP port to connect to
        $mail->setFrom($from, $fromName);
        $mail->addAddress($to, $toName);     // Add a recipient
        $mail->addReplyTo($from, $fromName);

        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body = $template;

        $mail->send();

        //techcellar creds
//            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
//            $mail->isSMTP();                                       // Set mailer to use SMTP
//            $mail->Host = 'mail.techcellar.net';              // Specify main and backup SMTP servers
//            $mail->SMTPAuth = true;                               // Enable SMTP authentication
//
//            $mail->Username = 'smtp@techcellar.net';                 // SMTP username
//            $mail->Password = 'Tbs112233';                           // SMTP password
//            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
//            $mail->Port = 587;                                    // TCP port to connect to
//            $mail->setFrom($from, $fromName);
//            $mail->addAddress($to, $toName);     // Add a recipient
//            $mail->addReplyTo($from, $fromName);
//
//            $mail->isHTML(true);                                  // Set email format to HTML
//            $mail->Subject = $subject;
//            $mail->Body = $template;
//
//            $mail->send();
        //end techcellar
//            echo 'Message has been sent';
//        } catch (Exception $e) {
////            echo 'Message could not be sent.';
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
//        }
    }

    // --------------------------------------------------------------------

    /*
     * Copy postdata to object
     *
     * @access		public
     * @param 		object
     * @param 		string
     * @return		void
     */
    public function copyFromPost(&$object, $except = '') {
        foreach ($this->tools->getPost() as $key => $value)
            if (key_exists($key, $object) AND $key != $except) {
                // Does not change password if field is empty
                if ($key == 'password' AND empty($value))
                    continue;

                $object->{$key} = $value;
            }
    }

}
