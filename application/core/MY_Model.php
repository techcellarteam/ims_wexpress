<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Model extends CI_Model {
    /* int object id */

    public $id;

    /* string table name */
    protected $table = '';
    protected $alias = '';
    public $Misc = '';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct($id = NULL) {
        parent::__construct();

        $this->Misc = new Misc();

        if (!empty($id)) {
            // WHERE
            $this->db->where(array($this->identifier => $id));

            $query = $this->db->get($this->table);
            $result = $query->row();

            $this->id = $id;
        }

        if (!empty($result)) {
            foreach ($result AS $key => $value)
                if (key_exists($key, $this))
                    $this->{$key} = $value;
        }
    }

    /*
     * WHERE
     * @return void
     */

    public function where($where) {
        //get the last 30 days transactions
        if (empty($where) && $this->alias) {
//            $this->db->where("($this->alias.added_date BETWEEN (NOW() - INTERVAL 30 DAY) AND NOW())");
        }
        if (!empty($where))
            $this->db->where($where);
    }

    /*
     * WHERE STRING
     * @return void
     */

    public function where_string($where) {
        if (!empty($where))
            foreach ($where as $val) {
                if ($val)
                    $this->db->where($val);
            }
    }

    /*
     * LIKE
     * @return void
     */

    public function like($like) {
        if (!empty($like))
            $this->db->like($like);
    }

    /*
     * GROUP BY
     * @return void
     */

    public function group_by($group_by) {
        if (!empty($group_by))
            $this->db->group_by($group_by);
    }

    /*
     * ORDER BY
     * @return void
     */

    public function orderby($order_by) {
        if (!empty($order_by)) {
            foreach ($order_by as $field => $direction)
                $this->db->order_by($field, $direction);
        }
    }

    /*
     * LIMIT - OFFSET
     * @return void
     */

    public function limit($limit, $offset) {
        if ($offset > 0) {
            $offset = ($offset * $limit) - $limit;
            $this->db->limit($limit, $offset);
        }
    }

    /*
     * PAGE LIMIT - OFFSET
     * @return void
     */

    public function pagelimit($page, $number) {
        if (($page - 1) > 0) {
            $this->db->limit($number, (($page - 1) * $number));
        } else {
            $this->db->limit($number);
        }
//        $this->db->limit($number, (($page - 1) * $number));
    }

    // --------------------------------------------------------------------

    /*
     * Executes INSERT
     * Add records
     *
     * @access		public
     * @param 		array
     * @return		int
     */
    public function add($fields = array()) {
        // auto fill-in date added
        if (key_exists('added_date', $this)) {
            $this->added_date = date('Y-m-d H:i:s');
            $this->added_by = $this->session->userdata['admin']['user_id'];
        }

        // auto fill-in date updated
        if (key_exists('updated_date', $this))
            $this->updated_date = date('Y-m-d H:i:s');

        if (key_exists('updated_by', $this))
            $this->updated_by = $this->session->userdata['admin']['user_id'];

        // get fields
        if (!empty($fields))
            $this->db->insert($this->table, $fields);
        else
            $this->db->insert($this->table, $this->getObjectFields());



        $this->id = $this->db->insert_id();
        return $this->id;
    }

    // --------------------------------------------------------------------

    /*
     * Executes UPDATE
     * Updates records
     *
     * @access		public
     * @param 		array
     * @return		bool
     */
    public function update($where = array(), $fields = array()) {
        // auto fill-in date updated
        if (key_exists('updated_date', $this))
            $this->updated_date = date('Y-m-d H:i:s');

        if (key_exists('updated_by', $this))
            $this->updated_by = $this->session->userdata['admin']['user_id'];

        // WHERE
        if (!empty($where))
            $this->db->where($where);
        else
            $this->db->where(array($this->identifier => $this->misc->decode_id($this->id)));

        // update fields
        if (!empty($fields))
            $this->db->update($this->table, $fields);
        else
            $this->db->update($this->table, $this->getObjectFields());

        return TRUE;
    }

    // --------------------------------------------------------------------

    /*
     * Executes DELETE
     * Delete records
     *
     * @access		public
     * @param 		array
     * @return		bool
     */
    public function check_other_table($where = array(), $table = '') {
        // SELECT
        $this->db->select('*');

        // WHERE
        $this->db->where($where);
        if (key_exists('date_update', $table)) {
            if (key_exists('enabled', $table)) {
                $this->db->where('enabled', 1);
            }
        }
        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update($this->table, $data);
        return $key;
    }

    // --------------------------------------------------------------------

    /*
     * Executes DELETE
     * Delete records
     *
     * @access		public
     * @param 		array
     * @return		bool
     */
    public function delete($where = array()) {
        // WHERE
        if (!empty($where))
            $this->db->where($where);
        else
            $this->db->where(array($this->identifier => $this->id));

        // DELETE
        $this->db->delete($this->table);

        return TRUE;
    }

    // --------------------------------------------------------------------

    /*
     * Redirect if object is empty
     *
     * @access		public
     * @param		string
     * @return		void
     */
    public function redirectIfEmpty($redirect = '') {
        if (!$this->{$this->identifier} || empty($this->{$this->identifier}) || (isset($this->enabled) && $this->enabled == 1)) {
            $this->session->set_flashdata('note', 'There is no value attached to that ID.');
            redirect($redirect);
        }
    }

    // --------------------------------------------------------------------

    /*
     * Executes query, returns with the value of the field
     *
     * @access		public
     * @param		string
     * @return		mixed
     */
    public function getLastNo($field = '', $likefieldname = '', $likedata = '', $where = array()) {
        $this->db->select_max($field);

        // LIKE
        if ($likefieldname) {
            $this->db->like($likefieldname, $likedata);
        }
        // where
        if ($where) {
            $this->db->where($where);
        }
        $query = $this->db->get($this->table);
        $row = $query->row();

        if (!$row)
            return FALSE;

        return $row->$field;
    }
    
    // --------------------------------------------------------------------

    /*
     * Executes query, returns with the value of the field
     *
     * @access		public
     * @param		string
     * @return		mixed
     */
    public function PRgetLastNo($field = '', $likefieldname = '', $likedata = '', $where = array()) {
        $this->db->select_max("SUBSTRING($field, 8)",'pr_number');

        // LIKE
        if ($likefieldname) {
            $this->db->like($likefieldname, $likedata);
        }
        // where
        if ($where) {
            $this->db->where($where);
        }
        $query = $this->db->get($this->table);
        $row = $query->row();

        if (!$row)
            return FALSE;

        return $row->$field;
    }
    
    // --------------------------------------------------------------------

    /*
     * Executes query, returns with the value of the field
     *
     * @access		public
     * @param		string
     * @return		mixed
     */
    public function POgetLastNo($field = '', $likefieldname = '', $likedata = '', $where = array()) {
        $this->db->select_max("SUBSTRING($field, 8)",'po_number');

        // LIKE
        if ($likefieldname) {
            $this->db->like($likefieldname, $likedata);
        }
        // where
        if ($where) {
            $this->db->where($where);
        }
        $query = $this->db->get($this->table);
        $row = $query->row();

        if (!$row)
            return FALSE;

        return $row->$field;
    }

    // --------------------------------------------------------------------

    /*
     * Get account field value
     *
     * @access		public
     * @param		mixed
     * @param		array
     * @return		object
     */
    public function getFieldValue($fieldname = '', $where = array(), $like = array()) {
        // SELECT
        $this->db->select($fieldname);

        // WHERE
        if ($where) {
            $this->db->where($where);
        }

        if ($like) {
            $this->db->like($like);
        }

        $query = $this->db->get($this->table . ' t');
        $row = $query->row();

        if ($row)
            return $row->{$fieldname};

        return FALSE;
    }

    // --------------------------------------------------------------------

    /*
     * Executes query, returns with the value of the field
     *
     * @access		public
     * @param		string
     * @return		mixed
     */
    public function getMinNo($field = '', $likefieldname = '', $likedata = '', $where = array()) {
        $this->db->select_min($field);

        // LIKE
        if ($likefieldname) {
            $this->db->like($likefieldname, $likedata);
        }
        // where
        if ($where) {
            $this->db->where($where);
        }
        $query = $this->db->get($this->table);
        $row = $query->row();

        if (!$row)
            return FALSE;

        return $row;
    }

    // --------------------------------------------------------------------

    /*
     * Executes query, returns with the value of the field
     *
     * @access		public
     * @param		string
     * @return		mixed
     */
    public function getSum($field = '', $alias = '', $where = array(), $like = array(), $where_string = '', $table_alias = 't') {
        $this->db->select_sum($field, $alias);

        // LIKE
        if ($like) {
            $this->db->like($like);
        }
        // where
        if ($where) {
            $this->db->where($where);
        }

        // where
        if ($where_string) {
            $this->db->where($where_string);
        }
        $query = $this->db->get($this->table . ' ' . $table_alias);
        $row = $query->row();

        if (!$row)
            return FALSE;

        return $row->$alias;
    }

    // --------------------------------------------------------------------

    /*
     * Executes query, returns with the value of the field
     *
     * @access		public
     * @param		string
     * @return		mixed
     */
    function selectDistinct($field = '', $fields = '', $table = '', $compare = 'AND', $where = array()) {
        $this->db->distinct($field);
        $this->db->select($fields);

        // WHERE
        if ($compare == 'AND')
            $this->db->Where($where);

        if ($compare == 'OR')
            $this->db->or_where($where);

        $query = $this->db->get($table);
        $result = $query->result_array();

        return $result;
    }

}
