<style type="text/css">
    table, th, td {
        border: 1px solid black;
    }
    table {
        border-spacing: 1px;
    }

    td,th{
        text-align: left;
        padding: 5px;
    }
</style>
<?php if ($pr_chairman) { ?>
    <table>
        <thead>
            <tr>
                <th class="col-lg-1">PR #</th>
                <th class="col-lg-1">Requesting Branch</th>
                <th class="col-lg-1">Total Amount</th>
                <th class="col-lg-1">Status</th>
                <th class="col-lg-1">Aging</th>
            </tr>
        </thead>
        <tbody id='pr_items_body'>
            <?php foreach ($pr_chairman as $q) {
                ?>
                <tr id="id_table">
                    <td class='col-lg-1'>
                        <a href="<?php echo admin_url("purchase_requisitions/view_purchase_requisition/" . $this->Misc->encode_id($q->id_purchase_requisition)); ?>">
                            <?= $q->pr_number; ?>
                        </a>
                    </td>
                    <td class='col-lg-1'><?= $q->business_unit_name; ?></td>
                    <td class='col-lg-1'><?= number_format($q->total_amount, 2); ?></td>
                    <td class='col-lg-1'><?= $q->status . " " . (($q->second_approval_date != '0000-00-00 00:00:00')?date('m/d/Y h:i', strtotime($q->second_approval_date)):''); ?></td>
                    <td class='col-lg-1'><?= $q->aging; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php } ?>
<br>
<?php if ($po_chairman) { ?>
    <table>
        <thead>
            <tr>
                <th class="col-lg-1">PO#</th>
                <th class="col-lg-1">Requesting Branch</th>
                <th class="col-lg-1">Total Amount</th>
                <th class="col-lg-1">Status</th>
                <th class="col-lg-1">Aging</th>
            </tr>
        </thead>
        <tbody id='pr_items_body'>
            <?php
            foreach ($po_chairman as $q) {
                $date1 = date_create(date('Y-m-d'));
                $date2 = date_create(date('Y-m-d', strtotime($q->second_approval_date)));
                $date_diff = date_diff($date1, $date2)->format('%a');
                ?>
                <tr id="id_table">
                    <td class='col-lg-1'>
                        <a href="<?php echo admin_url("purchase_orders/view_purchase_order/" . $this->Misc->encode_id($q->id_purchase_order)); ?>">
                            <?= $q->po_number; ?>
                        </a>
                    </td>
                    <td class='col-lg-1'><?= $q->business_unit_name; ?></td>
                    <td class='col-lg-1'><?= number_format($q->total_amount, 2); ?></td>
                    <td class='col-lg-1'><?= $q->status . " " . (($q->second_approval_date != '0000-00-00 00:00:00')?date('m/d/Y h:i', strtotime($q->second_approval_date)):date('m/d/Y h:i', strtotime($q->first_approval_date))); ?></td>
                    <td class='col-lg-1'><?= $date_diff; ?></td>
                </tr>
            <?php }
            ?>
        </tbody>
    </table>
<?php } ?>

