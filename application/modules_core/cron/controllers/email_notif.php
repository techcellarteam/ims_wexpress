<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Purchase Requisitions Class
  |--------------------------------------------------------------------------
  |
  | Handles the Purchase Requisitions panel
  |
  | @category		Controller
  | @author		melin
 */

class Email_Notif extends MY_Controller {

    public $purchase_requisitions_model = '';
    public $purchase_orders_model = '';
    public $Misc = '';

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        //load models
        $this->load->model('admin/purchase_requisitions_model');
        $this->load->model('admin/purchase_orders_model');

        //initiate models
        $this->purchase_requisitions_model = new Purchase_Requisitions_Model();
        $this->purchase_orders_model = new Purchase_Orders_Model();
        $this->Misc = new Misc();
    }

    function index() {
        redirect(admin_dir('purchase_requisitions/list_purchase_requisition'));
    }

    function email_notification() {
        //set conditions
        //purchase requistion
        $condition_evp_pr = array('pr.status' => 'Queued to ELA');
        $condition_chairman_pr = array('pr.status' => 'Queued to DJF', 'total_amount >=' => 50000);

        //purchase order
        $condition_evp_po = array('po.status' => 'Queued to ELA');
        $condition_chairman_po = array('po.status' => 'Queued to DJF');

        //notification to evp
        $data_evp['pr_evp'] = $this->purchase_requisitions_model->getList($condition_evp_pr)->result();
        $data_evp['po_evp'] = $this->purchase_orders_model->getList($condition_evp_po)->result();
        
        $content_evp = $this->load->view(cron_dir('email_notification_evp.php'), $data_evp, TRUE);

        $data_chairman['pr_chairman'] = $this->purchase_requisitions_model->getList($condition_chairman_pr)->result();
        $data_chairman['po_chairman'] = $this->purchase_orders_model->getList($condition_chairman_po)->result();
        $content_chairman = $this->load->view(cron_dir('email_notification_chairman.php'), $data_chairman, TRUE);

        $subject = "PR/PO Approval Notification - [" . date('m/d/Y h:i') . "]";


        //to Executive vice president
        if (!empty($data_evp['pr_evp']) || !empty($data_evp['po_evp'])) {

            //save log
            $datas = array(
                'user_log_date' => date('Y-m-d H:i:s'),
                'user_log_detail' => 'cron',
                'user_log_table' => 'email_notification_vp',
                'user_log_value' => 1,
                'page_name' => $this->pagename,
                'method_name' => $this->methodname,
                'class_name' => $this->classname,
                'user_id' => 1
            );
            $this->load->model('admin/user_logs_model');
            $this->user_logs_model = new User_Logs_Model();
            $this->user_logs_model->insert_table($datas);
            $evp_email = 'emily.alcid@wwwexpress.com.ph';
            parent::send_email($content_evp, $subject, $evp_email, "", "alert@wexpress.com.ph", "Inventory Management System Alert", '', '');
            parent::send_email($content_evp, $subject, "alert@wexpress.com.ph", "", "alert@wexpress.com.ph", "Inventory Management System Alert", '', '');
//            parent::send_email($content_evp, $subject . " SENT VIA WEXPRESS SMTP", "roengen.mendoza@wwwexpress.com.ph", "", "alert@wexpress.com.ph", "Inventory Management System Alert", '', '');
//            parent::send_email($content_evp, $subject . " SENT VIA WEXPRESS SMTP", "melin@techcellar.com", "", "alert@wexpress.com.ph", "Inventory Management System Alert", '', '');
        }
        //to chairman
        if (!empty($data_chairman['pr_chairman']) || !empty($data_chairman['po_chairman'])) {
            //save log
            $datas = array(
                'user_log_date' => date('Y-m-d H:i:s'),
                'user_log_detail' => 'cron',
                'user_log_table' => 'email_notification_ch',
                'user_log_value' => 1,
                'page_name' => $this->pagename,
                'method_name' => $this->methodname,
                'class_name' => $this->classname,
                'user_id' => 1
            );
            $this->load->model('admin/user_logs_model');
            $this->user_logs_model = new User_Logs_Model();
            $this->user_logs_model->insert_table($datas);
            $dawn_email = 'dawn.feliciano@wwwexpress.com.ph';
            parent::send_email($content_chairman, $subject, $dawn_email, "", "alert@wexpress.com.ph", "Inventory Management System Alert", '', '');
            parent::send_email($content_chairman, $subject, "alert@wexpress.com.ph", "", "alert@wexpress.com.ph", "Inventory Management System Alert", '', '');
//            parent::send_email($content_chairman, $subject . " SENT VIA WEXPRESS SMTP", "roengen.mendoza@wwwexpress.com.ph", "", "alert@wexpress.com.ph", "Inventory Management System Alert", '', '');
//            parent::send_email($content_chairman, $subject . " SENT VIA WEXPRESS SMTP", "melin@techcellar.com", "", "alert@wexpress.com.ph", "Inventory Management System Alert", '', '');
        }
    }

}
