<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Purchase Order Items Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Purchase Order Items records
  |
  | @category		Model
  | @author		melin
 */

class Purchase_Order_Items_Model extends MY_Model {
    /* int primary key   */

    public $id_po_item;

    /* int purchase order id  */
    public $purchase_order_id;

    /* int item id  */
    public $item_id;

    /* text item unit cost  */
    public $item_unit_cost;

    /* int quantity  */
    public $quantity;

    /* int quantity received */
    public $qty_rcvd;

    /* int quantity good order */
    public $qty_go;

    /* int quantity bad order */
    public $qty_bo;

    /* tiny int(1) delete status */
    public $enabled;

    /* string table name */
    protected $table = 'purchase_order_items';

    /* string table identifier */
    protected $identifier = 'id_po_item';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_po_item'] = (int) $this->misc->decode_id($this->id);
        $fields['purchase_order_id'] = (int) ($this->purchase_order_id);
        $fields['item_id'] = (int) $this->item_id;
        $fields['item_unit_cost'] = $this->item_unit_cost;
        $fields['quantity'] = $this->quantity;
        $fields['qty_rcvd'] = $this->qty_rcvd;
        $fields['qty_bo'] = $this->qty_bo;
        $fields['qty_go'] = $this->qty_go;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('poi.id_po_item' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('poi.id_po_item' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("poi.id_po_item");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("poi.id_po_item");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    function calcTotalAmount($where = array(), $where_string = array()) {
        $this->db->select('sum(`poi`.`item_unit_cost`*`poi`.`quantity`)as poi_total_amount');
        self::_from();
        self::_join();
        self::_fix_arg();
        if ($where) {
            parent::where($where);
        }
        if ($where_string) {
            parent::where_string($where_string);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return !empty($row) ? $row->poi_total_amount : false;
        }
        return false;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("purchase_order_items poi");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			poi.*,
                        c.sku,c.description,
                        um.measurement_name,um.measurement_code,
                        cg.id_stock_classification,cg.stock_classification_name,
                        po.po_number,po.supplier_id,
                        bu.business_unit_name,
                        pri.quantity as pr_quantity,pri.initiator_price,
                        pri.last_ordered_price,pri.remarks,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('items c', 'c.id_item = poi.item_id', 'left');
        $this->db->join('stock_classifications cg', 'cg.id_stock_classification = c.stock_classification_id', 'left');
        $this->db->join('unit_measurements um', 'um.id_unit_measurement = c.unit_measurement_id', 'left');
        $this->db->join('purchase_orders po', 'po.id_purchase_order = poi.purchase_order_id', 'left');
        $this->db->join('business_units bu', 'bu.id_business_unit = po.business_unit_id', 'left');
        $this->db->join('purchase_requisition_items pri', 'pri.id_pr_item = poi.pr_item_id', 'left');
        $this->db->join('purchase_requisitions pr', 'pr.id_purchase_requisition = pri.purchase_requisition_id', 'left');
        $this->db->join('plate_numbers pl', 'pl.id_plate_number = pr.plate_number_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('poi.enabled' => 1));
    }

}
