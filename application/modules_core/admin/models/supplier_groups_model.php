<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Supplier Groups Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Supplier Groups records
  |
  | @category		Model
  | @author		melin
 */

class Supplier_Groups_Model extends MY_Model {
    /* int primary key   */

    public $id_supplier_group;

    /* int item id  */
    public $general_group_id;

    /* int suppliers id  */
    public $supplier_id;

    /* tiny int(1) delete status */
    public $enabled;

    /* int added by */
    public $added_by;

    /* date date added */
    public $added_date;

    /* string table name */
    protected $table = 'supplier_groups';

    /* string table identifier */
    protected $identifier = 'id_supplier_group';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_supplier_group'] = (int) $this->misc->decode_id($this->id);;
        $fields['general_group_id'] = $this->general_group_id;
        $fields['supplier_id'] = $this->supplier_id;
        $fields['enabled'] = $this->enabled;
        $fields['added_by'] = $this->added_by;
        $fields['added_date'] = $this->added_by;
        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('sp.id_supplier_group' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('sp.id_supplier_group' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("sp.id_supplier_group");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("sp.id_supplier_group");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }
    
    // --------------------------------------------------------------------

    /*
     * Get account field value
     *
     * @access		public
     * @param		mixed
     * @param		array
     * @return		object
     */
    public function getFieldValue($fieldname = '', $where = array(), $like = array()) {
        // SELECT
        $this->db->select($fieldname);
        self::_from();
        // JOIN
        self::_join();

        // WHERE
        if ($where) {
            $this->db->where($where);
        }

        if ($like) {
            $this->db->like($like);
        }

        $query = $this->db->get();
        $row = $query->row();

        if ($row)
            return $row->{$fieldname};

        return FALSE;
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("supplier_groups sp", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("supplier_groups sp");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			sp.*,
                        gg.group_name,gg.group_code,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('general_group gg', 'gg.id_general_group = sp.general_group_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('sp.enabled' => 1));
    }

}
