<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Purchase Requisitions Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Purchase Requisitions records
  |
  | @category		Model
  | @author		melin
 */

class Purchase_Requisitions_Model extends MY_Model {
    /* int primary key   */

    public $id_purchase_requisition;

    /* int document type ID  */
    public $document_type_id;

    /* int business Unit ID  */
    public $business_unit_id;

    /* int category ID  */
    public $category_id;

    /* int plate number ID  */
    public $plate_number_id;

    /* int purchase requisition number  */
    public $pr_number; //PR-CGS-YY0000000

    /* int srf_number - tfleet  */
    public $srf_number;

    /* date date needed  */
    public $date_needed;

    /* text purpose  */
    public $pr_remarks;

    /* date submission date  */
    public $date_submitted;

    /* int(11) first level approver user id */
    public $first_level_id;

    /* int(11) second level approver user id */
    public $second_level_id;

    /* int(11) third level approver user id */
    public $third_level_id;

    /* int(11) fourth level approver user id */
    public $fourth_level_id;

    /* date first level approval date */
    public $first_approval_date;

    /* date second level approval date */
    public $second_approval_date;

    /* date top level approval date */
    public $top_level_approval_date;

    /* date third level approval date */
    public $third_approval_date;

    /* date fourth level approval date */
    public $fourth_level_approval_date;

    /* date first level decline date */
    public $first_decline_date;

    /* date second level decline date */
    public $second_decline_date;

    /* date third level decline date */
    public $third_decline_date;

    /* date fourth level decline date */
    public $fourth_decline_date;

    /* smallint for stocking (4) */
    public $for_stocking;

    /* int */
    public $branch_level_id;

    /* datetime */
    public $branch_approval_date;

    /* datetime */
    public $branch_decline_date;
    
     /* int */
    public $gtwy_level_id;

    /* datetime */
    public $gtwy_approval_date;

    /* datetime */
    public $gtwy_decline_date;
    
    /* int */
    public $svp_level_id;

    /* datetime */
    public $svp_approval_date;

    /* datetime */
    public $svp_decline_date;

    /* dec for total amount */
    public $total_amount;

    /* varchar(32) item code  */
    public $status;

    /* tiny int(1) delete status */
    public $enabled;

    /* int added by */
    public $added_by;

    /* int updated by */
    public $updated_by;

    /* date date added */
    public $added_date;

    /* date date updated */
    public $updated_date;

    /* string table name */
    protected $table = 'purchase_requisitions';

    /* string alias */
    protected $alias = 'pr';

    /* string table identifier */
    protected $identifier = 'id_purchase_requisition';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_purchase_requisition'] = (int) $this->misc->decode_id($this->id);
        $fields['business_unit_id'] = $this->business_unit_id;
        $fields['document_type_id'] = $this->document_type_id;
        $fields['category_id'] = $this->category_id;
        $fields['plate_number_id'] = ($this->plate_number_id) ? $this->plate_number_id : '';
        $fields['pr_number'] = $this->pr_number;
        $fields['srf_number'] = $this->srf_number;
        $fields['pr_remarks'] = $this->pr_remarks;
        $fields['date_submitted'] = $this->date_submitted;
        $fields['top_level_approval_date'] = $this->top_level_approval_date;
        $fields['status'] = $this->status;
        $fields['for_stocking'] = $this->for_stocking;
        $fields['date_needed'] = $this->date_needed;
        $fields['added_date'] = $this->added_date;
        $fields['added_by'] = $this->added_by;
        $fields['updated_date'] = $this->updated_date;
        $fields['updated_by'] = $this->updated_by;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('pr.id_purchase_requisition' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('pr.id_purchase_requisition' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array('id_purchase_requisition' => 'ASC'), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = array(), $order_by = array('id_purchase_requisition' => 'ASC')) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("pr.id_purchase_requisition");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("pr.id_purchase_requisition");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("purchase_requisitions pr", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("purchase_requisitions pr");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			pr.*,
                        DATEDIFF((Select po.order_date FROM purchase_orders po WHERE po.purchase_requisition_id = pr.id_purchase_requisition ORDER BY id_purchase_order ASC LIMIT 1),date_submitted ) as aging,
                        DATEDIFF(second_approval_date,date_submitted ) as aging_pra,
                        CONCAT(du.user_fname,SPACE(1), du.user_lname ) as user_name,
                        bu.business_unit_name, bu.business_unit_code,bu.business_unit_head_id,bu.general_group_id,bu.location as bu_location,
                        dty.document_type_name,
                        cat.document_type_name as category_name,
                        gg.group_code,
                        CONCAT(pl.plate_number,SPACE(1),pl.make,SPACE(1), pl.model ) as plate_number
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('document_types dty', 'dty.id_document_type = pr.document_type_id', 'left');
        $this->db->join('document_types cat', 'cat.id_document_type = pr.category_id', 'left');
        $this->db->join('default_users du', 'du.id_user = pr.added_by', 'left');
        $this->db->join('business_units bu', 'bu.id_business_unit = pr.business_unit_id', 'left');
        $this->db->join('general_group gg', 'gg.id_general_group = bu.general_group_id', 'left');
        $this->db->join('plate_numbers pl', 'pl.id_plate_number = pr.plate_number_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('pr.enabled' => 1));
    }

}
