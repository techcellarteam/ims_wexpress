<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Items Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Items records
  |
  | @category		Model
  | @author		melin
 */

class Items_Model extends MY_Model {
    /* int primary key   */

    public $id_item;

    /* int item type id  */
    public $general_group_id;

    /* int commodity group id  */
    public $stock_classification_id;

    /* int unit measurement id  */
    public $unit_measurement_id;

    /* varchar(32) client sku */
    public $sku;

    /* varchar(64) description */
    public $description;

    /* int stock level alert  */
    public $with_supplier;

    /* tiny int(1) delete status */
    public $enabled;

    /* int added by */
    public $added_by;

    /* int updated by */
    public $updated_by;

    /* date date added */
    public $added_date;

    /* date date updated */
    public $updated_date;

    /* string table name */
    protected $table = 'items';

    /* string alias */
    protected $alias = '';

    /* string table identifier */
    protected $identifier = 'id_item';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_item'] = (int) $this->misc->decode_id($this->id);
        $fields['general_group_id'] = (int) ($this->general_group_id);
        $fields['stock_classification_id'] = (int) $this->stock_classification_id;
        $fields['unit_measurement_id'] = (int) $this->unit_measurement_id;
        $fields['description'] = $this->description;
        $fields['sku'] = $this->sku;
        $fields['with_supplier'] = $this->with_supplier;
        $fields['added_date'] = $this->added_date;
        $fields['added_by'] = $this->added_by;
        $fields['updated_date'] = $this->updated_date;
        $fields['updated_by'] = $this->updated_by;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('i.id_item' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('i.id_item' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = array(), $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListSupplier($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        $this->db->select("cs.item_unit_cost");
        self::_from();
        self::_join();
        $this->db->join('items_suppliers cs', 'cs.item_id = i.id_item', 'left');
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("i.id_item");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("items i");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			i.*,
                        gg.group_name,gg.group_code,
                        cg.stock_classification_name,
                        um.measurement_code,um.measurement_name,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('general_group gg', 'gg.id_general_group = i.general_group_id', 'left');
        $this->db->join('stock_classifications cg', 'cg.id_stock_classification = i.stock_classification_id', 'left');
        $this->db->join('unit_measurements um', 'um.id_unit_measurement = i.unit_measurement_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('i.enabled' => 1));
    }

}
