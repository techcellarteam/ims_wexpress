<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Purchase Order Items Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Purchase Order Items records
  |
  | @category		Model
  | @author		melin
 */

class Receiving_Report_Items_Model extends MY_Model {
    /* int primary key   */

    public $id_receiving_report_item;

    /* int purchase order id  */
    public $receiving_report_id;

    /* int purchase order item id  */
    public $po_item_id;

    /* int purchase order item id  */
    public $item_location_id;

    /* int qty received  */
    public $qty_received;

    /* varchar 64 remarks  */
    public $remarks;

    /* tiny int(1) delete status */
    public $enabled;

    /* string table name */
    protected $table = 'receiving_report_items';

    /* string table identifier */
    protected $identifier = 'id_receiving_report_item';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_receiving_report_item'] = (int) $this->misc->decode_id($this->id);
        $fields['receiving_report_id'] = (int) ($this->receiving_report_id);
        $fields['po_item_id'] = (int) $this->po_item_id;
        $fields['qty_rcvd'] = $this->qty_rcvd;
        $fields['remarks'] = $this->remarks;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('rri.id_receiving_report_item' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('rri.id_receiving_report_item' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("rri.id_receiving_report_item");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("rri.id_receiving_report_item");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    function getTotalAmount($where = array()) {
        $this->db->select("sum(`item_unit_cost`*`qty_received`) as total_amount");
        self::_from();
        $this->db->join('purchase_order_items poi', 'poi.id_po_item = rri.po_item_id', 'left');
        self::_fix_arg();
        parent::where($where);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $row = $query->row();
            return (!empty($row->total_amount)) ? $row->total_amount : false;
        }
    }
    function getTotalQty($where = array()) {
        $this->db->select_sum('qty_received');
        self::_from();
        self::_fix_arg();
        parent::where($where);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $row = $query->row();
            return (!empty($row->qty_received)) ? $row->qty_received : false;
        }
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("receiving_report_items rri");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			rri.*,
                        poi.item_id,poi.item_unit_cost,poi.quantity,
                        c.sku,c.description,
                        um.measurement_name,um.measurement_code,
                        cg.id_stock_classification,cg.stock_classification_name,
                        po.po_number,po.supplier_id,
		");
//        s.supplier_name,
//        pr.pr_number, pr.date_needed,
        //du.user_fname,du.user_lname,
        //s.supplier_name,
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('purchase_order_items poi', 'poi.id_po_item = rri.po_item_id', 'left');
        $this->db->join('items c', 'c.id_item = poi.item_id', 'left');
        $this->db->join('stock_classifications cg', 'cg.id_stock_classification = c.stock_classification_id', 'left');
        $this->db->join('unit_measurements um', 'um.id_unit_measurement = c.unit_measurement_id', 'left');
        $this->db->join('receiving_reports rr', 'rr.id_receiving_report = rri.receiving_report_id', 'left');
        $this->db->join('purchase_orders po', 'po.id_purchase_order = rr.purchase_order_id', 'left');
//        $this->db->join('purchase_requsitions pr', 'pr.id_purchase_requisition = po.purchase_requisition_id', 'left');
//        $this->db->join('default_users du', 'du.id_user = rr.added_by', 'left');
//        $this->db->join('suppliers s', 's.id_supplier = po.supplier_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('rri.enabled' => 1));
    }

}
