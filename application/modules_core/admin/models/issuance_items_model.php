<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Issuance Items Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Issuance Items records
  |
  | @category		Model
  | @author		melin
 */

class Issuance_Items_Model extends MY_Model {
    /* int primary key   */

    public $id_issuance_item;

    /* int(11) parent ID  */
    public $issuance_id;
    
    /* int(11) commodity ID*/
    public $item_id;

    /* int(11) item_location_id after released  */
    public $item_location_id;

    /* int(11) quantity  */
    public $item_quantity;

    /* int(11) released quantity*/
    public $item_released;

    /* tiny int(1) delete status */
    public $enabled;

    /* string table name */
    protected $table = 'issuance_items';

    /* string table identifier */
    protected $identifier = 'id_issuance_item';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_issuance_item'] = (int) $this->misc->decode_id($this->id);
        $fields['issuance_id'] = $this->issuance_id;
        $fields['item_id'] = $this->item_id;
        $fields['item_quantity'] = $this->item_quantity;
        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('isi.id_issuance_item' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('isi.id_issuance_item' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("isi.id_issuance_item");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }
    
    function getListEdit($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("isi.id_issuance_item");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("isi.id_issuance_item");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    // --------------------------------------------------------------------

    /*
     * Get account field value
     *
     * @access		public
     * @param		mixed
     * @param		array
     * @return		object
     */
    public function get_queued($fieldname,$where = array(), $like = array()) {
        // SELECT
        $this->db->select_sum($fieldname);
        self::_from();
        // WHERE
        if ($where) {
            $this->db->where($where);
        }

        $this->db->where(array('isi.enabled' => 1));

        if ($like) {
            $this->db->like($like);
        }

        $this->db->join('issuances iss', 'iss.id_issuance = isi.issuance_id', 'left');

        $query = $this->db->get();
        $row = $query->row();

        if ($row)
            return $row->{$fieldname};

        return FALSE;
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("issuance_items isi", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("issuance_items isi");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			isi.*,
                        c.id_item,
                        c.sku,c.description,
                        um.measurement_name,um.measurement_code,
                        cg.id_stock_classification,cg.stock_classification_name,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
//        $this->db->join('item_location il', 'il.id_item_location = isi.item_location_id', 'left');
//        $this->db->join('purchase_order_items poi', 'poi.item_id = isi.item_id', 'left');
        $this->db->join('items c', 'c.id_item = isi.item_id', 'left');
        $this->db->join('stock_classifications cg', 'cg.id_stock_classification = c.stock_classification_id', 'left');
        $this->db->join('unit_measurements um', 'um.id_unit_measurement = c.unit_measurement_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('isi.enabled' => 1));
    }

}
