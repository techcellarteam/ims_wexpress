<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Suppliers Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Suppliers records
  |
  | @category		Model
  | @author		melin
 */

class Suppliers_Model extends MY_Model {
    /* int primary key   */

    public $id_supplier;

    /* int(11) general group id  */
    public $general_group_id;

    /* int(11) commodity group id  */
    public $stock_classification_id;

    /* int(11) accreditation date  */
    public $accreditation_date;

    /* varchar(16) supplier type  */
    public $supplier_type;

    /* varchar(64) supplier name  */
    public $supplier_name;

    /* varchar(16) supplier code  */
    public $supplier_code;

    /* varchar(32) supplier email  */
    public $email;

    /* varchar(8) supplier terms of payment  */
    public $terms_payment;

    /* varchar(16) supplier tin no  */
    public $tin_no;

    /* int vat 1/0  */
    public $vat;

    /* varchar(16) supplier phone no  */
    public $phone_no;

    /* varchar(16) supplier fax no  */
    public $fax_no;

    /* varchar(16)  ontact person name  */
    public $c_name;

    /* varchar(32) contact person email  */
    public $c_email;

    /* int contact person phone no  */
    public $c_phone_no;

    /* varchar(64) contact person address  */
    public $c_address;

    /* varchar(16) contact person department */
    public $c_department;

    /* varchar(16 contact person job title  */
    public $c_job_title;

    /* varchar(64) billing  address  */
    public $b_address;

    /* varchar(16) billing city  */
    public $b_city;

    /* varchar(16) billing state  */
    public $b_state;

    /* int billing zip code  */
    public $b_zip_code;

    /* varchar(32) country  */
    public $b_country_code;

    /* text remarks  */
    public $remarks;

    /* text note  */
    public $note;

    /* tiny int(1) delete status */
    public $enabled;

    /* small int(1) nominated */
    public $nominated;

    /* int added by */
    public $added_by;

    /* int updated by */
    public $updated_by;

    /* date date added */
    public $added_date;

    /* date date updated */
    public $updated_date;

    /* string table name */
    protected $table = 'suppliers';

    /* string alias */
    protected $alias = '';

    /* string table identifier */
    protected $identifier = 'id_supplier';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id) && $this->uri->rsegment(3) != 'edit_supplier') {
            $fields['id_supplier'] = (int) $this->misc->decode_id($this->id);
        }
        $fields['general_group_id'] = $this->general_group_id;
        $fields['stock_classification_id'] = $this->stock_classification_id;
        $fields['accreditation_date'] = date('Y-m-d', strtotime($this->accreditation_date));
        $fields['supplier_type'] = $this->supplier_type;
        $fields['supplier_name'] = $this->supplier_name;
        $fields['supplier_code'] = $this->supplier_code;
        $fields['email'] = $this->email;
        $fields['terms_payment'] = $this->terms_payment;
        $fields['tin_no'] = $this->tin_no;
        $fields['vat'] = $this->vat;
        $fields['phone_no'] = $this->phone_no;
        $fields['fax_no'] = $this->fax_no;
        $fields['c_name'] = $this->c_name;
        $fields['c_email'] = $this->c_email;
        $fields['c_phone_no'] = $this->c_phone_no;
        $fields['c_address'] = $this->c_address;
        $fields['c_department'] = $this->c_department;
        $fields['c_job_title'] = $this->c_job_title;
        $fields['b_address'] = $this->b_address;
        $fields['b_city'] = $this->b_city;
        $fields['b_state'] = $this->b_state;
        $fields['b_zip_code'] = $this->b_zip_code;
        $fields['b_country_code'] = $this->b_country_code;
        $fields['remarks'] = $this->remarks;
        $fields['note'] = $this->note;
        $fields['nominated'] = $this->nominated;
        $fields['added_date'] = $this->added_date;
        $fields['added_by'] = $this->added_by;
        $fields['updated_date'] = $this->updated_date;
        $fields['updated_by'] = $this->updated_by;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('s.id_supplier' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('s.id_supplier' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array(),$group_by= array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where($where_string);
        parent::group_by($group_by);
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("s.id_supplier");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("suppliers s", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("suppliers s");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			s.*,c.ccode,c.country,
                        gg.group_name,gg.group_code,
                        cg.stock_classification_name,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('countries c', 'c.ccode = s.b_country_code', 'left');
        $this->db->join('general_group gg', 'gg.id_general_group = s.general_group_id', 'left');
        $this->db->join('stock_classifications cg', 'cg.id_stock_classification = s.stock_classification_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('s.enabled' => 1));
    }

}
