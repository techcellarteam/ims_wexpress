<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Item Location Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Item Location records
  |
  | @category		Model
  | @author		melin
 */

class Item_Location_Model extends MY_Model {
    /* int primary key   */

    public $id_item_location;

    /* int(11) parent ID  */
    public $parent_id;

    /* int(11) businness unit name  */
    public $location_id;

    /* int(11) po item ID  */
    public $po_item_id;

    /* int(11) commodity ID  */
    public $item_id;

    /* int(11) location name  */
    public $item_quantity;

    /* int(11) location code  */
    public $item_count;

    /* tiny int(1) delete status */
    public $enabled;

    /* int added by */
    public $added_by;

    /* date date added */
    public $added_date;

    /* string table name */
    protected $table = 'item_location';

    /* string table identifier */
    protected $identifier = 'id_item_location';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_item_location'] = (int) $this->misc->decode_id($this->id);
        $fields['location_id'] = $this->location_id;
        $fields['item_quantity'] = $this->item_quantity;
        $fields['item_count'] = $this->item_count;
        $fields['added_date'] = $this->added_date;
        $fields['added_by'] = $this->added_by;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('il.id_item_location' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('il.id_item_location' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("il.id_item_location");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("il.id_item_location");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    // --------------------------------------------------------------------

    /*
     * Executes query, returns with the value of the field
     *
     * @access		public
     * @param		string
     * @return		mixed
     */
    function selectDistinct($field = '', $fields = '', $compare = 'AND', $where = array(), $order_by = array()) {
        $this->db->distinct($field);
        $this->db->select($fields);
        self::_join();
        self::_from();

        // WHERE
        if ($compare == 'AND')
            $this->db->Where($where);

        if ($compare == 'OR')
            $this->db->or_where($where);

        if ($order_by) {
            $this->db->order_by($where);
        }

        $query = $this->db->get();
        return $query;
    }

    // --------------------------------------------------------------------

    /*
     * Executes query, returns with the value of the field
     *
     * @access		public
     * @param		string
     * @return		mixed
     */
    function selectDistinctItems($where = array(), $where_string = array(), $group_by = array()) {
        $this->db->distinct('id_item,item_unit_cost');
        $this->db->select('poi.item_unit_cost,
                        c.id_item,c.sku,c.description,
                        um.measurement_name,um.measurement_code,
                        cg.id_stock_classification,cg.stock_classification_name,
                        il.id_item_location,
                        pr.for_stocking,');
        $this->db->select_sum('il.item_count');
        self::_join();
        self::_from();

        // WHERE
        $this->db->where($where);

        if (!empty($where_string))
             parent::where_string($where_string);

        $this->group_by($group_by);

        $query = $this->db->get();
        return $query;
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("item_location il", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("item_location il");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			il.*,
                        poi.item_unit_cost,
                        c.id_item,c.sku,c.description,
                        um.measurement_name,um.measurement_code,
                        cg.id_stock_classification,cg.stock_classification_name,
                        pr.for_stocking,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('business_units bu', 'bu.id_business_unit = il.location_id', 'left');
        $this->db->join('purchase_order_items poi', 'poi.id_po_item = il.po_item_id', 'left');
        $this->db->join('items c', 'c.id_item = il.item_id', 'left');
        $this->db->join('stock_classifications cg', 'cg.id_stock_classification = c.stock_classification_id', 'left');
        $this->db->join('unit_measurements um', 'um.id_unit_measurement = c.unit_measurement_id', 'left');
        $this->db->join('purchase_orders po', 'po.id_purchase_order = poi.purchase_order_id', 'left');
        $this->db->join('purchase_requisitions pr', 'pr.id_purchase_requisition = po.purchase_requisition_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('il.enabled' => 1));
    }

}
