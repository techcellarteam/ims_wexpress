<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Request Payments Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Request Payments records
  |
  | @category		Model
  | @author		melin
 */

class Request_Payments_Model extends MY_Model {
    /* int primary key   */

    public $id_request_payment;

    /* text  */
    public $request_payment_number;

    /* text */
    public $cost_center_code;

    /* 	int(11) */
    public $cost_center_id;

    /* text */
    public $requested_by;

    /* int(11) */
    public $approved;

    /* int(11) */
    public $approved_by;

    /* 	datetime */
    public $approved_date;

    /* 	int(11) */
    public $verified;

    /* 	int(11) */
    public $verified_by;

    /* 	datetime */
    public $verified_date;

    /* text */
    public $request_payment_supplier;

    /* text */
    public $request_payment_payee;

    /* 	decimal(13,2) */
    public $request_payment_amount;

    /* 	decimal(13,2) */
    public $paid_amount;

    /* text */
    public $request_payment_particulars;

    /* datetime */
    public $request_payment_due;

    /* int(11) */
    public $entry_id;

    /* tiny int(1) delete status */
    public $enabled;

    /* int added by */
    public $added_by;

    /* int updated by */
    public $updated_by;

    /* date date added */
    public $added_date;

    /* date date updated */
    public $updated_date;

    /* string table name */
    protected $table = 'accounting_2016.request_payments';
    
    /* string alias */
    protected $alias = 'rfp';

    /* string table identifier */
    protected $identifier = 'id_request_payment';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_request_payment'] = (int) $this->misc->decode_id($this->id);;
        $fields['plate_number'] = $this->plate_number;
        $fields['added_date'] = $this->added_date;
        $fields['added_by'] = $this->added_by;
        $fields['updated_date'] = $this->updated_date;
        $fields['updated_by'] = $this->updated_by;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('rfp.id_request_payment' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('rfp.id_request_payment' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("rfp.id_request_payment");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("rfp.id_request_payment");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("request_payments p", $data);
        return $key;
    }

    // --------------------------------------------------------------------

    /*
     * Executes query, returns with the value of the field
     *
     * @access		public
     * @param		string
     * @return		mixed
     */
    public function getLastNo($field = '', $likefieldname = '', $likedata = '', $where = array()) {
        $this->db->select_max($field);
        self::_join();
        // LIKE
        if ($likefieldname) {
            $this->db->like($likefieldname, $likedata);
        }
        // where
        if ($where) {
            $this->db->where($where);
        }
        $query = $this->db->get($this->table);
        $row = $query->row();

        if (!$row)
            return FALSE;

        return $row->$field;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("accounting_2016.request_payments rfp");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			rfp.*,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
//        $this->db->join('accounting_2016.cost_centers cc', 'cc.cost_center_code = IC', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('rfp.enabled' => 1));
    }

}
