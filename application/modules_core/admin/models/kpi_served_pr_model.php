<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | KPI Served PR Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the KPI Served PR records
  |
  | @category		Model
  | @author		melin
 */

class KPI_Served_PR_Model extends MY_Model {

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('pr.id_purchase_requisition' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('pr.id_purchase_requisition' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array('id_purchase_requisition' => 'ASC'), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = array(), $order_by = array('id_purchase_requisition' => 'ASC')) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("pr.id_purchase_requisition");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("pr.id_purchase_requisition");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("purchase_requisitions pr", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("purchase_requisitions pr");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			pr.*,
                        dty.document_type_name,
                        gg.group_code,
                        po.id_purchase_order,po.po_number,
                        rr.invoice_number,rr.date_received,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('document_types dty', 'dty.id_document_type = pr.category_id', 'left');
        $this->db->join('default_users du', 'du.id_user = pr.added_by', 'left');
        $this->db->join('business_units bu', 'bu.id_business_unit = du.business_unit_id', 'left');
        $this->db->join('general_group gg', 'gg.id_general_group = bu.general_group_id', 'left');
        $this->db->join('purchase_orders po', 'po.purchase_requisition_id = pr.id_purchase_requisition', 'left');
        $this->db->join('receiving_reports rr', 'rr.purchase_order_id = po.id_purchase_order', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('pr.enabled' => 1));
    }

}
