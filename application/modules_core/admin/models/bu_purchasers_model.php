<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Business Unit Purchasers Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Business Unit Purchasers records
  |
  | @category		Model
  | @author		melin
 */

class BU_Purchasers_Model extends MY_Model {
    /* int primary key   */

    public $id_bu_purchaser;

    /* varchar(64) item name  */
    public $business_unit_id;

    /* varchar(32) item code  */
    public $purchaser_id;

    /* string table name */
    protected $table = 'bu_purchasers';

    /* string table identifier */
    protected $identifier = 'id_bu_purchaser';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_bu_purchaser'] = (int) $this->misc->decode_id($this->id);;
        $fields['business_unit_id'] = $this->business_unit_id;
        $fields['purchaser_id'] = $this->purchaser_id;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('bup.id_bu_purchaser' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('bup.id_bu_purchaser' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where=array(), $where_string='', $order_by=array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("bup.id_bu_purchaser");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("bup.id_bu_purchaser");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }
    
    /*
	 * Update Query
	 * @return id
	 */
	function update_table($data,$table_col,$key){
		$this->db->where($table_col,$key);
		$this->db->update("bu_purchasers bup",$data);
        return $key;
	}

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("bu_purchasers bup");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			bup.*,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('business_units bu', 'bu.id_business_unit = bup.business_unit_id', 'left');
        $this->db->join('default_users u', 'u.id_user = bup.purchaser_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
//        $this->db->where(array('bup.enabled' => 1));
    }

}
