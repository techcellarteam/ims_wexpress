<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Receiving Reports Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Receiving Reports records
  |
  | @category		Model
  | @author		melin
 */

class Receiving_Reports_Model extends MY_Model {
    /* int primary key   */

    public $id_receiving_report;

    /* int  purchase order id              */
    public $purchase_order_id;

    /* varchar(16)  receiving report number */
    public $rr_number;

    /* int  receiving report number */
    public $invoice_no;

    /* date date received */
    public $date_received;

    /* text status */
    public $status;

    /* smallint enabled (4) */
    public $enabled;

    /* int added by */
    public $added_by;

    /* int updated by */
    public $updated_by;

    /* date date added */
    public $added_date;

    /* date date updated */
    public $updated_date;

    /* string table name */
    protected $table = 'receiving_reports';
    
    /* string alias */
    protected $alias = 'rr';

    /* string table identifier */
    protected $identifier = 'id_receiving_report';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_receiving_report'] = (int) $this->misc->decode_id($this->id);;
        $fields['purchase_order_id'] = (int) $this->misc->decode_id($this->purchase_order_id);
        $fields['rr_number'] = $this->rr_number;
        $fields['invoice_no'] = $this->invoice_no;
        $fields['date_received'] = date('Y-m-d', strtotime($this->date_received));
        $fields['added_date'] = $this->added_date;
        $fields['added_by'] = $this->added_by;
        $fields['updated_date'] = $this->updated_date;
        $fields['updated_by'] = $this->updated_by;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('rr.id_receiving_report' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('rr.id_receiving_report' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("rr.id_receiving_report");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("rr.id_receiving_report");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    function getPR_RR($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        $this->db->join('purchase_requisitions pr', 'pr.id_purchase_requisition = po.purchase_requisition_id', 'left');
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("rr.id_receiving_report");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getInvoiceNumber($where = array(), $where_string = '', $order_by = array()) {
        $this->db->select('id_receiving_report,invoice_number,pr.prf_number');
        self::_from();
        $this->db->join('payment_requests pr', 'pr.receiving_report_id = rr.id_receiving_report', 'left');
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("rr.id_receiving_report");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("receiving_reports rr", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("receiving_reports rr");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			rr.*,
                        CONCAT(du.user_fname,SPACE(1), du.user_lname ) as user_name,
                        po.id_purchase_order,po.po_number,
                        prf.prf_number,prf.invoice_received_by,prf.invoice_received_date,
                        s.supplier_name,s.c_name as supplier_contact_person,
                        pr.id_purchase_requisition,pr.pr_number,pr.date_needed,
                        bu.business_unit_code,
                        rfp.request_payment_amount,rfp.request_payment_particulars,rfp.approved_date,"
        );
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('purchase_orders po', 'po.id_purchase_order = rr.purchase_order_id', 'left');
        $this->db->join('purchase_requisitions pr', 'pr.id_purchase_requisition = po.purchase_requisition_id', 'left');
        $this->db->join('payment_requests prf', 'prf.receiving_report_id = rr.id_receiving_report', 'left');
        $this->db->join('default_users du', 'du.id_user = rr.added_by', 'left');
        $this->db->join('suppliers s', 's.id_supplier = po.supplier_id', 'left');
        $this->db->join('accounting_2016.request_payments rfp', 'rfp.id_request_payment = prf.request_payment_id', 'left');
        $this->db->join('business_units bu', 'bu.id_business_unit = po.business_unit_id', 'left');
        
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('rr.enabled' => 1));
    }

}
