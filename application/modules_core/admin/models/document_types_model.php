<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Documents Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Documents records
  |
  | @category		Model
  | @author		melin
 */

class Document_Types_Model extends MY_Model {
    /* int primary key   */

    public $id_document_type;

    /* int class  */
    public $class_id;

    /* int class  */
    public $general_group_id;

    /* varchar document_type abbreviation */
    public $document_type_code;

    /* varchar document_type name  */
    public $document_type_name;

    /* varchar  */
    public $multiplier;

    /* tiny int enabled */
    public $enabled;

    /* int added by */
    public $added_by;

    /* date date added */
    public $added_date;

    /* int updated by */
    public $updated_by;

    /* date date updated */
    public $updated_date;

    /* string table name */
    protected $table = 'document_types';

    /* string table identifier */
    protected $identifier = 'id_document_type';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_document_type'] = (int) $this->misc->decode_id($this->id);
        $fields['class_id'] = $this->class_id;
        $fields['general_group_id'] = $this->general_group_id;
        $fields['document_type_code'] = $this->document_type_code;
        $fields['document_type_name'] = $this->document_type_name;
        $fields['multiplier'] = $this->multiplier;
        $fields['added_date'] = $this->added_date;
        $fields['added_by'] = $this->added_by;
        $fields['updated_by'] = $this->updated_by;
        $fields['updated_date'] = $this->updated_date;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('dty.id_document_type' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('dty.id_document_type' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("dty.id_document_type");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("dty.id_document_type");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("document_types dty", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("document_types dty");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			dty.*,
                        dc.class_title,class_name,
                        gg.group_name,gg.group_code,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('default_classes dc', 'dc.id_class = dty.class_id', 'left');
        $this->db->join('general_group gg', 'gg.id_general_group = dty.general_group_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('dty.enabled' => 1));
    }

}
