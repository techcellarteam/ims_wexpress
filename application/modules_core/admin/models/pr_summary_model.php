<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Purchase Requisitions Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Purchase Requisitions records
  |
  | @category		Model
  | @author		melin
 */

class PR_Summary_Model extends MY_Model {
    /* string table name */

    protected $table = 'purchase_requisitions';

    /* string table identifier */
    protected $identifier = 'id_purchase_requisition';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('pr.id_purchase_requisition' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('pr.id_purchase_requisition' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array('pr_number' => 'ASC')) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("pr.id_purchase_requisition");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("pr.id_purchase_requisition");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("purchase_requisitions pr", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("purchase_requisitions pr");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			pr.*,DATEDIFF(pr.top_level_approval_date,pr.added_date ) as added_approved,
                        DATEDIFF(prf.invoice_received_date,rr.date_received ) as received_invoiced,
                        CONCAT(pl.plate_number,SPACE(1),pl.make,SPACE(1), pl.model ) as plate_number,
                        CONCAT(du.user_fname,SPACE(1), du.user_lname ) as user_name,du.business_unit_id,
                        bu.business_unit_name, bu.business_unit_code,
                        po.id_purchase_order,po.po_number,po.total_amount as po_amount,po.added_date as po_added_date,po.status as po_status,
                        po.second_approval_date as po_second_approval_date, po.third_approval_date as po_third_approval_date,
                        po.second_level_id as po_second_level_id, po.third_level_id as po_third_level_id,
                        CONCAT(s.supplier_code,SPACE(1), s.supplier_name ) as supplier,
                        rr.invoice_number,rr.date_received,
                        prf.prf_number,prf.total_amount as prf_total_ammount,prf.added_date as prf_added_date,prf.invoice_received_date,
                        rfp.request_payment_payee,rfp.paid_amount,
                        CONCAT(rfp_app_by.user_fname,SPACE(1), rfp_app_by.user_lname ) as rfp_approved_by,rfp.approved_date as rfp_approval_date,
                        gg.group_code,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('plate_numbers pl', 'pl.id_plate_number = pr.plate_number_id', 'left');
        $this->db->join('default_users du', 'du.id_user = pr.added_by', 'left');
        $this->db->join('business_units bu', 'bu.id_business_unit = du.business_unit_id', 'left');
        $this->db->join('purchase_orders po', 'po.purchase_requisition_id = pr.id_purchase_requisition', 'left');
        $this->db->join('suppliers s', 's.id_supplier = po.supplier_id', 'left');
        $this->db->join('receiving_reports rr', 'rr.purchase_order_id = po.id_purchase_order', 'left');
        $this->db->join('payment_requests prf', 'prf.receiving_report_id = rr.id_receiving_report', 'left');
        $this->db->join('accounting_2016.request_payments rfp', 'rfp.id_request_payment = prf.request_payment_id', 'left');
        $this->db->join('accounting.users rfp_app_by', 'rfp_app_by.id_user = rfp.approved_by', 'left');
        $this->db->join('general_group gg', 'gg.id_general_group = bu.general_group_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('pr.enabled' => 1));
    }

}
