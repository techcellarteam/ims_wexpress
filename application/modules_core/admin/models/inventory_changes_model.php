<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Inventory Change Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Inventory Change records
  |
  | @category		Model
  | @author		melin
 */

class Inventory_Changes_Model extends MY_Model {
    /* int primary key   */
    public $id_inventory_change;

    /* int(11) item location ID  */
    public $item_location_id;

    /* int(11) administrator ID  */
    public $administrator_id;

    /* int(11) Stock Before */
    public $stock_before;

    /* int(11) Stock Quantity  */
    public $stock_quantity;

    /* int(11) Stock After */
    public $stock_after;

    /* date inventory date  */
    public $inventory_date;

    /* varchar(32) transaction */
    public $transaction;
    
    /* text remarks */
    public $remarks;

    /* int added by */
    public $added_by;

    /* date date added */
    public $added_date;

    /* string table name */
    protected $table = 'inventory_changes';
    /* string alias */
    protected $alias = 'ic';
    

    /* string table identifier */
    protected $identifier = 'id_inventory_change';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_inventory_change'] = (int) $this->misc->decode_id($this->id);
        $fields['item_location_id'] = $this->item_location_id;
        $fields['administrator_id'] = $this->administrator_id;
        $fields['stock_before'] = $this->stock_before;
        $fields['stock_quantity'] = $this->stock_quantity;
        $fields['stock_after'] = $this->stock_after;
        $fields['inventory_date'] = $this->inventory_date;
        $fields['transaction'] = $this->transaction;
        $fields['remarks'] = $this->remarks;
        $fields['added_by'] = $this->added_by;
        $fields['added_date'] = $this->added_date;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('ic.id_inventory_change' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('ic.id_inventory_change' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("ic.id_inventory_change");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("ic.id_inventory_change");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("inventory_changes ic", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("inventory_changes ic");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			ic.*,
                        c.sku,c.description,
                        um.measurement_name,um.measurement_code,
                        cg.id_stock_classification,cg.stock_classification_name,
                        CONCAT(du.user_fname,SPACE(1), du.user_lname ) as user_name,
                        CONCAT(c.sku,SPACE(1), c.description ) as commodity,
                        bu.business_unit_code,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('item_location il', 'il.id_item_location = ic.item_location_id', 'left');
        $this->db->join('business_units bu', 'bu.id_business_unit = il.location_id', 'left');
        $this->db->join('items c', 'c.id_item = il.item_id', 'left');
        $this->db->join('stock_classifications cg', 'cg.id_stock_classification = c.stock_classification_id', 'left');
        $this->db->join('unit_measurements um', 'um.id_unit_measurement = c.unit_measurement_id', 'left');
        $this->db->join('default_users du', 'du.id_user = ic.added_by', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
//        $this->db->where(array('ic.enabled' => 1));
    }

}
