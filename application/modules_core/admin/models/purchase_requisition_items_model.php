<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Purchase Requisition Items Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Purchase Requisition Items records
  |
  | @category		Model
  | @author		melin
 */

class Purchase_Requisition_Items_Model extends MY_Model {
    /* int primary key   */

    public $id_pr_item;

    /* int purchase requisition id  */
    public $purchase_requisition_id;

    /* int item id  */
    public $item_id;

    /* int quantity  */
    public $quantity;

    /* dec last ordered price  */
    public $last_ordered_price;

    /* dec initiator_price  */
    public $initiator_price;

    /* dec purchasing_price  */
    public $purchasing_price;

    /* text remarks  */
    public $remarks;

    /* tiny int(1) delete status */
    public $enabled;

    /* string table name */
    protected $table = 'purchase_requisition_items';

    /* string table identifier */
    protected $identifier = 'id_pr_item';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_pr_item'] = (int) $this->misc->decode_id($this->id);
        $fields['purchase_requisition_id'] = (int) ($this->purchase_requisition_id);
        $fields['item_id'] = (int) $this->item_id;
        $fields['quantity'] = $this->quantity;
        $fields['last_ordered_price'] = $this->last_ordered_price;
        $fields['initiator_price'] = $this->initiator_price;
        $fields['purchasing_price'] = $this->purchasing_price;
        $fields['remarks'] = $this->remarks;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('pri.id_pr_item' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('pri.id_pr_item' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getApproxAmount($where = array()) {
        $this->db->select("sum(`quantity`*`initiator_price`) as approx_amount");
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $row = $query->row();
            return (!empty($row->approx_amount)) ? $row->approx_amount : false;
        }
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = array(), $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("pri.id_pr_item");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("pri.id_pr_item");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }
    
    function getListToPO($where = array(), $where_string = array(), $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        $this->db->join('purchase_order_items poi', 'poi.pr_item_id = pri.id_pr_item and poi.enabled = 1', 'left');
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("pri.id_pr_item");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("purchase_requisition_items pri");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			pri.*,
                        c.sku,c.description,
			cg.stock_classification_name,cg.id_stock_classification,
                        um.measurement_name,um.measurement_code,
                        pr.pr_number,
                        du.user_fname,du.user_lname,du.business_unit_id,
                        bu.business_unit_name, bu.business_unit_code,bu.business_unit_head_id,bu.general_group_id,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('purchase_requisitions pr', 'pr.id_purchase_requisition = pri.purchase_requisition_id', 'left');
        $this->db->join('items c', 'c.id_item = pri.item_id', 'left');
        $this->db->join('stock_classifications cg', 'cg.id_stock_classification = c.stock_classification_id', 'left');
        $this->db->join('unit_measurements um', 'um.id_unit_measurement = c.unit_measurement_id', 'left');
        $this->db->join('default_users du', 'du.id_user = pr.added_by', 'left');
        $this->db->join('business_units bu', 'bu.id_business_unit = du.business_unit_id', 'left');
        $this->db->join('plate_numbers pl', 'pl.id_plate_number = pr.plate_number_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('pri.enabled' => 1));
    }

    // --------------------------------------------------------------------

    /*
     * Executes query, returns with the value of the field
     *
     * @access		public
     * @param		string
     * @return		mixed
     */
    function selectDistinct($field = '', $fields = '', $compare = 'AND', $where = array()) {
        $this->db->distinct($field);
        $this->db->select($fields);
        self::_join();
        self::_from();

        // WHERE
        if ($compare == 'AND')
            $this->db->Where($where);

        if ($compare == 'OR')
            $this->db->or_where($where);

        $query = $this->db->get();
        return $query;
    }

}
