<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Issuances Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Issuances Model records
  |
  | @category		Model
  | @author		melin
 */

class Issuances_Model extends MY_Model {
    /* int primary key   */

    public $id_issuance;

    /* int(11) issuance # */
    public $issuance_no;

    /* date issuance date  */
    public $issuance_date;

    /* int(11) business unit ID from  */
    public $bu_id_from;

    /* int(11) business unit ID to */
    public $bu_id_to;
    
    /* int(11) released by ID to */
    public $released_by;
    
    /* date  date released */
    public $date_released;

    /* text remarks  */
    public $remarks;

    /* varchar(16) status */
    public $status;

    /* small int(4)delete status */
    public $enabled;

    /* int added by */
    public $added_by;

    /* date date added */
    public $added_date;

    /* int updated by */
    public $updated_by;

    /* date date updated */
    public $updated_date;

    /* string table name */
    protected $table = 'issuances';
    
    /* string alias */
    protected $alias = 'iss';

    /* string table identifier */
    protected $identifier = 'id_issuance';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_issuance'] = (int) $this->misc->decode_id($this->id);
        $fields['issuance_no'] = $this->issuance_no;
        $fields['issuance_date'] = $this->issuance_date;
        $fields['bu_id_from'] = $this->bu_id_from;
        $fields['bu_id_to'] = $this->bu_id_to;
        $fields['remarks'] = $this->remarks;
        $fields['status'] = $this->status;
        $fields['added_date'] = $this->added_date;
        $fields['added_by'] = $this->added_by;
        $fields['updated_by'] = $this->updated_by;
        $fields['updated_date'] = $this->updated_date;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('iss.id_issuance' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('iss.id_issuance' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("iss.id_issuance");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("iss.id_issuance");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("issuances iss", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("issuances iss");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			iss.*,
                        CONCAT(bfr.business_unit_code,SPACE(1), bfr.business_unit_name ) as bu_name_from,
                        CONCAT(bto.business_unit_code,SPACE(1), bto.business_unit_name ) as bu_name_to,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('business_units bfr', 'bfr.id_business_unit = iss.bu_id_from', 'left');
        $this->db->join('business_units bto', 'bto.id_business_unit = iss.bu_id_to', 'left');
        $this->db->join('default_users du', 'du.id_user = iss.added_by', 'left');
        $this->db->join('business_units bu', 'bu.id_business_unit = du.business_unit_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('iss.enabled' => 1));
    }

}
