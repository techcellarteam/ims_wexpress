<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | KPI Served PO Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the KPI Served PO records
  |
  | @category		Model
  | @author		melin
 */

class KPI_Served_PO_Model extends MY_Model {

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('po.id_purchase_order' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('po.id_purchase_order' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array('id_purchase_order' => 'ASC'), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = array(), $order_by = array('id_purchase_order' => 'ASC')) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("po.id_purchase_order");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("po.id_purchase_order");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("purchase_orders po", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("purchase_orders po");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			po.*,
                        po.added_date as prepared_date,
                        dty.document_type_name,
                        gg.group_code,
                        pr.pr_number,
                        rr.invoice_number,rr.date_received,
                        prf.date_forwarded_acctg,
                        (CASE WHEN (`po`.`third_level_id` != 0) 
                                THEN `po`.`third_approval_date`
                            WHEN (`po`.`second_level_id` != 0) 
                                THEN `po`.`second_approval_date` 
                                ELSE `po`.`approval_date` 
                        END) as top_level_approval_date,
                        (CASE WHEN (`po`.`third_level_id` != 0) 
                                THEN DATEDIFF( rr.date_received,po.third_approval_date )
                            WHEN (`po`.`second_level_id` != 0) 
                                THEN DATEDIFF( rr.date_received,po.second_approval_date )
                                ELSE DATEDIFF( rr.date_received,po.approval_date )
                        END) as approved_served,
                        (CASE WHEN (`po`.`third_level_id` != 0) 
                                THEN DATEDIFF( prf.date_forwarded_acctg,rr.date_received )
                            WHEN (`po`.`second_level_id` != 0) 
                                THEN DATEDIFF( prf.date_forwarded_acctg,rr.date_received )
                                ELSE DATEDIFF( prf.date_forwarded_acctg,rr.date_received )
                        END) as served_forwarded,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('default_users du', 'du.id_user = po.added_by', 'left');
        $this->db->join('business_units bu', 'bu.id_business_unit = du.business_unit_id', 'left');
        $this->db->join('general_group gg', 'gg.id_general_group = bu.general_group_id', 'left');
        $this->db->join('purchase_requisitions pr', 'pr.id_purchase_requisition = po.purchase_requisition_id', 'left');
        $this->db->join('(Select max(id_receiving_report) as id_receiving_report,reciving_rep.purchase_order_id,reciving_rep.invoice_number,reciving_rep.date_received '
                . 'FROM receiving_reports reciving_rep '
                . 'LEFT JOIN purchase_orders po ON reciving_rep.purchase_order_id = po.id_purchase_order '
                . 'WHERE reciving_rep.purchase_order_id = po.id_purchase_order '
                . 'GROUP BY purchase_order_id) rr', 'rr.purchase_order_id = po.id_purchase_order', 'left');
        $this->db->join('payment_requests prf', 'prf.receiving_report_id = rr.id_receiving_report', 'left');
        $this->db->join('document_types dty', 'dty.id_document_type = pr.category_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('po.enabled' => 1));
    }

}
