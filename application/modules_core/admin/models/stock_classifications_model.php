<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Stock Classifications Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Stock Classifications records
  |
  | @category		Model
  | @author		melin
 */

class Stock_Classifications_Model extends MY_Model {
    /* int primary key   */

    public $id_stock_classification;

    /* varchar(128) commodity group name  */
    public $stock_classification_name;

    /* tiny int(1) delete status */
    public $enabled;

    /* int added by */
    public $added_by;

    /* int updated by */
    public $updated_by;

    /* date date added */
    public $added_date;

    /* date date updated */
    public $updated_date;

    /* string table name */
    protected $table = 'stock_classifications';
    
    /* string alias */
    protected $alias = '';

    /* string table identifier */
    protected $identifier = 'id_stock_classification';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_stock_classification'] = (int) $this->misc->decode_id($this->id);;
        $fields['stock_classification_name'] = $this->stock_classification_name;
        $fields['added_date'] = $this->added_date;
        $fields['added_by'] = $this->added_by;
        $fields['updated_date'] = $this->updated_date;
        $fields['updated_by'] = $this->updated_by;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('sc.id_stock_classification' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('sc.id_stock_classification' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where=array(), $where_string='', $order_by=array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("sc.id_stock_classification");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("sc.id_stock_classification");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }
    
    /*
	 * Update Query
	 * @return id
	 */
	function update_table($data,$table_col,$key){
		$this->db->where($table_col,$key);
		$this->db->update("stock_classifications sc",$data);
        return $key;
	}

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("stock_classifications sc");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			sc.*,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
//        $this->db->join('default_user_types ut', 'ut.id_user_type = sc.user_type_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('sc.enabled' => 1));
    }

}
