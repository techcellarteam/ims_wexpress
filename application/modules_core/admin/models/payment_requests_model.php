<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Plate Numbers Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Plate Numbers records
  |
  | @category		Model
  | @author		melin
 */

class Payment_Requests_Model extends MY_Model {
    /* int primary key   */

    public $id_payment_request;

    /* 	int(11) request payment ID from accounting */
    public $request_payment_id;

    /* 	int(11) receiving report id */
    public $receiving_report_id;

    /* varchar(16) plate number */
    public $prf_number;

    /* decimal(13,2) total amount */
    public $total_amount;

    /* smallint (4) emergency status (1/0) */
    public $emergency;

    /* text payment request remarks */
    public $remarks;
    
     /* date invoice received date */
    public $invoice_received_date;
    
     /* text invoice received by */
    public $invoice_received_by;

    /* tiny int(1) delete status */
    public $enabled;

    /* int added by */
    public $added_by;

    /* int updated by */
    public $updated_by;

    /* date date added */
    public $added_date;

    /* date date updated */
    public $updated_date;

    /* string table name */
    protected $table = 'payment_requests';
    
    /* string alias */
    protected $alias = 'prf';
    
    /* string table identifier */
    protected $identifier = 'id_payment_request';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_payment_request'] = (int) $this->misc->decode_id($this->id);;
        $fields['request_payment_id'] = $this->request_payment_id;
        $fields['receiving_report_id'] = $this->receiving_report_id;
        $fields['prf_number'] = $this->prf_number;
        $fields['total_amount'] = $this->total_amount;
        $fields['emergency'] = $this->emergency;
        $fields['remarks'] = $this->remarks;
        $fields['invoice_received_date'] = $this->invoice_received_date;
        $fields['invoice_received_by'] = $this->invoice_received_by;
        $fields['added_date'] = $this->added_date;
        $fields['added_by'] = $this->added_by;
        $fields['updated_date'] = $this->updated_date;
        $fields['updated_by'] = $this->updated_by;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('prf.id_payment_request' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('prf.id_payment_request' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("prf.id_payment_request");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("prf.id_payment_request");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("payment_requests prf", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("payment_requests prf");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			prf.*,
                        rr.date_received,rr.invoice_number,
                        po.po_number,id_purchase_order,
                        pr.pr_number,id_purchase_requisition,
                        s.supplier_code,s.supplier_name,s.c_name as supplier_contact_person,
                        bu.business_unit_code,
                        rfp.request_payment_due,rfp.paid_amount,rfp.request_payment_supplier,rfp.request_payment_payee,rfp.approved,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('receiving_reports rr', 'rr.id_receiving_report = prf.receiving_report_id', 'left');
        $this->db->join('purchase_orders po', 'po.id_purchase_order = rr.purchase_order_id', 'left');
        $this->db->join('purchase_requisitions pr', 'pr.id_purchase_requisition = po.purchase_requisition_id', 'left');
        $this->db->join('suppliers s', 's.id_supplier = po.supplier_id', 'left');
        $this->db->join('default_users du', 'du.id_user = prf.added_by', 'left');
        $this->db->join('business_units bu', 'bu.id_business_unit = po.business_unit_id', 'left');
        $this->db->join('accounting_2016.request_payments rfp', 'rfp.id_request_payment = prf.request_payment_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('prf.enabled' => 1));
    }

}
