<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Purchase Requisitions Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Purchase Requisitions records
  |
  | @category		Model
  | @author		melin
 */

class Purchase_Orders_Model extends MY_Model {
    /* int primary key   */

    public $id_purchase_order;

    /* int purchase requisition id  */
    public $purchase_requisition_id;

    /* int supplier id */
    public $supplier_id;

    /* text business_unit_id  */
    public $business_unit_id;

    /* int po number */
    public $po_number;

    /* int po order date */
    public $order_date;

    /* int supplier contact person */
    public $supplier_contact_person;

    /* int terms of payment  */
    public $terms_payment;

    /* dec(13,2) total amount  */
    public $total_amount;

    /* int approved by  */
    public $approved_by;

    /* datetime approval date  */
    public $approval_date;

    /* datetime declined date  */
    public $declined_date;

    /* int  2nd level approval  */
    public $second_level_id;

    /* int 3rd level approval  */
    public $third_level_id;

    /* datetime approval date  */
    public $second_approval_date;

    /* datetime declined date  */
    public $second_decline_date;

    /* datetime approval date  */
    public $third_approval_date;

    /* datetime declined date  */
    public $third_decline_date;

    /* int cancelled by */
    public $cancelled_by;

    /* datetime cancelled date  */
    public $cancelled_date;

    /* varchar(32) status  */
    public $status;

    /* varchar(128) remarks  */
    public $po_remarks;

    /* tiny int(1) delete status */
    public $enabled;

    /* int added by */
    public $added_by;

    /* int updated by */
    public $updated_by;

    /* date date added */
    public $added_date;

    /* date date updated */
    public $updated_date;

    /* string table name */
    protected $table = 'purchase_orders';

    /* string alias */
    protected $alias = 'po';

    /* string table identifier */
    protected $identifier = 'id_purchase_order';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_purchase_order'] = (int) $this->misc->decode_id($this->id);
        $fields['purchase_requisition_id'] = $this->purchase_requisition_id;
        $fields['supplier_id'] = $this->supplier_id;
        $fields['business_unit_id'] = $this->business_unit_id;
        $fields['po_number'] = $this->po_number;
        $fields['order_date'] = $this->order_date;
        $fields['status'] = $this->status;
        $fields['total_amount'] = str_replace(',', '', $this->total_amount);
        $fields['po_remarks'] = $this->po_remarks;
        $fields['added_by'] = $this->added_by;
        $fields['added_date'] = $this->added_date;
        $fields['updated_date'] = $this->updated_date;
        $fields['updated_by'] = $this->updated_by;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('po.id_purchase_order' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('po.id_purchase_order' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array('id_purchase_order' => 'ASC'), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array('id_purchase_order' => 'ASC')) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("po.id_purchase_order");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("po.id_purchase_order");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("purchase_orders po", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("purchase_orders po");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			po.*,
                        pr.pr_number,pr.date_needed,
                        CONCAT(pl.plate_number,SPACE(1),pl.make,SPACE(1), pl.model ) as plate_number,
                        du.user_fname,du.user_lname,
                        bu.business_unit_name, bu.business_unit_code,bu.general_group_id,bu.business_unit_head_id as bu_head_id,
                        s.supplier_name,s.supplier_code,s.terms_payment,s.vat,s.c_name as supplier_contact_person,s.b_address,
                        gg.group_code,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('purchase_requisitions pr', 'pr.id_purchase_requisition = po.purchase_requisition_id', 'left');
        $this->db->join('default_users du', 'du.id_user = po.added_by', 'left');
        $this->db->join('business_units bu', 'bu.id_business_unit = po.business_unit_id', 'left');
        $this->db->join('suppliers s', 's.id_supplier = po.supplier_id', 'left');
        $this->db->join('plate_numbers pl', 'pl.id_plate_number = pr.plate_number_id', 'left');
        $this->db->join('general_group gg', 'gg.id_general_group = bu.general_group_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('po.enabled' => 1));
    }

}
