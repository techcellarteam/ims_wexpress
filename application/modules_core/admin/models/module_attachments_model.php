<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Module Attachments Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Module Attachments records
  |
  | @category		Model
  | @author		melin
 */

class Module_Attachments_Model extends MY_Model {
    /* int primary key   */

    public $id_module_attachment;

    /* varchar class id */
    public $class_id;
    
    /* int reference  */
    public $reference_id;
    
    /* varchar(64) upload title  */
    public $upload_title;

    /* varchar (128) upload file name  */
    public $upload_file_name;
    
    /* varchar (32) upload file type  */
    public $upload_file_type;

    /* tiny int enabled */
    public $enabled;

    /* int added by */
    public $added_by;

    /* date date added */
    public $added_date;

    /* string table name */
    protected $table = 'module_attachments';

    /* string table identifier */
    protected $identifier = 'id_module_attachment';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_module_attachment'] = (int) $this->misc->decode_id($this->id);
        $fields['class_id'] = $this->class_id;
        $fields['reference_id'] = $this->reference_id;
        $fields['upload_title'] = $this->upload_title;
        $fields['upload_file_name'] = $this->upload_file_name;
        $fields['upload_file_type'] = $this->upload_file_type;
        
        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('ma.id_module_attachment' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('ma.id_module_attachment' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("ma.id_module_attachment");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("ma.id_module_attachment");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("module_attachments ma", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("module_attachments ma");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			ma.*,
                        dc.class_name,dc.class_title,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
        $this->db->join('default_classes dc', 'dc.id_class = ma.class_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
        $this->db->where(array('ma.enabled' => 1));
    }

}
