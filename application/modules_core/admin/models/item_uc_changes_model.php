<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Item Unit Cost Changes Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the Item Unit Cost Changes records
  |
  | @category		Model
  | @author		melin
 */

class Item_UC_Changes_Model extends MY_Model {
    /* int primary key   */

    public $id_item_uc_change;

    /* int supplier_id  */
    public $supplier_id;

    /* int item_id  */
    public $item_id;

    /* decimal(13,2) old item srp  */
    public $old_item_uc;

    /* decimal(13,2) new item srp  */
    public $new_item_uc;

    /* int added by */
    public $added_by;

    /* date date added */
    public $added_date;

    /* string table name */
    protected $table = 'item_uc_changes';

    /* string table identifier */
    protected $identifier = 'id_item_uc_change';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    function __construct($id = '') {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 		public
     * @return		array
     */
    public function getObjectFields() {
        if (isset($this->id))
            $fields['id_item_uc_change'] = (int) $this->misc->decode_id($this->id);
        $fields['supplier_id'] = $this->supplier_id;
        $fields['item_id'] = $this->item_id;
        $fields['old_item_uc'] = $this->old_item_uc;
        $fields['new_item_uc'] = $this->new_item_uc;
        $fields['added_date'] = $this->added_date;
        $fields['added_by'] = $this->added_by;

        return $fields;
    }

    function getFields($id) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('isc.id_item_uc_change' => $id));
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    function getValue($id, $select, $return = '') {
        $this->db->select($select);
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where(array('isc.id_item_uc_change' => $id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($return) {
                return (!empty($row->{$return})) ? $row->{$return} : false;
            }
            return (!empty($row->{$select})) ? $row->{$select} : false;
        }
        return false;
    }

    function getSearch($where = array(), $group_by = array(), $order_by = array(), $result = FALSE, $count = FALSE, $row = FALSE) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::group_by($group_by);
        parent::orderby($order_by);
        $query = $this->db->get();

        if ($result) {
            return $query->result();
        }

        if ($count) {
            return $query->num_rows();
        }

        if ($row) {
            if ($query->num_rows() > 0)
                return $query->row();
            return false;
        }

        return $query;
    }

    function getList($where = array(), $where_string = '', $order_by = array()) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("isc.id_item_uc_change");
        parent::orderby($order_by);
        return $query = $this->db->get();
    }

    function getListLimit($where, $where_string, $order_by, $page, $number) {
        self::_select();
        self::_from();
        self::_join();
        self::_fix_arg();
        parent::where($where);
        parent::where_string($where_string);
        // parent::group_by("isc.id_item_uc_change");
        parent::orderby($order_by);
        parent::pagelimit($page, $number);
        return $query = $this->db->get();
    }

    /*
     * Update Query
     * @return id
     */

    function update_table($data, $table_col, $key) {
        $this->db->where($table_col, $key);
        $this->db->update("item_uc_changes isc", $data);
        return $key;
    }

    /*
     * From
     * @return void
     */

    private function _from() {
        $this->db->from("item_uc_changes isc");
    }

    /*
     * SELECT
     * @return void
     */

    private function _select() {
        $this->db->select("
			isc.*,
		");
    }

    /*
     * JOIN
     * @return void
     */

    private function _join() {
//        $this->db->join('default_user_types ut', 'ut.id_user_type = isc.user_type_id', 'left');
    }

    /*
     * Fix Argument
     * @return void
     */

    private function _fix_arg() {
//        $this->db->where(array('isc.enabled' => 1));
    }

}
