<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Purchase Requisitions Class
  |--------------------------------------------------------------------------
  |
  | Handles the Purchase Requisitions panel
  |
  | @category		Controller
  | @author		melin
 */

class Purchase_Requisitions extends Admin_Core {

    public $purchase_requisitions_model;
    public $purchase_requisition_items_model;
    public $list_content = array();
    public $contents = array();
    public $users_model;
    public $user_bu_model;

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        //load models
        $this->load->model('admin/purchase_requisitions_model');
        $this->load->model('admin/purchase_requisition_items_model');
//        $this->load->model('admin/purchase_orders_model');
//        $this->load->model('admin/purchase_order_items_model');
        $this->load->model('admin/document_types_model');
        $this->load->model('admin/business_units_model');
        $this->load->model('admin/items_model');
        $this->load->model('admin/notifications_model');
        $this->load->model('admin/classes_model');
        $this->load->model('admin/module_attachments_model');
        $this->load->model('admin/user_types_model');
        $this->load->model('admin/users_model');
        $this->load->model('admin/suppliers_model');
        $this->load->model('admin/stock_classifications_model');
        $this->load->model('admin/general_group_model');
        $this->load->model('admin/user_business_units_model');

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));
        //class ID
        $this->class_id = $this->classes_model->getFieldValue('id_class', array('class_name' => $this->classname));

        //initialize PR code
        $this->pr_code = ($this->session->userdata['admin']['group_code'] == 'TFleet') ? 'TFL' : $this->session->userdata['admin']['group_code'];

        //initiate models
        $this->purchase_requisitions_model = new Purchase_Requisitions_Model();
//        $this->purchase_orders_model = new Purchase_Orders_Model();
        $this->document_types_model = new Document_Types_Model();
        $this->purchase_requisition_items_model = new Purchase_Requisition_Items_Model();
        $this->stock_classifications_model = new Stock_Classifications_Model();
        $this->general_group_model = new General_Group_Model();
        $this->users_model = new Users_Model();
        $this->user_bu_model = new User_Business_Units_Model();
        $this->business_units_model = new Business_Units_Model();


        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_purchase_requisition',
            ),
            'pr_number' => array(
                'label' => 'PR Number',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'pr_number',
            ),
            'business_unit_name' => array(
                'label' => 'Business Unit',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'business_unit_name',
            ),
            'user_name' => array(
                'label' => 'Initiator',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'user_name',
            ),
            'added_date' => array(
                'label' => 'Date Created',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'added_date',
            ),
            'aging' => array(
                'label' => 'Aging',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'aging',
            ),
            'aging_pra' => array(
                'label' => 'Aging 1',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'aging_pra',
            ),
            'total_amount' => array(
                'label' => 'Total Amount',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'total_amount',
                'is_searchable' => 0
            ),
            'status' => array(
                'label' => 'Status',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'status',
            ),
        );

        $this->contents = array('model_directory' => 'admin/purchase_requisitions_model',
            'model_name' => 'purchase_requisitions_model',
            'filters' => array(),
            'functionName' => 'Purchase Requisition'); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('purchase_requisitions/list_purchase_requisition'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_purchase_requisition() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'purchase-requisition-tools.js'))),
        );
        $this->load->view(admin_dir('purchase_requisitions/list_purchase_requisition'), $data);
    }

    function add_purchase_requisition() {
        $this->load->model('admin/plate_numbers_model');
        $this->plate_numbers_model = new Plate_Numbers_Model();
        $plate_numbers = $this->plate_numbers_model->getList()->result();
        $where_string = array("`document_type_name` LIKE '%Regular%'", "`group_code` IN (" . $this->session->userdata['admin']['user_bu_codes'] . ")");
        $this->document_types_model->getList(array('class_name' => $this->classname), $where_string)->result();

        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'purchase-requisition-tools.js'))),
            'stock_classifications' => $this->stock_classifications_model->getList()->result(),
            'plate_numbers' => $plate_numbers,
            'categories' => $this->document_types_model->getList(array('class_name' => $this->classname), $where_string)->result(),
            'user_bu' => $this->user_bu_model->getList(array('user_id' => $this->session->userdata['admin']['user_id']))->result(),
        );
        $this->load->view(admin_dir('purchase_requisitions/add_purchase_requisition'), $data);
    }

    function edit_purchase_requisition() {
        $id_purchase_requisition = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Purchase Requistion if Exist */
        $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
        if (stristr($row->status, 'decline') === false) {
            if (($row->second_level_id == 0) || (int) $this->session->userdata['admin']['user_type_id'] == 1) {

                $plate_numbers = '';
                if ($row->group_code == 'TFleet') {
                    $this->load->model('admin/plate_numbers_model');
                    $this->plate_numbers_model = new Plate_Numbers_Model();
                    $plate_numbers = $this->plate_numbers_model->getList()->result();
                }
                if ($row) {
                    $data = array(
                        'template' => parent::main_template(),
                        'js_files' => array('js_files' => array(js_dir('modules', 'purchase-requisition-tools.js'))),
                        'row' => $row,
                        'stock_classifications' => $this->stock_classifications_model->getList()->result(),
                        'pr_items' => $this->purchase_requisition_items_model->getList(array('purchase_requisition_id' => $id_purchase_requisition))->result(),
                        'plate_numbers' => $plate_numbers,
                        'categories' => $this->document_types_model->getList(array('class_name' => $this->classname, 'general_group_id' => 0))->result()
                    );
                    $this->load->view(admin_dir('purchase_requisitions/edit_purchase_requisition'), $data);
                } else {
                    redirect(admin_dir('profile/view_pagenotfound_page'));
                }
            } else {
                $arr = array(
                    'message' => $this->Misc->message_status("error", "PR not yet approved by Purchasing Head can only be edited"),
                );
                $this->load->view(admin_dir('template/deny'), $arr);
            }
        } else {
            $arr = array(
                'message' => $this->Misc->message_status("error", "PR was DECLINED."),
            );
            $this->load->view(admin_dir('template/deny'), $arr);
        }
    }

    function view_purchase_requisition() {
        $this->module_attachments = new Module_Attachments_Model();
        $id_purchase_requisition = $this->Misc->decode_id($this->uri->rsegment(3));
        $users = new Users_Model();
        /* Check Purchase Requistion if Exist */
        $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
        //check if PR Items are PO'ed
        $row->poed = $this->purchase_requisition_items_model->getList(array('purchase_requisition_id' => $id_purchase_requisition, 'poed' => 1))->num_rows();
        $pr_items = $this->purchase_requisition_items_model->getList(array('purchase_requisition_id' => $id_purchase_requisition))->result();
        $module_attachments = $this->module_attachments->getList(array('dc.class_name' => $this->classname, 'reference_id' => $id_purchase_requisition))->result();
        $user_logs_model = new User_Logs_Model();
        $logs = $user_logs_model->getSearch(array('class_name' => $this->classname, 'user_log_value' => $id_purchase_requisition), array(), array('id_user_log' => 'DESC'));

        $user_ids = $row->added_by; //for commenting
        if ($row->first_level_id) {
            $first_level = $this->users_model->getFields($row->first_level_id);
            $row->first_level = ($first_level) ? "$first_level->user_fname $first_level->user_lname [$first_level->business_unit_name]" : '---';
            $user_ids .= ($user_ids) ? ',' . $row->first_level_id : $row->first_level_id;
        }
        if ($row->branch_level_id) {
            $branch_level = $this->users_model->getFields($row->branch_level_id);
            $row->branch_level = ($branch_level) ? "$branch_level->user_fname $branch_level->user_lname [$branch_level->business_unit_name]" : '---';
            $user_ids .= ($user_ids) ? ',' . $row->branch_level_id : $row->branch_level_id;
        }
        if ($row->gtwy_level_id) {
            $gateway_level = $this->users_model->getFields($row->gtwy_level_id);
            $row->gateway_level = ($gateway_level) ? "$gateway_level->user_fname $gateway_level->user_lname [$gateway_level->business_unit_name]" : '---';
            $user_ids .= ($user_ids) ? ',' . $row->gtwy_level_id : $row->gtwy_level_id;
        }
        if ($row->svp_level_id) {
            $svp_level = $this->users_model->getFields($row->svp_level_id);
            $row->svp_level = ($svp_level) ? "$svp_level->user_fname $svp_level->user_lname [$svp_level->business_unit_name]" : '---';
            $user_ids .= ($user_ids) ? ',' . $row->svp_level_id : $row->svp_level_id;
        }
        if ($row->second_level_id) {
            $second_level = $this->users_model->getFields($row->second_level_id);
            $row->second_level = ($second_level) ? "$second_level->user_fname $second_level->user_lname [$second_level->business_unit_name]" : '---';
            $user_ids .= ($user_ids) ? ',' . $row->second_level_id : $row->second_level_id;
        }
        if ($row->third_level_id) {
            $third_level = $this->users_model->getFields($row->third_level_id);
            $row->third_level = ($third_level) ? "$third_level->user_fname $third_level->user_lname [$third_level->business_unit_name]" : '---';
            $user_ids .= ($user_ids) ? ',' . $row->third_level_id : $row->third_level_id;
        }
        if ($row->fourth_level_id) {
            $fourth_level = $this->users_model->getFields($row->fourth_level_id);
            $row->fourth_level = ($fourth_level) ? "$fourth_level->user_fname $fourth_level->user_lname [$fourth_level->business_unit_name]" : '---';
            $user_ids .= ($user_ids) ? ',' . $row->fourth_level_id : $row->fourth_level_id;
        }

        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'purchase-requisition-view-tools.js'))),
                'row' => $row,
                'pr_items' => $pr_items,
                'module_attachments' => $module_attachments,
                'logs' => $logs,
                'pr_users' => $users->getList(array(), array("`id_user` IN ($user_ids)"))->result(),
            );
            $this->load->view(admin_dir('purchase_requisitions/view_purchase_requisition'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function print_purchase_requisition() {
        $id_purchase_requisition = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Purchase Requistion if Exist */
        $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
        $pr_items = $this->purchase_requisition_items_model->getList(array('purchase_requisition_id' => $id_purchase_requisition))->result();

        if ($row) {
            //save log
            parent::save_log("PR form printed.", 'purchase_requisitions', $id_purchase_requisition);
            $added_by = $this->users_model->getFields($row->added_by);
            $row->added_by = "$added_by->user_fname $added_by->user_lname [$added_by->user_type_name]";
            $row->first_level = '';
            $row->second_level = '';
            $row->third_level = '';
            $row->fourth_level = '';
            if ($row->first_level_id) {
                $first_level = $this->users_model->getFields($row->first_level_id);
                $row->first_level = "$first_level->user_fname $first_level->user_lname [$first_level->business_unit_name]";
            }
            if ($row->second_level_id) {
                $second_level = $this->users_model->getFields($row->second_level_id);
                $row->second_level = "$second_level->user_fname $second_level->user_lname [$second_level->business_unit_name]";
            }

            if ($row->third_level_id) {
                $third_level = $this->users_model->getFields($row->third_level_id);
                $row->third_level = ($third_level) ? "$third_level->user_fname $third_level->user_lname [$third_level->business_unit_name]" : '---';
            }
            if ($row->fourth_level_id) {
                $fourth_level = $this->users_model->getFields($row->fourth_level_id);
                $row->fourth_level = ($fourth_level) ? "$fourth_level->user_fname $fourth_level->user_lname [$fourth_level->business_unit_name]" : '---';
            }
            $data = array(
                'row' => $row,
                'pr_items' => $pr_items,
                'approx_amount' => $this->purchase_requisition_items_model->getApproxAmount(array('purchase_requisition_id' => $id_purchase_requisition)),
            );
            $this->load->view(admin_dir('purchase_requisitions/extra/purchase_requisition_form'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_purchase_requisition') { /* Method for Account */
            self::_method_list_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'decline_purchase_requisition') {
            $this->tools->setPost('first_level_approval', '');
            $this->tools->setPost('approve', '');
            self::_method_decline_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'add_purchase_requisition') {
            self::_method_add_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'auto_approve_purchase_requisition') {
            self::_method_auto_approve_pr();
        } else if ($this->uri->rsegment(3) == 'add_submit_purchase_requisition') {
            self::_method_add_submit_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'edit_purchase_requisition') {
            self::_method_edit_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'delete_purchase_requisition') {
            self::_method_delete_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'view_purchase_order') {
            self::view_purchase_order();
        } else if ($this->uri->rsegment(3) == 'add_attachment') {
            self::_method_add_attachment();
        } else if ($this->uri->rsegment(3) == 'delete_attachment') {
            self::_method_delete_attachment();
        } else if ($this->uri->rsegment(3) == 'submit_purchase_requisition') {
            self::_method_submit_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'first_level_approval') {
            $this->tools->setPost('decline', '');
            self::_method_approve_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'second_level_approval') {
            $this->tools->setPost('decline', '');
            self::_method_approve_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'third_level_approval') {
            $this->tools->setPost('decline', '');
            self::_method_approve_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'fourth_level_approval') {
            $this->tools->setPost('decline', '');
            self::_method_approve_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'branch_level_approval') {
            $this->tools->setPost('decline', '');
            self::_method_approve_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'gtwy_level_approval') {
            $this->tools->setPost('decline', '');
            self::_method_approve_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'svp_level_approval') {
            $this->tools->setPost('decline', '');
            self::_method_approve_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'requeue_purchase_requisition') {
            $this->tools->setPost('approve', '');
            $this->tools->setPost('decline', '');
            self::_method_requeue_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'on_hold_pr') {
            self::_method_on_hold_pr();
        } else if ($this->uri->rsegment(3) == 'close_purchase_requisition') {
            self::_method_close_purchase_requisition();
        } else if ($this->uri->rsegment(3) == 'add_comment') {
            self::_method_add_comment();
        } else if ($this->uri->rsegment(3) == 'get_pr_items') {
            self::_method_get_pr_items();
        } else if ($this->uri->rsegment(3) == 'edit_pr_items') {
            self::_method_edit_pr_items();
        } else if ($this->uri->rsegment(3) == 'get_item_detail') {
            self::_method_get_item_detail();
        } else if ($this->uri->rsegment(3) == 'pr_select') {
            self::_method_pr_select();
        } else if ($this->uri->rsegment(3) == 'stock_classification_select') {
            self::_method_stock_classification_select();
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Validation of the Form -------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    private function _validate() {
        $this->form_validation->set_rules('business_unit_id', 'Business Unit', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('date_needed', 'Date Needed', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('pr_remarks', 'Remarks', 'htmlspecialchars|trim|required');
        if ($this->uri->rsegment(3) == 'add_purchase_requisition' || $this->uri->rsegment(3) == 'auto_approve_purchase_requisition' || $this->uri->rsegment(3) == 'add_submit_purchase_requisition') {
            $this->form_validation->set_rules('items', 'Items', 'required');
        }
        if ($this->tools->getPost('items')) {
            foreach ($this->tools->getPost('items') as $key => $val) {
                $this->form_validation->set_rules("quantity[$key]", 'Quantity', 'htmlspecialchars|trim|required|greater_than[0]');
                $this->form_validation->set_rules("initiators_price[$key]", 'Initiators Price', 'htmlspecialchars|trim|required');
                $this->form_validation->set_rules("remarks[$key]", 'Remarks', 'htmlspecialchars|trim');
            }
        }
        if ($this->tools->getPost('pr_items')) {
            foreach ($this->tools->getPost('pr_items') as $key => $val) {
                $this->form_validation->set_rules("pr_quantity[$key]", 'Quantity', 'htmlspecialchars|trim|required|greater_than[0]');
                $this->form_validation->set_rules("initiators_price[$key]", 'Initiators Price', 'htmlspecialchars|trim|decimal|required');
            }
        }
        if ($this->business_units_model->getValue($this->tools->getPost('business_unit_id'), 'group_code') == 'TFleet') {
            $this->form_validation->set_rules("srf_number", 'SRF Number', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules("plate_number_id", 'Plate Number', 'htmlspecialchars|trim|required');
        }
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_purchase_requisition() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }
        //set condition for the list
        $condition_string = array();
        $condition = array();

        //show purchase requisitions requested by the business_unit only if user is not PEM, FM, CEO,OM or Super Admin
        if ((int) $this->session->userdata['admin']['user_type_id'] != 1) {
            $user_type_code = $this->session->userdata['admin']['user_type_code'];
            switch ($user_type_code) {
                case 'PU Head':// can see approved by BU Head
                    $condition = array('pr.first_level_id !=' => 0);
                    break;
                case 'Purchaser':// can see approved by BU Head
                    $condition_string = array_merge($condition_string, array("((`pr`.`first_level_id` = 0 AND `pr`.`added_by` = " . $this->session->userdata['admin']['user_id'] . ") OR (`pr`.`first_level_id` != 0))"));
                    break;
                case 'BR Head (OPS)': // 2nd level approved for OPS other branches
                    $condition = array('pr.status' => 'Queued to Branch Head');
                    break;
                case 'GTWY Approver': // 2nd level approved for OPS other branches
                    $condition = array('pr.status' => 'Queued to Gateway Approver');
                    break;
                case 'SVP': // 2nd level approved for OPS manila & 3rd level approved for OPS other branches
                    $condition = array('pr.status' => 'Queued to SVP');
                    break;
                case 'EVP': // fourth level approved
                    $condition_string = array_merge($condition_string, array("(`pr`.`status` = 'Queued to ELA' OR `pr`.`status` = 'On-Hold (ELA)') AND `pr`.`business_unit_id` IN (" . $this->session->userdata['admin']['business_units'] . ")"));
                    break;
                case 'Chairman': //fifth level approved
                    $condition_string = array_merge($condition_string, array("((`pr`.`status` = 'Queued to DJF' AND `pr`.`total_amount` > 50000) OR `pr`.`status` = 'APPROVED (TFleet Head)' OR `pr`.`status` = 'On-Hold (DJF)')"));
                    break;
                case 'Administrator':
                    break;
                default:
//                    $group_id = $this->users_model->getValue($this->session->userdata['admin']['user_id'], 'general_group_id');
//                    $condition = array('bu.general_group_id' => $group_id);
                    $condition_string = array_merge($condition_string, array("`pr`.`business_unit_id` IN (" . $this->session->userdata['admin']['business_units'] . ")"));
                    break;
            }
        }

        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_purchase_requisition') {
                        $condition = array_merge($condition, array($var => $val));
                    } elseif ($var == 'added_date') {
                        $condition = array_merge($condition, array('pr.' . $var . " like" => "%" . $val . "%"));
                    } elseif ($var == 'user_name') {
                        $condition_string = array_merge($condition_string, array("(`du`.`user_fname` LIKE '%" . $val . "%' OR `du`.`user_lname` LIKE '%" . $val . "%' OR concat(`du`.`user_fname`,SPACE(1),`du`.`user_lname`) LIKE '%" . $val . "%')"));
                    } elseif ($var == 'plate_number') {
                        $condition_string = array_merge($condition_string, array("(concat(`pl`.`plate_number`,SPACE(1),`pl`.`make`,SPACE(1),`pl`.`model`) LIKE '%" . $val . "%')"));
                    } elseif ($var == 'aging') {
                        $aging_where = '= ' . $val;
                        $test = (!((int) strpos($val, '>') < 0) || !((int) strpos($val, '>=') < 0) || !((int) strpos($val, '<') < 0) || !((int) strpos($val, '<=') < 0));
                        if ($test) {
                            $condition_string = array_merge($condition_string, array("(SELECT DATEDIFF(NOW(),date_submitted )) " . $aging_where));
                        }
                    } elseif ($var == 'aging_pra') {
                        $aging_where = '= ' . $val;
                        $test = (!((int) strpos($val, '>') < 0) || !((int) strpos($val, '>=') < 0) || !((int) strpos($val, '<') < 0) || !((int) strpos($val, '<=') < 0));
                        if ($test) {
                            $condition_string = array_merge($condition_string, array("(SELECT DATEDIFF(second_approval_date,date_submitted )) " . $aging_where));
                        }
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }
        //arrange from oldest to newest
        if (empty($this->tools->getPost('sort'))) {
            $this->tools->setPost('sort', array('sort_by' => 'id_purchase_requisition', 'sort_type' => 'DESC'));
        }
        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get Purchase Requistion List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);

        //for TFleet Group
        if (parent::searchArrayValues('TFleet', $data['list']->result_array())) {
            self::array_insert($this->list_content, 2, array('plate_number' => array(
                    'label' => 'Plate NO',
                    'type' => 'text',
                    'type-class' => 'col-lg-12 uniform-input',
                    'class' => 'col-lg-1',
                    'var-value' => 'plate_number',
            )));
        }

        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array('');

        $this->load->view(admin_dir('lists/list'), $data);
    }

    /*
     * 	Add New Purchase Requistion Method : Auto Approve
     */

    function _method_auto_approve_pr() {
        if (IS_AJAX) {
            self::_validate();

            //check form validation then save
            if ($this->form_validation->run() == TRUE) {
                $users = new Users_Model();
                $this->db->trans_start();
                //getPost data set to this model fields
                parent::copyFromPost($this->purchase_requisitions_model, 'id_purchase_requisition');
                //get lates added purchase requisition number
                $pr_number = $this->purchase_requisitions_model->PRgetLastNo('pr_number', 'added_date', date('Y'));
                $this->purchase_requisitions_model->pr_number = ($pr_number) ? $this->pr_code . "-PR-" . ((int) str_replace('-', '', filter_var($pr_number, FILTER_SANITIZE_NUMBER_INT)) + 1) : $this->pr_code . '-PR-' . date('y') . '0000001'; // set purchase requisition number
                $this->purchase_requisitions_model->date_needed = date('Y-m-d', strtotime($this->tools->getPost('date_needed')));
                $pr_bu = $this->business_units_model->getFields($this->tools->getPost('business_unit_id'));

                $message = "PR# $pr_number was APPROVED (BU Head).";
                if ($this->session->userdata['admin']['group_code'] == 'TFleet') {
                    $this->purchase_requisitions_model->status = "APPROVED (Tfleet Head)";
                    $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('Purchasing Head','Purchaser')"))->result();
                } else {
                    if ($this->session->userdata['admin']['group_code'] == 'OPS') {
                        if ($pr_bu->location == 'Manila - Gateway') {
                            //notify SVP for Gateway BUs manila branches as second level before Gateway/Transport Division Approver
                            $to_be_notified = $users->getList(array(), array("`user_type_code` IN ('GTWY Approver')"))->result();
                            $this->purchase_requisitions_model->status = "Queued to Gateway Approver";
                        } else if ($pr_bu->location == 'Manila') {
                            //notify SVP for manila branches as second level before purchasing
                            $to_be_notified = $users->getList(array(), array("`user_type_code` IN ('SVP')"))->result();
                            $this->purchase_requisitions_model->status = "Queued to SVP";
                        } else {
                            if ($pr_bu->location == '') {
                                //notify branch head as second level before purchasing
                                $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('BR Head')"))->result();
                                $this->purchase_requisitions_model->status = "Queued to Branch Head";
                            } else {
                                //notify PEM of approved purchase order by the business_unit head
                                $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('Purchasing Head','Purchaser')"))->result();
                                $this->purchase_requisitions_model->status = "Queued to Purchasing for Approval";
                            }
                        }
                    } else {
                        $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('Purchasing Head','Purchaser')"))->result();
                        $this->purchase_requisitions_model->status = "Queued to Purchasing for Approval";
                    }
                }
                $this->purchase_requisitions_model->date_submitted = date('Y-m-d H:i:s');
                //get document type ID
                $document_type_id = $this->document_types_model->getFieldValue('id_document_type', array('class_id' => $this->class_id), array('document_type_code' => $this->session->userdata['admin']['group_code']));
                $this->purchase_requisitions_model->document_type_id = $document_type_id;

                /* Check Purchase Requistion Successfully Save */
                $total_amount = 0;
                if ($pr_id = $this->purchase_requisitions_model->add()) {
                    $items = $this->tools->getPost('items');
                    //save purchase order items
                    foreach ($items as $key => $val) {
                        $initiators_price = str_replace(',', '', $this->tools->getPost('initiators_price', $key));
                        $pr_item_data = array('purchase_requisition_id' => $pr_id,
                            'item_id' => $val,
                            'quantity' => $this->tools->getPost('quantity', $key),
                            'last_ordered_price' => str_replace(',', '', $this->tools->getPost('last_ordered_price', $key)),
                            'initiator_price' => $initiators_price,
                            'remarks' => $this->tools->getPost('remarks', $key),
                        );
                        $purchase_requisition_item = new Purchase_Requisition_Items_Model();
                        $purchase_requisition_item->add($pr_item_data);
                        $total_amount +=($initiators_price * $this->tools->getPost('quantity', $key));
                    }
                    //save log
                    parent::save_log("SUBMITTED and APPROVED (auto) this PR", 'purchase_requisitions', $pr_id);

                    //create notifications for deparment head approver if status is request
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successfully SUBMITTED and APPROVED purchase requisition'),
                        'id' => $this->purchase_requisitions_model->id
                    );
                    if ($to_be_notified) {
                        foreach ($to_be_notified as $q) {
                            $notif_data = array(
                                'class_id' => $this->class_id,
                                'reference_id' => $pr_id,
                                'user_id' => $q->id_user,
                                'link' => 'view_purchase_requisition',
                                'todo' => 1,
                                'status' => $message,);
                            $notification_model = new Notifications_Model();
                            $notification_model->add($notif_data);
                        }
                    }
                } else {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", "Unsuccessful PR Creation"),
                        'id' => 0
                    );
                }
                $this->db->trans_complete();
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	Add New Purchase Requistion Method
     */

    function _method_add_purchase_requisition() {
        if (IS_AJAX) {
            self::_validate();

            //check form validation then save
            if ($this->form_validation->run() == TRUE) {
                $this->db->trans_start();
                //getPost data set to this model fields
                parent::copyFromPost($this->purchase_requisitions_model, 'id_purchase_requisition');
                $pr_bu = $this->business_units_model->getFields($this->tools->getPost('business_unit_id'));
                //get lates added purchase requisition number
                $pr_number = $this->purchase_requisitions_model->PRgetLastNo('pr_number', 'added_date', date('Y'));
                $this->purchase_requisitions_model->pr_number = ($pr_number) ? $this->pr_code . "-PR-" . ((int) str_replace('-', '', filter_var($pr_number, FILTER_SANITIZE_NUMBER_INT)) + 1) : $this->pr_code . '-PR-' . date('y') . '0000001'; // set purchase requisition number
                $this->purchase_requisitions_model->date_needed = date('Y-m-d', strtotime($this->tools->getPost('date_needed')));
                $this->purchase_requisitions_model->status = "Draft";
                //get document type ID
                $document_type_id = $this->document_types_model->getFieldValue('id_document_type', array('class_id' => $this->class_id), array('document_type_code' => $pr_bu->group_code));
                $this->purchase_requisitions_model->document_type_id = $document_type_id;

                /* Check Purchase Requistion Successfully Save */
                $total_amount = 0;
                if ($pr_id = $this->purchase_requisitions_model->add()) {
                    $items = $this->tools->getPost('items');
                    //save purchase order items
                    foreach ($items as $key => $val) {
                        $initiators_price = str_replace(',', '', $this->tools->getPost('initiators_price', $key));
                        $pr_item_data = array('purchase_requisition_id' => $pr_id,
                            'item_id' => $val,
                            'quantity' => $this->tools->getPost('quantity', $key),
                            'last_ordered_price' => str_replace(',', '', $this->tools->getPost('last_ordered_price', $key)),
                            'initiator_price' => $initiators_price,
                            'remarks' => $this->tools->getPost('remarks', $key)
                        );
                        $purchase_requisition_item = new Purchase_Requisition_Items_Model();
                        $purchase_requisition_item->add($pr_item_data);
                        $total_amount +=($initiators_price * $this->tools->getPost('quantity', $key));
                    }
                    $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $pr_id), array('total_amount' => $total_amount));
                    //save log
                    parent::save_log("Created as 'DRAFT' ", 'purchase_requisitions', $pr_id);

                    //create notifications for deparment head approver if status is request
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successfully saved as DRAFT'),
                        'id' => $this->purchase_requisitions_model->id
                    );
                } else {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", "Unsuccessful PR Creation"),
                        'id' => 0
                    );
                }
                $this->db->trans_complete();
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	Add New Purchase Requistion Method
     */

    function _method_add_submit_purchase_requisition() {
        if (IS_AJAX) {
            self::_validate();

            //check form validation then save
            if ($this->form_validation->run() == TRUE) {
                $this->db->trans_start();
                //getPost data set to this model fields
                parent::copyFromPost($this->purchase_requisitions_model, 'id_purchase_requisition');
                //get lates added purchase requisition number
                $pr_number = $this->purchase_requisitions_model->PRgetLastNo('pr_number', 'added_date', date('Y'));
                $this->purchase_requisitions_model->pr_number = ($pr_number) ? $this->pr_code . "-PR-" . ((int) str_replace('-', '', filter_var($pr_number, FILTER_SANITIZE_NUMBER_INT)) + 1) : $this->pr_code . '-PR-' . date('y') . '0000001'; // set purchase requisition number
                $this->purchase_requisitions_model->date_needed = date('Y-m-d', strtotime($this->tools->getPost('date_needed')));
                $this->purchase_requisitions_model->status = "Submitted";
                $this->purchase_requisitions_model->date_submitted = date('Y-m-d H:i:s');
                //get document type ID
                $document_type_id = $this->document_types_model->getFieldValue('id_document_type', array('class_id' => $this->class_id), array('document_type_code' => $this->session->userdata['admin']['group_code']));
                $this->purchase_requisitions_model->document_type_id = $document_type_id;
                //check if business unit head is the creator- auto approve

                /* Check Purchase Requistion Successfully Save */
                $total_amount = 0;
                if ($pr_id = $this->purchase_requisitions_model->add()) {
                    $items = $this->tools->getPost('items');
                    //save purchase order items
                    foreach ($items as $key => $val) {
                        $initiators_price = str_replace(',', '', $this->tools->getPost('initiators_price', $key));
                        $pr_item_data = array('purchase_requisition_id' => $pr_id,
                            'item_id' => $val,
                            'quantity' => $this->tools->getPost('quantity', $key),
                            'last_ordered_price' => str_replace(',', '', $this->tools->getPost('last_ordered_price', $key)),
                            'initiator_price' => $initiators_price,
                            'remarks' => $this->tools->getPost('remarks', $key)
                        );
                        $purchase_requisition_item = new Purchase_Requisition_Items_Model();
                        $purchase_requisition_item->add($pr_item_data);
                        $total_amount +=($initiators_price * $this->tools->getPost('quantity', $key));
                    }
                    $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $pr_id), array('total_amount' => $total_amount));
                    //save log
                    parent::save_log(" This PR# " . $this->purchase_requisitions_model->pr_number . " was submitted.", 'purchase_requisitions', $pr_id);

                    //create notifications for bu head approver if status is submitted
                    $to_be_notified = $this->users_model->getList(array('id_user' => $this->session->userdata['admin']['business_unit_head_id']))->result();
                    if (!empty($to_be_notified)) {
                        foreach ($to_be_notified as $q) {
                            $notif_data = array(
                                'class_id' => $this->class_id,
                                'reference_id' => $pr_id,
                                'user_id' => $q->id_user,
                                'link' => 'view_purchase_requisition',
                                'todo' => 1,
                                'status' => "PR # " . $this->purchase_requisitions_model->pr_number . " was submitted.");
                            $notification_model = new Notifications_Model();
                            $notification_model->add($notif_data);
                        }
                    }
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successfully SUBMITTED PURCHASE REQUISITION'),
                        'id' => $pr_id
                    );
                } else {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", "Unsuccessful PR Creation"),
                        'id' => 0
                    );
                }
                $this->db->trans_complete();
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	Edit Purchase Requistion Type Method
     */

    function _method_edit_purchase_requisition() {
        if (IS_AJAX) {
            $id_purchase_requisition = $this->Misc->decode_id($this->tools->getPost('id'));

            /* Check Purchase Requistion Type if Exist */
            $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
            $items_model = new Items_Model();
            self::_validate();
            if ($row) {
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    /* Update Purchase Requistion Info */
                    //getPost data set to this model fields
                    $pr_model = new Purchase_Requisitions_Model($id_purchase_requisition);
                    parent::copyFromPost($pr_model, 'id_purchase_requisition');
                    $pr_model->date_needed = date('Y-m-d', strtotime($this->tools->getPost('date_needed')));
                    if ($pr_model->update()) {
                        parent::save_log("Updated", 'purchase_requisitions', $id_purchase_requisition);
                        $pr_items = $this->purchase_requisition_items_model->getList(array('purchase_requisition_id' => $id_purchase_requisition))->result();
                        $edited_pr_items = $this->tools->getPost('pr_items');

                        //check previously saved pr items if not already existing upon editing transaction
                        $total_amount = 0;
                        if ($edited_pr_items) {
                            foreach ($pr_items as $q) {
                                $inititators_price = (float) str_replace(',', '', $this->tools->getPost('initiators_price', $q->id_pr_item));
                                $item_model = $items_model->getFields($q->item_id);
                                if (!array_key_exists($q->id_pr_item, $edited_pr_items)) {
                                    $this->purchase_requisition_items_model->update(array('id_pr_item' => $q->id_pr_item), array('enabled' => 0));
                                } else {
                                    $pr_item_data = array(
                                        'last_ordered_price' => (float) str_replace(',', '', $this->tools->getPost('last_ordered_price', $q->id_pr_item)),
                                        'initiator_price' => $inititators_price,
                                        'quantity' => $this->tools->getPost('pr_quantity', $q->id_pr_item),
                                        'remarks' => $this->tools->getPost('remarks', $q->id_pr_item)
                                    );
                                    $this->purchase_requisition_items_model->update(array('id_pr_item' => $q->id_pr_item), $pr_item_data);
                                    $total_amount +=($inititators_price * $this->tools->getPost('pr_quantity', $q->id_pr_item));
                                    if ($inititators_price != $q->initiator_price) {
                                        parent::save_log("Price changed: $item_model->description " . $q->initiator_price . " to $inititators_price.", 'purchase_requisitions', $id_purchase_requisition);
                                    }
                                }
                            }
                        } else {
                            $this->purchase_requisition_items_model->update(array('purchase_requisition_id' => $id_purchase_requisition), array('enabled' => 0));
                        }
                        // add additional pr items to pr transaction
                        $items = $this->tools->getPost('items');
                        if (!empty($items)) {
                            foreach ($items as $key => $item_id) {
                                $inititators_price = str_replace(',', '', $this->tools->getPost('initiators_price', $key));
                                $item = new Items_Model($item_id);
                                $pr_item_data = array('purchase_requisition_id' => $id_purchase_requisition,
                                    'item_id' => $item->id_item,
                                    'quantity' => $this->tools->getPost('pr_quantity', $key),
                                    'last_ordered_price' => str_replace(',', '', $this->tools->getPost('last_ordered_price', $key)),
                                    'initiator_price' => $inititators_price,
                                    'remarks' => $this->tools->getPost('remarks', $key)
                                );
                                $purchase_requisition_item = new Purchase_Requisition_Items_Model();
                                $purchase_requisition_item->add($pr_item_data);
                                $total_amount +=($inititators_price * $this->tools->getPost('pr_quantity', $key));
                            }
                        }
                        $status = $pr_model->status;
                        $count_poed = count($this->purchase_requisition_items_model->getList(array('purchase_requisition_id' => $id_purchase_requisition, 'poed' => 1))->result());
                        $count_pr = count($this->purchase_requisition_items_model->getList(array('purchase_requisition_id' => $id_purchase_requisition))->result());
                        $un_poed_items = $this->purchase_requisition_items_model->getListToPO(array('purchase_requisition_id' => $id_purchase_requisition), array("((poed = 0) OR (poi.quantity != pri.quantity) OR (poi.id_po_item IS NULL))"))->result();

                        if ($count_poed > 0 && $status == "Partially PO'ed" OR $status == "Completely PO'ed") {
                            if ($count_poed && ($count_poed != $count_pr)) {
                                $status = "Partially PO'ed";
                            } else {
                                $status = "Completely PO'ed";
                            }

                            if ($un_poed_items) {
                                $status = "Partially PO'ed";
                            }
                        }

                        //update total amount
                        $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $id_purchase_requisition), array('total_amount' => $total_amount,
                            'status' => $status
                        ));

                        $arr = array(
                            'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                            'id' => $id_purchase_requisition
                        );
                        $this->db->trans_complete();
                    } else {
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('error', 'Unsuccessful'),
                            'id' => 0
                        );
                    }
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Approve Purchase Requistion Method
     */

    function _method_delete_purchase_requisition() {
        if (IS_AJAX) {
            $id_purchase_requisition = $this->Misc->decode_id($this->tools->getPost('item'));
            /* Check Purchase Requistion if Exist */
            $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
            if ($row) {
                if ((strtolower($row->status) != 'draft')) {// || (strtolower($row->status) != 'approved') || (strtolower($row->status) != '1st level approved') || (strtolower($row->status) != '2nd level approved')
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('error', 'PR with status of request can only be deleted'),
                        'id' => 0
                    );
                } else {
                    $this->db->trans_start();
                    $datas = array(
                        'status' => 'DELETED',
                        'enabled' => 0,
                        'updated_by' => $this->my_session->get('admin', 'user_id'),
                        'updated_date' => date('Y-m-d H:i:s')
                    );
                    /* Delete Purchase Requistion */
                    $this->purchase_requisitions_model->update_table($datas, "id_purchase_requisition", $id_purchase_requisition);
                    parent::save_log("Deleted", 'purchase_requisitions', $id_purchase_requisition);
                    $this->db->trans_complete();
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'PR was DELETED'),
                        'id' => 1
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Approval Purchase Requistion Method
     */

    function _method_submit_purchase_requisition() {
        if (IS_AJAX) {
            $id_purchase_requisition = $this->Misc->decode_id($this->tools->getPost('id'));
            /* Check Purchase Requistion if Exist */
            $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
            if ($row) {
                $this->db->trans_start();
                //update status as submitted
                $datas = array(
                    'status' => 'Submitted',
                    'date_submitted' => date('Y-m-d H:i:s'),
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Update Purchase Requistion */
                $this->purchase_requisitions_model->update_table($datas, "id_purchase_requisition", $id_purchase_requisition);

                parent::save_log(" This PR $row->pr_number was submitted.", 'purchase_requisitions', $id_purchase_requisition);

                $to_be_notified = $this->users_model->getList(array('id_user' => $row->business_unit_head_id))->result();
                if (!empty($to_be_notified)) {
                    foreach ($to_be_notified as $q) {
                        $notif_data = array(
                            'class_id' => $this->class_id,
                            'reference_id' => $id_purchase_requisition,
                            'user_id' => $q->id_user,
                            'link' => 'view_purchase_requisition',
                            'todo' => 1,
                            'status' => "PR $row->pr_number was submitted.");
                        $notification_model = new Notifications_Model();
                        $notification_model->add($notif_data);
                    }
                }
                $this->db->trans_complete();
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Submitted Purchase Requisition'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Approval Purchase Requistion Method
     */

    function _method_approve_purchase_requisition() {
        if (IS_AJAX) {
            $id_purchase_requisition = $this->Misc->decode_id($this->tools->getPost('id'));

            $users = new Users_Model();
            /* Check Purchase Requistion if Exist */
            $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
            $total_amount = $this->tools->getPost('total_amount');
            if ($row) {
                $this->db->trans_start();
                //update notification of user as done
                $notif_model = new Notifications_Model();
                $notif_model->update(array('class_id' => $this->class_id,
                    'reference_id' => $id_purchase_requisition,
                    'user_id' => $this->session->userdata['admin']['user_id']), array('done' => 1));

                $message = '';
                $pr_data = array();
                //BU Head
                if (($this->tools->getPost('first_level_approval')) && $this->tools->getPost('first_level_approval') == 'APPROVED (BU Head)') {
                    //check if BU is OPS
                    $message = "PR# $row->pr_number was APPROVED (BU Head).";
                    if ($row->group_code == 'OPS') {
                        if ($row->bu_location == 'Manila - Gateway') {
                            //notify SVP for manila branches as second level before purchasing
                            $to_be_notified = $users->getList(array(), array("`user_type_code` IN ('GTWY Approver')"))->result();
                            //update status as Approved
                            $pr_data = array('status' => "Queued to Gateway Approver",
                                'first_level_id' => $this->session->userdata['admin']['user_id'],
                                'first_approval_date' => date('Y-m-d H:i:s'));
                        } else if ($row->bu_location == 'Manila') {
                            //notify SVP for manila branches as second level before purchasing
                            $to_be_notified = $users->getList(array(), array("`user_type_code` IN ('SVP')"))->result();
                            //update status as Approved
                            $pr_data = array('status' => "Queued to SVP",
                                'first_level_id' => $this->session->userdata['admin']['user_id'],
                                'first_approval_date' => date('Y-m-d H:i:s'));
                        } else {
                            if ($row->bu_location == '') {
                                //notify branch head as second level
                                $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('BR Head')"))->result();
                                //update status as Approved
                                $pr_data = array('status' => "Queued to Branch Head",
                                    'first_level_id' => $this->session->userdata['admin']['user_id'],
                                    'first_approval_date' => date('Y-m-d H:i:s'));
                            } else {
                                //notify PEM of approved purchase order by the business_unit head
                                $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('Purchasing Head','Purchaser')"))->result();
                                //update status as Approved
                                $pr_data = array('status' => "Queued to Purchasing for Approval",
                                    'first_level_id' => $this->session->userdata['admin']['user_id'],
                                    'first_approval_date' => date('Y-m-d H:i:s'));
                            }
                        }
                    } else {
                        //notify PEM of approved purchase order by the business_unit head
                        $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('Purchasing Head','Purchaser')"))->result();
                        //update status as Approved
                        $pr_data = array('status' => "Queued to Purchasing for Approval",
                            'first_level_id' => $this->session->userdata['admin']['user_id'],
                            'first_approval_date' => date('Y-m-d H:i:s'));
                    }
                    parent::save_log($this->tools->getPost('first_level_approval'), 'purchase_requisitions', $id_purchase_requisition);
                }
                //for TFleet Approval
                if ($this->tools->getPost('first_level_approval') && $this->tools->getPost('first_level_approval') == 'APPROVED (Tfleet Head)') {
                    $this->form_validation->set_rules("top_level_approval_date", 'Top Level Approval Date', 'htmlspecialchars|trim|required');
                    if ($this->form_validation->run() == TRUE) {
                        if ($total_amount <= 50000) {
                            //notify PEM of approved purchase order by the business_unit head
                            $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('Purchasing Head','Purchaser') OR `id_user` = $row->business_unit_head_id"))->result();
                            $message = "PR# $row->pr_number was APPROVED (Tfleet Head).";
                            //update status as Approved
                            $pr_data = array('status' => "Generate PO",
                                'first_level_id' => $this->session->userdata['admin']['user_id'],
                                'first_approval_date' => date('Y-m-d H:i:s'),
                                'fourth_level_id' => $users->getList(array('user_type_name' => 'Chairman and President', 'u.enabled' => 1))->row()->id_user, //auto approved
                                'fourth_approval_date' => date('Y-m-d H:i:s'),
                                'top_level_approval_date' => date('Y-m-d', strtotime($this->tools->getPost('top_level_approval_date')))
                            );
                        } else {
                            //notify PEM of approved purchase order by the business_unit head
                            $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('Chairman and President')"))->result();
                            $message = "PR# $row->pr_number was APPROVED (Tfleet Head).";
                            //update status as Approved
                            $pr_data = array('status' => "APPROVED (Tfleet Head)",
                                'first_level_id' => $this->session->userdata['admin']['user_id'],
                                'first_approval_date' => date('Y-m-d H:i:s'));
                        }
                        parent::save_log($this->tools->getPost('first_level_approval'), 'purchase_requisitions', $id_purchase_requisition);
                    } else {
                        $error = $this->Misc->oneline_string(validation_errors());
                        $arr = array(
                            'message_alert' => $this->Misc->message_status("error", $error),
                            'id' => 0
                        );
                    }
                }
                // Branch Head
                if ($this->tools->getPost('branch_level_approval') && $this->tools->getPost('branch_level_approval') == 'APPROVED (Branch Head)') {
                    //notify SVP for branches as third level before purchasing
                    $to_be_notified = $users->getList(array(), array("`user_type_code` IN ('SVP')"))->result();
                    //update status as Approved
                    $pr_data = array('status' => "Queued to SVP",
                        'branch_level_id' => $this->session->userdata['admin']['user_id'],
                        'branch_approval_date' => date('Y-m-d H:i:s'));

                    parent::save_log($this->tools->getPost('branch_level_approval'), 'purchase_requisitions', $id_purchase_requisition);
                }
                //Gateway Approver
                if ($this->tools->getPost('gtwy_level_approval') && $this->tools->getPost('gtwy_level_approval') == 'APPROVED (Gateway Approver)') {
                    //notify SVP for branches as third level before purchasing
                    $to_be_notified = $users->getList(array(), array("`user_type_code` IN ('SVP')"))->result();
                    //update status as Approved
                    $pr_data = array('status' => "Queued to SVP",
                        'gtwy_level_id' => $this->session->userdata['admin']['user_id'],
                        'gtwy_approval_date' => date('Y-m-d H:i:s'));

                    parent::save_log($this->tools->getPost('gtwy_level_approval'), 'purchase_requisitions', $id_purchase_requisition);
                }
                //SVP
                if ($this->tools->getPost('svp_level_approval') && $this->tools->getPost('svp_level_approval') == 'APPROVED (SVP)') {
                    //notify PEM of approved purchase order by the business_unit head
                    $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('Purchasing Head','Purchaser')"))->result();
                    $message = "PR# $row->pr_number was " . $this->tools->getPost('branch_approval');
                    //update status as Approved
                    $pr_data = array('status' => "Queued to Purchasing for Approval",
                        'svp_level_id' => $this->session->userdata['admin']['user_id'],
                        'svp_approval_date' => date('Y-m-d H:i:s'));
                    parent::save_log($this->tools->getPost('svp_level_approval'), 'purchase_requisitions', $id_purchase_requisition);
                }
                //ELA
                if (($this->tools->getPost('second_level_approval')) && $this->tools->getPost('second_level_approval') == 'Queued to ELA') {
                    $this->form_validation->set_rules("top_level_approval_date", 'Top Level Approval Date', 'htmlspecialchars|trim|required');
                    $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('Executive Vice President','Purchasing Head','Purchaser') OR `id_user` = $row->business_unit_head_id"))->result();

                    if ($this->form_validation->run() == TRUE) {
                        //check if amount is less or equal to 2k
                        if (($total_amount <= 2000) && ($row->group_code != 'TFleet')) {
                            //update status as Approved (Auto Approved by ELA)
                            $message = "PR# $row->pr_number is auto-approved by ELA, amount is less than / equal 2k -> Generate PO.";
                            //update status as Approved
                            $pr_data = array('status' => "Generate PO",
                                'second_level_id' => $this->session->userdata['admin']['user_id'],
                                'second_approval_date' => date('Y-m-d H:i:s'),
                                'top_level_approval_date' => date('Y-m-d', strtotime($this->tools->getPost('top_level_approval_date'))));
                            parent::save_log("Auto-approved by Top Management, amount is less than 2k -> Generate PO.", 'purchase_requisitions', $id_purchase_requisition);
                        } else {
                            $message = "PR# $row->pr_number was Queued to ELA.";
                            //update status as Approved
                            $pr_data = array('status' => "Queued to ELA",
                                'second_level_id' => $this->session->userdata['admin']['user_id'],
                                'second_approval_date' => date('Y-m-d H:i:s'),
                                'top_level_approval_date' => date('Y-m-d', strtotime($this->tools->getPost('top_level_approval_date'))));
                            parent::save_log($this->tools->getPost('second_level_approval'), 'purchase_requisitions', $id_purchase_requisition);
                        }
                    } else {
                        $error = $this->Misc->oneline_string(validation_errors());
                        $arr = array(
                            'message_alert' => $this->Misc->message_status("error", $error),
                            'id' => 0
                        );
                    }
                }

                if ($this->tools->getPost('third_level_approval') && $this->tools->getPost('third_level_approval') == 'Queued to DJF') {
                    //check if amount is less or equal to 50k
                    $status = "Queued to DJF";
                    if (($total_amount <= 50000) && ($row->group_code != 'TFleet')) {
                        $status = "Generate PO";
                        $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('Purchasing Head','Purchaser') OR `id_user` = $row->business_unit_head_id"))->result();
                    } else {
                        $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('Chairman and President','Purchasing Head','Purchaser') OR `id_user` = $row->business_unit_head_id"))->result();
                    }
                    $message = "PR# $row->pr_number was Queued to DJF.";
                    //update status as Approved
                    $pr_data = array('status' => $status,
                        'third_level_id' => $this->session->userdata['admin']['user_id'],
                        'third_approval_date' => date('Y-m-d H:i:s'));
                    parent::save_log($this->tools->getPost('third_level_approval'), 'purchase_requisitions', $id_purchase_requisition);
                }
                //DJF
                if ($this->tools->getPost('fourth_level_approval') && $this->tools->getPost('fourth_level_approval') == 'PR Approved (DJF)') {
                    $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('Purchasing Head','Purchaser') OR `id_user` = $row->business_unit_head_id"))->result();
                    $message = "PR# $row->pr_number was PR Approved (DJF).";
                    //update status as Approved
                    $pr_data = array('status' => "Generate PO",
                        'fourth_level_id' => $this->session->userdata['admin']['user_id'],
                        'fourth_approval_date' => date('Y-m-d H:i:s'));
                    parent::save_log($this->tools->getPost('fourth_level_approval'), 'purchase_requisitions', $id_purchase_requisition);
                }
                //update status as Approved
                if ($pr_data && $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $id_purchase_requisition), $pr_data)) {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', $message),
                        'id' => 1
                    );
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                if (!empty($to_be_notified)) {
                    foreach ($to_be_notified as $q) {
                        $notif_data = array(
                            'class_id' => $this->class_id,
                            'reference_id' => $id_purchase_requisition,
                            'user_id' => $q->id_user,
                            'link' => 'view_purchase_requisition',
                            'todo' => 1,
                            'status' => $message);
                        $notification_model = new Notifications_Model();
                        $notification_model->add($notif_data);
                    }
                }
                //notify requestor
                self::notify_creator($row->added_by, $id_purchase_requisition, $message);

                $this->db->trans_complete();
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Decline Purchase Requistion Method
     */

    function _method_decline_purchase_requisition() {
        if (IS_AJAX) {
            $id_purchase_requisition = $this->Misc->decode_id($this->tools->getPost('id'));
            $this->user_type_model = new User_Types_Model();
            /* Check Purchase Requistion if Exist */
            $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
            if ($row) {
                $this->db->trans_start();
                parent::save_log($this->tools->getPost('decline') . " this PR# $row->pr_number", 'purchase_requisitions', $id_purchase_requisition);
                if ($this->tools->getPost('decline') == 'DECLINED (BU Head)') {
                    //update status as Declined
                    $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $id_purchase_requisition), array('status' => $this->tools->getPost('decline'),
                        'first_level_id' => $this->session->userdata['admin']['user_id'],
                        'first_decline_date' => date('Y-m-d H:i:s')));
                }

                if ($this->tools->getPost('decline') == 'DECLINED (Tfleet Head)') {
                    //update status as Declined
                    $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $id_purchase_requisition), array('status' => $this->tools->getPost('decline'),
                        'first_level_id' => $this->session->userdata['admin']['user_id'],
                        'first_decline_date' => date('Y-m-d H:i:s')));
                }

                if (($this->tools->getPost('decline') == 'DECLINED (Branch Head)')) {
                    //update status as Declined
                    $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $id_purchase_requisition), array('status' => $this->tools->getPost('decline'),
                        'branch_level_id' => $this->session->userdata['admin']['user_id'],
                        'branch_decline_date' => date('Y-m-d H:i:s')));
                }

                if ($this->tools->getPost('decline') == 'DECLINED (SVP)') {
                    //update status as Declined
                    $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $id_purchase_requisition), array('status' => $this->tools->getPost('decline'),
                        'svp_level_id' => $this->session->userdata['admin']['user_id'],
                        'svp_decline_date' => date('Y-m-d H:i:s')));
                }

                if ($this->tools->getPost('decline') == 'DECLINED (Purchasing Head)') {
                    //update status as Declined
                    $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $id_purchase_requisition), array('status' => $this->tools->getPost('decline'),
                        'second_level_id' => $this->session->userdata['admin']['user_id'],
                        'second_decline_date' => date('Y-m-d H:i:s')));
                }
                if ($this->tools->getPost('decline') == 'DECLINED (Exec VP)') {
                    //update status as Declined
                    $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $id_purchase_requisition), array('status' => $this->tools->getPost('decline'),
                        'third_level_id' => $this->session->userdata['admin']['user_id'],
                        'third_decline_date' => date('Y-m-d H:i:s')));
                }
                if ($this->tools->getPost('decline') == 'DECLINED (Chairman & Pres.)') {
                    //update status as Declined
                    $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $id_purchase_requisition), array('status' => $this->tools->getPost('decline'),
                        'fourth_level_id' => $this->session->userdata['admin']['user_id'],
                        'fourth_decline_date' => date('Y-m-d H:i:s')));
                }

                //notify creator
                $status = "PR # " . $row->pr_number . " was DECLINED by " . $this->session->userdata['admin']['user_type_code'];
                self::notify_creator($row->added_by, $id_purchase_requisition, $status);

                //update notification of user as done
                $notif_model = new Notifications_Model();
                $notif_model->update(array('class_id' => $this->class_id,
                    'reference_id' => $id_purchase_requisition,
                    'user_id' => $this->session->userdata['admin']['user_id']), array('done' => 1));

                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', $this->tools->getPost('decline')),
                    'id' => 1
                );
                $this->db->trans_complete();
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	ReQueue Purchase Requistion Method
     */

    function _method_requeue_purchase_requisition() {
        if (IS_AJAX) {
            $id_purchase_requisition = $this->Misc->decode_id($this->tools->getPost('id'));

            $users = new Users_Model();
            /* Check Purchase Requistion if Exist */
            $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
            if ($row) {
                $this->db->trans_start();
                //update notification of user as done
                $notif_model = new Notifications_Model();
                $notif_model->update(array('class_id' => $this->class_id,
                    'reference_id' => $id_purchase_requisition,
                    'user_id' => $this->session->userdata['admin']['user_id']), array('done' => 1));
                if ($row->group_code == 'TFleet') { //should be queued to TFleet Head
                    $to_be_notified = $to_be_notified = $this->users_model->getList(array('id_user' => $row->business_unit_head_id))->result();
                } else {
                    $to_be_notified = $users->getList(array(), array("`user_type_name` IN ('Purchasing Head','Purchaser') OR `id_user` = $row->business_unit_head_id"))->result();
                }
                $message = "PR# $row->pr_number was " . $this->tools->getPost('reQueue') . " by " . $this->my_session->get('admin', 'user_type_name');
                //update status as Approved
                $pr_data = array('status' => $this->tools->getPost('reQueue'),
                    'updated_date' => date('Y-m-d H:i:s'));

                if ($this->purchase_requisitions_model->update(array('id_purchase_requisition' => $id_purchase_requisition), $pr_data)) {
                    parent::save_log($this->tools->getPost('reQueue') . " by " . $this->my_session->get('admin', 'user_type_name'), 'purchase_requisitions', $id_purchase_requisition);
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', $message),
                        'id' => 1
                    );

                    if (!empty($to_be_notified)) {
                        foreach ($to_be_notified as $q) {
                            $notif_data = array(
                                'class_id' => $this->class_id,
                                'reference_id' => $id_purchase_requisition,
                                'user_id' => $q->id_user,
                                'link' => 'view_purchase_requisition',
                                'todo' => 1,
                                'status' => $message);
                            $notification_model = new Notifications_Model();
                            $notification_model->add($notif_data);
                        }
                    }
                    //notify requestor
                    self::notify_creator($row->added_by, $id_purchase_requisition, $message);
                }

                $this->db->trans_complete();
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Add Module Attachments
     */

    function _method_add_attachment() {
        $id_purchase_requisition = $this->Misc->decode_id($this->tools->getPost('id'));
        $this->classes_model = new Classes_Model();
        /* Check Purchase Requistion if Exist */
        $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
        if ($row && !empty($_FILES)) {
            $this->form_validation->set_rules('upload', 'File Name', 'htmlspecialchars|trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->db->trans_start();
                // Set the upload path
                $upload_path = './upload/Purchase Requisitions/' . $row->pr_number . '/';
                // Check if upload path exists
                // Create the directory if not
                $filetype = '*';
                $upload = parent::upload_file($upload_path, '', 'file', $filetype, '5000');

                // Check if upload is sucessful
                // Display errors if it fails
                if ($upload[0]) { // TRUE
                    $module_attachment_model = new Module_Attachments_Model();
                    $data = array(
                        'class_id' => $this->class_id,
                        'reference_id' => $id_purchase_requisition,
                        'upload_title' => $this->tools->getPost("upload"),
                        'upload_file_name' => $upload['file_name'],
                        'upload_file_type' => $upload['file_type'],
                        'added_by' => $this->session->userdata['admin']['user_id'],
                        'added_date' => date('Y-m-d H:i:s'),
                    );
                    $module_attachment_model->add($data);
                    //add to logs
                    parent::save_log("Uploaded Attachment: " . $this->tools->getPost("upload"), 'purchase_requisitions', $id_purchase_requisition);
                    // success status
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successful Uploading Attachment'),
                        'id' => 1
                    );
                } else {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('error', 'Error in Uploading'),
                        'id' => 1
                    );
                }
                $this->db->trans_complete();
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
        } else {
            $arr = array(
                'message_alert' => $this->Misc->message_status('error', 'Attach file to upload.'),
                'id' => 1
            );
        }
        $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
    }

    /*
     * 	Delete Module Attachment Method
     */

    function _method_delete_attachment() {
        if (IS_AJAX) {
            $id_module_attachment = $this->Misc->decode_id($this->tools->getPost('item'));
            /* Check Purchase Requistion if Exist */
            $row = $this->module_attachments_model->getFields($id_module_attachment);
            if ($row) {
                $this->db->trans_start();
                $datas = array(
                    'enabled' => 0,
                );
                /* Delete Purchase Requistion */
                $this->module_attachments_model->update_table($datas, "id_module_attachment", $id_module_attachment);
                parent::save_log("Deleted Attachment" . sprintf('%05d', $row->upload_title), 'purchase_requisitions', $id_module_attachment);
                $this->db->trans_complete();
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Deleted'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Save Remarks as reviewed by PEM
     */

    function _method_save_remarks() {
        if (IS_AJAX) {
            $id_purchase_requisition = $this->Misc->decode_id($this->tools->getPost('id'));
            $id_module_signatory = $this->Misc->decode_id($this->tools->getPost('save_pem_remarks_module_signatory_id'));
            $this->transaction_signatories_model = new Transaction_Signatories_Model();
            $users = new Users_Model();
            $to_be_notified = array();
            /* Check Purchase Requistion if Exist */
            $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
            $reviewed_status = '';
            $reviewed_status .= ((int) $this->tools->getPost('budget_status') == 1) ? ' Within Budget. ' : 'Without Budget. ';
            $reviewed_status .= ((int) $this->tools->getPost('document_status') == 1) ? ' Complete Document. ' : 'Incomplete Document. ';

            if ($row) {
                $this->db->trans_start();
                $datas = array(
                    'budget_status' => $this->tools->getPost('budget_status'),
                    'document_status' => $this->tools->getPost('document_status'),
                    'updated_date' => date('Y-m-d H:i:s'),
                    'status' => 'Reviewed'
                );
                /* Save Remarks for this Purchase Requistion */
                $this->purchase_requisitions_model->update_table($datas, "id_purchase_requisition", $id_purchase_requisition);
                parent::save_log("Reviewed: " . $reviewed_status, 'purchase_requisitions', $id_purchase_requisition);

                $tran_data = array(
                    'module_signatory_id' => $id_module_signatory,
                    'reference_id' => $row->id_purchase_requisition,
                    'added_date' => date('Y-m-d H:i:s'),
                    'signatory_detail' => 'PEM Review'
                );
                /* Save Transaction Signatory */
                $this->transaction_signatories_model->add($tran_data);

                //update notification of user as done
                $notif_model = new Notifications_Model();
                $notif_model->update(array('class_id' => $this->class_id,
                    'reference_id' => $id_purchase_requisition,
                    'user_id' => $this->session->userdata['admin']['user_id']), array('done' => 1));

                $message = '';
                //if complete documents
                if ((int) $this->tools->getPost('document_status') == 1) { // either within or without budget OM will be the one approved first
                    $to_be_notified = $users->getList(array(), ( "user_type_code IN ('OM')"))->result();
                    $message = "Reviewed: $reviewed_status Waiting for your Approval";
                } else {
                    $to_be_notified = $users->getList(array(), ( "user_type_code IN ('PEM')"))->result();
                    $message = "Documents must be complete to proceed. Update remarks if files are complete.";
                }

                //notify business_unit head if within budget
                foreach ($to_be_notified as $q) {
                    $notif_data = array(
                        'class_id' => $this->class_id,
                        'reference_id' => $id_purchase_requisition,
                        'user_id' => $q->id_user,
                        'link' => 'view_purchase_requisition',
                        'todo' => 1,
                        'status' => $message,);
                    $notification_model = new Notifications_Model();
                    $notification_model->add($notif_data);
                }
                $this->db->trans_complete();
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Saved Remarks'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	save comments
     */

    function _method_add_comment() {
        if (IS_AJAX) {
            $id_purchase_requisition = $this->Misc->decode_id($this->tools->getPost('id'));
            /* Check Purchase Requistion if Exist */
            $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
            if ($row) {
                $this->form_validation->set_rules('comment', 'Comment', 'htmlspecialchars|trim|required');
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    // save comment as log
                    parent::save_log($this->tools->getPost('comment'), 'purchase_requisitions', $id_purchase_requisition);

                    //notify business_unit requestor
                    $status = "Comment: " . $this->tools->getPost('comment');
                    self::notify_creator($row->added_by, $id_purchase_requisition, $status);

                    //check if comCmentator is Chairman/EVP
                    if ($this->session->userdata['admin']['user_type_code'] == 'Chairman' || $this->session->userdata['admin']['user_type_code'] == 'EVP') {
                        self::comment_email_notif();
                    }
                    $this->db->trans_complete();
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Saved Remarks'),
                        'id' => 1
                    );
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Close Purchase Requistion Method
     */

    function _method_on_hold_pr() {
        if (IS_AJAX) {
            $id_purchase_requisition = $this->Misc->decode_id($this->tools->getPost('id'));
            /* Check Purchase Requistion if Exist */
            $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
            if ($row) {
                $this->db->trans_start();
                $datas = array(
                    'status' => $this->tools->getPost('on_hold'),
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete Purchase Requistion */
                $this->purchase_requisitions_model->update_table($datas, "id_purchase_requisition", $id_purchase_requisition);
                parent::save_log("PR is ON-HOLD", 'purchase_requisitions', $id_purchase_requisition);
                //notify requestor
                self::notify_creator($row->added_by, $id_purchase_requisition, "PR# $row->pr_number is ON-HOLD");

                $this->db->trans_complete();
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'PR is ON-HOLD'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Close Purchase Requistion Method
     */

    function _method_close_purchase_requisition() {
        if (IS_AJAX) {
            $id_purchase_requisition = $this->Misc->decode_id($this->tools->getPost('id'));
            /* Check Purchase Requistion if Exist */
            $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
            if ($row) {
                $this->db->trans_start();
                $datas = array(
                    'status' => $this->tools->getPost('close'),
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete Purchase Requistion */
                $this->purchase_requisitions_model->update_table($datas, "id_purchase_requisition", $id_purchase_requisition);
                parent::save_log("CLOSED this PR", 'purchase_requisitions', $id_purchase_requisition);
                //notify requestor
                self::notify_creator($row->added_by, $id_purchase_requisition, "CLOSED PR# $row->pr_number");

                $this->db->trans_complete();
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'PR was CLOSED'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Get PR ITEMS
     */

    function _method_get_pr_items() {
        if (IS_AJAX) {
            $id_purchase_requisition = $this->tools->getPost('pr_id');
            $stock_classification_id = $this->tools->getPost('stock_classification_id');
            $data = array();
            if ($id_purchase_requisition && $stock_classification_id) {
                $pr_items = $this->purchase_requisition_items_model->getListToPO(array('purchase_requisition_id' => $id_purchase_requisition,
                            'cg.id_stock_classification' => $stock_classification_id,
//                            'poed' => 0 // not yet PO'ed
                                ), array("((poed = 0) OR (poi.quantity != pri.quantity) OR (poi.id_po_item IS NULL))"))->result();
                $data = array('items' => $pr_items,
                    'label' => 'Item',
                    'id' => 'item_id',
                    'value' => 'description',
                    'value2' => 'description');
                $this->load->view(admin_dir('template/select_template'), $data);
            } else {
                $this->load->view(admin_dir('template/select_template'), $data);
            }
        }
    }

    /*
     * 	Get PR ITEMS: Edit Function
     */

    function _method_edit_pr_items() {
        if (IS_AJAX) {
            $id_purchase_requisition = $this->tools->getPost('pr_id');
            $stock_classification_id = $this->tools->getPost('stock_classification_id');
            $added_items = $this->tools->getPost('added_items');
            $where_string = array();
            if ($added_items != '') {
                $where_string = array("`pri`.`item_id` NOT IN ($added_items)");
            }
            $data = array();
            if ($id_purchase_requisition && $stock_classification_id) {
                $pr_items = $this->purchase_requisition_items_model->getList(array('purchase_requisition_id' => $id_purchase_requisition,
                            'cg.id_stock_classification' => $stock_classification_id), $where_string)->result();
                $data = array('items' => $pr_items,
                    'label' => 'Item',
                    'id' => 'item_id',
                    'value' => 'description',
                    'value2' => 'description');
                $this->load->view(admin_dir('template/select_template'), $data);
            } else {
                $this->load->view(admin_dir('template/select_template'), $data);
            }
        }
    }

    /*
     * 	Get PR ITEM Detail
     */

    function _method_get_item_detail() {
        if (IS_AJAX) {
            $id_item = $this->tools->getPost('item_id');
            $id_purchase_requisition = $this->tools->getPost('purchase_requisition_id');
            $id_pr_item = $this->purchase_requisition_items_model->getFieldValue('id_pr_item', array('item_id' => $id_item,
                'purchase_requisition_id' => $id_purchase_requisition, 'enabled' => 1));
            /* Check Purchase Order if Exist */
            $pr_item = $this->purchase_requisition_items_model->getFields($id_pr_item);
            if ($pr_item) {
                $data['item'] = $pr_item;
                $this->load->view(admin_dir('purchase_requisitions/extra/pr_item_detail'), $data);
            }
        } else {
            $arr = array(
                'message_alert' => $this->Misc->message_status('error', 'Forbidden Direct Access'),
                'id' => 1
            );
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	Get PR to Select Option
     */

    function _method_pr_select() {
        if (IS_AJAX) {
            $where = $this->tools->getPost('where');
            if ($where) {
                $data = array('items' => $this->purchase_requisitions_model->getList($where)->result(),
                    'label' => 'Purchase Requisition',
                    'id' => 'id_purchase_requisition',
                    'value' => 'pr_number'
                );
                $this->load->view(admin_dir('template/select_template'), $data);
            }
        }
    }

    /*
     * 	Get commodity group based on PR to select option
     */

    function _method_stock_classification_select() {
        if (IS_AJAX) {
            $gg_id = $this->tools->getPost('general_group_id');
            $pr_id = $this->tools->getPost('purchase_requisition_id');
            if ($gg_id && $pr_id) {
                $where = array('c.general_group_id' => $gg_id, 'pr.id_purchase_requisition' => $pr_id);
                $pr_items = $this->purchase_requisition_items_model->selectDistinct('id_stock_classification', 'id_stock_classification, stock_classification_name', 'AND', $where)->result();
                $data = array('items' => $pr_items,
                    'label' => 'Stock Classification',
                    'id' => 'id_stock_classification',
                    'value' => 'stock_classification_name');
                $this->load->view(admin_dir('template/select_template'), $data);
            }
        }
    }

    /*
     * Notify creator of PR
     */

    private function notify_creator($added_by, $id_purchase_requisition, $status) {
        //notify Initiator for declined PR
        $notif_data = array(
            'class_id' => $this->class_id,
            'reference_id' => $id_purchase_requisition,
            'user_id' => $added_by,
            'link' => 'view_purchase_requisition',
            'status' => $status);
        $notification_model = new Notifications_Model();
        $notification_model->add($notif_data);

//        self::email_notification($id_purchase_requisition);
    }

    /*
     * Email Notification if ELA and DJF had commented
     */

    private function comment_email_notif() {
        $id_purchase_requisition = $this->Misc->decode_id($this->tools->getPost('purchase_requisition_id'));
        $configs_model = new Configs_Model();
        $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
        $administrator = $this->users_model->getFields($this->session->userdata['admin']['user_id']);

        $email_content = array(
            'purchase_requisition' => $row,
            'administrator' => $administrator,
            'comment' => $this->tools->getPost('comment'), //"Comment, test mail. Kindly reply if received."
        );

        $email_templates = $this->load->view(admin_dir('email_templates/comment_notification_pr'), $email_content, TRUE);

        $subject = "$row->pr_number Comment by $administrator->user_fname";
        $from = 'alert@wexpress.com.ph';
        $fromName = 'Inventory Management System Alert';
        $cc = $configs_model->getFieldValue('config_value', array('config_name' => 'SMTP_CC'));
        $bcc = $configs_model->getFieldValue('config_value', array('config_name' => 'SMTP_BCC'));

        $to = "alert@wexpress.com.ph";
        $toName = "Inventory Management System Alert";
        parent::send_email($email_templates, $subject, $to, $toName, $from, $fromName, $cc, $bcc);

        if ($this->tools->getPost('comment_user_id')) {
            $to_user = $this->users_model->getFields($this->tools->getPost('comment_user_id'));
            if ($to_user->user_email) {
                $to = $to_user->user_email;
                $toName = "$to_user->user_fname $to_user->user_lname";
                parent::send_email($email_templates, $subject, $to, $toName, $from, $fromName, $cc, $bcc);
            }

            //save log
            $datas = array(
                'user_log_date' => date('Y-m-d H:i:s'),
                'user_log_detail' => "comment to $toName",
                'user_log_table' => 'email_notification',
                'user_log_value' => 1,
                'page_name' => $this->pagename,
                'method_name' => $this->methodname,
                'class_name' => $this->classname,
                'user_id' => 1
            );
        } else {
            //save log
            $datas = array(
                'user_log_date' => date('Y-m-d H:i:s'),
                'user_log_detail' => "comment to ALL",
                'user_log_table' => 'email_notification',
                'user_log_value' => 1,
                'page_name' => $this->pagename,
                'method_name' => $this->methodname,
                'class_name' => $this->classname,
                'user_id' => 1
            );

            $first_level = $this->users_model->getFields($row->first_level_id);
            $second_level = $this->users_model->getFields($row->second_level_id);
            $third_level = $this->users_model->getFields($row->third_level_id);
            $branch = $this->users_model->getFields($row->branch_level_id);
            $svp = $this->users_model->getFields($row->svp_level_id);
            $added_by = $this->users_model->getFields($row->added_by);
            $gtwy_level = $this->users_model->getFields($row->gtwy_level_id);

            if ($first_level->user_email) {
                $to = $first_level->user_email;
                $toName = "$first_level->user_fname $first_level->user_lname";
                parent::send_email($email_templates, $subject, $to, $toName, $from, $fromName, $cc, $bcc);
            }

            if ($second_level->user_email) {
                $to = $second_level->user_email;
                $toName = "$second_level->user_fname $second_level->user_lname";
                parent::send_email($email_templates, $subject, $to, $toName, $from, $fromName, $cc, $bcc);
            }

            if ($third_level->user_email) {
                $to = $third_level->user_email;
                $toName = "$third_level->user_fname $third_level->user_lname";
                parent::send_email($email_templates, $subject, $to, $toName, $from, $fromName, $cc, $bcc);
            }

            if ($branch->user_email) {
                $to = $branch->user_email;
                $toName = "$branch->user_fname $branch->user_lname";
                parent::send_email($email_templates, $subject, $to, $toName, $from, $fromName, $cc, $bcc);
            }

            if ($svp->user_email) {
                $to = $svp->user_email;
                $toName = "$svp->user_fname $svp->user_lname";
                parent::send_email($email_templates, $subject, $to, $toName, $from, $fromName, $cc, $bcc);
            }

            if ($added_by->user_email) {
                $to = $added_by->user_email;
                $toName = "$added_by->user_fname $added_by->user_lname";
                parent::send_email($email_templates, $subject, $to, $toName, $from, $fromName, $cc, $bcc);
            }

            if ($gtwy_level->user_email) {
                $to = $gtwy_level->user_email;
                $toName = "$gtwy_level->user_fname $gtwy_level->user_lname";
                parent::send_email($email_templates, $subject, $to, $toName, $from, $fromName, $cc, $bcc);
            }
        }

        $this->load->model('admin/user_logs_model');
        $this->user_logs_model = new User_Logs_Model();
        $this->user_logs_model->insert_table($datas);
    }

    /*
     * Email Notification
     */

    private function email_notification($id_purchase_requisition) {
        $configs_model = new Configs_Model();
        $row = $this->purchase_requisitions_model->getFields($id_purchase_requisition);
        $initiator = $this->users_model->getFields($row->added_by);
        $administrator = $this->users_model->getName($this->session->userdata['admin']['user_id']);

        $email_templates = '';
        $subject = '';

        $email_content = array(
            'purchase_requisition' => $row,
            'user' => $initiator,
            'administrator' => $administrator,
        );

        if ($this->tools->getPost('approve')) {
            $email_templates = $this->load->view(admin_dir('email_templates/approved_purchase_requisition'), $email_content, TRUE);
            $subject = $this->tools->getPost('approve') . " PR# $row->pr_number.";
        }
        if ($this->tools->getPost('decline')) {
            $email_templates = $this->load->view(admin_dir('email_templates/declined_purchase_requisition'), $email_content, TRUE);
            $subject = $this->tools->getPost('decline') . " PR# $row->pr_number.";
        }
        if ($this->tools->getPost('close')) {
            $email_templates = $this->load->view(admin_dir('email_templates/closed_purchase_requisition'), $email_content, TRUE);
            $subject = $this->tools->getPost('close') . " PR# $row->pr_number.";
        }

        $to = $initiator->user_email;
        $toName = "$initiator->user_fname $initiator->user_lname";
        $from = 'no-reply@techcellar.com';
        $fromName = 'Inventory System';
        $cc = $configs_model->getFieldValue('config_value', array('config_name' => 'SMTP_CC'));
        $bcc = $configs_model->getFieldValue('config_value', array('config_name' => 'SMTP_BCC'));
        parent::send_email($email_templates, $subject, $to, $toName, $from, $fromName, $cc, $bcc);
    }

    /*
     * Check PR
     */

    protected function check_pr_status() {
        if ($this->session->userdata['admin']['user_type_id'] == 1) {
            $this->db->trans_start();
            /* Check Purchase Requistion if Exist */
            $list = $this->purchase_requisitions_model->getList(array('status' => "Partially PO'ed"))->result();
            foreach ($list as $q) {
                $status = 'Draft';
                $count_poed = count($this->purchase_requisition_items_model->getList(array('purchase_requisition_id' => $q->id_purchase_requisition, 'poed' => 1))->result());
                $count_pr = count($this->purchase_requisition_items_model->getList(array('purchase_requisition_id' => $q->id_purchase_requisition))->result());
                $un_poed_items = $this->purchase_requisition_items_model->getListToPO(array('purchase_requisition_id' => $q->id_purchase_requisition), array("((poed = 0) OR (poi.quantity != pri.quantity) OR (poi.id_po_item IS NULL))"))->result();


                if ($count_poed && ($count_poed != $count_pr)) {
                    $status = "Partially PO'ed";
                } else {
                    $status = "Completely PO'ed";
                }

                if ($un_poed_items) {
                    $status = "Partially PO'ed";
                }

                //update total amount
                $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $q->id_purchase_requisition), array('status' => $status));
            }
            $this->db->trans_complete();
            echo 'Success';
        }
    }

    /*
     * Check PR
     */

    protected function check_calculation() {
        if ($this->session->userdata['admin']['user_type_id'] == 1) {
            $this->db->trans_start();
            /* Check Purchase Requistion if Exist */
            $list = $this->purchase_requisitions_model->getList(array('pr.enabled' => 1))->result();
            foreach ($list as $q) {
                $total_amount = $this->purchase_requisition_items_model->getSum('initiator_price * `quantity`', 'total', array('purchase_requisition_id' => $q->id_purchase_requisition, 'enabled' => 1));

                if ($total_amount != $q->total_amount) {
                    //update total amount
                    $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $q->id_purchase_requisition), array('total_amount' => $total_amount));
                }
            }
            $this->db->trans_complete();
            echo 'Success';
        }
    }

}
