<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu extends Admin_Core {

    public $user_types_model = '';
    public $links_model = '';

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        $this->load->model('admin/user_types_model');
        $this->load->model('admin/links_model');

        $this->user_types_model = new user_types_model();
        $this->links_model = new links_model();

        $this->contents = array('model_directory' => 'admin/links_model',
            'model_name' => 'links_model',
            'filters' => array(),
            'functionName' => 'Menu Page'); // use to call functions for access
    }

    function index() {
        redirect('admin/menu/list_typemenu_page');
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_typemenu_page() {
        $data = array(
            'template' => parent::main_template()
        );
        $this->load->view(admin_dir('menu/list_typemenu_page'), $data);
    }

    function view_menu_page() {
        $id_user_type = $this->Misc->decode_id($this->uri->rsegment(3));
        $row = $this->user_types_model->getFields($id_user_type);
        if ($row) {
            $data = array(
                'row' => $row,
                'template' => parent::main_template(),
                'sidemenu' => parent::_query_menuhierarchy($this->links_model->getSearch(array('ut.id_user_type' => $id_user_type, 'l.link_location' => 2), "", array('l.link_order' => 'ASC'))),
                'topmenu' => $this->links_model->getSearch(array('ut.id_user_type' => $id_user_type, 'l.link_location' => 1, 'l.parent_link_id' => 0), "", array('l.link_order' => 'ASC'), true)
            );
            $this->load->view(admin_dir('menu/view_menu_page'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_usertype') {
            self::_method_list_usertype();
        } else if ($this->uri->rsegment(3) == 'add_link') {
            self::_method_add_link();
        } else if ($this->uri->rsegment(3) == 'edit_link') {
            self::_method_edit_link();
        } else if ($this->uri->rsegment(3) == 'delete_link') {
            self::_method_delete_link();
        } else if ($this->uri->rsegment(3) == 'copy_link') {
            self::_method_copy_link();
        }
    }

    /*
     * 	Add New Link
     */

    function _method_add_link() {
        if (IS_AJAX) {
            $id_user_type = $this->Misc->decode_id($_POST['item']);
            /* Check User Type if Exist */
            $row = $this->user_types_model->getFields($id_user_type);
            if ($row) {
                $this->form_validation->set_rules('location', 'Location', 'htmlspecialchars|trim|required');
                $this->form_validation->set_rules('name', 'Name', 'htmlspecialchars|trim|required');
                $this->form_validation->set_rules('external', 'URL Source', 'htmlspecialchars|trim');
                $this->form_validation->set_rules('url', 'URL', 'htmlspecialchars|trim|required');
                $this->form_validation->set_rules('icon', 'Icon', 'htmlspecialchars|trim');
                $this->form_validation->set_rules('parent', 'Parent', 'htmlspecialchars|trim');
                $this->form_validation->set_rules('header', 'Header', 'htmlspecialchars|trim');
                $this->form_validation->set_rules('newtab', 'New Tab', 'htmlspecialchars|trim');
                if ($this->form_validation->run() == FALSE) {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                } else {
                    if ($_POST['location'] == 1) {
                        $_POST['parent'] = 0;
                        $_POST['header'] = 0;
                    }
                    $datas = array(
                        'link_url' => $_POST['url'],
                        'link_name' => $_POST['name'],
                        'link_icon' => $_POST['icon'],
                        'link_newtab' => $_POST['newtab'],
                        'link_external' => $_POST['external'],
                        'link_location' => $_POST['location'],
                        'link_head' => $_POST['header'],
                        'link_order' => self::_newlink_order($_POST['location'], $_POST['parent'], $id_user_type), /* Get Order Number */
                        'parent_link_id' => $_POST['parent'],
                        'enabled' => 1,
                        'added_by' => $this->my_session->get('admin', 'user_id'),
                        'added_date' => date('Y-m-d H:i:s'),
                        'user_type_id' => $id_user_type
                    );
                    $id_link = $this->links_model->insert_table($datas);
                    /* Add new Link */
                    parent::save_log("add new link " . $_POST['name'], 'links', $id_link);
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                        'id' => 1
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Edit Link
     */

    function _method_edit_link() {
        if (IS_AJAX) {
            $id_user_type = $this->Misc->decode_id($_POST['item']);
            $id_link = $this->Misc->decode_id($_POST['id']);
            /* Check Link and User Type if Exist */
            $row = $this->links_model->getSearch(array("l.id_link" => $id_link, "id_user_type" => $id_user_type), "", "", "", "", true);
            if ($row) {
                $this->form_validation->set_rules('location', 'Location', 'htmlspecialchars|trim|required');
                $this->form_validation->set_rules('name', 'Name', 'htmlspecialchars|trim|required');
                $this->form_validation->set_rules('external', 'URL Source', 'htmlspecialchars|trim');
                $this->form_validation->set_rules('url', 'URL', 'htmlspecialchars|trim|required');
                $this->form_validation->set_rules('icon', 'Icon', 'htmlspecialchars|trim');
                $this->form_validation->set_rules('parent', 'Parent', 'htmlspecialchars|trim');
                $this->form_validation->set_rules('header', 'Header', 'htmlspecialchars|trim');
                $this->form_validation->set_rules('newtab', 'New Tab', 'htmlspecialchars|trim');
                if ($this->form_validation->run() == FALSE) {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                } else {
                    if ($_POST['location'] == 1) {
                        $_POST['parent'] = 0;
                        $_POST['header'] = 0;
                    }

                    $datas = array(
                        'link_url' => $_POST['url'],
                        'link_name' => $_POST['name'],
                        'link_icon' => $_POST['icon'],
                        'link_newtab' => $_POST['newtab'],
                        'link_external' => $_POST['external'],
                        'link_location' => $_POST['location'],
                        'link_head' => $_POST['header'],
                        'link_order' => $_POST['order'],
                        'parent_link_id' => $_POST['parent'],
                        'updated_by' => $this->my_session->get('admin', 'user_id'),
                        'updated_date' => date('Y-m-d H:i:s')
                    );
                    $this->links_model->update_table($datas, "id_link", $id_link);
                    /* Update Link Info */
                    parent::save_log("update link " . $this->Misc->update_name_log($row->link_name, $_POST['name']), 'links', $id_link);
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                        'id' => 1
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Delete Link
     */

    function _method_delete_link() {
        if (IS_AJAX) {
            $id_link = $this->Misc->decode_id($_POST['item']);
            $id_user_type = $this->Misc->decode_id($_POST['id']);
            /* Check Link and User Type if Exist */
            $row = $this->links_model->getSearch(array("l.id_link" => $id_link, "id_user_type" => $id_user_type), "", "", "", "", true);
            if ($row) {
                $datas = array(
                    'enabled' => 0,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete Link */
                $this->links_model->update_table($datas, "id_link", $id_link);

                $datas = array(
                    'enabled' => 0,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete Class Function by Class */
                $this->links_model->update_table($datas, "parent_link_id", $id_link);

                parent::save_log("delete link " . $row->link_name, 'links', $id_link);

                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Deleted'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Copy User Type's Link to Other User Type
     */

    function _method_copy_link() {
        if (IS_AJAX) {
            $this->form_validation->set_rules('from', 'Link From', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules('to', 'Copy To', 'htmlspecialchars|trim|required');
            if ($this->form_validation->run() == FALSE) {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            } else {
                if ($_POST['to'] != $_POST['from']) {

                    /* Start Query */
                    $this->db->trans_start();

                    $data = array(
                        'enabled' => 0,
                        'updated_by' => $this->my_session->get('admin', 'user_id'),
                        'updated_date' => date('Y-m-d H:i:s')
                    );
                    /* Delete Old Link */
                    $this->links_model->update_table($data, "user_type_id", $_POST['to']);

                    $result = $this->links_model->getSearch(array("id_user_type" => $_POST['from'], "l.parent_link_id" => 0), "", "", true);
                    foreach ($result as $q) {
                        $data = array(
                            'link_url' => $q->link_url,
                            'link_name' => $q->link_name,
                            'link_title' => $q->link_title,
                            'link_detail' => $q->link_detail,
                            'link_icon' => $q->link_icon,
                            'link_location' => $q->link_location,
                            'link_newtab' => $q->link_newtab,
                            'link_external' => $q->link_external,
                            'link_head' => $q->link_head,
                            'link_order' => $q->link_order,
                            'parent_link_id' => $q->parent_link_id,
                            'user_type_id' => $_POST['to'],
                            'enabled' => 1,
                            'added_by' => $this->my_session->get('admin', 'user_id'),
                            'added_date' => date('Y-m-d H:i:s')
                        );
                        /* Add New Link */
                        $id_link = $this->links_model->insert_table($data);

                        $result2 = $this->links_model->getSearch(array("id_user_type" => $_POST['from'], "l.parent_link_id" => $q->id_link), "", "", true);
                        $layer = '';
                        foreach ($result2 as $q2) {
                            $layer2 = array(
                                'link_url' => $q2->link_url,
                                'link_name' => $q2->link_name,
                                'link_title' => $q2->link_title,
                                'link_detail' => $q2->link_detail,
                                'link_icon' => $q2->link_icon,
                                'link_location' => $q2->link_location,
                                'link_newtab' => $q2->link_newtab,
                                'link_external' => $q2->link_external,
                                'link_head' => $q2->link_head,
                                'link_order' => $q2->link_order,
                                'parent_link_id' => $id_link,
                                'user_type_id' => $_POST['to'],
                                'enabled' => 1,
                                'added_by' => $this->my_session->get('admin', 'user_id'),
                                'added_date' => date('Y-m-d H:i:s')
                            );
                            $layer[] = $layer2;
                        }

                        if ($layer != '') {
                            /* Add New Sub Link */
                            $this->links_model->insert_batch_table($layer);
                        }
                    }
                    parent::save_log("Copy menu");

                    /* End Query */
                    $this->db->trans_complete();
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', "Successfuly Saved"),
                        'id' => 1
                    );
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", "Error in Link From and Copy To Fields"),
                        'id' => 0
                    );
                }
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	List User Type
     */

    function _method_list_usertype() {
        if (IS_AJAX) {
            $datas = array(
                'search' => $_POST['search'],
                'page' => $_POST['page'],
                'sort' => $_POST['sort'],
                'display' => $_POST['display'],
                'num_button' => 5
            );
            /* Get User Type List */
            $data = modules::run(admin_dir('lists/_request_data'), '_user_type_list', $datas);
            $data['search'] = $_POST['search'];
            $data['sort'] = $_POST['sort'];
            $this->load->view(admin_dir('lists/menu/usertype_menu'), $data);
        }
    }

    /* ----------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Additional Module --------------------------------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------------------------------------------------------------------------------- */


    /*
     * Generate Order Number for New Added Link 
     */

    function _newlink_order($link_location, $parent_link_id, $user_type_id) {
        $count = $this->links_model->getSearch(array("l.link_location" => $link_location, "id_user_type" => $user_type_id, "l.parent_link_id" => $parent_link_id), "", "", "", true);
        return $count + 1;
    }

    /*
     * Show Link Popup Form
     */

    function popupform_link() {
        if (IS_AJAX) {
            $redirect = (!empty($_POST['redirect'])) ? $_POST['redirect'] : "";

            if ($_POST['type'] == 1) { /* Add Form */
                $id_user_type = $this->Misc->decode_id($_POST['id']);
                $parent_link = $this->links_model->getSearch(array('ut.id_user_type' => $id_user_type, 'l.link_location' => 2, 'l.parent_link_id' => 0), "", array('l.link_name' => 'ASC'), true);
                $this->load->view(admin_dir('menu/extra/popupform_link'), array('add' => 1, 'redirect' => $redirect, 'parent_link' => $parent_link, 'id_user_type' => $id_user_type));
            } else if ($_POST['type'] == 2 and ! empty($_POST['item'])) { /* Edit Form */
                $id_user_type = $this->Misc->decode_id($_POST['id']);
                $parent_link = $this->links_model->getSearch(array('ut.id_user_type' => $id_user_type, 'l.link_location' => 2, 'l.parent_link_id' => 0), "", array('l.link_name' => 'ASC'), true);
                $id_link = $this->Misc->decode_id($_POST['item']);
                $row = $this->links_model->getFields($id_link);
                if ($row) {
                    $this->load->view(admin_dir('menu/extra/popupform_link'), array('edit' => 1, 'row' => $row, 'redirect' => $redirect, 'parent_link' => $parent_link, 'id_user_type' => $id_user_type));
                }
            } else if ($_POST['type'] == 3) { /* Copy Form */
                $user_types = $this->user_types_model->getSearch("", "", array('user_type_name' => 'ASC'), true);
                $this->load->view(admin_dir('menu/extra/popupform_link'), array('copy' => 1, 'redirect' => $redirect, 'user_types' => $user_types));
            }
        }
    }

}
