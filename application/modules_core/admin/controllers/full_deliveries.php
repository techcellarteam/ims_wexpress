<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Full Deliveries Class
  |--------------------------------------------------------------------------
  |
  | Handles the Full Deliveries panel
  |
  | @category		Controller
  | @author		melin
 */

class Full_Deliveries extends Admin_Core {

    public $purchase_orders_model = '';
    public $purchase_orders_items_model = '';
    public $rr_model = '';
    public $rr_items_model = '';
    public $list_content = array();
    public $contents = array();
    public $users_model;

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        //load models
        $this->load->model('admin/purchase_requisitions_model');
        $this->load->model('admin/purchase_requisition_items_model');
        $this->load->model('admin/purchase_orders_model');
        $this->load->model('admin/purchase_order_items_model');
        $this->load->model('admin/document_types_model');
        $this->load->model('admin/business_units_model');
        $this->load->model('admin/items_model');
        $this->load->model('admin/notifications_model');
        $this->load->model('admin/classes_model');
        $this->load->model('admin/module_attachments_model');
        $this->load->model('admin/user_types_model');
        $this->load->model('admin/users_model');
        $this->load->model('admin/suppliers_model');
        $this->load->model('admin/stock_classifications_model');
        $this->load->model('admin/general_group_model');
        $this->load->model('admin/receiving_reports_model');
        $this->load->model('admin/receiving_report_items_model');

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));
        //class ID
        $this->class_id = $this->classes_model->getFieldValue('id_class', array('class_name' => $this->classname));
        //initiate models
        $this->purchase_orders_model = new Purchase_Orders_Model();
        $this->purchase_order_items_model = new Purchase_Order_Items_Model();
        $this->purchase_requisitions_model = new Purchase_Orders_Model();
        $this->purchase_requisition_items_model = new Purchase_Requisition_Items_Model();
        $this->document_types_model = new Document_Types_Model();
        $this->stock_classifications_model = new Stock_Classifications_Model();
        $this->general_group_model = new General_Group_Model();
        $this->users_model = new Users_Model();
        $this->suppliers_model = new Suppliers_Model();
        $this->rr_model = new Receiving_Reports_Model();
        $this->rr_items_model = new Receiving_Report_Items_Model();

        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_purchase_order',
            ),
            'po_number' => array(
                'label' => 'PO NO',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'po_number',
            ),
            'pr_number' => array(
                'label' => 'PR NO',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'pr_number',
            ),
            'supplier_code' => array(
                'label' => 'Supplier',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'supplier_code',
            ),
            'business_unit_code' => array(
                'label' => 'Business Unit',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'business_unit_code',
            ),
            'total_amount' => array(
                'label' => 'Total Amount',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'total_amount',
            ),
            'order_date' => array(
                'label' => 'Order Date',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'order_date',
            ),
            'status' => array(
                'label' => 'Status',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'status',
            ),
        );

        $this->contents = array('model_directory' => 'admin/purchase_orders_model',
            'model_name' => 'purchase_orders_model',
            'filters' => array(),
            'functionName' => 'Full Delivery'); // use to call functions for access

        $this->tools->setPostArray('sort', array('sort_by' => 'id_purchase_order', 'sort_type' => 'DESC'));
    }

    function index() {
        redirect(admin_dir('full_deliveries/list_full_delivery'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_full_delivery() {
        $data = array(
            'template' => parent::main_template(),
        );
        $this->load->view(admin_dir('full_deliveries/list_full_delivery'), $data);
    }

    function view_full_delivery() {
        $id_purchase_order = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Purchase Order if Exist */
        $row = $this->purchase_orders_model->getFields($id_purchase_order);
        $po_items = $this->purchase_order_items_model->getList(array('purchase_order_id' => $id_purchase_order))->result();
        $user_logs_model = new User_Logs_Model();
        $logs = $user_logs_model->getSearch(array('user_log_table' => 'purchase_orders', 'user_log_value' => $id_purchase_order), array(), array('id_user_log' => 'DESC'))->result();

        //receiving items
        $receiving_reports = $this->rr_model->getList(array('purchase_order_id' => $id_purchase_order))->result();
        $rr_items = $this->rr_items_model->getList(array('po.id_purchase_order' => $id_purchase_order))->result();
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'full-delivery-tools.js'))),
                'row' => $row,
                'po_items' => $po_items,
                'logs' => $logs,
                'receiving_reports' => $receiving_reports,
                'rr_items' => $rr_items,
            );
            $this->load->view(admin_dir('full_deliveries/view_full_delivery'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function edit_full_delivery() {
        $id_purchase_order = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Purchase Order if Exist */
        $row = $this->purchase_orders_model->getFields($id_purchase_order);
        $po_items = $this->purchase_order_items_model->getList(array('purchase_order_id' => $id_purchase_order))->result();
        $user_logs_model = new User_Logs_Model();
        $logs = $user_logs_model->getSearch(array('user_log_table' => 'purchase_orders', 'user_log_value' => $id_purchase_order), array(), array('id_user_log' => 'DESC'))->result();

        //receiving items
        $receiving_reports = $this->rr_model->getList(array('purchase_order_id' => $id_purchase_order))->result();
        $rr_items = $this->rr_items_model->getList(array('po.id_purchase_order' => $id_purchase_order))->result();
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'receiving-order-tools.js'))),
                'row' => $row,
                'po_items' => $po_items,
                'logs' => $logs,
                'receiving_reports' => $receiving_reports,
                'rr_items' => $rr_items,
            );
            $this->load->view(admin_dir('full_deliveries/edit_full_delivery'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_full_delivery') { /* Method for Account */
            self::_method_list_full_delivery();
        } else if ($this->uri->rsegment(3) == 'view_full_delivery') {
            self::view_full_delivery();
        } else if ($this->uri->rsegment(3) == 'edit_full_delivery') {
            self::edit_full_delivery();
        } else if ($this->uri->rsegment(3) == 'add_comment') {
            self::_method_add_comment();
        }
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_full_delivery() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }
        $condition = array();
        //show purchase requisitions requested by the business_unit only if user is not PEM, FM, CEO,OM or Super Admin
        if ((int) $this->session->userdata['admin']['user_type_id'] != 1) {
            $user_type_code = $this->session->userdata['admin']['user_type_code'];
            switch ($user_type_code) {
                case 'PU Head':
                    break;
                case 'Purchaser':
                    break;
                case 'PO Initiator':
                    break;
                case 'Administrator':
                    break;
                default:
                    $group_id = $this->users_model->getValue($this->session->userdata['admin']['user_id'], 'general_group_id');
                    $condition = array('du.business_unit_id' => $this->session->userdata['admin']['business_unit_id'],
                        'bu.general_group_id' => $group_id);
                    break;
            }
        }
        //set condition for the list
        $condition_string = array("`po`.`status` IN ('Full Delivery')");
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_purchase_order') {
                        $condition = array_merge($condition, array($var => $val));
                    } elseif ($var == 'added_date') {
                        $condition = array_merge($condition, array('po.' . $var . " like" => "%" . $val . "%"));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get Purchase Order List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array('');

        $this->load->view(admin_dir('lists/list'), $data);
    }

    /*
     * 	save comments
     */

    function _method_add_comment() {
        if (IS_AJAX) {
            $id_purchase_order = $this->Misc->decode_id($this->tools->getPost('id'));
            /* Check Purchase Order if Exist */
            $row = $this->purchase_orders_model->getFields($id_purchase_order);
            if ($row) {
                $this->form_validation->set_rules('comment', 'Comment', 'htmlspecialchars|trim|required');
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    // save comment as log
                    parent::save_log($this->tools->getPost('comment'), 'purchase_orders', $id_purchase_order);

                    //notify business_unit requestor
                    $status = "Comment: " . $this->tools->getPost('comment');
                    self::notify_creator($row->added_by, $id_purchase_order, $status);

                    $this->db->trans_complete();
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Saved Remarks'),
                        'id' => 1
                    );
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * Notify creator of PO
     */

    private function notify_creator($added_by, $id_purchase_order, $status) {
        //notify Initiator for declined PO
        $notif_data = array(
            'class_id' => $this->class_id,
            'reference_id' => $id_purchase_order,
            'user_id' => $added_by,
            'link' => 'view_full_delivery',
            'status' => $status);
        $notification_model = new Notifications_Model();
        $notification_model->add($notif_data);
    }

}
