<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Unit Measurements Class
  |--------------------------------------------------------------------------
  |
  | Handles the Unit Measurements panel
  |
  | @category		Controller
  | @author		melin
 */

class Unit_Measurements extends Admin_Core {

    public $unit_measurements_model = '';
    public $list_content = array();
    public $contents = array();

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        //load models
        $this->load->model('admin/unit_measurements_model');
        //initiate models
        $this->unit_measurements_model = new Unit_Measurements_Model();
        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_unit_measurement',
            ),
            'measurement_name' => array(
                'label' => 'Unit Measurement Name',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-4',
                'var-value' => 'measurement_name',
            ),
            'measurement_code' => array(
                'label' => 'Unit Measurement Code',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-3',
                'var-value' => 'measurement_code',
            ),
        );

        $this->contents = array('model_directory' => 'admin/unit_measurements_model',
            'model_name' => 'unit_measurements_model',
            'filters' => array(),
            'functionName' => 'Unit Measurement'); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('unit_measurements/list_unit_measurement'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_unit_measurement() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'uom-tools.js'))),
        );
        $this->load->view(admin_dir('unit_measurements/list_unit_measurement'), $data);
    }

    function add_unit_measurement() {
        $data = array(
            'template' => parent::main_template(),
        );
        $this->load->view(admin_dir('unit_measurements/add_unit_measurement'), $data);
    }

    function edit_unit_measurement() {
        $id_unit_measurement = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Unit Measurement if Exist */
        $row = $this->unit_measurements_model->getFields($id_unit_measurement);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
            );
            $this->load->view(admin_dir('unit_measurements/edit_unit_measurement'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function view_unit_measurement() {
        $id_unit_measurement = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Unit Measurement if Exist */
        $row = $this->unit_measurements_model->getFields($id_unit_measurement);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
            );
            $this->load->view(admin_dir('unit_measurements/view_unit_measurement'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_unit_measurement') { /* Method for Account */
            self::_method_list_unit_measurement();
        } else if ($this->uri->rsegment(3) == 'add_unit_measurement') {
            self::_method_add_unit_measurement();
        } else if ($this->uri->rsegment(3) == 'edit_unit_measurement') {
            self::_method_edit_unit_measurement();
        } else if ($this->uri->rsegment(3) == 'delete_unit_measurement') {
            self::_method_delete_unit_measurement();
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Validation of the Form -------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    private function _validate() {
        $this->form_validation->set_rules('measurement_name', 'Name', 'htmlspecialchars|trim|max_length[32]|required');
        $this->form_validation->set_rules('measurement_code', 'Code', 'htmlspecialchars|trim|max_length[8]|required');
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_unit_measurement() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }

        //set condition for the list
        $condition = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_unit_measurement') {
                        $condition = array_merge($condition, array($var => $val));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'contents' => $this->contents
        );

        /* Get Unit Measurement List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array('edit', 'view');

        $this->load->view(admin_dir('lists/list'), $data);
    }

    /*
     * 	Add New Unit Measurement Method
     */

    function _method_add_unit_measurement() {
        if (IS_AJAX) {
            self::_validate();

            //check form validation then save
            if ($this->form_validation->run() == TRUE) {
                $this->db->trans_start();
                //getPost data set to this model fields
                parent::copyFromPost($this->unit_measurements_model, 'id_unit_measurement');

                /* Check Unit Measurement Code Name Exist */
                $count = $this->unit_measurements_model->getSearch(array("measurement_name like" => $this->tools->getPost('measurement_name')), "", "", "", true);
                if ($count == 0) {
                    $this->my_session->get('admin', 'user_id');
                    $this->unit_measurements_model->add();
                    parent::save_log("add new unit measurement " . $this->tools->getPost('measurement_name'), 'unit_measurements', $this->unit_measurements_model->id);
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                        'id' => $this->unit_measurements_model->id_unit_measurement
                    );
                } else {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", "Code already exist"),
                        'id' => 0
                    );
                }
                $this->db->trans_complete();
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	Edit Unit Measurement Type Method
     */

    function _method_edit_unit_measurement() {
        if (IS_AJAX) {
            $id_unit_measurement = $this->Misc->decode_id($this->tools->getPost('id'));

            /* Check Unit Measurement Type if Exist */
            $row = $this->unit_measurements_model->getFields($id_unit_measurement);
            if ($row) {
                self::_validate();
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    /* Check Unit Measurement Exist */
                    $count = $this->unit_measurements_model->getSearch(array('id_unit_measurement !=' => $id_unit_measurement, "measurement_name like" => $this->tools->getPost('measurement_name')), "", "", "", true);
                    if ($count == 0) {
                        /* Update Unit Measurement Info */
                        //getPost data set to this model fields
                        parent::copyFromPost($this->unit_measurements_model, 'id_unit_measurement');
                        $this->unit_measurements_model->update(array('id_unit_measurement' => $id_unit_measurement));
                        parent::save_log("update unit measurement " . $this->Misc->update_name_log($row->measurement_name, $this->tools->getPost('measurement_name')), 'unit_measurements', $id_unit_measurement);
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                            'id' => $id_unit_measurement
                        );
                    } else {
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('error', "Brand Name already exist"),
                            'id' => 0
                        );
                    }
                    $this->db->trans_complete();
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Delete Unit Measurement Method
     */

    function _method_delete_unit_measurement() {
        if (IS_AJAX) {
            $id_unit_measurement = $this->Misc->decode_id($this->tools->getPost('item'));
            /* Check Unit Measurement if Exist */
            $row = $this->unit_measurements_model->getFields($id_unit_measurement);
            if ($row) {
                $datas = array(
                    'enabled' => 0,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete Unit Measurement */
                $this->unit_measurements_model->update_table($datas, "id_unit_measurement", $id_unit_measurement);
                parent::save_log("delete unit measurement" . $row->measurement_name, 'unit_measurements', $id_unit_measurement);
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Deleted'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

}
