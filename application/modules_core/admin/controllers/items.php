<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Items Class
  |--------------------------------------------------------------------------
  |
  | Handles the ASM panel
  |
  | @category		Controller
  | @author		melin
 */

class Items extends Admin_Core {

    public $items_model = '';

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);


        parent:: __construct();
        $this->load->model('admin/items_model');
        $this->load->model('admin/stock_classifications_model');
        $this->load->model('admin/general_group_model');
        $this->load->model('admin/suppliers_model');
        $this->load->model('admin/item_suppliers_model');
        $this->load->model('admin/users_model');
        $this->load->model('admin/unit_measurements_model');
        $this->load->model('admin/item_location_model');
        $this->load->model('admin/business_units_model');

        $this->items_model = new Items_Model();
        $this->group_model = new General_Group_Model();
        $this->stock_classifications_model = new Stock_Classifications_Model();
        $this->suppliers_model = new Suppliers_Model();
        $this->item_suppliers_model = new Item_Suppliers_Model();
        $this->unit_measurements_model = new Unit_Measurements_Model();
        $this->business_units_model = new Business_Units_Model();

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        //for list view
        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_item',
            ),
            'group_code' => array(
                'label' => 'Group',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'group_code',
            ),
            'stock_classification_name' => array(
                'label' => 'Stock Classification',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'stock_classification_name',
            ),
            'sku' => array(
                'label' => 'SKU',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'sku',
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'description',
            ),
            'with_supplier' => array(
                'label' => 'With Nominated Suppliers',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'with_supplier',
            )
        );

        $this->contents = array('model_directory' => 'admin/items_model',
            'model_name' => 'items_model',
            'filters' => array(),
            'functionName' => 'Item',); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('items/list_item'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_item() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'item-tools.js'))),
        );

        $this->load->view(admin_dir('items/list_item'), $data);
    }

    function add_item() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'item-tools.js'))),
            'groups' => $this->group_model->getList()->result(),
            'stock_classifications' => $this->stock_classifications_model->getList()->result(),
            'suppliers' => $this->suppliers_model->getList()->result(),
            'unit_measurements' => $this->unit_measurements_model->getList()->result(),
        );
        $this->load->view(admin_dir('items/add_item'), $data);
    }

    function edit_item() {
        $id_item = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Item if Exist */
        $row = $this->items_model->getFields($id_item);
        if ($row) {
            //not to include suppliers already chosen
            $item_supp = $this->item_suppliers_model->getList(array('item_id' => $id_item))->result();
            $suppliers = array();
            foreach ($item_supp as $q) {
                array_push($suppliers, $q->supplier_id);
            }

            $where_string = '';
            foreach (array_unique($suppliers) as $idx => $val) {
                $where_string .= (count(array_unique($suppliers)) == ((int) $idx + 1)) ? $val : $val . ',';
            }
            if ($where_string) {
                $where_string = '`id_supplier` NOT IN (' . $where_string . ')';
            }
            //prepare items
            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'item-tools.js'))),
                'row' => $row,
                'groups' => $this->group_model->getList()->result(),
                'stock_classifications' => $this->stock_classifications_model->getList()->result(),
                'suppliers' => $this->suppliers_model->getList(array('general_group_id' => $row->general_group_id), $where_string)->result(),
                'item_suppliers' => $this->item_suppliers_model->getList(array('item_id' => $id_item))->result(),
                'item_id' => $row->id_item,
                'unit_measurements' => $this->unit_measurements_model->getList()->result(),
            );
            $this->load->view(admin_dir('items/edit_item'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function view_item() {
        $id_item = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Item if Exist */
        $row = $this->items_model->getFields($id_item);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
                'stock_classifications' => $this->items_model->getList()->result(),
                'item_suppliers' => $this->item_suppliers_model->getList(array('item_id' => $id_item))->result(),
            );
            $this->load->view(admin_dir('items/view_item'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function add_item_suppliers() {
        $id_item = $this->Misc->decode_id($this->tools->getPost('id'));
        /* Check Item if Exist */
        $row = $this->items_model->getFields($id_item);

        //not to include suppliers already chosen
        $item_supp = $this->item_suppliers_model->getList(array('item_id' => $id_item))->result();
        $suppliers = array();
        foreach ($item_supp as $q) {
            array_push($suppliers, $q->supplier_id);
        }

        $where_string = '';
        foreach (array_unique($suppliers) as $idx => $val) {
            $where_string .= (count(array_unique($suppliers)) == ((int) $idx + 1)) ? $val : $val . ',';
        }
        if ($where_string) {
            $where_string = '`id_supplier` NOT IN (' . $where_string . ')';
        }

        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
                'suppliers' => $this->suppliers_model->getList(array(), $where_string)->result(),
            );
            $this->load->view(admin_dir('items/add_item_supplier'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_item') { /* Method for Account */
            self::_method_list_item();
        } else if ($this->uri->rsegment(3) == 'add_item') {
            self::_method_add_item();
        } else if ($this->uri->rsegment(3) == 'edit_item') {
            self::_method_edit_item();
        } else if ($this->uri->rsegment(3) == 'delete_item') {
            self::_method_delete_item();
        } else if ($this->uri->rsegment(3) == 'add_item_suppliers') {
            self::_method_add_item_suppliers();
        } else if ($this->uri->rsegment(3) == 'select_item') {
            self::_method_select_item();
        } else if ($this->uri->rsegment(3) == 'get_item_detail') {
            self::_method_get_item_detail();
        } else if ($this->uri->rsegment(3) == 'set_item_input') {
            self::_method_set_item_input();
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Validation of the Form -------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    private function _validate() {
        $this->form_validation->set_rules('sku', 'SKU', 'htmlspecialchars|trim|required|max_length[64]');
        $this->form_validation->set_rules('stock_classification_id', 'Stock Classification', 'htmlspecialchars|trim|required|integer');
        $this->form_validation->set_rules('description', 'description', 'htmlspecialchars|trim|max_length[64]|required');
        $this->form_validation->set_rules('general_group_id', 'Group', 'required');
        if ($this->tools->getPost('supplier_id')) {
            foreach ($this->tools->getPost('supplier_id') as $key => $val) {
                $supplier_name = $this->tools->getPost('supplier_name', $key);
                $this->form_validation->set_rules("item_unit_cost[$key]", "Unit Cost of $supplier_name", 'htmlspecialchars|trim|required');
            }
        }

        if ($this->tools->getPost('item_suppliers')) {
            foreach ($this->tools->getPost('item_suppliers') as $key => $val) {
                $supplier_name = $this->tools->getPost('i_supplier_id', $key);
                $this->form_validation->set_rules("unit_cost[$key]", "Unit Cost of $supplier_name", 'htmlspecialchars|trim|required');
            }
        }
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_item() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }

        //set condition for the list
        $condition = array();
        $condition_string = array();
        if ((int) $this->session->userdata['admin']['user_type_id'] != 1) {
            $user_type_code = $this->session->userdata['admin']['user_type_code'];
            switch ($user_type_code) {
                case 'PU Head':
                case 'Purchaser':
                case 'PO Initiator':
                case 'Administrator':
                case 'EVP':
                case 'Chairman':
                    break;
                default:
                    $group_id = $this->users_model->getValue($this->session->userdata['admin']['user_id'], 'general_group_id');
                    $condition = array('i.general_group_id' => $group_id);
                    break;
            }
        }
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_item') {
                        $condition = array_merge($condition, array($var => $val));
                    } elseif ($var == 'stock_classification_name') {
                        $condition = array_merge($condition, array("cg.stock_classification_name like" => "%" . trim($val) . "%"));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get Item List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array(); // to long to be poped-up

        $this->load->view(admin_dir('lists/list'), $data);
    }

    /*
     * 	Add New Item Method
     */

    function _method_add_item() {
        if (IS_AJAX) {
            self::_validate();
            //check form validation then save
            $arr = array();
            $message_alert = '';
            if ($this->form_validation->run() == TRUE) {
                $this->db->trans_start();
                //getPost data set to this model fields
                $this->items_model->cost = $this->tools->getPost('cost');
                parent::copyFromPost($this->items_model, 'id_item');
                foreach ($this->tools->getPost('general_group_id') as $key => $group_id) {
                    /* Check SKU Name Exist */
                    $count = $this->items_model->getSearch(array("sku like" => $this->tools->getPost('sku'),
                        'description like' => $this->tools->getPost('description'),
                        'general_group_id' => $group_id,
                        'stock_classification_id' => $this->tools->getPost('stock_classification_id')), "", "", "", true);
                    if ($count == 0) {
                        $this->items_model->general_group_id = $group_id;
                        if ($this->items_model->add()) {
                            parent::save_log("add new commodity " . $this->tools->getPost('sku'), 'items', $this->items_model->id);

                            //add supplier in this commodity
                            if ($this->tools->getPost('supplier_id')) {
                                foreach ($this->tools->getPost('supplier_id') as $key => $val) {
                                    $item_suppliers = new Item_Suppliers_Model();
                                    $item_suppliers->add(array('item_id' => $this->items_model->id,
                                        'supplier_id' => $this->tools->getPost('supplier_id', $key),
                                        'item_unit_cost' => $this->tools->getPost('item_unit_cost', $key),
                                        'added_date' => date('Y-m-d H:i:s'),
                                        'added_by' => $this->session->userdata['admin']['user_id']
                                    ));
                                }
                            }
                            $message_alert = 'Item Successfully Saved';
                            $arr = array(
                                'message_alert' => $this->Misc->message_status('success', $message_alert),
                                'id' => 1
                            );
                        }
                    } else {
                        $group_code = $this->group_model->getFieldValue('group_code', array('id_general_group' => $group_id));
                        $message_alert.= " Item [" . $this->tools->getPost('sku') . "] in group code [" . $group_code . "] already exist. </br>";
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('error', $message_alert),
                            'id' => 0
                        );
                    }
                }
                $this->db->trans_complete();
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	Edit Item Type Method
     */

    function _method_edit_item() {
        if (IS_AJAX) {
            $id_item = $this->Misc->decode_id($this->tools->getPost('id'));

            /* Check Item Type if Exist */
            $row = $this->items_model->getFields($id_item);
            if ($row) {
                self::_validate();
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();

                    /* Update Item Info */
                    $items_model = new Items_Model($id_item);
                    //getPost data set to this model fields
                    parent::copyFromPost($items_model, 'id_item');
                    $count = $this->items_model->getSearch(array('id_item !=' => $id_item,
                        "sku like" => $this->tools->getPost('sku'),
                        'description like' => $this->tools->getPost('description'),
                        'stock_classification_id' => $this->tools->getPost('stock_classification_id'),
                        'general_group_id' => $this->tools->getPost('general_group_id')), "", "", "", true);
                    if ($count == 0) {
                        if ($items_model->update()) {
                            parent::save_log("update commodity " . $this->Misc->update_name_log($row->sku, $this->tools->getPost('sku')), 'items', $id_item);

                            $item_suppliers = $this->tools->getPost('item_suppliers');

                            if (!empty($item_suppliers)) {
                                $ts = $this->item_suppliers_model->getList(array('item_id' => $id_item))->result();
                                foreach ($ts as $q) {
                                    $item_supp = new Item_Suppliers_Model($q->id_item_supplier);
                                    $item_unit_cost_change = new Item_UC_Changes_Model();
                                    if (in_array($q->id_item_supplier, $item_suppliers)) {// if in list update changes
                                        $item_supp->update(array('id_item_supplier' => $q->id_item_supplier), array('item_unit_cost' => $this->tools->getPost('unit_cost', $q->id_item_supplier),
                                            'updated_by' => $this->my_session->get('admin', 'user_id'),
                                            'updated_date' => date('Y-m-d H:i:s')));

                                        // add to commodity srp changes logs
                                        $item_unit_cost_change->add(array('supplier_id' => $item_supp->supplier_id,
                                            'item_id' => $id_item,
                                            'old_item_uc' => $item_supp->item_unit_cost,
                                            'new_item_uc' => $this->tools->getPost('unit_cost', $q->id_item_supplier),
                                            'added_by' => $this->my_session->get('admin', 'user_id'),
                                            'added_date' => date('Y-m-d H:i:s')));
                                    } else {
                                        //remove since not already in the list
                                        $item_supp->update(array('id_item_supplier' => $q->id_item_supplier), array('enabled' => 0,
                                            'updated_by' => $this->my_session->get('admin', 'user_id'),
                                            'updated_date' => date('Y-m-d H:i:s')));
                                    }
                                }
                            } else { //delete all entered commodity supplier
                                $this->item_suppliers_model->update(array('item_id' => $id_item), array('enabled' => 0));
                            }

                            //add supplier in this commodity
                            if ($this->tools->getPost('supplier_id')) {
                                foreach ($this->tools->getPost('supplier_id') as $key => $val) {
                                    $item_suppliers = new Item_Suppliers_Model();
                                    $item_suppliers->add(array('item_id' => $items_model->id_item,
                                        'supplier_id' => $this->tools->getPost('supplier_id', $key),
                                        'item_unit_cost' => $this->tools->getPost('item_unit_cost', $key)
                                    ));
                                }
                            }
                            $arr = array(
                                'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                                'id' => $this->items_model->id
                            );
                        }
                    } else {
                        $arr = array(
                            'message_alert' => $this->Misc->message_status("error", "Item already exist"),
                            'id' => 0
                        );
                    }
                    $this->db->trans_complete();
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Delete Item Method
     */

    function _method_delete_item() {
        if (IS_AJAX) {
            $id_item = $this->Misc->decode_id($this->tools->getPost('item'));
            /* Check Item if Exist */
            $row = $this->items_model->getFields($id_item);
            if ($row) {
                $datas = array(
                    'enabled' => 0,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete Item */
                $this->items_model->update_table($datas, "id_item", $id_item);
                parent::save_log("delete commodity " . $row->sku, 'items', $id_item);
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Deleted'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * Function adding Suppliers to Specific Item
     */

    function _method_add_item_suppliers() {
        if (IS_AJAX) {
            if ($this->tools->getPost('supplier_id')) {
                foreach ($this->tools->getPost('supplier_id') as $key => $val) {
                    $supplier_name = $this->tools->getPost('supplier_name', $key);
                    $this->form_validation->set_rules("item_unit_cost[$key]", "Unit Cost of $supplier_name", 'htmlspecialchars|trim|required');
                }
            }

            $id_item = $this->Misc->decode_id($this->tools->getPost('id'));
            /* Check Item if Exist */
            $row = $this->items_model->getFields($id_item);
            if ($row) {
                //add supplier in this commodity
                if ($this->tools->getPost('supplier_id')) {
                    foreach ($this->tools->getPost('supplier_id') as $key => $val) {
                        $this->item_suppliers_model = new Item_Suppliers_Model();
                        $item_unit_cost_change = new Item_UC_Changes_Model();
                        $supplier_name = $this->tools->getPost('supplier_name', $key);
                        $item_supplier = $this->item_suppliers_model->getList(array('supplier_id' => (int) $val, 'item_id' => $id_item))->row();

                        if (count($item_supplier) > 0) {
                            $item_suppliers = new Item_Suppliers_Model($item_supplier->id_item_supplier);
                            parent::save_log("updated " . $supplier_name . "'s price from" . $item_suppliers->item_unit_cost . " to " . $this->tools->getPost('item_unit_cost', $key), 'items', $id_item);
                            $item_suppliers->update('', array('item_unit_cost' => $this->tools->getPost('item_unit_cost', $key),
                                'updated_by' => $this->my_session->get('admin', 'user_id'),
                                'updated_date' => date('Y-m-d H:i:s')));
                            // add to commodity srp changes logs
                            $item_unit_cost_change->add(array('supplier_id' => $val,
                                'item_id' => $id_item,
                                'old_item_uc' => $item_suppliers->item_unit_cost,
                                'new_item_uc' => $this->tools->getPost('item_unit_cost', $key),
                                'added_by' => $this->my_session->get('admin', 'user_id'),
                                'added_date' => date('Y-m-d H:i:s')));
                        } else {
                            $item_suppliers = new Item_Suppliers_Model();
                            $item_suppliers->add(array('item_id' => $id_item,
                                'supplier_id' => $this->tools->getPost('supplier_id', $key),
                                'item_unit_cost' => $this->tools->getPost('item_unit_cost', $key),
                                'added_by' => $this->my_session->get('admin', 'user_id'),
                                'added_date' => date('Y-m-d H:i:s')
                            ));
                            // add to commodity srp changes logs
                            $item_unit_cost_change->add(array('supplier_id' => $val,
                                'item_id' => $id_item,
                                'old_item_uc' => 0,
                                'new_item_uc' => $this->tools->getPost('item_unit_cost', $key),
                                'added_by' => $this->my_session->get('admin', 'user_id'),
                                'added_date' => date('Y-m-d H:i:s')));
                            parent::save_log("added price details of " . $row->sku . " (" . $this->tools->getPost('item_unit_cost', $key) . ") from " . $supplier_name, 'items', $id_item);
                        }
                    }
                }
                parent::save_log("updated commodity supplier(s) price" . $row->sku, 'items', $id_item);
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Stock Classifications Suppliers Updates'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * Function formatting Stock Classifications on dropdown based on commodity type
     */

    function _method_select_item() {
        if (!IS_AJAX) {
            $arr = array(
                'message_alert' => $this->Misc->message_status('error', 'Forbidden Direct Access'),
                'id' => 1
            );
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
        $this->users_model = new Users_Model();
        $bu = $this->business_units_model->getFields($this->tools->getPost('business_unit_id'));
        $stock_classification_id = $this->tools->getPost('stock_classification_id');
        $where = array('general_group_id' => $bu->general_group_id, 'stock_classification_id' => $stock_classification_id);
        $data = array('items' => $this->items_model->getList($where)->result());
        $this->load->view(admin_dir('items/extra/select_item'), $data);
    }

    /*
     * Function getting commodity details based on selected commodity
     */

    function _method_get_item_detail() {
        if (!IS_AJAX) {
            $arr = array(
                'message_alert' => $this->Misc->message_status('error', 'Forbidden Direct Access'),
                'id' => 1
            );
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
        $this->load->model('admin/purchase_order_items_model');
        $this->po_items = new Purchase_Order_Items_Model();
        $item_id = $this->tools->getPost('item_id');
        $data = array('quantity' => $this->tools->getPost('quantity'));
        if ($this->tools->getPost('supplier_id')) {
            $items = $this->items_model->getListSupplier(array('id_item' => $item_id, 'es.supplier_id' => $this->tools->getPost('supplier_id')))->result();
            $data['item'] = $items;
            $this->load->view(admin_dir('items/extra/get_item_detail_po'), $data);
        } else {
            $items = $this->items_model->getFields($item_id);
            $id_po_item = $this->po_items->getLastNo('id_po_item', array('item_id' => $item_id));
            $items->last_ordered_price = '0.00';
            if ($id_po_item) {
                $poi = new Purchase_Order_Items_Model($id_po_item);
                $items->last_ordered_price = $poi->item_unit_cost;
            }
            $items->initiators_price = $this->tools->getPost('initiators_price');
            $items->quantity = $this->tools->getPost('quantity');
            $items->remarks = $this->tools->getPost('remarks');
            $data['item'] = $items;
            $this->load->view(admin_dir('items/extra/get_item_detail'), $data);
        }
    }

    /*
     * Function setting commodity details based on inputted commodity
     */

    function _method_set_item_input() {
        if (!IS_AJAX) {
            $arr = array(
                'message_alert' => $this->Misc->message_status('error', 'Forbidden Direct Access'),
                'id' => 1
            );
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }

        $other_request = $this->tools->getPost('other_request');
        $data = array('commodity' => $other_request,
            'quantity' => $this->tools->getPost('quantity'));
        $this->load->view(admin_dir('items/extra/set_item_input'), $data);
    }

    /*
     * Check Item if Exist
     */

    function check_item() {
        if (!IS_AJAX) {
            $arr = array(
                'message_alert' => $this->Misc->message_status('error', 'Forbidden Direct Access'),
                'id' => 1
            );
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }


        $count = $this->items_model->getSearch(array("sku like" => $this->tools->getPost('sku'),
            'description like' => $this->tools->getPost('description'),
            'stock_classification_id' => $this->tools->getPost('stock_classification_id'),
            'general_group_id' => $this->tools->getPost('general_group_id'),
            'unit_measurement_id' => $this->tools->getPost('unit_measurement_id')), "", "", "", true);
        if ($count) { // already existing
            $arr = array(
                'message_alert' => $this->Misc->message_status('error', 'Item Already Existing'),
                'success' => 0
            );
        } else {
            $arr = array(
                'message_alert' => $this->Misc->message_status('success', 'You May Proceed'),
                'id' => 1
            );
        }
        $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
    }

}
