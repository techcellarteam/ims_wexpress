<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Business Units Inventory Class
  |--------------------------------------------------------------------------
  |
  | Handles the Business Units Inventory panel
  |
  | @category		Controller
  | @author		melin
 */

class Business_Units_Inventory extends Admin_Core {

    public $list_content = array();
    public $contents = array();
    public $item_location_model;
    public $business_units_model;

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        //load models
        $this->load->model('admin/purchase_requisitions_model');
        $this->load->model('admin/purchase_requisition_items_model');
        $this->load->model('admin/purchase_orders_model');
        $this->load->model('admin/purchase_order_items_model');
        $this->load->model('admin/document_types_model');
        $this->load->model('admin/business_units_model');
        $this->load->model('admin/items_model');
        $this->load->model('admin/notifications_model');
        $this->load->model('admin/classes_model');
        $this->load->model('admin/module_attachments_model');
        $this->load->model('admin/user_types_model');
        $this->load->model('admin/users_model');
        $this->load->model('admin/suppliers_model');
        $this->load->model('admin/stock_classifications_model');
        $this->load->model('admin/general_group_model');
        $this->load->model('admin/receiving_reports_model');
        $this->load->model('admin/receiving_report_items_model');
        $this->load->model('admin/item_location_model');
        $this->load->model('admin/inventory_changes_model');
        $this->load->model('admin/business_units_model');

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));
        //class ID
        $this->class_id = $this->classes_model->getFieldValue('id_class', array('class_name' => $this->classname));
        //initiate models
        $this->item_location_model = new Item_Location_Model();
        $this->inventory_changes_model = new Inventory_Changes_Model();
        $this->business_units_model = new Business_Units_Model();

        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_business_unit',
            ),
            'group_code' => array(
                'label' => 'Group',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-2',
                'var-value' => 'group_code',
            ),
            'business_unit_name' => array(
                'label' => 'Business Unit Name',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-4',
                'var-value' => 'business_unit_name',
            ),
            'business_unit_code' => array(
                'label' => 'Business Unit Code',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-2',
                'var-value' => 'business_unit_code',
            ),
        );

        $this->contents = array('model_directory' => 'admin/business_units_model',
            'model_name' => 'business_units_model',
            'filters' => array(),
            'functionName' => 'BU Inventory'); // use to call functions for access

        $this->tools->setPostArray('sort', array('sort_by' => 'id_business_unit', 'sort_type' => 'DESC'));
    }

    function index() {
        redirect(admin_dir('business_units_inventory/list_bu_inventory'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_bu_inventory() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'bu-inventory-tools.js'))),
        );
        $this->load->view(admin_dir('business_units_inventory/list_bu_inventory'), $data);
    }

    function view_bu_inventory() {
        $id_business_unit = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Purchase Order if Exist */
        $row = $this->business_units_model->getFields($id_business_unit);
        $il_items = $this->item_location_model->selectDistinctItems(array('il.location_id' => $id_business_unit, 'item_count >' => 0), '', array('id_item', 'item_unit_cost'))->result();
        $user_logs_model = new User_Logs_Model();
        $logs = $user_logs_model->getSearch(array('user_log_table' => 'item_location', 'user_log_value' => $id_business_unit), array(), array('id_user_log' => 'DESC'))->result();

        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'bu-inventory-tools.js'))),
                'row' => $row,
                'il_items' => $il_items,
                'logs' => $logs,
            );
            $this->load->view(admin_dir('business_units_inventory/view_bu_inventory'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function edit_bu_inventory() {
        $id_business_unit = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Purchase Order if Exist */
        $row = $this->business_units_model->getFields($id_business_unit);
        $il_items = $this->item_location_model->getList(array('il.location_id' => $id_business_unit))->result();
        $user_logs_model = new User_Logs_Model();
        $logs = $user_logs_model->getSearch(array('user_log_table' => 'item_location', 'user_log_value' => $id_business_unit), array(), array('id_user_log' => 'DESC'))->result();

        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'bu-inventory-tools.js'))),
                'row' => $row,
                'il_items' => $il_items,
                'logs' => $logs,
            );
            $this->load->view(admin_dir('business_units_inventory/edit_bu_inventory'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_bu_inventory') { /* Method for Account */
            self::_method_list_bu_inventory();
        } else if ($this->uri->rsegment(3) == 'view_bu_inventory') {
            self::view_bu_inventory();
        } else if ($this->uri->rsegment(3) == 'edit_bu_inventory') {
            self::_method_edit_bu_inventory();
        } else if ($this->uri->rsegment(3) == 'add_comment') {
            self::_method_add_comment();
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Validation of the Form -------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    private function _validate() {
        if ($this->tools->getPost('qty_edit')) {
            foreach ($this->tools->getPost('qty_edit') as $key => $val) {
                $item_qty = $this->item_location_model->getFieldValue('item_count', array('id_item_location' => $key));
                $this->form_validation->set_rules("qty_edit[$key]", 'Qty Edit', 'htmlspecialchars|trim|required');
                if ($item_qty != $val) {
                    $this->form_validation->set_rules("remarks[$key]", 'Remarks', 'htmlspecialchars|trim|required');
                }
            }
        }
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_bu_inventory() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }
        $condition = array();
        //show purchase requisitions requested by the business_unit only if user is not PEM, FM, CEO,OM or Super Admin
        if ((int) $this->session->userdata['admin']['user_type_id'] != 1) {
            $user_type_code = $this->session->userdata['admin']['user_type_code'];
            switch ($user_type_code) {
                case 'PU Head':
                    break;
                default:
                    $group_id = $this->users_model->getValue($this->session->userdata['admin']['user_id'], 'general_group_id');
                    $condition = array('du.business_unit_id' => $this->session->userdata['admin']['business_unit_id'],
                        'bu.general_group_id' => $group_id);
                    break;
            }
        }
        //set condition for the list
        $condition_string = '';
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_business_unit') {
                        $condition = array_merge($condition, array($var => $val));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get Purchase Order List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array('');

        $this->load->view(admin_dir('lists/list'), $data);
    }

    /*
     * 	Edit Manual Adjustment
     */

    function _method_edit_bu_inventory() {
        self::_validate();

        $id_business_unit = $this->Misc->decode_id($this->tools->getPost('business_unit_id'));
        $row = $this->business_units_model->getFields($id_business_unit);

        if ($row) {
            if ($this->form_validation->run() == TRUE) {
                $this->db->trans_start();
                $items = $this->tools->getPost('qty_edit');
                $adjustment_date = date('Y-m-d', strtotime($this->tools->getPost('adjustment_date')));
                foreach ($items as $item_location_id => $value) {
                    //edit manual inventory
                    $item_qty = $this->item_location_model->getFieldValue('item_count', array('id_item_location' => $item_location_id));
                    if ($item_qty != $value) {
                        $remarks = $this->tools->getPost('remarks', $item_location_id);
                        $this->item_location_model->update(array('id_item_location' => $item_location_id), array('item_count' => $value));
                        $new_qty = (int) ($item_qty + $value);
                        $transaction = 'Manual Adjustment (+)';
                        if ($value < $item_qty) {
                            $transaction = 'Manual Adjustment (-)';
                        }
                        //update data to inventory change
                        $inventory_data = array(
                            'item_location_id' => $item_location_id,
                            'administrator_id' => $this->session->userdata['admin']['user_id'],
                            'stock_before' => $item_qty,
                            'stock_quantity' => $value,
                            'stock_after' => $new_qty,
                            'inventory_date' => $adjustment_date,
                            'remarks' => $remarks, //from manual adjustment
                            'transaction' => $transaction, //from manual adjustment
                            'added_by' => $this->session->userdata['admin']['user_id'],
                            'added_date' => date('Y-m-d H:i:s')
                        );
                        $this->inventory_changes_model->add($inventory_data);
                        parent::save_log("Manual Adjustment from $item_qty to $new_qty: $remarks", 'item_location', $item_location_id);
                    }
                }
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Successfully Edited manualy'),
                    'id' => 1
                );
                $this->db->trans_complete();
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	save comments
     */

    function _method_add_comment() {
        if (IS_AJAX) {
            $id_business_unit = $this->Misc->decode_id($this->tools->getPost('id'));
            /* Check Purchase Order if Exist */
            $row = $this->business_units_model->getFields($id_business_unit);
            if ($row) {
                $this->form_validation->set_rules('comment', 'Comment', 'htmlspecialchars|trim|required');
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    // save comment as log
                    parent::save_log($this->tools->getPost('comment'), 'item_location', $id_business_unit);

                    //notify business_unit requestor
                    $status = "Comment: " . $this->tools->getPost('comment');
                    self::notify_creator($row->added_by, $id_business_unit, $status);

                    $this->db->trans_complete();
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Saved Remarks'),
                        'id' => 1
                    );
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * Notify creator of PO
     */

    private
            function notify_creator($added_by, $id_business_unit, $status) {
        //notify Initiator for declined PO
        $notif_data = array(
            'class_id' => $this->class_id,
            'reference_id' => $id_business_unit,
            'user_id' => $added_by,
            'link' => 'view_bu_inventory',
            'status' => $status);
        $notification_model = new Notifications_Model();
        $notification_model->add($notif_data);
    }

}
