<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | KPI Report - Invoice Submission Records Class
  |--------------------------------------------------------------------------
  |
  | Handles the KPI Report - Invoice Submission Records panel
  |
  | @category		Controller
  | @author		melin
 */

class KPI_Invoice_Submission extends Admin_Core {

    public $purchase_order;
    public $payment_requests_model;

    function __construct() {
        parent:: __construct();

        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);

        $this->load->model('admin/purchase_orders_model');
        $this->load->model('admin/general_group_model');
        $this->load->model('admin/payment_requests_model');
        $this->load->model('admin/users_model');

        $this->purchase_order = new Purchase_Orders_Model();
        $this->payment_requests_model = new Payment_Requests_Model();
        $this->users_model = new Users_Model();

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        //for list view
        $this->list_content = array(
            'id' => array(
                'label' => 'ID',
                'type' => 'hidden',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => '',
                'var-value' => 'id_purchase_requisition',
            ),
            'emergency' => array(
                'label' => 'Category',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'emergency',
            ),
            'po_number' => array(
                'label' => 'PO Number',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'po_number',
                'nowrap' => 1
            ),
            'date_received' => array(
                'label' => 'Date Served',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'date_received',
                'nowrap' => 1
            ),
            'prf_number' => array(
                'label' => 'PRF Number',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'prf_number',
                'nowrap' => 1
            ),
            'date_forwarded_acctg' => array(
                'label' => 'Date Forwarded to Accounting',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'date_forwarded_acctg',
                'nowrap' => 1
            ),
            'served_forwarded' => array(
                'label' => 'No. of Days (Date Served vs Date Forwarded to Accounting) ',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'served_forwarded',
            ),
        );

        $this->contents = array('model_directory' => 'admin/payment_requests_model',
            'model_name' => 'payment_requests_model',
            'filters' => array(),
            'functionName' => 'Record',); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('reports/kpi_invoice_submission'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_record() {
        $data = array(
            'template' => parent::main_template(),
        );

        $this->load->view(admin_dir('reports/kpi_invoice_submission'), $data);
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if (($this->uri->rsegment(3) == 'list_record') || ($this->uri->rsegment(3) == 'pdf_report')) { /* Method for Account */
            self::_method_list_record();
        }
    }

    /* ---------- ENROLLMENT LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_record() {
        if (!IS_AJAX && ($this->uri->rsegment(3) == 'list_record')) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }

        //set condition for the list
        $condition = array();
        $condition_string = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    $date_from = $this->tools->getPost('search', 'date_from');
                    $date_to = $this->tools->getPost('search', 'date_to');
                    if ($var == 'user_name') {
                        $condition_string = array_merge($condition_string, array("du`.`user_fname` LIKE '%" . trim($val) . "%' OR `du`.`user_lname` LIKE '%" . trim($val) . "%'"));
                    } elseif ($var == 'po_number') {
                        $condition_string = array_merge($condition_string, array("`po`.`po_number` LIKE '%" . trim($val) . "%'"));
                    } elseif ($var == 'date_from') {
                        if (isset($date_to)) {
                            $condition_string = array_merge($condition_string, array("pr.date_received BETWEEN '$val' AND '$date_to'"));
                        }
                    } elseif ($var == 'date_to') {
                        $condition_string = array_merge($condition_string, array("pr.date_received BETWEEN '$date_from' AND '$val'"));
                    } elseif ($var == 'served_forwarded') {
                        $aging_where = "";
                        $test = (!((int) strpos($val, '>') < 0) || !((int) strpos($val, '>=') < 0) || !((int) strpos($val, '<') < 0) || !((int) strpos($val, '<=') < 0));
                        if ($test) {
                            $aging_where .= "(SELECT DATEDIFF(prf.date_forwarded_acctg,rr.date_received ) = $val AND `rr`.`date_received` != '0000-00-00' AND `prf`.`date_forwarded_acctg` != '0000-00-00')";
                            $condition_string = array_merge($condition_string, array($aging_where));
                        }
                    } elseif ($var == 'date_received') {
                        $condition = array_merge($condition, array("rr.date_received like" => "%" . date('Y-m-d', strtotime($val)) . "%"));
                    } elseif ($var == 'date_forwarded_acctg') {
                        $condition = array_merge($condition, array("prf.date_forwarded_acctg like" => "%" . date('Y-m-d', strtotime($val)) . "%"));
                    } elseif ($var == 'emergency') {
                        if (strtolower($val) == 'emergency') {
                            $condition = array_merge($condition, array("prf.emergency" => 1));
                        } else {
                            $condition = array_merge($condition, array("prf.emergency" => 0));
                        }
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get KPI Report - Invoice Submission Records List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array(); // to long to be poped-up

        foreach ($data['list']->result() as $q) {
            $q->emergency = ($q->emergency) ? 'Emergency' : 'Non-emergency';
            $q->served_forwarded = '';
            $date1 = $q->date_received;
            $date2 = $q->date_forwarded_acctg;

            if (strtotime($date1) > 0 && strtotime($date2) > 0) {
                $diff = date_diff(date_create($date1), date_create($date2));
                $q->served_forwarded = $diff->format("%a") + 1;
            }
        }
        if ($this->uri->rsegment(3) == 'pdf_report') {
            $data['title'] = 'KPI Report - Invoice Submission Records Report';
            parent::save_log("KPI Report - Invoice Submission Records PDF Report Generated", 'report_rec_iss', '', 'kpi_invoice_submission_pdf');
            $this->load->view(admin_dir('reports/kpi_invoice_submission_pdf'), $data);
        } else {
            $this->load->view(admin_dir('lists/reports/kpi_invoice_submission'), $data);
        }
    }

}
