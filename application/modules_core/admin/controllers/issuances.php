<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Issuances Class
  |--------------------------------------------------------------------------
  |
  | Handles the Issuances panel
  |
  | @category		Controller
  | @author		melin
 */

class Issuances extends Admin_Core {

    public $issuances_model = '';
    public $issuance_items_model = '';
    public $item_location_model = '';
    public $list_content = array();
    public $contents = array();
    public $users_model;

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        //load models

        $this->load->model('admin/purchase_requisitions_model');
        $this->load->model('admin/purchase_requisition_items_model');
        $this->load->model('admin/purchase_orders_model');
        $this->load->model('admin/purchase_order_items_model');
        $this->load->model('admin/document_types_model');
        $this->load->model('admin/business_units_model');
        $this->load->model('admin/items_model');
        $this->load->model('admin/notifications_model');
        $this->load->model('admin/classes_model');
        $this->load->model('admin/module_attachments_model');
        $this->load->model('admin/user_types_model');
        $this->load->model('admin/users_model');
        $this->load->model('admin/stock_classifications_model');
        $this->load->model('admin/general_group_model');
        $this->load->model('admin/receiving_reports_model');
        $this->load->model('admin/receiving_report_items_model');
        $this->load->model('admin/issuances_model');
        $this->load->model('admin/issuance_items_model');
        $this->load->model('admin/item_location_model');
        $this->load->model('admin/inventory_changes_model');

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));
        //class ID
        $this->class_id = $this->classes_model->getFieldValue('id_class', array('class_name' => $this->classname));
        //initiate models
        $this->purchase_orders_model = new Purchase_Orders_Model();
        $this->purchase_order_items_model = new Purchase_Order_Items_Model();
        $this->purchase_requisitions_model = new Purchase_Orders_Model();
        $this->purchase_requisition_items_model = new Purchase_Requisition_Items_Model();
        $this->document_types_model = new Document_Types_Model();
        $this->stock_classifications_model = new Stock_Classifications_Model();
        $this->general_group_model = new General_Group_Model();
        $this->users_model = new Users_Model();
        $this->rr_model = new Receiving_Reports_Model();
        $this->issuance_items_model = new Receiving_Report_Items_Model();
        $this->issuances_model = new Issuances_Model();
        $this->issuance_items_model = new Issuance_Items_Model();
        $this->item_location_model = new Item_Location_Model();
        $this->inventory_changes_model = new Inventory_Changes_Model();
        $this->business_units_model = new Business_Units_Model();

        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_issuance',
            ),
            'issuance_no' => array(
                'label' => 'Issuance No',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'issuance_no',
            ),
            'issuance_date' => array(
                'label' => 'Issuance Date',
                'type' => 'datepicker',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'issuance_date',
            ),
            'bu_name_to' => array(
                'label' => 'Business Unit To',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'bu_name_to',
            ),
            'remarks' => array(
                'label' => 'Remarks',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'remarks',
            ),
            'status' => array(
                'label' => 'Status',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'status',
            ),
        );

        $this->contents = array('model_directory' => 'admin/issuances_model',
            'model_name' => 'issuances_model',
            'filters' => array(),
            'functionName' => 'Issuance'); // use to call functions for access

        $this->tools->setPostArray('sort', array('sort_by' => 'id_issuance', 'sort_type' => 'DESC'));
    }

    function index() {
        redirect(admin_dir('issuances/list_issuance'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_issuance() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'issuances-tools.js'))),
        );
        $this->load->view(admin_dir('issuances/list_issuance'), $data);
    }

    function add_issuance() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'issuances-tools.js'))),
            'items' => $this->item_location_model->getList(array('bu.enabled' => 1), array("bu.business_unit_code like '%CID%'"))->result(),
            'business_units' => $this->business_units_model->getList(array(), array("business_unit_code NOT LIKE '%CID%'"))->result(),
            'stock_classifications' => $this->item_location_model->selectDistinct('stock_classification_name', 'stock_classification_name,id_stock_classification', array(), array('id_stock_classification'))->result(),
        );
        $this->load->view(admin_dir('issuances/add_issuance'), $data);
    }

    function edit_issuance() {
        $id_issuance = $this->Misc->decode_id($this->uri->rsegment(3));
        $row = $this->issuances_model->getFields($id_issuance);
        if ($row && (int) $row->date_released == '0000-00-00') {
            $issuance_items = $this->issuance_items_model->getList(array('issuance_id' => $id_issuance), array(), array('id_item' => 'ASC'))->result();
            foreach ($issuance_items as $q) {
                $qty_queued = $this->issuance_items_model->get_queued('item_quantity', array('item_id' => $q->item_id, 'issuance_id !=' => $id_issuance, 'item_released = ' => 0));
                $qty_available = $this->item_location_model->selectDistinctItems(array('il.item_id' => $q->item_id), array("business_unit_code LIKE '%CID%'"), array('id_item'))->row();
                $q->qty_queued = ($qty_queued) ? $qty_queued : 0;
                $q->qty_available = ($qty_available->item_count) ? $qty_available->item_count : 0;
            }

            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'issuances-tools.js'))),
                'row' => $row,
                'issuance_items' => $issuance_items,
                'business_units' => $this->business_units_model->getList(array(), array("business_unit_code NOT LIKE '%CID%'"))->result(),
                'stock_classifications' => $this->item_location_model->selectDistinct('stock_classification_name', 'stock_classification_name,id_stock_classification', array(), array('id_stock_classification'))->result(),
            );
            $this->load->view(admin_dir('issuances/edit_issuance'), $data);
        } else {
            $this->session->set_flashdata('note', $this->Misc->message_status('error', "Unreleased items can be edited."));
            redirect(admin_dir($this->classname . '/list_issuance'));
        }
    }

    function view_issuance() {
        $id_issuance = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Purchase Order if Exist */
        $row = $this->issuances_model->getFields($id_issuance);
        $user_logs_model = new User_Logs_Model();
        $logs = $user_logs_model->getSearch(array('user_log_table' => 'issuances', 'user_log_value' => $id_issuance), array(), array('id_user_log' => 'DESC'))->result();

        //receiving items
        $issuance_items = $this->issuance_items_model->getList(array('issuance_id' => $id_issuance))->result();
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'issuances-tools.js'))),
                'row' => $row,
                'logs' => $logs,
                'issuance_items' => $issuance_items,
            );
            $this->load->view(admin_dir('issuances/view_issuance'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function print_issuance() {
        $id_issuance = $this->Misc->decode_id($this->uri->rsegment(4));
        /* Check Purchase Order if Exist */
        $row = $this->issuances_model->getFields($id_issuance); //receiving items
        $issuance_items = $this->issuance_items_model->getList(array('issuance_id' => $id_issuance))->result();
        if ($row) {
            $data = array(
                'row' => $row,
                'issuance_items' => $issuance_items,
            );
            parent::save_log("printed issuance form", 'issuances', $id_issuance);
            $this->load->view(admin_dir('issuances/extra/issuance_form'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Validation of the Form -------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    private function _validate() {
        $this->form_validation->set_rules('issuance_date', 'Issuance Date', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('bu_id_to', 'Business Unit TO', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('issuance_remarks', 'Issuance Remarks', 'htmlspecialchars|trim');
        $this->form_validation->set_rules('items', 'Items to Issue', 'required');
        if (!empty($this->tools->getPost('items'))) {
            foreach ($this->tools->getPost('items') as $key => $q) {
                $this->form_validation->set_rules("qty_to_issue[$key]", 'QTY to Issue', 'integer|trim|required|greater_than_equal_to[0]');
                $this->form_validation->set_rules("remarks[$key]", 'QTY to Issue', 'htmlspecialchars|trim');
            }
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_issuance') { /* Method for Account */
            self::_method_list_issuance();
        } else if ($this->uri->rsegment(3) == 'view_issuance') {
            self::view_issuance();
        } else if ($this->uri->rsegment(3) == 'add_issuance') {
            self::_method_add_issuance();
        } else if ($this->uri->rsegment(3) == 'edit_issuance') {
            self::_method_edit_issuance();
        } else if ($this->uri->rsegment(3) == 'add_comment') {
            self::_method_add_comment();
        } else if ($this->uri->rsegment(3) == 'print_issuance') {
            self::print_issuance();
        } else if ($this->uri->rsegment(3) == 'release_issuance') {
            self::_method_release_issuance();
        } else if ($this->uri->rsegment(3) == 'select_location_item') {
            self::_method_select_location_item();
        } else if ($this->uri->rsegment(3) == 'get_item_detail') {
            self::_method_get_item_detail();
        } else if ($this->uri->rsegment(3) == 'delete_issuance') {
            self::_method_delete_issuance();
        }
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    /*
     * ADD details to PRF
     */

    function _method_add_issuance() {
        if (IS_AJAX) {
            self::_validate();
            if ($this->form_validation->run() == TRUE) {
                $this->issuances_model = new Issuances_Model();

                $this->db->trans_start();
                //receiving report
                $last_issuance_no = $this->issuances_model->getLastNo('issuance_no', 'added_date', date('Y-m'));
                $issuance_no = isset($last_issuance_no) ? 'I-' . ((int) str_replace('-', '', filter_var($last_issuance_no, FILTER_SANITIZE_NUMBER_INT)) + 1) : 'I-' . date('ym') . '00001'; // set purchase requisition number
                //accounting data                
                $issuance_data = array(
                    'issuance_no' => $issuance_no,
                    'issuance_date' => date('Y-m-d', strtotime($this->tools->getPost('issuance_date'))),
                    'bu_id_from' => $this->business_units_model->getFieldValue('id_business_unit', array(), array('business_unit_code' => 'CID')),
                    'bu_id_to' => $this->tools->getPost('bu_id_to'),
                    'remarks' => $this->tools->getPost('issuance_remarks'),
                    'status' => 'Release Items',
                    'added_by' => $this->session->userdata['admin']['user_id'],
                    'added_date' => date('Y-m-d H:i:s'),
                );

                if ($issuance_id = $this->issuances_model->add($issuance_data)) {
                    //save issuance items
                    $issuance_items = $this->tools->getPost('items');
                    foreach ($issuance_items as $key => $item_id) {
                        //add items to issue is greater than zero
                        if ((int) $this->tools->getPost('qty_to_issue', $key) > 0) {
                            $issuance_items = array(
                                'issuance_id' => $issuance_id,
                                'item_id' => $item_id,
                                'item_quantity' => $this->tools->getPost('qty_to_issue', $key)
                            );
                            $this->issuance_items_model->add($issuance_items);
                        }
                    }

                    parent::save_log("added Issuance# $issuance_no ", 'issuances', $issuance_id);
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successfuly saved transaction'),
                        'id' => 1
                    );
                    $this->db->trans_complete();
                }
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    /*
     * EDIT details to PRF
     */

    function _method_edit_issuance() {
        if (IS_AJAX) {
            self::_validate();
            $id_issuance = $this->Misc->decode_id($this->tools->getPost('id'));
            $row = $this->issuances_model->getFields($id_issuance);
            if ($this->form_validation->run() == TRUE) {
                if ($row) {
                    $this->db->trans_start();
                    //accounting data                
                    $issuance_data = array(
                        'issuance_date' => date('Y-m-d', strtotime($this->tools->getPost('issuance_date'))),
                        'bu_id_to' => $this->tools->getPost('bu_id_to'),
                        'remarks' => $this->tools->getPost('issuance_remarks'),
                        'updated_by' => $this->session->userdata['admin']['user_id'], //from accounting user
                        'updated_date' => date('Y-m-d H:i:s'),
                    );

                    if ($this->issuances_model->update(array('id_issuance' => $id_issuance), $issuance_data)) {
                        $existing_items = $this->issuance_items_model->getListEdit(array('issuance_id' => $id_issuance))->result();
                        $issuance_items = $this->tools->getPost('items');
                        foreach ($existing_items as $q) {
                            if (in_array($q->item_id, $issuance_items)) {
                                $sale_item_data = array('item_quantity' => $this->tools->getPost('qty_to_issue', $q->item_id),
                                    'enabled' => 1);
                                $this->issuance_items_model->update(array('item_id' => $q->item_id), $sale_item_data);
                                unset($issuance_items[$q->item_id]);
                            } else {
                                $this->issuance_items_model->update(array('item_id' => $q->item_id), array('enabled' => 0));
                            }
                        }

                        if (!empty($issuance_items)) {
                            foreach ($issuance_items as $key => $item_id) {
                                $issuance_items = array(
                                    'issuance_id' => $id_issuance,
                                    'item_id' => $item_id,
                                    'item_quantity' => $this->tools->getPost('qty_to_issue', $key),
                                );
                                $this->issuance_items_model->add($issuance_items);
                            }
                        }
                    }
                    parent::save_log("updated this transaction", 'issuances', $id_issuance);
                    $this->db->trans_complete();
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successfuly saved transaction'),
                        'id' => 1
                    );
                }
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    function _method_list_issuance() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }
        $condition = array();
        //show purchase requisitions requested by the business_unit only if user is not PEM, FM, CEO,OM or Super Admin
        if ((int) $this->session->userdata['admin']['user_type_id'] != 1) {
            $user_type_code = $this->session->userdata['admin']['user_type_code'];
            switch ($user_type_code) {
                case 'PU Head':
                    break;
                default:
                    $group_id = $this->users_model->getValue($this->session->userdata['admin']['user_id'], 'general_group_id');
                    $condition = array('du.business_unit_id' => $this->session->userdata['admin']['business_unit_id'],
                        'bu.general_group_id' => $group_id);
                    break;
            }
        }
        //set condition for the list
        $condition_string = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_issuance') {
                        $condition = array_merge($condition, array($var => $val));
                    } elseif ($var == 'due_date') {
                        $condition = array_merge($condition, array('prf.' . $var . " like" => "%" . $val . "%"));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get Purchase Order List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array('');
        $this->load->view(admin_dir('lists/list'), $data);
    }

    /*
     * RELEASE details to PRF
     */

    function _method_release_issuance() {
        if (IS_AJAX) {
            $this->form_validation->set_rules('date_released', 'Release Date', 'htmlspecialchars|trim|required');
            $id_issuance = $this->Misc->decode_id($this->tools->getPost('id'));
            $row = $this->issuances_model->getFields($id_issuance);
            if ($this->form_validation->run() == TRUE) {
                if ($row) {
                    $this->db->trans_start();
                    $release_date = date('Y-m-d', strtotime($this->tools->getPost('date_released')));
                    $issuance_items = $this->issuance_items_model->getList(array('issuance_id' => $id_issuance))->result();
                    $item_unavailable = '';
                    $total_quantity = 0;
                    $total_released = 0;
                    foreach ($issuance_items as $q) {
                        $total_quantity += $q->item_quantity;
                        $check_if_available = $this->item_location_model->getSum('item_count', 'total_available', array('item_id' => $q->id_item,
                            'location_id' => $row->bu_id_from,
                            't.item_count >' => 0));

                        if ($check_if_available) {
                            //get item location based on commodity id
                            $items = $this->item_location_model->getList(array('il.item_id' => $q->id_item,
                                        'location_id' => $row->bu_id_from,
                                        'item_count >' => 0))->result();
                            $qty_to_release = $q->item_quantity;
                            foreach ($items as $qq) {
                                if ($qty_to_release > 0) {
                                    $stock_before = $qq->item_count;
                                    if ($qty_to_release <= $qq->item_count) { //release
                                        $to_release = $qty_to_release;
                                        $total_released += $to_release;
                                        $n_item_count = ($qq->item_count - $qty_to_release);
                                        //subtract from location's inventory (purchasing)
                                        $this->item_location_model->update(array('id_item_location' => $qq->id_item_location), array('item_count' => $n_item_count));
                                    } else { //subtract all item_quantity
                                        $to_release = ($qq->item_count);
                                        $total_released += $to_release;
                                        $this->item_location_model->update(array('id_item_location' => $qq->id_item_location), array('item_count' => 0));
                                    }
                                    //add to new location's inventory (business unit TO)
                                    $n_item_data = array(
                                        'parent_id' => $qq->id_item_location,
                                        'location_id' => $row->bu_id_to,
                                        'po_item_id' => $qq->po_item_id,
                                        'item_id' => $qq->item_id,
                                        'item_quantity' => $to_release,
                                        'item_count' => $to_release,
                                        'added_by' => $this->session->userdata['admin']['user_id'],
                                        'added_date' => date('Y-m-d H:i:s')
                                    );
                                    $n_item_loc_id = $this->item_location_model->add($n_item_data);

                                    //update issuance item details
                                    $this->issuance_items_model->update(array('item_id' => $qq->id_item, 'issuance_id' => $id_issuance, 'enabled' => 1), array('item_released' => $to_release, 'item_location_id' => $n_item_loc_id));

                                    //update inventory changes
                                    $inventory_data_from = array(
                                        'item_location_id' => $qq->id_item_location,
                                        'administrator_id' => $this->session->userdata['admin']['user_id'],
                                        'stock_before' => $stock_before,
                                        'stock_quantity' => $to_release,
                                        'stock_after' => $n_item_count,
                                        'inventory_date' => $release_date,
                                        'transaction' => 'Issuance (-)', //subtract from issuance
                                        'added_by' => $this->session->userdata['admin']['user_id'],
                                        'added_date' => date('Y-m-d H:i:s')
                                    );
                                    $this->inventory_changes_model->add($inventory_data_from);

                                    $inventory_data_to = array(
                                        'item_location_id' => $n_item_loc_id,
                                        'administrator_id' => $this->session->userdata['admin']['user_id'],
                                        'stock_before' => 0,
                                        'stock_quantity' => $to_release,
                                        'stock_after' => $to_release,
                                        'inventory_date' => $release_date,
                                        'transaction' => 'Issuance (+)', //add from issuance
                                        'added_by' => $this->session->userdata['admin']['user_id'],
                                        'added_date' => date('Y-m-d H:i:s')
                                    );
                                    $this->inventory_changes_model->add($inventory_data_to);

                                    //track how many more to release
                                    $qty_to_release = ($qty_to_release - $to_release);
                                    if ($qty_to_release > 0) {
                                        break;
                                    }
                                }
                            }
                        } else {
                            $item_unavailable .= ($item_unavailable) ? ', ' . $q->sku : $q->sku;
                        }
                    }
                    if ($total_released == $total_quantity) {
                        $this->issuances_model->update(array('id_issuance' => $id_issuance), array('date_released' => $release_date,
                            'released_by' => $this->session->userdata['admin']['user_id'],
                            'status' => 'Released'));
                    } else {
                        $this->issuances_model->update(array('id_issuance' => $id_issuance), array('date_released' => $release_date,
                            'released_by' => $this->session->userdata['admin']['user_id'],
                            'status' => 'Partially Released'));
                    }
                    parent::save_log("released items in this transaction", 'issuances', $id_issuance);
                    $this->db->trans_complete();
                    $message = 'Successfuly saved transaction.';
                    if ($item_unavailable) {
                        $message .= 'Some items were unavailable: ' . $item_unavailable;
                    }
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', $message),
                        'id' => 1
                    );
                }
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	Delete Payment Request Method
     */

    function _method_delete_issuance() {
        if (IS_AJAX) {
            $issuance_id = $this->Misc->decode_id($this->tools->getPost('item'));
            $row = $this->issuances_model->getFields($issuance_id);
            if ($row && (int) $row->released_by == 0) {
                $this->db->trans_start();
                $datas = array(
                    'enabled' => 0,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete Purchase Order */
                $this->issuances_model->update_table($datas, "id_issuance", $issuance_id);
                parent::save_log("Deleted", 'issuances', $issuance_id);
                $this->db->trans_complete();
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Issuace Request was deleted'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            } else {
//                $arr = array(
//                    'message_alert' => $this->Misc->message_status('success', "RFP that was approved can't be deleted"),
//                    'id' => 0
//                );
//                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
                $this->session->set_flashdata('note', $this->Misc->message_status('error', "Issuance that was approved can't be deleted."));
                redirect(admin_dir($this->classname . '/list_issuance'));
            }
        }
    }

    /*
     * 	save comments
     */

    function _method_add_comment() {
        if (IS_AJAX) {
            $id_issuance = $this->Misc->decode_id($this->tools->getPost('id'));
            /* Check Purchase Order if Exist */
            $row = $this->issuances_model->getFields($id_issuance);
            if ($row) {
                $this->form_validation->set_rules('comment', 'Comment', 'htmlspecialchars|trim|required');
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    // save comment as log
                    parent::save_log($this->tools->getPost('comment'), 'issuances', $id_issuance);

                    //notify business_unit requestor
                    $status = "Comment: " . $this->tools->getPost('comment');
                    self::notify_creator($row->added_by, $id_issuance, $status);

                    $this->db->trans_complete();
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Saved Remarks'),
                        'id' => 1
                    );
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * Function formatting Stock Classifications on dropdown based on commodity type
     */

    function _method_select_location_item() {
        if (!IS_AJAX) {
            $arr = array(
                'message_alert' => $this->Misc->message_status('error', 'Forbidden Direct Access'),
                'id' => 1
            );
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
        $where_string = array();
        $item_ids = '';
        $added_items = $this->tools->getPost('added_items');
        if (!empty($added_items)) {
            foreach ($added_items as $key => $id_item) {
                $item_ids = ($item_ids) ? $item_ids . ',' . $id_item : $id_item;
            }
        }
        $this->users_model = new Users_Model();
        $users = $this->users_model->getFields($this->session->userdata['admin']['user_id']);
        $stock_classification_id = $this->tools->getPost('stock_classification_id');
        $where = array('c.general_group_id' => $users->general_group_id,
            'stock_classification_id' => $stock_classification_id,
            'item_count >' => 0);
        $where_string[0] = "bu.business_unit_code like '%CID%'";
        if ($item_ids != '') {
            $where_string[1] = "id_item NOT IN ($item_ids)";
        }
        $items = $this->item_location_model->selectDistinctItems($where, $where_string, array('id_item'))->result();
        $data = array('items' => $items,
            'label' => 'Select Item',
            'id' => 'id_item',
            'value' => 'description',
            'value2' => 'item_count');
        $this->load->view(admin_dir('template/select_template'), $data);
    }

    /*
     * 	get Item Detail
     */

    function _method_get_item_detail() {
        if (!IS_AJAX) {
            $arr = array(
                'message_alert' => $this->Misc->message_status('error', 'Forbidden Direct Access'),
                'id' => 1
            );
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }

        $item_ids = '';
        $items = array();
        $qty_queued = array();
        $where_string = array();
        $selected_items = $this->tools->getPost('selected_items');

        $added_items = $this->tools->getPost('added_items');
        foreach ($selected_items as $key => $id_item) {
            if (!empty($added_items)) {
                if (!in_array($id_item, $added_items)) {
                    $item_ids = ($item_ids) ? $item_ids . ',' . $id_item : $id_item;
                    $qty_queued[$id_item] = $this->issuance_items_model->get_queued('item_quantity', array('item_id' => $id_item, 'item_released = ' => 0));
                }
            } else {
                $item_ids = ($item_ids) ? $item_ids . ',' . $id_item : $id_item;
                $qty_queued[$id_item] = $this->issuance_items_model->get_queued('item_quantity', array('item_id' => $id_item, 'item_released = ' => 0));
            }
        }
//echo '<pre>';
//print_R($this->db->last_query());
//echo '</pre>';

        $where_string[0] = "business_unit_code LIKE '%CID%'";
        if ($item_ids != '') {
            $where_string[1] = "id_item IN ($item_ids)";
            $items = $this->item_location_model->selectDistinctItems(array(), $where_string, array('id_item'))->result();
        }
        $data['items'] = $items;
        $data['qty_queued'] = $qty_queued;
        $this->load->view(admin_dir('issuances/extra/item_detail'), $data);
    }

    /*
     * Notify creator of PO
     */

    private function notify_creator($added_by, $id_issuance, $status) {
        //notify Initiator for declined PO
        $notif_data = array(
            'class_id' => $this->class_id,
            'reference_id' => $id_issuance,
            'user_id' => $added_by,
            'link' => 'view_issuance',
            'status' => $status);
        $notification_model = new Notifications_Model();
        $notification_model->add($notif_data);
    }

}
