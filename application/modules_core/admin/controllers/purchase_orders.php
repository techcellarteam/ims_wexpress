<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Purchase Orders Class
  |--------------------------------------------------------------------------
  |
  | Handles the Purchase Orders panel
  |
  | @category		Controller
  | @author		melin
 */

class Purchase_Orders extends Admin_Core {

    public $purchase_orders_model = '';
    public $purchase_orders_items_model = '';
    public $purchase_requisitions_model = '';
    public $purchase_order_items_model = '';
    public $list_content = array();
    public $contents = array();
    public $users_model;

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);

        parent:: __construct();
        //load models
        $this->load->model('admin/purchase_requisitions_model');
        $this->load->model('admin/purchase_requisition_items_model');
        $this->load->model('admin/purchase_orders_model');
        $this->load->model('admin/purchase_order_items_model');
        $this->load->model('admin/document_types_model');
        $this->load->model('admin/business_units_model');
        $this->load->model('admin/items_model');
        $this->load->model('admin/notifications_model');
        $this->load->model('admin/classes_model');
        $this->load->model('admin/module_attachments_model');
        $this->load->model('admin/user_types_model');
        $this->load->model('admin/users_model');
        $this->load->model('admin/suppliers_model');
        $this->load->model('admin/stock_classifications_model');
        $this->load->model('admin/general_group_model');
        $this->load->model('admin/receiving_reports_model');
        $this->load->model('admin/receiving_report_items_model');
        $this->load->model('admin/re_po_model');

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));
        //class ID
        $this->class_id = $this->classes_model->getFieldValue('id_class', array('class_name' => $this->classname));
        //initiate models
        $this->purchase_orders_model = new Purchase_Orders_Model();
        $this->purchase_order_items_model = new Purchase_Order_Items_Model();
        $this->purchase_requisitions_model = new Purchase_Requisitions_Model();
        $this->purchase_requisition_items_model = new Purchase_Requisition_Items_Model();
        $this->document_types_model = new Document_Types_Model();
        $this->stock_classifications_model = new Stock_Classifications_Model();
        $this->general_group_model = new General_Group_Model();
        $this->users_model = new Users_Model();
        $this->suppliers_model = new Suppliers_Model();
        $this->rr_model = new Receiving_Reports_Model();
        $this->rr_items_model = new Receiving_Report_Items_Model();
        $this->re_po_model = new Re_Po_Model();

        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_purchase_order',
            ),
            'po_number' => array(
                'label' => 'PO NO',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'po_number',
            ),
            'pr_number' => array(
                'label' => 'PR NO',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'pr_number',
            ),
            'supplier_code' => array(
                'label' => 'Supplier',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'supplier_code',
            ),
            'business_unit_code' => array(
                'label' => 'Business Unit',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'business_unit_code',
            ),
            'total_amount' => array(
                'label' => 'Total Amount',
                'type' => 'decimal',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'total_amount',
            ),
            'order_date' => array(
                'label' => 'Order Date',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'order_date',
            ),
            'status' => array(
                'label' => 'Status',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'status',
            ),
        );

        $this->contents = array('model_directory' => 'admin/purchase_orders_model',
            'model_name' => 'purchase_orders_model',
            'filters' => array(),
            'functionName' => 'Purchase Order'); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('purchase_orders/list_purchase_order'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_purchase_order() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'purchase-order-tools.js'))),
        );
        $this->load->view(admin_dir('purchase_orders/list_purchase_order'), $data);
    }

    function add_purchase_order() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'purchase-order-tools.js'))),
            'groups' => $this->general_group_model->getList()->result());
        $this->load->view(admin_dir('purchase_orders/add_purchase_order'), $data);
    }

    function edit_purchase_order() {
        $id_purchase_order = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Purchase Order if Exist */
        $row = $this->purchase_orders_model->getFields($id_purchase_order);
        if (($row->status != 'Queued to Purchasing Head') || ($row->status != 'Queued to BU Head')) {
            $arr = array(
                'message' => $this->Misc->message_status($this->Misc->message_status('error', "PO not yet APPROVED can be edited.")),
            );
            $this->load->view(admin_dir('template/deny'), $arr);
        }
        if ($row) {
            $where = array('c.general_group_id' => $row->general_group_id, 'pr.id_purchase_requisition' => $row->purchase_requisition_id);
            $stock_classifications = $this->purchase_requisition_items_model->selectDistinct('id_stock_classification', 'id_stock_classification, stock_classification_name', 'AND', $where)->result();
            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'purchase-order-tools.js'))),
                'row' => $row,
                'suppliers' => $this->suppliers_model->getList(array('general_group_id' => $row->general_group_id))->result(),
                'stock_classifications' => $stock_classifications,
                'po_items' => $this->purchase_order_items_model->getList(array('purchase_order_id' => $id_purchase_order))->result(),
            );
            $this->load->view(admin_dir('purchase_orders/edit_purchase_order'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function view_purchase_order() {
        $this->module_attachments = new Module_Attachments_Model();
        $id_purchase_order = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Purchase Order if Exist */
        $row = $this->purchase_orders_model->getFields($id_purchase_order);
        $po_items = $this->purchase_order_items_model->getList(array('purchase_order_id' => $id_purchase_order))->result();
        $module_attachments = $this->module_attachments->getList(array('dc.class_name' => $this->classname, 'reference_id' => $id_purchase_order))->result();
        $user_logs_model = new User_Logs_Model();
        $logs = $user_logs_model->getSearch(array('class_name' => $this->classname, 'user_log_value' => $id_purchase_order), array(), array('id_user_log' => 'DESC'))->result();

        if ($row->first_level_id) {
            $first_level = $this->users_model->getFields($row->first_level_id);
            $row->first_level = ($first_level) ? "$first_level->user_fname $first_level->user_lname [$first_level->business_unit_name]" : '---';
        }

        if ($row->approval_date != '0000-00-00 00:00:00') {
            $approver = $this->users_model->getFields($row->approved_by);
            $row->approved = "$approver->user_fname $approver->user_lname [$approver->business_unit_name]";
        }

        if ($row->declined_date != '0000-00-00 00:00:00') {
            $approver = $this->users_model->getFields($row->approved_by);
            $row->declined = "$approver->user_fname $approver->user_lname [$approver->business_unit_name]";
        }

        if ($row->second_level_id) {
            $second_level = $this->users_model->getFields($row->second_level_id);
            $row->second_level = ($second_level) ? "$second_level->user_fname $second_level->user_lname [$second_level->business_unit_name]" : '---';
        }
        if ($row->third_level_id) {
            $third_level = $this->users_model->getFields($row->third_level_id);
            $row->third_level = ($third_level) ? "$third_level->user_fname $third_level->user_lname [$third_level->business_unit_name]" : '---';
        }

        $row->repo = '';
        if ($row->status == "Re-Po'ed") {
            $child_po_id = $this->re_po_model->getFieldValue('purchase_order_id', array('parent_id' => $id_purchase_order));
            $row->repo = $this->purchase_orders_model->getFields($child_po_id);
        }


        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'purchase-order-view-tools.js'))),
                'row' => $row,
                'po_items' => $po_items,
                'module_attachments' => $module_attachments,
                'logs' => $logs,
            );
            $this->load->view(admin_dir('purchase_orders/view_purchase_order'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function print_purchase_order() {
        $id_purchase_order = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Purchase Order if Exist */
        $row = $this->purchase_orders_model->getFields($id_purchase_order);
        $po_items = $this->purchase_order_items_model->getList(array('purchase_order_id' => $id_purchase_order))->result();

        if ($row) {//save log
            parent::save_log("PO form printed.", 'purchase_orders', $id_purchase_order);
            $added_by = $this->users_model->getFields($row->added_by);
            $row->added_by = "$added_by->user_fname $added_by->user_lname [$added_by->business_unit_name]";
            $row->approved = '';
            $row->declined = '';
            if ($row->first_level_id) {
                $first_level = $this->users_model->getFields($row->first_level_id);
                $row->first_level = ($first_level) ? "$first_level->user_fname $first_level->user_lname [$first_level->business_unit_name]" : '---';
            }
            if ($row->approved_by) {
                if ($row->approval_date != '0000-00-00 00:00:00') {
                    $approver = $this->users_model->getFields($row->approved_by);
                    $row->approved = "$approver->user_fname $approver->user_lname [$approver->business_unit_name]";
                } else {
                    $declined = $this->users_model->getFields($row->approved_by);
                    $row->declined = "$declined->user_fname $declined->user_lname [$declined->business_unit_name]";
                }
            }
            if ($row->approval_date != '0000-00-00 00:00:00') {
                $approver = $this->users_model->getFields($row->approved_by);
                $row->approved = "$approver->user_fname $approver->user_lname [$approver->business_unit_name]";
            }

            if ($row->declined_date != '0000-00-00 00:00:00') {
                $approver = $this->users_model->getFields($row->approved_by);
                $row->declined = "$approver->user_fname $approver->user_lname [$approver->business_unit_name]";
            }

            if ($row->second_level_id) {
                $second_level = $this->users_model->getFields($row->second_level_id);
                $row->second_level = ($second_level) ? "$second_level->user_fname $second_level->user_lname [$second_level->business_unit_name]" : '---';
            }
            if ($row->third_level_id) {
                $third_level = $this->users_model->getFields($row->third_level_id);
                $row->third_level = ($third_level) ? "$third_level->user_fname $third_level->user_lname [$third_level->business_unit_name]" : '---';
            }
            $data = array(
                'row' => $row,
                'po_items' => $po_items,
            );
            $this->load->view(admin_dir('purchase_orders/extra/purchase_order_form'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function receive_purchase_order() {
        $id_purchase_order = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Purchase Order if Exist */
        $row = $this->purchase_orders_model->getFields($id_purchase_order);
        $po_items = $this->purchase_order_items_model->getList(array('purchase_order_id' => $id_purchase_order))->result();

        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'purchase-order-view-tools.js'))),
                'row' => $row,
                'po_items' => $po_items,
            );
            $this->load->view(admin_dir('purchase_orders/receive_purchase_order'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_purchase_order') { /* Method for Account */
            self::_method_list_purchase_order();
        } else if ($this->uri->rsegment(3) == 'add_purchase_order') {
            self::_method_add_purchase_order();
        } else if ($this->uri->rsegment(3) == 'edit_purchase_order') {
            self::_method_edit_purchase_order();
        } else if ($this->uri->rsegment(3) == 'view_purchase_order') {
            self::view_purchase_order();
        } else if ($this->uri->rsegment(3) == 'delete_purchase_order') {
            self::_method_delete_purchase_order();
        } else if ($this->uri->rsegment(3) == 'add_attachment') {
            self::_method_add_attachment();
        } else if ($this->uri->rsegment(3) == 'delete_attachment') {
            self::_method_delete_attachment();
        } else if ($this->uri->rsegment(3) == 'approve_purchase_order') {
            $this->tools->getPost('decline', '');
            self::_method_approve_purchase_order();
        } else if ($this->uri->rsegment(3) == 'decline_purchase_order') {
            $this->tools->getPost('approve', '');
            self::_method_decline_purchase_order();
        } else if ($this->uri->rsegment(3) == 'close_purchase_order') {
            self::_method_close_purchase_order();
        } else if ($this->uri->rsegment(3) == 'add_comment') {
            self::_method_add_comment();
        } else if ($this->uri->rsegment(3) == 're_po') {
            self::_method_re_po();
        } else if ($this->uri->rsegment(3) == 'cancel') {
            self::_method_cancel_purchase_order();
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Validation of the Form -------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    private function _validate() {
        $this->form_validation->set_rules('supplier_id', 'Supplier', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('purchase_requisition_id', 'Purchase Order', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('po_remarks', 'Remarks', 'htmlspecialchars|trim');
        if ($this->uri->rsegment(3) == 'add_purchase_order') {
            $this->form_validation->set_rules('items', 'Items', 'required');
        }
        if ($this->tools->getPost('items')) {
            foreach ($this->tools->getPost('items') as $key => $val) {
                $number = $key + 1;
                $this->form_validation->set_rules("quantity[$key]", 'Quantity', 'htmlspecialchars|trim|required');
                $this->form_validation->set_rules("item_unit_cost[$key]", "Purchasing Price [$number]", 'htmlspecialchars|trim|required');
            }
        }
        if ($this->tools->getPost('po_items')) {
            foreach ($this->tools->getPost('po_items') as $key => $val) {
                $number = $key + 1;
                $this->form_validation->set_rules("po_quantity[$key]", 'Quantity', 'htmlspecialchars|trim|required');
                $this->form_validation->set_rules("item_unit_cost[$key]", "Purchasing Price [$number]", 'htmlspecialchars|trim|decimal');
            }
        }
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_purchase_order() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }
        $condition_string = array();
        $condition = array();
        //show purchase requisitions requested by the business_unit only if user is not PEM, FM, CEO,OM or Super Admin
        if ((int) $this->session->userdata['admin']['user_type_id'] != 1) {
            $user_type_code = $this->session->userdata['admin']['user_type_code'];
            switch ($user_type_code) {
                case 'PU Head':
                    break;
                case 'Purchaser':
                    break;
                case 'PO Initiator':
                    break;
                case 'BU Head (TFleet)':
                    $group_id = $this->users_model->getValue($this->session->userdata['admin']['user_id'], 'general_group_id');
                    $condition = array('bu.general_group_id' => $group_id);
                    break;
                case 'Administrator':
                    break;
                case 'EVP': // 2nd level approved
                    $condition = array('po.status' => 'Queued to ELA');
                    break;
                case 'Chairman': //third level approved
                    $condition = array('po.status' => 'Queued to DJF');
                    break;
                default:
                    $group_id = $this->users_model->getValue($this->session->userdata['admin']['user_id'], 'general_group_id');
                    $condition = array('du.business_unit_id' => $this->session->userdata['admin']['business_unit_id'],
                        'bu.general_group_id' => $group_id);
                    break;
            }
        }
        //set condition for the list
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_purchase_order') {
                        $condition = array_merge($condition, array($var => $val));
                    } elseif ($var == 'added_date') {
                        $condition = array_merge($condition, array('po.' . $var . " like" => "%" . $val . "%"));
                    } elseif ($var == 'status' || $var == 'total_amount') {
                        $condition = array_merge($condition, array('po.' . $var . " like" => "%" . $val . "%"));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }
        //arrange from oldest to newest
        if (empty($this->tools->getPost('sort'))) {
            $this->tools->setPostArray('sort', array('sort_by' => 'id_purchase_order', 'sort_type' => 'DESC'));
        }
        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get Purchase Order List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array('');

        $this->load->view(admin_dir('lists/list'), $data);
    }

    /*
     * 	Add New Purchase Order Method
     */

    function _method_add_purchase_order() {
        if (IS_AJAX) {
            self::_validate();

            //check form validation then save
            if ($this->form_validation->run() == TRUE) {
                $this->db->trans_start();
                //getPost data set to this model fields
                parent::copyFromPost($this->purchase_orders_model, 'id_purchase_order');
                //purchase requisition
                $purchase_requisition_id = $this->tools->getPost('purchase_requisition_id');
                $purchase_requisition = $this->purchase_requisitions_model->getFields($purchase_requisition_id);
                $group_code = $this->general_group_model->getFields($purchase_requisition->general_group_id);

                $this->purchase_orders_model->business_unit_id = $purchase_requisition->business_unit_id;
                $this->po_code = ($group_code->group_code == 'TFleet') ? 'TFL' : $group_code->group_code;
                //get lates added purchase requisition number
                $po_number = $this->purchase_orders_model->POgetLastNo('po_number', 'added_date', date('Y'));
                $this->purchase_orders_model->po_number = isset($po_number) ? $this->po_code . "-PO-" . ((int) str_replace('-', '', filter_var($po_number, FILTER_SANITIZE_NUMBER_INT)) + 1) : $this->po_code . '-PO-' . date('y') . '0000001'; // set purchase requisition number
                $this->purchase_orders_model->order_date = date('Y-m-d H:i:s');
                $this->purchase_orders_model->status = ($group_code->group_code == 'TFleet') ? 'Queued to BU Head' : 'Queued to Purchasing Head';
                //get document type ID
//                $document_type_id = $this->document_types_model->getFieldValue('id_document_type', array('class_id' => $this->class_id), array('document_type_code' => $this->session->userdata['admin']['group_code']));
//                $this->purchase_orders_model->document_type_id = $document_type_id;

                /* Check Purchase Order Successfully Save */
                $total_amount = 0;
                $total_quantity = 0;
                if ($po_id = $this->purchase_orders_model->add()) {
                    $items = $this->tools->getPost('items');
                    //save purchase order items
                    foreach ($items as $key => $val) {
                        $item_unit_cost = str_replace(',', '', $this->tools->getPost('item_unit_cost', $key));
                        $quantity = $this->tools->getPost('quantity', $key);
                        $pr_item = $this->purchase_requisition_items_model->getFields($this->tools->getPost('pr_item_id', $key));
                        $po_item_data = array('purchase_order_id' => $po_id,
                            'pr_item_id' => $pr_item->id_pr_item,
                            'item_id' => $val,
                            'quantity' => $quantity,
                            'item_unit_cost' => $item_unit_cost
                        );
                        $purchase_order_item = new Purchase_Order_Items_Model();
                        $purchase_order_item->add($po_item_data);

                        //update purchasing price of PR
                        $this->purchase_requisition_items_model->update(array('id_pr_item' => $pr_item->id_pr_item), array('purchasing_price' => str_replace(',', '', $this->tools->getPost('item_unit_cost', $key)),
                            'poed' => 1));
                        $total_quantity += $quantity;
                        $total_amount +=($item_unit_cost * $quantity);
                    }
                    //save log
                    parent::save_log("Requested this PO", 'purchase_orders', $po_id);

                    $status = "Completely PO'ed";
                    $check_poed = count($this->purchase_requisition_items_model->getList(array('purchase_requisition_id' => $purchase_requisition_id, 'poed' => 0))->result());
                    $check_pr_quantity = $this->purchase_requisition_items_model->getSum('quantity', 'quantity', array('purchase_requisition_id' => $purchase_requisition_id));
                    if ($check_poed OR ( $total_quantity != $check_pr_quantity)) {
                        $status = "Partially PO'ed";
                    }
                    //update status of purchase requisition
                    $purchase_requisition = $this->purchase_requisitions_model->getFields($purchase_requisition_id);
                    $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $purchase_requisition_id), array('status' => $status,
                        'total_amount' => $total_amount,
                        'pr_remarks' => $purchase_requisition->pr_remarks . "$status PO# " . $this->purchase_orders_model->po_number));

                    //create notifications for deparment head approver if status is request
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successfully added PO'),
                        'id' => $this->purchase_orders_model->id
                    );
                } else {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", "Unsuccessful request of PO"),
                        'id' => 0
                    );
                }
                $this->db->trans_complete();
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	Edit Purchase Order Type Method
     */

    function _method_edit_purchase_order() {
        if (IS_AJAX) {
            $id_purchase_order = $this->Misc->decode_id($this->tools->getPost('id'));

            /* Check Purchase Order Type if Exist */
            $row = $this->purchase_orders_model->getFields($id_purchase_order);
            self::_validate();
            if ($row) {
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    /* Update Purchase Order Info */
                    //getPost data set to this model fields
                    $po_model = new Purchase_Orders_Model($id_purchase_order);
                    parent::copyFromPost($po_model, 'id_purchase_order');
                    $po_model->order_date = date('Y-m-d', strtotime($this->tools->getPost('order_date')));

                    if ($po_model->update()) {
                        parent::save_log("Updated", 'purchase_orders', $id_purchase_order);
                        $po_items = $this->purchase_order_items_model->getList(array('purchase_order_id' => $id_purchase_order))->result();
                        $edited_po_items = $this->tools->getPost('po_items');
                        $total_quantity = 0;

                        //check previously saved po items if not already existing upon editing transaction
                        if ($edited_po_items) {
                            foreach ($po_items as $q) {
                                if (!in_array($q->id_po_item, $edited_po_items)) {
                                    $this->purchase_order_items_model->update(array('id_po_item' => $q->id_po_item), array('enabled' => 0));
                                    //update purchasing price of PR
                                    $this->purchase_requisition_items_model->update(array('id_pr_item' => $q->pr_item_id), array('poed' => 0));
                                } else {
                                    $po_item_data = array(
                                        'quantity' => $this->tools->getPost('po_quantity', $q->item_id),
                                        'item_unit_cost' => str_replace(',', '', $this->tools->getPost('item_unit_cost', $q->item_id))
                                    );
                                    $this->purchase_order_items_model->update(array('id_po_item' => $q->id_po_item), $po_item_data);
                                    $total_quantity += $this->tools->getPost('po_quantity', $q->item_id);
                                }
                            }
                        } else {
                            $this->purchase_order_items_model->update(array('purchase_order_id' => $id_purchase_order), array('enabled' => 0));
                        }
                        // add additional po items to po transaction
                        $items = $this->tools->getPost('items');
                        if (!empty($items)) {
                            foreach ($items as $key => $item_id) {
                                $item = new Items_Model($item_id);
                                $po_item_data = array('purchase_order_id' => $id_purchase_order,
                                    'pr_item_id' => $this->tools->getPost('pr_item_id', $key),
                                    'item_id' => $item->id_item,
                                    'quantity' => $this->tools->getPost('quantity', $key),
                                    'item_unit_cost' => str_replace(',', '', $this->tools->getPost('item_unit_cost', $key))
                                );
                                $purchase_order_item = new Purchase_Order_Items_Model();
                                $purchase_order_item->add($po_item_data);
                                //update purchasing price of PR
                                $this->purchase_requisition_items_model->update(array('id_pr_item' => $this->tools->getPost('pr_item_id', $key)), array('purchasing_price' => str_replace(',', '', $this->tools->getPost('item_unit_cost', $key)),
                                    'poed' => 1));
                                $total_quantity += $this->tools->getPost('quantity', $key);
                            }
                        }

                        $status = "Completely PO'ed";
                        $check_poed = count($this->purchase_requisition_items_model->getList(array('purchase_requisition_id' => $po_model->purchase_requisition_id, 'poed' => 0))->result());
                        $check_pr_quantity = $this->purchase_requisition_items_model->getSum('quantity', 'quantity', array('purchase_requisition_id' => $po_model->purchase_requisition_id));
                        if ($check_poed OR ( $total_quantity != $check_pr_quantity)) {
                            $status = "Partially PO'ed";
                        }

                        $this->purchase_requisitions_model->update(array('id_purchase_requisition' => $po_model->purchase_requisition_id), array('status' => $status,
                            'total_amount' => $this->purchase_order_items_model->calcTotalAmount(array('purchase_order_id' => $id_purchase_order))));

                        $arr = array(
                            'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                            'id' => 1
                        );
                        $this->db->trans_complete();
                    } else {
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('error', 'Unsuccessful'),
                            'id' => 0
                        );
                    }
                    $this->db->trans_complete();
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Delete Purchase Order Method
     */

    function _method_delete_purchase_order() {
        if (IS_AJAX) {
            $id_purchase_order = $this->Misc->decode_id($this->tools->getPost('item'));
            /* Check Purchase Order if Exist */
            $row = $this->purchase_orders_model->getFields($id_purchase_order);
            if ($row) {
                if ((strtolower($row->status) != 'draft')) {// 
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('error', 'PO with status not yet approved can deleted'),
                        'id' => 1
                    );
                } else {
                    $this->db->trans_start();
                    $datas = array(
                        'status' => 'DELETED',
                        'enabled' => 0,
                        'updated_by' => $this->my_session->get('admin', 'user_id'),
                        'updated_date' => date('Y-m-d H:i:s')
                    );
                    /* Delete Purchase Order */
                    $this->purchase_orders_model->update_table($datas, "id_purchase_order", $id_purchase_order);
                    parent::save_log("Deleted", 'purchase_orders', $id_purchase_order);
                    $this->db->trans_complete();
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'PO was DELETED'),
                        'id' => 1
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Approval Purchase Order Method
     */

    function _method_approve_purchase_order() {
        if (IS_AJAX) {
            $id_purchase_order = $this->Misc->decode_id($this->tools->getPost('id'));
            $users = new Users_Model();
            /* Check Purchase Order if Exist */
            $row = $this->purchase_orders_model->getFields($id_purchase_order);
            if ($row) {
                $this->db->trans_start();
                parent::save_log($this->tools->getPost('approve'), 'purchase_orders', $id_purchase_order);
                //update notification of user as done
                $notif_model = new Notifications_Model();
                $notif_model->update(array('class_id' => $this->class_id,
                    'reference_id' => $id_purchase_order,
                    'user_id' => $this->session->userdata['admin']['user_id']), array('done' => 1));

                $message = $this->tools->getPost('approve');
                $status = $this->tools->getPost('approve');
                if (($this->tools->getPost('approve') == 'Queued to Purchasing Head')) {
                    $to_be_notified = $users->getList(array('user_type_name' => 'Purchasing Head'))->result();
                    $message = "PO# $row->po_number was " . $this->tools->getPost('approve');
                    //update status as Approved
                    $this->purchase_orders_model->update(array('id_purchase_order' => $id_purchase_order), array('status' => $status,
                        'first_level_id' => $this->session->userdata['admin']['user_id'],
                        'first_approval_date' => date('Y-m-d H:i:s')));
                } else if ($this->tools->getPost('approve') == 'Queued to ELA') {
                    if (($row->total_amount <= 2000) && ($row->group_code != 'TFleet')) {
                        parent::save_log("This PO has is less than / equal to 2k, auto-approved by Top Management", 'purchase_orders', $id_purchase_order);
                        $message = "PO# $row->po_number was auto-approved by Top Management, this is less than / equal to 2k.";
                        //update status as Approved
                        $this->purchase_orders_model->update(array('id_purchase_order' => $id_purchase_order), array('status' => 'For Receiving',
                            'approved_by' => $this->session->userdata['admin']['user_id'],
                            'approval_date' => date('Y-m-d H:i:s')));
                    } else {
                        //notify Executive
                        if ($row->group_code != 'TFleet') {
                            $to_be_notified = $users->getList(array('user_type_name' => 'Executive Vice President'))->result();
                        } else {
                            $to_be_notified = $users->getList(array('user_type_name' => 'Chairman and President'))->result();
                        }
                        $message = "PO# $row->po_number was " . $this->tools->getPost('approve');
                        //update status as Approved
                        $this->purchase_orders_model->update(array('id_purchase_order' => $id_purchase_order), array('status' => $status,
                            'approved_by' => $this->session->userdata['admin']['user_id'],
                            'approval_date' => date('Y-m-d H:i:s')));
                    }
                } else if ($row->group_code == 'TFleet' && $this->tools->getPost('approve') == 'Queued to DJF') {
                    // this was approved by PHead to be routed to DJF
                    $to_be_notified = $users->getList(array('user_type_name' => 'Chairman and President'))->result();
                    $message = "PO# $row->po_number was " . $this->tools->getPost('approve');
                    //update status as Approved
                    $this->purchase_orders_model->update(array('id_purchase_order' => $id_purchase_order), array('status' => $status,
                        'approved_by' => $this->session->userdata['admin']['user_id'],
                        'approval_date' => date('Y-m-d H:i:s')));
                } else if ($this->tools->getPost('approve') == 'PO Approved (ELA)') {
                    if ($row->total_amount > 50000) {
                        $to_be_notified = $users->getList(array('user_type_name' => 'Chairman and President'))->result();
                        $status = 'Queued to DJF';
                    } else {
                        $to_be_notified = $users->getList(array(), array("(`user_type_name` = 'Purchasing Head' OR `id_user` = $row->bu_head_id)"))->result();
                        $status = 'For Receiving';
                    }
                    $message = "PO# $row->po_number was " . $this->tools->getPost('approve');
                    //update status as Approved
                    $this->purchase_orders_model->update(array('id_purchase_order' => $id_purchase_order), array('status' => $status,
                        'second_level_id' => $this->session->userdata['admin']['user_id'],
                        'second_approval_date' => date('Y-m-d H:i:s')));
                } else if ($this->tools->getPost('approve') == 'PO Approved (DJF)') {
                    $to_be_notified = $users->getList(array(), array("(`user_type_name` = 'Purchasing Head' OR `id_user` = $row->bu_head_id)"))->result();
                    $message = "PO# $row->po_number was " . $this->tools->getPost('approve');
                    $status = 'For Receiving';
                    //update status as Approved
                    $this->purchase_orders_model->update(array('id_purchase_order' => $id_purchase_order), array('status' => $status,
                        'third_level_id' => $this->session->userdata['admin']['user_id'],
                        'third_approval_date' => date('Y-m-d H:i:s')));
                }
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', $message),
                    'id' => 1
                );

                if (!empty($to_be_notified)) {
                    foreach ($to_be_notified as $q) {
                        $notif_data = array(
                            'class_id' => $this->class_id,
                            'reference_id' => $id_purchase_order,
                            'user_id' => $q->id_user,
                            'link' => 'view_purchase_order',
                            'todo' => 1,
                            'status' => $message);
                        $notification_model = new Notifications_Model();
                        $notification_model->add($notif_data);
                    }
                }
                //notify requestor
                self::notify_creator($row->added_by, $id_purchase_order, $message);

                $this->db->trans_complete();
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Decline Purchase Order Method
     */

    function _method_decline_purchase_order() {
        if (IS_AJAX) {
            $id_purchase_order = $this->Misc->decode_id($this->tools->getPost('id'));
            $this->user_type_model = new User_Types_Model();
            /* Check Purchase Order if Exist */
            $row = $this->purchase_orders_model->getFields($id_purchase_order);
            if ($row) {
                $this->db->trans_start();
                parent::save_log($this->tools->getPost('decline') . " this PO# $row->po_number", 'purchase_orders', $id_purchase_order);
                if ($this->tools->getPost('decline') == 'DECLINED (Purchasing Head)') {
                    //update status as Declined
                    $this->purchase_orders_model->update(array('id_purchase_order' => $id_purchase_order), array('status' => $this->tools->getPost('decline'),
                        'approved_by' => $this->session->userdata['admin']['user_id'], //saved to approved by
                        'declined_date' => date('Y-m-d H:i:s')));
                }
                if ($this->tools->getPost('decline') == 'DECLINED (Exec VP)') {
                    //update status as Declined
                    $this->purchase_orders_model->update(array('id_purchase_order' => $id_purchase_order), array('status' => $this->tools->getPost('decline'),
                        'second_level_id' => $this->session->userdata['admin']['user_id'], //saved to approved by
                        'second_decline_date' => date('Y-m-d H:i:s')));
                }
                if ($this->tools->getPost('decline') == 'DECLINED (Chairman & Pres.)') {
                    //update status as Declined
                    $this->purchase_orders_model->update(array('id_purchase_order' => $id_purchase_order), array('status' => $this->tools->getPost('decline'),
                        'third_level_id' => $this->session->userdata['admin']['user_id'], //saved to approved by
                        'third_decline_date' => date('Y-m-d H:i:s')));
                }
                //notify creator
                $status = "PO # " . $row->po_number . " was Declined " . $this->session->userdata['admin']['user_type_code'];
                self::notify_creator($row->added_by, $id_purchase_order, $status);

                //update notification of user as done
                $notif_model = new Notifications_Model();
                $notif_model->update(array('class_id' => $this->class_id,
                    'reference_id' => $id_purchase_order,
                    'user_id' => $this->session->userdata['admin']['user_id']), array('done' => 1));
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Declined Purchase Order'),
                    'id' => 1
                );
                $this->db->trans_complete();
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Add Module Attachments
     */

    function _method_add_attachment() {
        $id_purchase_order = $this->Misc->decode_id($this->tools->getPost('id'));
        $this->classes_model = new Classes_Model();
        /* Check Purchase Order if Exist */
        $row = $this->purchase_orders_model->getFields($id_purchase_order);
        if ($row && !empty($_FILES)) {
            $this->form_validation->set_rules('upload', 'File Name', 'htmlspecialchars|trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->db->trans_start();
                // Set the upload path
                $upload_path = './upload/Purchase Orders/' . $row->po_number . '/';
                // Check if upload path exists
                // Create the directory if not
                $filetype = '*';
                $upload = parent::upload_file($upload_path, '', 'file', $filetype, '5000');

                // Check if upload is sucessful
                // Display errors if it fails
                if ($upload[0]) { // TRUE
                    $module_attachment_model = new Module_Attachments_Model();
                    $data = array(
                        'class_id' => $this->class_id,
                        'reference_id' => $id_purchase_order,
                        'upload_title' => $this->tools->getPost("upload"),
                        'upload_file_name' => $upload['file_name'],
                        'upload_file_type' => $upload['file_type'],
                        'added_by' => $this->session->userdata['admin']['user_id'],
                        'added_date' => date('Y-m-d H:i:s'),
                    );
                    $module_attachment_model->add($data);
                    //add to logs
                    parent::save_log("Uploaded Attachment: " . $this->tools->getPost("upload"), 'purchase_orders', $id_purchase_order);
                    // success status
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successful Uploading Attachment'),
                        'id' => 1
                    );
                } else {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('error', 'Error in Uploading'),
                        'id' => 1
                    );
                }
                $this->db->trans_complete();
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
        } else {
            $arr = array(
                'message_alert' => $this->Misc->message_status('error', 'Attach file to upload.'),
                'id' => 1
            );
        }
        $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
    }

    /*
     * 	Delete Module Attachment Method
     */

    function _method_delete_attachment() {
        if (IS_AJAX) {
            $id_module_attachment = $this->Misc->decode_id($this->tools->getPost('item'));
            /* Check Purchase Order if Exist */
            $row = $this->module_attachments_model->getFields($id_module_attachment);
            if ($row) {
                $this->db->trans_start();
                $datas = array(
                    'enabled' => 0,
                );
                /* Delete Purchase Order */
                $this->module_attachments_model->update_table($datas, "id_module_attachment", $id_module_attachment);
                parent::save_log("Deleted Attachment" . sprintf('%05d', $row->upload_title), 'purchase_orders', $id_module_attachment);
                $this->db->trans_complete();
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Deleted'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	save comments
     */

    function _method_add_comment() {
        if (IS_AJAX) {
            $id_purchase_order = $this->Misc->decode_id($this->tools->getPost('id'));
            /* Check Purchase Order if Exist */
            $row = $this->purchase_orders_model->getFields($id_purchase_order);
            if ($row) {
                $this->form_validation->set_rules('comment', 'Comment', 'htmlspecialchars|trim|required');
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    // save comment as log
                    parent::save_log($this->tools->getPost('comment'), 'purchase_orders', $id_purchase_order);

                    //notify business_unit requestor
                    $status = "Comment: " . $this->tools->getPost('comment');
                    self::notify_creator($row->added_by, $id_purchase_order, $status);

                    $this->db->trans_complete();
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Saved Remarks'),
                        'id' => 1
                    );
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Close Purchase Order Method
     */

    function _method_close_purchase_order() {
        if (IS_AJAX) {
            $id_purchase_order = $this->Misc->decode_id($this->tools->getPost('id'));
            /* Check Purchase Order if Exist */
            $row = $this->purchase_orders_model->getFields($id_purchase_order);
            if ($row) {
                $this->db->trans_start();
                $datas = array(
                    'status' => $this->tools->getPost('close'),
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete Purchase Order */
                $this->purchase_orders_model->update_table($datas, "id_purchase_order", $id_purchase_order);
                parent::save_log("CLOSED this PO", 'purchase_orders', $id_purchase_order);
                //notify requestor
                self::notify_creator($row->added_by, $id_purchase_order, "CLOSED PO# $row->po_number");

                $this->db->trans_complete();
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'PO was CLOSED'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * RE - PO 
     */

    function _method_cancel_purchase_order() {
        if (IS_AJAX) {
            $id_purchase_order = $this->Misc->decode_id($this->tools->getPost('id'));
            $this->user_type_model = new User_Types_Model();
            /* Check Purchase Order if Exist */
            $row = $this->purchase_orders_model->getFields($id_purchase_order);
            if ($row) {
                $this->db->trans_start();
                $status = $this->tools->getPost('cancel');
                parent::save_log($status . " this PO", 'purchase_orders', $id_purchase_order);
                //update status as Cancelled
                $this->purchase_orders_model->update(array('id_purchase_order' => $id_purchase_order), array('status' => $this->tools->getPost('cancel'),
                    'cancelled_by' => $this->session->userdata['admin']['user_id'], //saved to approved by
                    'cancelled_date' => date('Y-m-d H:i:s')));

                //notify creator
                $status = "PO # " . $row->po_number . " was Cancelled " . $this->session->userdata['admin']['user_type_code'];
                self::notify_creator($row->added_by, $id_purchase_order, $status);

                //Update purchase requisition status
                $id_purchase_requisition = $row->purchase_requisition_id;

                $datas = array(
                    'status' => 'CLOSED',
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );

                $this->purchase_requisitions_model->update_table($datas, "id_purchase_requisition", $id_purchase_requisition);
                parent::save_log("Closed this PR due to Cancelled PO  $row->po_number", 'purchase_requisitions', $id_purchase_requisition, 'purchase_requisitions', 'purchase_requisitions');

                //notify requestor
                self::notify_creator($row->added_by, $id_purchase_requisition, "AUTO-CLOSED PR# $row->pr_number");

                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Purchase Order Cancelled'),
                    'id' => 1
                );
                $this->db->trans_complete();
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * RE - PO 
     */

    function _method_re_po() {
        if (IS_AJAX) {
            $this->db->trans_start();

            $id_purchase_order = $this->Misc->decode_id($this->tools->getPost('id'));
            $users = new Users_Model();
            /* Check Purchase Order if Exist */
            $row = $this->purchase_orders_model->getFields($id_purchase_order);
            $group_code = $this->general_group_model->getFields($row->general_group_id);
            $this->po_code = ($group_code->group_code == 'TFleet') ? 'TFL' : $group_code->group_code;
            $status = ($group_code->group_code == 'TFleet') ? 'Queued to BU Head' : 'Queued to Purchasing Head';

            $po_number = $this->purchase_orders_model->POgetLastNo('po_number', 'added_date', date('Y'));
            $new_po_number = $this->po_code . "-PO-" . ((int) str_replace('-', '', filter_var($po_number, FILTER_SANITIZE_NUMBER_INT)) + 1);

            $new_po_data = array(
                'purchase_requisition_id' => $row->purchase_requisition_id,
                'supplier_id' => $row->supplier_id,
                'business_unit_id' => $row->business_unit_id,
                'status' => $row->status,
                'total_amount' => $row->total_amount,
                'po_remarks' => $row->po_remarks,
                'po_number' => $new_po_number,
                'order_date' => date('Y-m-d H:i:s'),
                'status' => $status,
                'added_by' => $row->added_by,
                'added_date' => $row->added_date,
                'updated_by' => $row->updated_by,
                'updated_date' => $row->updated_date,
            );

            if ($new_po_id = $this->purchase_orders_model->add($new_po_data)) {
                $po_items = $this->purchase_order_items_model->getList(array('purchase_order_id' => $id_purchase_order))->result();
                foreach ($po_items as $q) {
                    $new_po_item = array(
                        'purchase_order_id' => $new_po_id,
                        'pr_item_id' => $q->pr_item_id,
                        'item_id' => $q->item_id,
                        'item_unit_cost' => $q->item_unit_cost,
                        'quantity' => $q->quantity,
                        'qty_rcvd' => $q->qty_rcvd,
                    );

                    $this->purchase_order_items_model->add($new_po_item);
                }

                parent::save_log("Re-Po'ed this transaction from PO# $row->po_number", 'purchase_orders', $new_po_id);

                //update status as REPO'ed
                $status = $this->tools->getPost('re_po');
                $this->purchase_orders_model->update(array('id_purchase_order' => $id_purchase_order), array('status' => $status));
                parent::save_log("Re-Po'ed this transaction. New PO# is $new_po_number", 'purchase_orders', $id_purchase_order);

                //add to re-po record
                $re_po_data = array(
                    'parent_id' => $id_purchase_order,
                    'purchase_order_id' => $new_po_id,
                    're_poed_by' => $this->session->userdata['admin']['user_id'],
                    'date_re_poed' => date('Y-m-d H:i:s')
                );
                $this->re_po_model->add($re_po_data);

                $to_be_notified = $users->getList(array(), array("(`user_type_name` = 'Purchasing Head' OR `id_user` = $row->bu_head_id)"))->result();
                $message = "PO# $row->po_number was re-poed from $row->po_number";

                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', $this->tools->getPost('re_po') . " this transaction."),
                    'id' => 1
                );

                if (!empty($to_be_notified)) {
                    foreach ($to_be_notified as $q) {
                        $notif_data = array(
                            'class_id' => $this->class_id,
                            'reference_id' => $id_purchase_order,
                            'user_id' => $q->id_user,
                            'link' => 'view_purchase_order',
                            'todo' => 1,
                            'status' => $message);
                        $notification_model = new Notifications_Model();
                        $notification_model->add($notif_data);
                    }
                }

                //notify requestor
                self::notify_creator($row->added_by, $id_purchase_order, $message);
            } else {
                $arr = array(
                    'message_alert' => $this->Misc->message_status('error', "Failed to RE-PO."),
                    'id' => 1
                );
            }



            $this->db->trans_complete();
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * Notify creator of PO
     */

    private function notify_creator($added_by, $id_purchase_order, $status) {
        //notify Initiator for declined PO
        $notif_data = array(
            'class_id' => $this->class_id,
            'reference_id' => $id_purchase_order,
            'user_id' => $added_by,
            'link' => 'view_purchase_order',
            'status' => $status);
        $notification_model = new Notifications_Model();
        $notification_model->add($notif_data);

//        self::email_notification($id_purchase_order);
    }

    /*
     * Email Notification
     */

    private function email_notification($id_purchase_order) {
        $configs_model = new Configs_Model();
        $row = $this->purchase_orders_model->getFields($id_purchase_order);
        $initiator = $this->users_model->getFields($row->added_by);
        $administrator = $this->users_model->getFields($this->session->userdata['admin']['user_id']);

        $email_templates = '';
        $subject = '';

        $email_content = array(
            'purchase_requisition' => $row,
            'user' => $initiator,
            'administrator' => $administrator,
        );

        if ($this->tools->getPost('approve')) {
            $email_templates = $this->load->view(admin_dir('email_templates/approved_purchase_order'), $email_content, TRUE);
            $subject = $this->tools->getPost('approve') . " PO# $row->po_number.";
        }
        if ($this->tools->getPost('decline')) {
            $email_templates = $this->load->view(admin_dir('email_templates/declined_purchase_order'), $email_content, TRUE);
            $subject = $this->tools->getPost('decline') . " PO# $row->po_number.";
        }
        if ($this->tools->getPost('close')) {
            $email_templates = $this->load->view(admin_dir('email_templates/closed_purchase_order'), $email_content, TRUE);
            $subject = $this->tools->getPost('close') . " PO# $row->po_number.";
        }
        $to = $initiator->user_email;
        $toName = "$initiator->user_fname $initiator->user_lname";
        $from = 'no-reply@techcellar.com';
        $fromName = 'Inventory System';
        $cc = $configs_model->getFieldValue('config_value', array('config_name' => 'SMTP_CC'));
        $bcc = $configs_model->getFieldValue('config_value', array('config_name' => 'SMTP_BCC       '));
        parent::send_email($email_templates, $subject, $to, $toName, $from, $fromName, $cc, $bcc);
    }

    /*
     * Check PR
     */

    protected function check_calculation() {
        if ($this->session->userdata['admin']['user_type_id'] == 1) {
            $this->db->trans_start();
            /* Check Purchase Requistion if Exist */
            $list = $this->purchase_orders_model->getList(array('po.enabled' => 1))->result();
            foreach ($list as $q) {
                $total_amount = $this->purchase_order_items_model->getSum('item_unit_cost * `quantity`', 'total', array('purchase_order_id' => $q->id_purchase_order, 'enabled' => 1));

                if ($total_amount != $q->total_amount) {
                    //update total amount
                    $this->purchase_orders_model->update(array('id_purchase_order' => $q->id_purchase_order), array('total_amount' => $total_amount));
                }
            }
            $this->db->trans_complete();
            echo 'Success';
        }
    }

}
