<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | PO Status Summary Class
  |--------------------------------------------------------------------------
  |
  | Handles the PO Status Summary panel
  |
  | @category		Controller
  | @author		melin
 */

class Report_PO_Status_Summary extends Admin_Core {

    public $purchase_order = '';
    public $purchase_order_items_model = '';

    function __construct() {
        parent:: __construct();

        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);

        $this->load->model('admin/purchase_orders_model');
        $this->load->model('admin/purchase_order_items_model');
        $this->load->model('admin/users_model');

        $this->purchase_order = new Purchase_Orders_Model();
        $this->purchase_order_items_model = new Purchase_Order_Items_Model();
        $this->users_model = new Users_Model();

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        //for list view
        $this->list_content = array(
            'id' => array(
                'label' => 'ID',
                'type' => 'hidden',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => '',
                'var-value' => 'id_purchase_order',
            ),
            'po_number' => array(
                'label' => 'PO NO',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'po_number',
            ),
            'acctg_unit' => array(
                'label' => 'Acctg Unit',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'acctg_unit',
            ),
            'added_date' => array(
                'label' => 'Date Filled',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'added_date',
            ),
            'supplier_name' => array(
                'label' => 'Supplier Name',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'supplier_name',
            ),
            'po_remarks' => array(
                'label' => 'Remarks',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1 ',
                'var-value' => 'po_remarks',
            ),
            'total_amount' => array(
                'label' => 'Amount',
                'type' => 'decimal',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'total_amount',
            ),
            'po.status' => array(
                'label' => 'Status',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'status',
            )
        );

        $this->contents = array('model_directory' => 'admin/purchase_orders_model',
            'model_name' => 'purchase_orders_model',
            'filters' => array(),
            'functionName' => 'Record',); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('reports/report_po_status_summary'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_record() {
        $data = array(
            'template' => parent::main_template(),
        );

        $this->load->view(admin_dir('reports/report_po_status_summary'), $data);
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if (($this->uri->rsegment(3) == 'list_record') || ($this->uri->rsegment(3) == 'pdf_report')) { /* Method for Account */
            self::_method_list_record();
        }
    }

    /* ---------- ENROLLMENT LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_record() {
        if (!IS_AJAX && ($this->uri->rsegment(3) == 'list_record')) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }

        //set condition for the list
        $condition = array('po.enabled !=' => 0); //not deleted
        //not declined
        $condition_string = array("(`po`.`first_decline_date` = '0000-00-00 00:00:00' && `po`.`declined_date` = '0000-00-00 00:00:00' && `po`.`second_decline_date` = '0000-00-00 00:00:00' && `po`.`third_decline_date` = '0000-00-00 00:00:00'".")");
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    $date_from = $this->tools->getPost('search', 'date_from');
                    $date_to = $this->tools->getPost('search', 'date_to');
                    if ($var == 'supplier') {
                        $condition_string = array_merge($condition_string, array("s`.`supplier_code` LIKE '%" . trim($val) . "%' OR `s`.`supplier_name` LIKE '%" . trim($val) . "%'"));
                    } elseif ($var == 'acctg_unit') {
                        $condition_string = array_merge($condition_string, array("(concat(`pl`.`plate_number`,SPACE(1),`pl`.`make`,SPACE(1),`pl`.`model`) LIKE '%" . $val . "%' OR concat(`bu`.`business_unit_name`,SPACE(1),`bu`.`business_unit_code`) LIKE '%" . $val . "%')"));
                    } elseif ($var == 'date_from') {
                        if (isset($date_to)) {
                            $condition_string = array_merge($condition_string, array("po.order_date BETWEEN '$val' AND '$date_to'"));
                        }
                    } elseif ($var == 'status' || $var == 'total_amount') {
                        $condition = array_merge($condition, array('po.' . $var . " like" => "%" . $val . "%"));
                    } elseif ($var == 'date_to') {
                        $condition_string = array_merge($condition_string, array("po.order_date BETWEEN '$date_from' AND '$val'"));
                    } elseif ($var == 'order_date') {
                        $condition = array_merge($condition, array("po.order_date like" => "%" . date('Y-m-d', strtotime($val)) . "%"));
                        } elseif ($var == 'added_date') {
                        $condition = array_merge($condition, array("po.added_date like" => "%" . date('Y-m-d', strtotime($val)) . "%"));
                    
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get PO Status Summary List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array(); // to long to be poped-up
        foreach ($data['list']->result() as $q) {
            $q->acctg_unit = ($q->business_unit_name == 'TFleet') ? $q->plate_number : $q->business_unit_name;
            if ($q->first_level_id) {
                $first_level = $this->users_model->getFields($q->first_level_id);
                $q->first_level = ($first_level) ? "$first_level->user_fname $first_level->user_lname" : '---';
            }

            if ($q->approval_date != '0000-00-00 00:00:00') {
                $approver = $this->users_model->getFields($q->approved_by);
                $q->approved = "$approver->user_fname $approver->user_lname";
            }

            if ($q->declined_date != '0000-00-00 00:00:00') {
                $approver = $this->users_model->getFields($q->approved_by);
                $q->declined = "$approver->user_fname $approver->user_lname";
            }

            if ($q->second_level_id) {
                $second_level = $this->users_model->getFields($q->second_level_id);
                $q->second_level = ($second_level) ? "$second_level->user_fname $second_level->user_lname" : '---';
            }
            if ($q->third_level_id) {
                $third_level = $this->users_model->getFields($q->third_level_id);
                $q->third_level = ($third_level) ? "$third_level->user_fname $third_level->user_lname" : '---';
            }
        }
        $purchase_orders = array();
        $purchase_order_items = $this->purchase_order_items_model->getList($condition, $condition_string)->result();
        foreach ($purchase_order_items as $pri) {
            $purchase_orders[$pri->purchase_order_id][] = $pri;
        }
        $data['purchase_orders'] = $purchase_orders;
        if ($this->uri->rsegment(3) == 'pdf_report') {
            $data['title'] = 'PO Status Summary Report';
            parent::save_log("PO Status Summary Report PDF generated", 'report_po_status_summary', '', 'report_po_status_summary_pdf');
            $this->load->view(admin_dir('reports/report_po_status_summary_pdf'), $data);
        } else {
            $this->load->view(admin_dir('lists/reports/report_po_status_summary'), $data);
        }
    }

}
