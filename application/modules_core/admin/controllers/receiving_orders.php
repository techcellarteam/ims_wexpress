<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Receiving Orders Class
  |--------------------------------------------------------------------------
  |
  | Handles the Receiving Orders panel
  |
  | @category		Controller
  | @author		melin
 */

class Receiving_Orders extends Admin_Core {

    public $purchase_orders_model = '';
    public $purchase_orders_items_model = '';
    public $rr_model = '';
    public $rr_items_model = '';
    public $list_content = array();
    public $contents = array();
    public $users_model;

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        //load models
        $this->load->model('admin/purchase_requisitions_model');
        $this->load->model('admin/purchase_requisition_items_model');
        $this->load->model('admin/purchase_orders_model');
        $this->load->model('admin/purchase_order_items_model');
        $this->load->model('admin/document_types_model');
        $this->load->model('admin/business_units_model');
        $this->load->model('admin/items_model');
        $this->load->model('admin/notifications_model');
        $this->load->model('admin/classes_model');
        $this->load->model('admin/module_attachments_model');
        $this->load->model('admin/user_types_model');
        $this->load->model('admin/users_model');
        $this->load->model('admin/suppliers_model');
        $this->load->model('admin/stock_classifications_model');
        $this->load->model('admin/general_group_model');
        $this->load->model('admin/receiving_reports_model');
        $this->load->model('admin/receiving_report_items_model');
        $this->load->model('admin/item_location_model');
        $this->load->model('admin/inventory_changes_model');
        $this->load->model('admin/business_units_model');

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));
        //class ID
        $this->class_id = $this->classes_model->getFieldValue('id_class', array('class_name' => $this->classname));
        //initiate models
        $this->purchase_orders_model = new Purchase_Orders_Model();
        $this->purchase_order_items_model = new Purchase_Order_Items_Model();
        $this->purchase_requisitions_model = new Purchase_Requisitions_Model();
        $this->purchase_requisition_items_model = new Purchase_Requisition_Items_Model();
        $this->document_types_model = new Document_Types_Model();
        $this->stock_classifications_model = new Stock_Classifications_Model();
        $this->general_group_model = new General_Group_Model();
        $this->users_model = new Users_Model();
        $this->suppliers_model = new Suppliers_Model();
        $this->rr_model = new Receiving_Reports_Model();
        $this->rr_items_model = new Receiving_Report_Items_Model();
        $this->item_location_model = new Item_Location_Model();
        $this->inventory_changes_model = new Inventory_Changes_Model();
        $this->business_units_model = new Business_Units_Model();

        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_purchase_order',
            ),
            'po_number' => array(
                'label' => 'PO NO',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'po_number',
            ),
            'pr_number' => array(
                'label' => 'PR NO',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'pr_number',
            ),
            'supplier_code' => array(
                'label' => 'Supplier',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'supplier_code',
            ),
            'business_unit_code' => array(
                'label' => 'Business Unit',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'business_unit_code',
            ),
            'total_amount' => array(
                'label' => 'Total Amount',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'total_amount',
            ),
            'order_date' => array(
                'label' => 'Order Date',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'order_date',
            ),
            'status' => array(
                'label' => 'Status',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'status',
            ),
        );

        $this->contents = array('model_directory' => 'admin/purchase_orders_model',
            'model_name' => 'purchase_orders_model',
            'filters' => array(),
            'functionName' => 'Receiving Order'); // use to call functions for access

        $this->tools->setPostArray('sort', array('sort_by' => 'id_purchase_order', 'sort_type' => 'DESC'));
    }

    function index() {
        redirect(admin_dir('receiving_orders/list_receiving_order'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_receiving_order() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'receiving-order-tools.js'))),
        );
        $this->load->view(admin_dir('receiving_orders/list_receiving_order'), $data);
    }

    function view_receiving_order() {
        $id_purchase_order = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Purchase Order if Exist */
        $row = $this->purchase_orders_model->getFields($id_purchase_order);
        $po_items = $this->purchase_order_items_model->getList(array('purchase_order_id' => $id_purchase_order))->result();
        $user_logs_model = new User_Logs_Model();
        $logs = $user_logs_model->getSearch(array('user_log_table' => 'purchase_orders', 'user_log_value' => $id_purchase_order), array(), array('id_user_log' => 'DESC'))->result();

        //receiving items
        $receiving_reports = $this->rr_model->getList(array('purchase_order_id' => $id_purchase_order))->result();
        $rr_items = $this->rr_items_model->getList(array('po.id_purchase_order' => $id_purchase_order))->result();
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'receiving-order-tools.js'))),
                'row' => $row,
                'po_items' => $po_items,
                'logs' => $logs,
                'receiving_reports' => $receiving_reports,
                'rr_items' => $rr_items,
            );
            $this->load->view(admin_dir('receiving_orders/view_receiving_order'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function receive_purchase_order() {
        $id_purchase_order = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Purchase Order if Exist */
        $row = $this->purchase_orders_model->getFields($id_purchase_order);
        $po_items = $this->purchase_order_items_model->getList(array('purchase_order_id' => $id_purchase_order))->result();

        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'receiving-order-tools.js'))),
                'row' => $row,
                'po_items' => $po_items,
            );
            $this->load->view(admin_dir('receiving_orders/receive_order'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_receiving_order') { /* Method for Account */
            self::_method_list_receiving_order();
        } else if ($this->uri->rsegment(3) == 'view_receiving_order') {
            self::view_receiving_order();
        } else if ($this->uri->rsegment(3) == 'receive_purchase_order') {
            self::_method_receive_purchase_order();
        } else if ($this->uri->rsegment(3) == 'edit_receive_po') {
            self::_method_edit_receive_po();
        } else if ($this->uri->rsegment(3) == 'add_comment') {
            self::_method_add_comment();
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Validation of the Form -------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    private function _validate() {
        $this->form_validation->set_rules('invoice_number', 'Invoice Number', 'htmlspecialchars|trim|required|is_unique[receiving_reports.invoice_number]');
        $this->form_validation->set_rules('date_received', 'Date Received', 'htmlspecialchars|trim|required');
        if ($this->tools->getPost('qty_rcvd')) {
            foreach ($this->tools->getPost('qty_rcvd') as $key => $val) {
                $this->form_validation->set_rules("qty_rcvd[$key]", 'Quantity Received', 'htmlspecialchars|trim|required|is_natural');
            }
        }
    }

    private function _validate_edit() {
        $this->form_validation->set_rules('invoice_number[]', 'Invoice Number', 'htmlspecialchars|trim|required');
        if ($this->tools->getPost('qty_rcvd')) {
            foreach ($this->tools->getPost('qty_rcvd') as $key => $val) {
                $this->form_validation->set_rules("qty_rcvd[$key]", 'Quantity Received', 'htmlspecialchars|trim|required|is_natural');
            }
        }
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_receiving_order() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }
        $condition = array();
        //show purchase requisitions requested by the business_unit only if user is not PEM, FM, CEO,OM or Super Admin
        if ((int) $this->session->userdata['admin']['user_type_id'] != 1) {
            $user_type_code = $this->session->userdata['admin']['user_type_code'];
            switch ($user_type_code) {
                case 'PU Head':
                    break;
                case 'Purchaser':
                    break;
                case 'PO Initiator':
                    break;
                case 'Administrator':
                    break;
                default:
                    $group_id = $this->users_model->getValue($this->session->userdata['admin']['user_id'], 'general_group_id');
                    $condition = array('du.business_unit_id' => $this->session->userdata['admin']['business_unit_id'],
                        'bu.general_group_id' => $group_id);
                    break;
            }
        }
        //set condition for the list
        $condition_string = array("`po`.`status` IN ('For Receiving','Partial Delivery')");
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_purchase_order') {
                        $condition = array_merge($condition, array($var => $val));
                    } elseif ($var == 'added_date') {
                        $condition = array_merge($condition, array('po.' . $var . " like" => "%" . $val . "%"));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get Purchase Order List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array('');

        $this->load->view(admin_dir('lists/list'), $data);
    }

    /*
     * 	Receive Purchase Order Items Method
     */

    function _method_receive_purchase_order() {
        if (IS_AJAX) {
            self::_validate();
            $id_purchase_order = $this->Misc->decode_id($this->tools->getPost('id'));
            /* Check Purchase Order if Exist */
            $row = $this->purchase_orders_model->getFields($id_purchase_order);
            $po_items = $this->purchase_order_items_model->getList(array('purchase_order_id' => $id_purchase_order))->result();
            $purchase_requisition = $this->purchase_requisitions_model->getFields($row->purchase_requisition_id);
            if ($row) {
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    //add receiving report details
                    $last_rr_number = $this->rr_model->getLastNo('rr_number', 'added_date', date('Y'));
                    $rr_number = isset($last_rr_number) ? ((int) str_replace('-', '', filter_var($last_rr_number, FILTER_SANITIZE_NUMBER_INT)) + 1) : date('y') . '00001'; // set purchase requisition number
                    $date_received = date('Y-m-d', strtotime($this->tools->getPost('date_received')));
                    $business_unit_id = $row->business_unit_id;
                    $invoice_number = $this->tools->getPost('invoice_number');
                    if ((int) $purchase_requisition->for_stocking == 1) {
                        $business_unit_id = $this->business_units_model->getFieldValue('id_business_unit', array('business_unit_code' => 'CID'));
                    }
                    $rr_data = array(
                        'purchase_order_id' => $id_purchase_order,
                        'rr_number' => $rr_number,
                        'invoice_number' => $invoice_number,
                        'date_received' => $date_received,
                        'added_by' => $this->session->userdata['admin']['user_id'],
                        'added_date' => date('Y-m-d H:i:s')
                    );
                    $receiving_report_id = $this->rr_model->add($rr_data);
                    $total_qty = 0;
                    $total_rcvd = 0;
                    foreach ($po_items as $q) {
                        $total_qty += $q->quantity;
                        if ((int) $this->tools->getPost('qty_rcvd', $q->id_po_item) > 0) {
                            $qty_rcvd = $this->tools->getPost('qty_rcvd', $q->id_po_item);
                            $new_qty = ($q->qty_rcvd + $qty_rcvd);
                            $total_rcvd += $new_qty;
                            $this->purchase_order_items_model->update(array('id_po_item' => $q->id_po_item), array('qty_rcvd' => $new_qty));

                            //added item location model
                            $il_data = array(
                                'location_id' => $business_unit_id,
                                'po_item_id' => $q->id_po_item,
                                'item_id' => $this->tools->getPost('item_id', $q->id_po_item),
                                'item_quantity' => $qty_rcvd,
                                'item_count' => $qty_rcvd,
                                'added_by' => $this->session->userdata['admin']['user_id'],
                                'added_date' => date('Y-m-d H:i:s')
                            );
                            $item_location_id = $this->item_location_model->add($il_data);

                            //add receiving report item
                            $rr_item_data = array('receiving_report_id' => $receiving_report_id,
                                'po_item_id' => $q->id_po_item,
                                'item_location_id' => $item_location_id,
                                'qty_received' => $qty_rcvd,
                                'remarks' => $this->tools->getPost('remarks', $q->id_po_item));
                            $this->rr_items_model->add($rr_item_data);

                            //update data to inventory change
                            $inventory_data = array(
                                'item_location_id' => $item_location_id,
                                'administrator_id' => $this->session->userdata['admin']['user_id'],
                                'stock_before' => 0,
                                'stock_quantity' => $qty_rcvd,
                                'stock_after' => $qty_rcvd,
                                'inventory_date' => $date_received,
                                'transaction' => 'Goods Inbound', //from purchase order
                                'added_by' => $this->session->userdata['admin']['user_id'],
                                'added_date' => date('Y-m-d H:i:s')
                            );
                            $this->inventory_changes_model->add($inventory_data);
                        }
                    }

                    //check if partial/fully received                                                                                                                                                                   
                    $status = $row->status;
                    if ($total_rcvd > 0) {
                        if ($total_rcvd < $total_qty) {
                            $status = 'Partial Delivery';
                        } else {
                            $status = 'Full Delivery';
                        }

                        $this->purchase_orders_model->update(array('id_purchase_order' => $id_purchase_order), array('status' => $status));

                        // save comment as log
                        parent::save_log("PO receive items from invoice# $invoice_number", 'receiving_reports', $id_purchase_order, '', 'purchase_orders');
                        self::notify_creator($row->added_by, $id_purchase_order, $status);
                        self::notify_creator($row->bu_head_id, $id_purchase_order, $status);

                        $arr = array(
                            'message_alert' => $this->Misc->message_status('success', "PO received $status"),
                            'id' => 1
                        );
                    } else {
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('error', "Input Received Values"),
                            'id' => 0
                        );
                    }

                    $this->db->trans_complete();
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Edit Received Purchase Order Items Method
     */

    function _method_edit_receive_po() {
        if (IS_AJAX) {
            self::_validate_edit();
            $id_purchase_order = $this->Misc->decode_id($this->tools->getPost('id'));
            /* Check Purchase Order if Exist */
            $row = $this->purchase_orders_model->getFields($id_purchase_order);
            if ($row) {
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    $invoice_number = $this->tools->getPost('invoice_number');
                    $received = $this->tools->getPost('qty_rcvd');
                    
                    foreach ($invoice_number as $receiving_report_id => $inv_number) {
                        $this->rr_model->update(array('id_receiving_report' => $receiving_report_id), array('invoice_number' => $inv_number));
                    }
                    $total_qty = $this->purchase_order_items_model->getSum('quantity','quantity',array('purchase_order_id' => $id_purchase_order));
                    $total_rcvd = array_sum($received);
                    foreach ($received as $rr_item_id => $qty_received) {
                        $rr_item = $this->rr_items_model->getFields($rr_item_id);
                        //save changes if with not equal of qty received
                        if ($rr_item->qty_received != $qty_received) {
                            $date_received = $this->tools->getPost('date_received', $rr_item->receiving_report_id);
                            $remarks = $this->tools->getPost('remarks', $rr_item_id);
                            $stock_before = $rr_item->qty_received;
                            // update receiving report item and PO Item table
                            $this->rr_items_model->update(array('id_receiving_report_item' => $rr_item_id), array('qty_received' => $qty_received,
                                'remarks' => $remarks));
                            $this->purchase_order_items_model->update(array('id_po_item' => $rr_item->po_item_id), array('qty_rcvd' => $qty_received));

                            //disabled item location detail and new one
                            $item_location = $this->item_location_model->getFields($rr_item->item_location_id);
                            $this->item_location_model->update(array('id_item_location' => $rr_item->item_location_id), array('enabled' => 0));

                            //added item location model
                            $il_data = array(
                                'location_id' => $item_location->location_id,
                                'po_item_id' => $item_location->po_item_id,
                                'item_id' => $rr_item->item_id,
                                'item_quantity' => $qty_received,
                                'item_count' => $qty_received,
                                'added_by' => $this->session->userdata['admin']['user_id'],
                                'added_date' => date('Y-m-d H:i:s')
                            );
                            $item_location_id = $this->item_location_model->add($il_data);
                            
                            //add changes to inventory movement logs
                            $inventory_data = array(
                                'item_location_id' => $item_location_id,
                                'administrator_id' => $this->session->userdata['admin']['user_id'],
                                'stock_before' => $stock_before,
                                'stock_quantity' => $qty_received,
                                'stock_after' => $qty_received,
                                'inventory_date' => $date_received,
                                'transaction' => 'Goods Inbound from Edited RR',
                                'remarks' => $remarks,
                                'added_by' => $this->session->userdata['admin']['user_id'],
                                'added_date' => date('Y-m-d H:i:s')
                            );
                            $this->inventory_changes_model->add($inventory_data);
                        }
                    }
                    //check if partial/fully received                                                                                                                                                                   
                    $status = $row->status;
                    if ($total_rcvd > 0) {
                        if ($total_rcvd < $total_qty) {
                            $status = 'Partial Delivery';
                        } else {
                            $status = 'Full Delivery';
                        }

                        $this->purchase_orders_model->update(array('id_purchase_order' => $id_purchase_order), array('status' => $status));
                    }

                    // save comment as log
                    parent::save_log("RR was edited and corrected received quantity", 'receiving_reports', $id_purchase_order, '', 'purchase_orders');
                    self::notify_creator($row->added_by, $id_purchase_order, $status);
                    self::notify_creator($row->bu_head_id, $id_purchase_order, $status);

                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', "RR was successfully edited, $status"),
                        'id' => $id_purchase_order
                    );
                    $this->db->trans_complete();
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	save comments
     */

    function _method_add_comment() {
        if (IS_AJAX) {
            $id_purchase_order = $this->Misc->decode_id($this->tools->getPost('id'));
            /* Check Purchase Order if Exist */
            $row = $this->purchase_orders_model->getFields($id_purchase_order);
            if ($row) {
                $this->form_validation->set_rules('comment', 'Comment', 'htmlspecialchars|trim|required');
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    // save comment as log
                    parent::save_log($this->tools->getPost('comment'), 'purchase_orders', $id_purchase_order);

                    //notify business_unit requestor
                    $status = "Comment: " . $this->tools->getPost('comment');
                    self::notify_creator($row->added_by, $id_purchase_order, $status);

                    $this->db->trans_complete();
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Saved Remarks'),
                        'id' => 1
                    );
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * Notify creator of PO
     */

    private
            function notify_creator($added_by, $id_purchase_order, $status) {
        //notify Initiator for declined PO
        $notif_data = array(
            'class_id' => $this->class_id,
            'reference_id' => $id_purchase_order,
            'user_id' => $added_by,
            'link' => 'view_receiving_order',
            'status' => $status);
        $notification_model = new Notifications_Model();
        $notification_model->add($notif_data);
    }

}
