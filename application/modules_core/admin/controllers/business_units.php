<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Business Units Class
  |--------------------------------------------------------------------------
  |
  | Handles the Business Units panel
  |
  | @category		Controller
  | @author		melin
 */

class Business_Units extends Admin_Core {

    public $business_units_model = '';
    public $list_content = array();
    public $contents = array();

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        //load models
        $this->load->model('admin/business_units_model');
        $this->load->model('admin/general_group_model');
        $this->load->model('admin/bu_purchasers_model');
        $this->load->model('admin/users_model');

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        //initiate models
        $this->business_units_model = new Business_Units_Model();
        $this->group_model = new General_Group_Model();
        $this->users_model = new Users_Model();
        $this->bu_purchasers_model = new BU_Purchasers_Model();
        //set list contents
        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_business_unit',
            ),
            'group_code' => array(
                'label' => 'Group',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-2',
                'var-value' => 'group_code',
            ),
            'business_unit_name' => array(
                'label' => 'Business Unit Name',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-4',
                'var-value' => 'business_unit_name',
            ),
            'business_unit_code' => array(
                'label' => 'Business Unit Code',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-2',
                'var-value' => 'business_unit_code',
            ),
            'location' => array(
                'label' => 'Location',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-2',
                'var-value' => 'location',
            ),
        );

        $this->contents = array('model_directory' => 'admin/business_units_model',
            'model_name' => 'business_units_model',
            'filters' => array(),
            'functionName' => 'Business Unit'); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('business_units/list_business_unit'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_business_unit() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'business-unit-tools.js'))),
        );
        $this->load->view(admin_dir('business_units/list_business_unit'), $data);
    }

    function add_business_unit() {
        $data = array(
            'template' => parent::main_template(),
//            'js_files' => array('js_files' => array(js_dir('modules', 'business-unit-tools.js'))),
            'users' => $this->users_model->getList()->result(),
            'groups' => $this->group_model->getList()->result(),
            'purchasers' => $this->users_model->getList()->result(),
        );
        $this->load->view(admin_dir('business_units/add_business_unit'), $data);
    }

    function edit_business_unit() {
        $id_business_unit = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Business Unit if Exist */
        $row = $this->business_units_model->getFields($id_business_unit);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
//                'js_files' => array('js_files' => array(js_dir('modules', 'business-unit-tools.js'))),
                'users' => $this->users_model->getList()->result(),
                'groups' => $this->group_model->getList()->result(),
                'purchasers' => $this->users_model->getList()->result(),
            );
            $this->load->view(admin_dir('business_units/edit_business_unit'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function view_business_unit() {
        $id_business_unit = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Business Unit if Exist */
        $row = $this->business_units_model->getFields($id_business_unit);
        $bu_purchasers = $this->misc->value_to_string($this->bu_purchasers_model->getList(array('bup.business_unit_id' => $id_business_unit))->result(), 'purchaser_id');
        $purchasers = array();
        if (!empty($bu_purchasers)) {
            $purchasers = $this->users_model->getList(array(), array("`id_user` IN ($bu_purchasers)"))->result();
        }
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
                'purchasers' => $purchasers,
            );
            $this->load->view(admin_dir('business_units/view_business_unit'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_business_unit') { /* Method for Account */
            self::_method_list_business_unit();
        } else if ($this->uri->rsegment(3) == 'add_business_unit') {
            self::_method_add_business_unit();
        } else if ($this->uri->rsegment(3) == 'edit_business_unit') {
            self::_method_edit_business_unit();
        } else if ($this->uri->rsegment(3) == 'delete_business_unit') {
            self::_method_delete_business_unit();
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Validation of the Form -------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    private function _validate() {
        $this->form_validation->set_rules('business_unit_head_id', 'Business Unit Head', 'htmlspecialchars|trim|int');
        $this->form_validation->set_rules('general_group_id', 'Group', 'htmlspecialchars|trim|int');
        $this->form_validation->set_rules('business_unit_name', 'Name', 'htmlspecialchars|trim|required|max_length[64]');
        $this->form_validation->set_rules('business_unit_code', 'Code', 'htmlspecialchars|trim|required|max_length[16]');
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_business_unit() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }

        //set condition for the list
        $condition = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_business_unit') {
                        $condition = array_merge($condition, array($var => $val));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'contents' => $this->contents
        );

        /* Get Business Unit List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array('edit', 'view');

        $this->load->view(admin_dir('lists/list'), $data);
    }

    /*
     * 	Add New Business Unit Method
     */

    function _method_add_business_unit() {
        if (IS_AJAX) {
            self::_validate();

            //check form validation then save
            if ($this->form_validation->run() == TRUE) {
                $this->db->trans_start();
                //getPost data set to this model fields
                parent::copyFromPost($this->business_units_model, 'id_business_unit');

                /* Check Business Unit Code Name Exist */
                $count = $this->business_units_model->getSearch(array("business_unit_name like" => $this->tools->getPost('business_unit_name')), "", "", "", true);
                if ($count == 0) {
                    $bu_id = $this->business_units_model->add();
                    //add business unit purchasers
                    if ($this->tools->getPost('purchasers')) {
                        foreach ($this->tools->getPost('purchasers') as $key => $q) {
                            $this->bu_purchasers_model->add(array('business_unit_id' => $bu_id, 'purchaser_id' => $q));
                        }
                    }
                    parent::save_log("add new business unit " . $this->tools->getPost('business_unit_name'), 'business_units', $this->business_units_model->id);
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                        'id' => $this->business_units_model->id
                    );
                } else {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", "Code already exist"),
                        'id' => 0
                    );
                }
                $this->db->trans_complete();
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	Edit Business Unit Type Method
     */

    function _method_edit_business_unit() {
        if (IS_AJAX) {
            $id_business_unit = $this->Misc->decode_id($this->tools->getPost('id'));

            /* Check Business Unit Type if Exist */
            $row = $this->business_units_model->getFields($id_business_unit);
            if ($row) {
                self::_validate();
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    /* Check Business Unit Exist */
                    $count = $this->business_units_model->getSearch(array('id_business_unit !=' => $id_business_unit, "business_unit_name like" => $this->tools->getPost('business_unit_name')), "", "", "", true);
                    if ($count == 0) {
                        /* Update Business Unit Info */
                        //getPost data set to this model fields
                        parent::copyFromPost($this->business_units_model, 'id_business_unit');
                        $this->business_units_model->update(array('id_business_unit' => $id_business_unit));
                        // updated business unit purchasers
                        if ($this->tools->getPost('purchasers')) {
                            foreach ($this->tools->getPost('purchasers') as $key => $q) {
                                $check_id = (int) $this->bu_purchasers_model->getFieldValue('id_bu_purchaser', array('business_unit_id' => $id_business_unit, 'purchaser_id' => $q));
                                if ($check_id == 0) {
                                    $this->bu_purchasers_model->add(array('business_unit_id' => $id_business_unit, 'purchaser_id' => $q));
                                }
                            }
                        }
                        parent::save_log("update business unit " . $this->Misc->update_name_log($row->business_unit_name, $this->tools->getPost('business_unit_name')), 'business_units', $id_business_unit);
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                            'id' => $id_business_unit
                        );
                    } else {
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('error', "Business Unit Name already exist"),
                            'id' => 0
                        );
                    }
                    $this->db->trans_complete();
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Delete Business Unit Method
     */

    function _method_delete_business_unit() {
        if (IS_AJAX) {
            $id_business_unit = $this->Misc->decode_id($this->tools->getPost('item'));
            /* Check Business Unit if Exist */
            $row = $this->business_units_model->getFields($id_business_unit);
            if ($row) {
                $datas = array(
                    'enabled' => 0,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete Business Unit */
                $this->business_units_model->update_table($datas, "id_business_unit", $id_business_unit);
                parent::save_log("delete business unit" . $row->business_unit_name, 'business_units', $id_business_unit);
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Deleted'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

}
