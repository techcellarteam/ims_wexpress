<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Full Deliveries Class
  |--------------------------------------------------------------------------
  |
  | Handles the Full Deliveries panel
  |
  | @category		Controller
  | @author		melin
 */

class Payment_Requests extends Admin_Core {

    public $payment_requests_model = '';
    public $purchase_orders_model = '';
    public $purchase_orders_items_model = '';
    public $rr_model = '';
    public $rr_items_model = '';
    public $list_content = array();
    public $contents = array();
    public $users_model;

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        //load models
        $this->load->model('admin/purchase_requisitions_model');
        $this->load->model('admin/purchase_requisition_items_model');
        $this->load->model('admin/purchase_orders_model');
        $this->load->model('admin/purchase_order_items_model');
        $this->load->model('admin/document_types_model');
        $this->load->model('admin/business_units_model');
        $this->load->model('admin/items_model');
        $this->load->model('admin/notifications_model');
        $this->load->model('admin/classes_model');
        $this->load->model('admin/module_attachments_model');
        $this->load->model('admin/user_types_model');
        $this->load->model('admin/users_model');
        $this->load->model('admin/suppliers_model');
        $this->load->model('admin/stock_classifications_model');
        $this->load->model('admin/general_group_model');
        $this->load->model('admin/receiving_reports_model');
        $this->load->model('admin/receiving_report_items_model');
        $this->load->model('admin/payment_requests_model');
        $this->load->model('admin/request_payments_model');

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));
        //class ID
        $this->class_id = $this->classes_model->getFieldValue('id_class', array('class_name' => $this->classname));
        //initiate models
        $this->payment_requests_model = new Payment_Requests_Model();
        $this->purchase_orders_model = new Purchase_Orders_Model();
        $this->purchase_order_items_model = new Purchase_Order_Items_Model();
        $this->purchase_requisitions_model = new Purchase_Orders_Model();
        $this->purchase_requisition_items_model = new Purchase_Requisition_Items_Model();
        $this->document_types_model = new Document_Types_Model();
        $this->stock_classifications_model = new Stock_Classifications_Model();
        $this->general_group_model = new General_Group_Model();
        $this->users_model = new Users_Model();
        $this->suppliers_model = new Suppliers_Model();
        $this->rr_model = new Receiving_Reports_Model();
        $this->rr_items_model = new Receiving_Report_Items_Model();

        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_payment_request',
            ),
            'emergency' => array(
                'label' => 'Category',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'emergency',
            ),
            'prf_number' => array(
                'label' => 'PRF NO',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'prf_number',
            ),
            'po_number' => array(
                'label' => 'PO NO',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'po_number',
            ),
            'supplier_code' => array(
                'label' => 'Supplier',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'supplier_code',
            ),
            'business_unit_code' => array(
                'label' => 'Business Unit',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'business_unit_code',
            ),
            'total_amount' => array(
                'label' => 'Total Amount',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'total_amount',
            ),
            'paid_amount' => array(
                'label' => 'Paid Amount',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'paid_amount',
            ),
            'request_payment_due' => array(
                'label' => 'Due Date',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'request_payment_due',
            ),
            'status' => array(
                'label' => 'Status',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'status',
            ),
        );

        $this->contents = array('model_directory' => 'admin/payment_requests_model',
            'model_name' => 'payment_requests_model',
            'filters' => array(),
            'functionName' => 'Payment Request'); // use to call functions for access

        $this->tools->setPostArray('sort', array('sort_by' => 'id_payment_request', 'sort_type' => 'DESC'));
    }

    function index() {
        redirect(admin_dir('payment_requests/list_payment_request'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_payment_request() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'payment-request-tools.js'))),
        );
        $this->load->view(admin_dir('payment_requests/list_payment_request'), $data);
    }

    function add_payment_request() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'payment-request-tools.js'))),
            'invoice_numbers' => $this->rr_model->getInvoiceNumber(array('invoice_number !=' => '', 'prf_number' => null))->result()
        );
        $this->load->view(admin_dir('payment_requests/add_payment_request'), $data);
    }

    function edit_payment_request() {
        $payment_request_id = $this->Misc->decode_id($this->uri->rsegment(3));
        $row = $this->payment_requests_model->getFields($payment_request_id);
        if ($row && (int) $row->approved == 0) {
            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'payment-request-tools.js'))),
                'row' => $row,
                'rr_items' => $this->rr_items_model->getList(array('id_receiving_report' => $row->receiving_report_id))->result(),
            );
            $this->load->view(admin_dir('payment_requests/edit_payment_request'), $data);
        } else {
            $arr = array(
                'message' => $this->Misc->message_status($this->Misc->message_status('error', "RFP not yet approved can edited.")),
            );
            $this->load->view(admin_dir('template/deny'), $arr);
        }
    }

    function view_payment_request() {
        $id_payment_request = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Purchase Order if Exist */
        $row = $this->payment_requests_model->getFields($id_payment_request);
        $user_logs_model = new User_Logs_Model();
        $logs = $user_logs_model->getSearch(array('user_log_table' => 'payment_requests', 'user_log_value' => $id_payment_request), array(), array('id_user_log' => 'DESC'))->result();

        //receiving items
        $rr_items = $this->rr_items_model->getList(array('id_receiving_report' => $row->receiving_report_id))->result();
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'js_files' => array('js_files' => array(js_dir('modules', 'payment-request-tools.js'))),
                'row' => $row,
                'logs' => $logs,
                'rr_items' => $rr_items,
            );
            $this->load->view(admin_dir('payment_requests/view_payment_request'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function display_date_input() {
        $id_payment_request = $this->Misc->decode_id($this->tools->getPost('id'));
        /* Check Payment Request if Exist */
        $row = $this->payment_requests_model->getFields($id_payment_request);
        $data = array(
                'row' => $row,
            );
        if ($row->date_forwarded_acctg == '0000-00-00') {
            
            $this->load->view(admin_dir('payment_requests/extra/date_forwared_input'), $data);
        }else{
             $this->load->view(admin_dir('payment_requests/extra/date_ok'), $data);
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Validation of the Form -------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    private function _validate() {
        $this->form_validation->set_rules('emergency', 'Categroy', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('receiving_report_id', 'Invoice Number', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('due_date', 'Due Date', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('remarks', 'Remarks', 'htmlspecialchars|trim');
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_payment_request') { /* Method for Account */
            self::_method_list_payment_request();
        } else if ($this->uri->rsegment(3) == 'view_payment_request') {
            self::view_payment_request();
        } else if ($this->uri->rsegment(3) == 'add_payment_request') {
            self::_method_add_prf();
        } else if ($this->uri->rsegment(3) == 'edit_payment_request') {
            self::_method_edit_prf();
        } else if ($this->uri->rsegment(3) == 'add_comment') {
            self::_method_add_comment();
        } else if ($this->uri->rsegment(3) == 'getPRFdetails') {
            self::_method_getPRFdetails();
        } else if ($this->uri->rsegment(3) == 'delete_payment_request') {
            self::_method_delete_payment_request();
        } else if ($this->uri->rsegment(3) == 'display_date_input') {
            self::_method_display_date_input();
        }
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    /*
     * ADD details to PRF
     */

    function _method_add_prf() {
        if (IS_AJAX) {
            self::_validate();
            if ($this->form_validation->run() == TRUE) {
                $this->payment_requests_model = new Payment_Requests_Model();
                $this->request_payments_model = new Request_Payments_Model();

                $id_receiving_report = $this->tools->getPost('receiving_report_id');
                $receiving_report = $this->rr_model->getFields($id_receiving_report);
                /* Check Purchase Order if Exist */
                $row = $this->purchase_orders_model->getFields($receiving_report->purchase_order_id);
                if ($row) {
                    $this->db->trans_start();
                    //receiving report
                    $rr_total_amount = $this->rr_items_model->getTotalAmount(array('receiving_report_id' => $id_receiving_report));
                    //$last_prf_number = $this->payment_requests_model->getLastNo('prf_number', 'added_date', date('Y-m'));
                    $last_rfp_number = $this->request_payments_model->getLastNo('request_payment_number', 'added_date', date('Y-m'), array('cost_center_code' => 'IC'));
                    $rfp_number = ($last_rfp_number) ? ++$last_rfp_number : 1; // set prf number
                    //get rfp number from accounting
                    $prf_number = isset($rfp_number) ? 'IC-' . date('Y') . '-' . date('m') . '-' . sprintf('%05d', ($rfp_number)) : 'IC-' . date('Y') . '-' . date('m') . '-' . '00001'; // set purchase requisition number
                    //accounting data                
                    $rfp_data = array(
                        'request_payment_number' => $rfp_number,
                        'cost_center_code' => 'IC',
                        'cost_center_id' => 30, //from accounting
                        'po_number' => $row->po_number, //from accounting
                        'requested_by' => $this->session->userdata['admin']['name'],
                        'request_payment_supplier' => $row->supplier_name,
                        'request_payment_payee' => $row->supplier_name, //($row->supplier_contact_person) ? $row->supplier_contact_person : $row->supplier_name,
                        'request_payment_amount' => $rr_total_amount,
                        'request_payment_due' => date('Y-m-d', strtotime($this->tools->getPost('due_date'))), //date($row->approval_date, strtotime("+$row->terms_payment days")),
                        'request_payment_particulars' => $this->tools->getPost('request_payment_particulars'),
                        'added_by' => 8, //from accounting user
                        'added_date' => date('Y-m-d H:i:s'),
                    );

                    if ($request_payment_id = $this->request_payments_model->add($rfp_data)) {
                        $prf_data = array(
                            'request_payment_id' => $request_payment_id,
                            'receiving_report_id' => $receiving_report->id_receiving_report,
                            'prf_number' => $prf_number,
                            'total_amount' => $rr_total_amount,
                            'emergency' => $this->tools->getPost('emergency'),
                            'remarks' => $this->tools->getPost('remarks'),
                            'invoice_received_date' => date('Y-m-d', strtotime($this->tools->getPost('invoice_received_date'))),
                            'invoice_received_by' => $this->tools->getPost('invoice_received_by'),
                            'added_by' => $this->session->userdata['admin']['user_id'],
                            'added_date' => date('Y-m-d H:i:s')
                        );
                        $id_payment_request = $this->payment_requests_model->add($prf_data);
                        parent::save_log("PRF# $rfp_number was added in this Transaction", 'purchase_orders', $receiving_report->purchase_order_id);
                        parent::save_log("PRF# $rfp_number was added in this Transaction", 'payment_requests', $id_payment_request);
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('success', 'Successfuly Requested Payment'),
                            'id' => 1
                        );
                        $this->db->trans_complete();
                    }
                }
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    /*
     * EDIT details to PRF
     */

    function _method_edit_prf() {
        if (IS_AJAX) {
            self::_validate();
            $payment_request_id = $this->Misc->decode_id($this->tools->getPost('id'));
            $row = $this->payment_requests_model->getFields($payment_request_id);
            if ($this->form_validation->run() == TRUE) {
                $id_receiving_report = $row->receiving_report_id;
                $receiving_report = $this->rr_model->getFields($id_receiving_report);
                if ($row) {
                    $this->db->trans_start();
                    //accounting data                
                    $rfp_data = array(
                        'request_payment_payee' => $this->tools->getPost('request_payment_payee'),
                        'request_payment_due' => date('Y-m-d', strtotime($this->tools->getPost('due_date'))), //date($row->approval_date, strtotime("+$row->terms_payment days")),
                        'updated_by' => 8, //from accounting user
                        'updated_date' => date('Y-m-d H:i:s'),
                    );

                    if ($this->request_payments_model->update(array('id_request_payment' => $row->request_payment_id), $rfp_data)) {
                        $prf_data = array(
                            'emergency' => $this->tools->getPost('emergency'),
                            'remarks' => $this->tools->getPost('remarks'),
                            'invoice_received_date' => date('Y-m-d', strtotime($this->tools->getPost('invoice_received_date'))),
                            'invoice_received_by' => $this->tools->getPost('invoice_received_by'),
                            'updated_by' => $this->session->userdata['admin']['user_id'],
                            'updated_date' => date('Y-m-d H:i:s')
                        );
                        $this->payment_requests_model->update(array('id_payment_request' => $row->id_payment_request), $prf_data);
                        parent::save_log("PRF# $row->prf_number was updated", 'purchase_orders', $receiving_report->purchase_order_id);
                        parent::save_log("PRF# $row->prf_number was updated", 'payment_requests', $row->id_payment_request);
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('success', 'Successfuly updated request for payment'),
                            'id' => 1
                        );
                        $this->db->trans_complete();
                    }
                }
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    function _method_list_payment_request() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }
        $condition = array();
        //show purchase requisitions requested by the business_unit only if user is not PEM, FM, CEO,OM or Super Admin
        if ((int) $this->session->userdata['admin']['user_type_id'] != 1) {
            $user_type_code = $this->session->userdata['admin']['user_type_code'];
            switch ($user_type_code) {
                case 'PU Head':
                    break;
                default:
                    $group_id = $this->users_model->getValue($this->session->userdata['admin']['user_id'], 'general_group_id');
                    $condition = array('du.business_unit_id' => $this->session->userdata['admin']['business_unit_id'],
                        'bu.general_group_id' => $group_id);
                    break;
            }
        }
        //set condition for the list
        $condition_string = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_payment_request') {
                        $condition = array_merge($condition, array($var => $val));
                    } elseif ($var == 'due_date') {
                        $condition = array_merge($condition, array('prf.' . $var . " like" => "%" . $val . "%"));
                    } elseif ($var == 'po_number') {
                        $condition = array_merge($condition, array('po.' . $var . " like" => "%" . $val . "%"));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get Purchase Order List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        foreach ($data['list']->result() as $q) {
            $q->status = (($q->total_amount > 0) ? (($q->paid_amount / $q->total_amount) * 100) . "% paid" : '0%');
            $q->emergency = ($q->emergency) ? 'Emergency' : 'Non-emergency';
        }
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array('');
        $this->load->view(admin_dir('lists/list'), $data);
    }

    /*
     * 	Delete Payment Request Method
     */

    function _method_delete_payment_request() {
        if (IS_AJAX) {
            $payment_request_id = $this->Misc->decode_id($this->tools->getPost('item'));
            $row = $this->payment_requests_model->getFields($payment_request_id);
            if ($row && (int) $row->approved == 0) {
                $this->db->trans_start();
                $datas = array(
                    'enabled' => 0,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete Purchase Order */
                $this->payment_requests_model->update_table($datas, "id_payment_request", $payment_request_id);
                parent::save_log("Deleted", 'payment_requests', $payment_request_id);
                $this->db->trans_complete();
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'PRF was deleted'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            } else {
                $arr = array(
                    'message' => $this->Misc->message_status($this->Misc->message_status('error', "RFP that was approved can't be deleted.")),
                );
                $this->load->view(admin_dir('template/deny'), $arr);
            }
        }
    }

    /*
     * 	Get PRF Details
     */

    function _method_getPRFdetails() {
        $receiving_report_id = $this->tools->getPost('receiving_report_id');
        $data['rr_items'] = $this->rr_items_model->getList(array('id_receiving_report' => $receiving_report_id))->result();
        $this->load->view(admin_dir('payment_requests/extra/particulars'), $data);
    }

    /*
     * 	save comments
     */

    function _method_add_comment() {
        if (IS_AJAX) {
            $id_payment_request = $this->Misc->decode_id($this->tools->getPost('id'));
            /* Check Purchase Order if Exist */
            $row = $this->payment_requests_model->getFields($id_payment_request);
            if ($row) {
                $this->form_validation->set_rules('comment', 'Comment', 'htmlspecialchars|trim|required');
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    // save comment as log
                    parent::save_log($this->tools->getPost('comment'), 'payment_requests', $id_payment_request);

                    //notify business_unit requestor
                    $status = "Comment: " . $this->tools->getPost('comment');
                    self::notify_creator($row->added_by, $id_payment_request, $status);

                    $this->db->trans_complete();
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Saved Remarks'),
                        'id' => 1
                    );
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * Save Date forwarded to accounting
     */

    function _method_display_date_input() {
        if (IS_AJAX) {
            $this->form_validation->set_rules('date_forwarded_acctg', 'Date Forwared to Acctg', 'htmlspecialchars|trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->payment_requests_model = new Payment_Requests_Model();

                $id_payment_request = $this->tools->getPost('id');
                /* Check Purchase Order if Exist */
                $row = $this->payment_requests_model->getFields($id_payment_request);
                if ($row) {
                    $this->db->trans_start();
                    $date_forwarded_acctg = date('Y-m-d', strtotime($this->tools->getPost('date_forwarded_acctg')));
                    if ($this->payment_requests_model->update(array('id_payment_request' => $id_payment_request), array('date_forwarded_acctg' => $date_forwarded_acctg))) {
                        // save comment as log
                        parent::save_log("Saved Invoice Date Forwared to Acctg", 'payment_requests', $id_payment_request);
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('success', 'Saved Remarks'),
                            'id' => 1
                        );
                        $this->db->trans_complete();
                    }
                } else {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", "Payment Request NOT found"),
                        'id' => 0
                    );
                }
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * Notify creator of PO
     */

    private function notify_creator($added_by, $id_payment_request, $status) {
        //notify Initiator for declined PO
        $notif_data = array(
            'class_id' => $this->class_id,
            'reference_id' => $id_payment_request,
            'user_id' => $added_by,
            'link' => 'view_payment_request',
            'status' => $status);
        $notification_model = new Notifications_Model();
        $notification_model->add($notif_data);
    }

}
