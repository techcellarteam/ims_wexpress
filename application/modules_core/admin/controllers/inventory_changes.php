<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Inventory Changes Class
  |--------------------------------------------------------------------------
  |
  | Handles the Inventory Changes panel
  |
  | @category		Controller
  | @author		melin
 */

class Inventory_Changes extends Admin_Core {

    public $list_content = array();
    public $contents = array();
    public $inventory_changes_model;

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        //load models
        $this->load->model('admin/inventory_changes_model');
        $this->load->model('admin/classes_model');
        $this->load->model('admin/users_model');

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));
        //class ID
        $this->class_id = $this->classes_model->getFieldValue('id_class', array('class_name' => $this->classname));
        //initiate models
        $this->inventory_changes_model = new Inventory_Changes_Model();

        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_inventory_change',
            ),
            'business_unit_code' => array(
                'label' => 'Business Unit Code',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'business_unit_code',
            ),
            'stock_classification_name' => array(
                'label' => 'Item Grp',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'stock_classification_name',
            ),
            'commodity' => array(
                'label' => 'Item',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-2',
                'var-value' => 'commodity',
            ),
            'measurement_code' => array(
                'label' => 'Measurement Code',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'measurement_code',
            ),
            'user_name' => array(
                'label' => 'Administrator',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'user_name',
            ),
            'stock_before' => array(
                'label' => 'Stock Before',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'stock_before',
            ),
            'stock_quantity' => array(
                'label' => 'Stock Quantity',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'stock_quantity',
            ),
            'stock_after' => array(
                'label' => 'Stock After',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'stock_after',
            ),
            'inventory_date' => array(
                'label' => 'Inventory Date',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'inventory_date',
            ),
            'transaction' => array(
                'label' => 'Transaction',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-2',
                'var-value' => 'transaction',
            ),
            'remarks' => array(
                'label' => 'Remarks',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-2',
                'var-value' => 'remarks',
            ),
        );

        $this->contents = array('model_directory' => 'admin/inventory_changes_model',
            'model_name' => 'inventory_changes_model',
            'filters' => array(),
            'functionName' => 'Inventory Change'); // use to call functions for access

        $this->tools->setPostArray('sort', array('sort_by' => 'id_inventory_change', 'sort_type' => 'DESC'));
    }

    function index() {
        redirect(admin_dir('inventory_changes/list_inventory_change'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_inventory_change() {
        $data = array(
            'template' => parent::main_template()
        );
        $this->load->view(admin_dir('inventory_changes/list_inventory_change'), $data);
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_inventory_change') { /* Method for Account */
            self::_method_list_inventory_change();
        }
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_inventory_change() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }
        $condition = array();
        //show purchase requisitions requested by the business_unit only if user is not PEM, FM, CEO,OM or Super Admin
        if ((int) $this->session->userdata['admin']['user_type_id'] != 1) {
            $user_type_code = $this->session->userdata['admin']['user_type_code'];
            switch ($user_type_code) {
                case 'PU Head':
                    break;
                default:
                    $group_id = $this->users_model->getValue($this->session->userdata['admin']['user_id'], 'general_group_id');
                    $condition = array('du.business_unit_id' => $this->session->userdata['admin']['business_unit_id'],
                        'bu.general_group_id' => $group_id);
                    break;
            }
        }
        //set condition for the list
        $condition_string = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_inventory_change') {
                        $condition = array_merge($condition, array($var => $val));
                    } else if ($var == 'user_name') {
                        $condition_string = array_merge($condition_string, array("`user_fname` like '%$val%' OR `user_mname` like '%$val%' OR `user_lname` like '%$val%'"));
                    } else if ($var == 'commodity') {
                        $condition_string = array_merge($condition_string, array("`sku` like '%$val%' OR `description` like '%$val%'"));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get Purchase Order List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array('');

        $this->load->view(admin_dir('lists/list'), $data);
    }

}
