<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log extends Admin_Core {

    private $users_model = '';

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        $this->load->model('admin/users_model');
        $this->load->model('admin/user_business_units_model');

        $this->users_model = new users_model();
        $this->user_bu_model = new User_Business_Units_Model();
    }

    function index() {
        redirect(admin_url('log/login'));
    }

    function reset_newpassword() {
        $id_user = $this->Misc->decode_id($this->uri->rsegment(3));
        $user_resetkey = $this->uri->rsegment(4);
        if ($user_resetkey)
            $row = $this->users_model->getSearch(array('u.id_user' => $id_user, 'u.user_resetkey' => $user_resetkey), "", "", "", "", true);
        if ($row) {
            $this->form_validation->set_rules('newpassword', 'New Password', 'required|xss_clean|trim');
            $this->form_validation->set_rules('confirm', 'Confirm Password', 'required|matches[newpassword]|trim');
            if ($this->form_validation->run() == FALSE) {
                $data['inlinejs'][] = parent::sticky_message(array('type' => 'error', 'text' => validation_errors()));
                $this->load->view(admin_dir('log/reset_newpassword'), $data);
            } else {
                $data = array(
                    'user_password' => md5($_POST['newpassword']),
                    'user_resetkey' => ""
                );
                $this->users_model->update_table($data, "id_user", $row->id_user);

                $data['inlinejs'][] = parent::sticky_message(array('type' => 'success', 'text' => "Successfully Changed"));
                $data['id'] = 1;
                $this->load->view(admin_dir('log/reset_newpassword'), $data);
            }
        }
    }

    function reset_password() {
        $data['template'] = parent::log_template();

        $this->form_validation->set_rules('code', 'ID', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['inlinejs'][] = parent::sticky_message(array('type' => 'error', 'text' => validation_errors()));
            $this->load->view(admin_dir('log/reset_password'), $data);
        } else {
            $code = $_POST['code'];
            $email = $_POST['email'];

            /* Check User Login */
            $row = $this->users_model->getSearch(array('u.user_code' => $code, 'u.user_email' => $email), "", "", "", "", true);
            if ($row) {
                $user_resetkey = md5(date('YmdHissiHdmy') . rand(1, 100000));
                $id_user = $this->Misc->encode_id($row->id_user);
                $content = "To reset your password please click this link";
                $content.=" <a href='" . admin_url('log/reset_newpassword/' . $id_user . '/' . $user_resetkey) . "'>RESET PASSWORD</a>";

                $data = array(
                    'user_resetkey' => $user_resetkey
                );
                $this->users_model->update_table($data, "id_user", $row->id_user);

                parent::send_email($content, "Reset Password", $row->user_email, "", "no-reply@aet.com", "PHZP AET");

                $data['inlinejs'][] = parent::sticky_message(array('type' => 'success', 'text' => "Please check your email"));
                $this->load->view(admin_dir('log/reset_password'), $data);
            } else {
                $error = "Wrong ID or Email";
                $data['inlinejs'][] = parent::sticky_message(array('type' => 'error', 'text' => $error));
                $this->load->view(admin_dir('log/reset_password'), $data);
            }
        }
    }

    function login() {
        $data['template'] = parent::log_template();

        $this->form_validation->set_rules('code', 'ID', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['inlinejs'][] = parent::sticky_message(array('type' => 'error', 'text' => validation_errors()));
            $this->load->view(admin_dir('log/login'), $data);
        } else {
            $code = $_POST['code'];
            $password = md5($_POST['password']);

            /* Check User Login */
            $row = $this->users_model->getSearch(array('u.user_code' => $code, 'u.user_password' => $password), "", "", "", "", true);
            if ($row) {
                //get business units
                $user_bu = $this->misc->query_to_string($this->user_bu_model->getList(array('user_id' => $row->id_user)), 'business_unit_id');
                //get business units group codes
                $user_bu_codes = '';
                $bu_codes = array();
                foreach ($this->user_bu_model->getList(array('user_id' => $row->id_user))->result() as $q) {
                    if (!in_array($q->group_code, $bu_codes)) {
                        array_push($bu_codes, $q->group_code);
                        $user_bu_codes = ($user_bu_codes) ? $user_bu_codes . ",'$q->group_code'" : "'$q->group_code'";
                    }
                }
                //auto idle
                modules::run(admin_dir('survey/_auto_idle'));

                $name = $this->Misc->display_name($row->user_fname, $row->user_mname, $row->user_lname);
                $sessdata = array(
                    'login' => TRUE,
                    'name' => $name,
                    'user_picture' => $row->user_picture,
                    'user_id' => $row->id_user,
                    'user_code' => $row->user_code,
                    'user_email' => $row->user_email,
                    'user_type_name' => $row->user_type_name,
                    'user_type_code' => $row->user_type_code,
                    'user_type_id' => $row->user_type_id,
                    'group_code' => $row->group_code,
                    'business_unit_id' => $row->business_unit_id,
                    'business_units' => $user_bu,
                    'user_bu_codes' => $user_bu_codes,
                    'business_unit_head_id' => $row->business_unit_head_id
                );

                /* Set Session */
                $this->session->set_userdata('admin', $sessdata);


                parent::save_log();
                if ($this->session->userdata('redirect')) {
                    $redirect = $this->session->userdata('redirect');
                    $this->session->unset_userdata('redirect');
                    redirect($redirect);
                }
                redirect('admin/profile');
            } else {
                $error = "Wrong ID or Password";
                $data['inlinejs'][] = parent::sticky_message(array('type' => 'error', 'text' => $error));
                $this->load->view(admin_dir('log/login'), $data);
            }
        }
    }

    function logout() {
        parent::save_log();
        $this->session->unset_userdata();
        $this->session->sess_destroy();
        redirect('admin/log');
    }

}
