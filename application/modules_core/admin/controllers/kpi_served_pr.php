<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | KPI Report - Served PR Records Class
  |--------------------------------------------------------------------------
  |
  | Handles the KPI Report - Served PR Records panel
  |
  | @category		Controller
  | @author		melin
 */

class KPI_Served_PR extends Admin_Core {

    public $purchase_requisition;
    public $receiving_reports_model;

    function __construct() {
        parent:: __construct();

        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);

        $this->load->model('admin/purchase_requisitions_model');
        $this->load->model('admin/users_model');
        $this->load->model('admin/document_types_model');

        $this->purchase_requisition = new Purchase_Requisitions_Model();
        $this->users_model = new Users_Model();
        $this->document_types_model = new Document_Types_Model();

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        //for list view
        $this->list_content = array(
            'id' => array(
                'label' => 'ID',
                'type' => 'hidden',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => '',
                'var-value' => 'id_purchase_requisition',
            ),
            'document_type_name' => array(
                'label' => 'Category',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'document_type_name',
            ),
            'pr_number' => array(
                'label' => 'PR #',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'pr_number',
                'nowrap' => 1
            ),
            'date_submitted' => array(
                'label' => 'Date PR Prepared',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'date_submitted',
                'nowrap' => 1
            ),
            'top_level_approval_date' => array(
                'label' => 'Date PR Approved',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'top_level_approval_date',
                'nowrap' => 1
            ),
            'prepared_approved' => array(
                'label' => 'No. of days (Date PR prepared vs Date PR Approved)',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'prepared_approved',
            ),
            'po_number' => array(
                'label' => 'PO Number',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'po_number',
                'nowrap' => 1
            ),
            'invoice_number' => array(
                'label' => 'Invoice Number',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'invoice_number',
                'nowrap' => 1
            ),
            'date_received' => array(
                'label' => 'Date Served',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'date_received',
                'nowrap' => 1
            ),
            'approved_received' => array(
                'label' => 'No. of days (Date PR  Approved vs Date PR Served)',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'approved_received',
                'nowrap' => 1
            ),
        );

        $this->contents = array('model_directory' => 'admin/kpi_served_pr_model',
            'model_name' => 'kpi_served_pr_model',
            'filters' => array(),
            'functionName' => 'Record',); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('reports/kpi_served_pr'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_record() {
        $where_string = array("`document_type_name` LIKE '%Regular%'");

        $data = array(
            'template' => parent::main_template(),
            'categories' => $this->document_types_model->getList(array('class_name' => 'purchase_requisitions'), $where_string)->result(),
        );

        $this->load->view(admin_dir('reports/kpi_served_pr'), $data);
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if (($this->uri->rsegment(3) == 'list_record') || ($this->uri->rsegment(3) == 'pdf_report')) { /* Method for Account */
            self::_method_list_record();
        }
    }

    /* ---------- ENROLLMENT LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_record() {
        if (!IS_AJAX && ($this->uri->rsegment(3) == 'list_record')) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }

        //set condition for the list
        $condition = array();
        $condition_string = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    $date_from = $this->tools->getPost('search', 'date_from');
                    $date_to = $this->tools->getPost('search', 'date_to');
                    if ($var == 'user_name') {
                        $condition_string = array_merge($condition_string, array("du`.`user_fname` LIKE '%" . trim($val) . "%' OR `du`.`user_lname` LIKE '%" . trim($val) . "%'"));
                    } elseif ($var == 'po_number') {
                        $condition_string = array_merge($condition_string, array("`po`.`po_number` LIKE '%" . trim($val) . "%'"));
                    } elseif ($var == 'date_from') {
                        if (isset($date_to)) {
                            $condition_string = array_merge($condition_string, array("pr.date_submitted BETWEEN '$val' AND '$date_to'"));
                        }
                    } elseif ($var == 'date_to') {
                        $condition_string = array_merge($condition_string, array("pr.date_submitted BETWEEN '$date_from' AND '$val'"));
                    } elseif ($var == 'prepared_approved') {
                        $aging_where = "";
                        $test = (!((int) strpos($val, '>') < 0) || !((int) strpos($val, '>=') < 0) || !((int) strpos($val, '<') < 0) || !((int) strpos($val, '<=') < 0));
                        if ($test) {
                            $aging_where .= "(SELECT DATEDIFF(pr.top_level_approval_date,pr.date_submitted ) = $val AND `pr`.`top_level_approval_date` != '0000-00-00')";
                            $condition_string = array_merge($condition_string, array($aging_where));
                        }
                    } elseif ($var == 'approved_received') {
                        $aging_where = "";
                        $test = (!((int) strpos($val, '>') < 0) || !((int) strpos($val, '>=') < 0) || !((int) strpos($val, '<') < 0) || !((int) strpos($val, '<=') < 0));
                        if ($test) {
                            $aging_where .= "(SELECT DATEDIFF(rr.date_received,pr.top_level_approval_date ) = $val AND `rr`.`date_received` != '0000-00-00')";
                            $condition_string = array_merge($condition_string, array($aging_where));
                        }
                    } elseif ($var == 'date_submitted') {
                        $condition = array_merge($condition, array("pr.date_submitted like" => "%" . date('Y-m-d', strtotime($val)) . "%"));
                    } elseif ($var == 'date_received') {
                        $condition = array_merge($condition, array("rr.date_received like" => "%" . date('Y-m-d', strtotime($val)) . "%"));
                    } elseif ($var == 'top_level_approval_date') {
                        $condition = array_merge($condition, array("pr.top_level_approval_date like" => "%" . date('Y-m-d', strtotime($val)) . "%"));
                    } elseif ($var == 'document_type_name') {
                        $condition = array_merge($condition, array("dty.document_type_name" => trim($val)));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . trim($val) . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get KPI Report - Served PR Records List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        echo '<pre>';print_R($this->db->last_query());echo '</pre>';
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array(); // to long to be poped-up

        foreach ($data['list']->result() as $q) {
            $q->prepared_approved = '';
            $q->approved_received = '';
            $date1 = $q->date_submitted;
            $date2 = $q->top_level_approval_date;   
            $date3 = $q->date_received;            

            if (strtotime($date1) > 0 && strtotime($date2) > 0) {
                $diff = date_diff(date_create($date1), date_create($date2));
                $q->prepared_approved = $diff->format("%a") + 1;
            }
            
            if (strtotime($date2) > 0 && strtotime($date3) > 0) {
                $diff = date_diff(date_create($date1), date_create($date2));
                $q->approved_received = $diff->format("%a") + 1;
            }
        }
        if ($this->uri->rsegment(3) == 'pdf_report') {
            $data['title'] = 'KPI Report - Served PR Records Report';
            parent::save_log("KPI Report - Served PR Records PDF Report Generated", 'report_rec_iss', '', 'kpi_served_pr_pdf');
            $this->load->view(admin_dir('reports/kpi_served_pr_pdf'), $data);
        } else {
            $this->load->view(admin_dir('lists/reports/kpi_served_pr'), $data);
        }
    }

}
