<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Document Types Class
  |--------------------------------------------------------------------------
  |
  | Handles the Document Types panel
  |
  | @category		Controller
  | @author		melin
 */

class Document_Types extends Admin_Core {

    public $document_types_model = '';
    public $list_content = array();
    public $contents = array();

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        //load models
        $this->load->model('admin/document_types_model');
        $this->load->model('admin/classes_model');
        $this->load->model('admin/general_group_model');
        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        //initiate models
        $this->document_types_model = new Document_Types_Model();
        $this->classes_model = new Classes_Model();
        $this->group_model = new General_Group_Model();
        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_document_type',
            ),
            'group_code' => array(
                'label' => 'GRP',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'group_code',
            ),
            'class_title' => array(
                'label' => 'Module',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'class_title',
            ),
            'document_type_name' => array(
                'label' => 'Document Type Name',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'document_type_name',
            ),
            'document_type_code' => array(
                'label' => 'Document Type Code',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'document_type_code',
            ),
        );

        $this->contents = array('model_directory' => 'admin/document_types_model',
            'model_name' => 'document_types_model',
            'filters' => array(),
            'functionName' => 'Document Type'); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('document_types/list_document_type'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_document_type() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'document-type-tools.js'))),
        );
        $this->load->view(admin_dir('document_types/list_document_type'), $data);
    }

    function add_document_type() {
        $data = array(
            'template' => parent::main_template(),
            'modules' => $this->classes_model->getList()->result(),
            'groups' => $this->group_model->getList()->result(),
        );
        $this->load->view(admin_dir('document_types/add_document_type'), $data);
    }

    function edit_document_type() {
        $id_document_type = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Document Type if Exist */
        $row = $this->document_types_model->getFields($id_document_type);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
                'modules' => $this->classes_model->getList()->result(),
                'groups' => $this->group_model->getList()->result(),
            );
            $this->load->view(admin_dir('document_types/edit_document_type'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function view_document_type() {
        $id_document_type = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Document Type if Exist */
        $row = $this->document_types_model->getFields($id_document_type);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
                'modules' => $this->classes_model->getList()->result(),
            );
            $this->load->view(admin_dir('document_types/view_document_type'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_document_type') { /* Method for Account */
            self::_method_list_document_type();
        } else if ($this->uri->rsegment(3) == 'add_document_type') {
            self::_method_add_document_type();
        } else if ($this->uri->rsegment(3) == 'edit_document_type') {
            self::_method_edit_document_type();
        } else if ($this->uri->rsegment(3) == 'delete_document_type') {
            self::_method_delete_document_type();
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Validation of the Form -------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    private function _validate() {
        $this->form_validation->set_rules('class_id', 'Module Name', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('document_type_name', 'Name', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('general_group_id', 'Group Code', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('document_type_code', 'Code', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('multiplier', 'Multiplier', 'htmlspecialchars|trim|required');
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_document_type() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }

        //set condition for the list
        $condition = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_document_type') {
                        $condition = array_merge($condition, array($var => $val));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'contents' => $this->contents
        );

        /* Get Document Type List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array('edit', 'view');

        $this->load->view(admin_dir('lists/list'), $data);
    }

    /*
     * 	Add New Document Type Method
     */

    function _method_add_document_type() {
        if (IS_AJAX) {
            self::_validate();

            //check form validation then save
            if ($this->form_validation->run() == TRUE) {
                $this->db->trans_start();
                //getPost data set to this model fields
                parent::copyFromPost($this->document_types_model, 'id_document_type');

                /* Check Document Type Code Name Exist */
                $count = $this->document_types_model->getSearch(array("document_type_name like" => $this->tools->getPost('document_type_name'),
                    'class_id' => $this->tools->getPost('class_id'),
                    'general_group_id' => $this->tools->getPost('general_group_id')), "", "", "", true);
                if ($count == 0) {
                    $this->my_session->get('admin', 'user_id');
                    $id = $this->document_types_model->add();
                    parent::save_log("add new document type " . $this->tools->getPost('document_type_name'), 'document_types', $id);
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                        'id' => $this->document_types_model->id
                    );
                } else {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", "Code already exist"),
                        'id' => 0
                    );
                }
                $this->db->trans_complete();
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	Edit Document Type Type Method
     */

    function _method_edit_document_type() {
        if (IS_AJAX) {
            $id_document_type = $this->Misc->decode_id($this->tools->getPost('id'));

            /* Check Document Type Type if Exist */
            $row = $this->document_types_model->getFields($id_document_type);
            if ($row) {
                self::_validate();
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    /* Check Document Type Exist */
                    $count = $this->document_types_model->getSearch(array('id_document_type !=' => $id_document_type,
                        "document_type_name like" => $this->tools->getPost('document_type_name'),
                        'general_group_id' => $this->tools->getPost('general_group_id')), "", "", "", true);
                    if ($count == 0) {
                        /* Update Document Type Info */
                        //getPost data set to this model fields
                        parent::copyFromPost($this->document_types_model, 'id_document_type');
                        $this->document_types_model->update(array('id_document_type' => $id_document_type));
                        parent::save_log("update document type " . $this->Misc->update_name_log($row->document_type_name, $this->tools->getPost('document_type_name')), 'document_types', $id_document_type);
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                            'id' => $id_document_type
                        );
                    } else {
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('error', "Document Type Name already exist"),
                            'id' => 0
                        );
                    }
                    $this->db->trans_complete();
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Delete Document Type Method
     */

    function _method_delete_document_type() {
        if (IS_AJAX) {
            $id_document_type = $this->Misc->decode_id($this->tools->getPost('item'));
            /* Check Document Type if Exist */
            $row = $this->document_types_model->getFields($id_document_type);
            if ($row) {
                $datas = array(
                    'enabled' => 0,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete Document Type */
                $this->document_types_model->update_table($datas, "id_document_type", $id_document_type);
                parent::save_log("delete document type" . $row->document_type_name, 'document_types', $id_document_type);
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Deleted'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

}
