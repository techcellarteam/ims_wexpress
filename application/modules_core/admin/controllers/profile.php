<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends Admin_Core {

    public $users_model = '';

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();

        $this->load->model('admin/users_model');
        $this->users_model = new users_model();

        $this->contents = array('model_directory' => 'admin/users_model',
            'model_name' => 'users_model',
            'filters' => array(),
            'functionName' => 'Profile Page',); // use to call functions for access
    }

    function index() {
        redirect('admin/profile/view_profile_page');
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function view_pagenotfound_page() {
        $this->load->view(admin_dir('profile/view_pagenotfound_page'));
    }

    function view_profile_page() {
        $id_user = $this->my_session->get('admin', 'user_id');
        $result = $this->users_model->getFields($id_user);
        if ($result) {
            $data = array(
                'template' => parent::main_template(),
                'result' => $result
            );
            $this->load->view(admin_dir('profile/view_profile_page'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function edit_password_page() {
        $id_user = $this->my_session->get('admin', 'user_id');
        $result = $this->users_model->getFields($id_user);
        if ($result) {
            $data = array(
                'template' => parent::main_template(),
                'result' => $result
            );
            $this->load->view(admin_dir('profile/edit_password_page'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function edit_myinfo_page() {
        $id_user = $this->my_session->get('admin', 'user_id');
        $result = $this->users_model->getFields($id_user);
        if ($result) {
            $data = array(
                'template' => parent::main_template(),
                'result' => $result
            );
            $this->load->view(admin_dir('profile/edit_myinfo_page'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function take_picture_page() {
        $id_user = $this->my_session->get('admin', 'user_id');
        $result = $this->users_model->getFields($id_user);
        if ($result) {
            $data = array(
                'template' => parent::main_template(),
                'result' => $result
            );
            $this->load->view(admin_dir('profile/take_picture_page'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'edit_password') {
            self::_method_edit_password();
        } else if ($this->uri->rsegment(3) == 'upload_mypicture') {
            self::_method_upload_mypicture();
        } else if ($this->uri->rsegment(3) == 'edit_myinfo') {
            self::_method_edit_myinfo();
        }
    }

    function _method_edit_myinfo() {
        if (IS_AJAX) {
            $id_user = $this->my_session->get('admin', 'user_id');
            $this->form_validation->set_rules('fname', 'First Name', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules('mname', 'Middle Name', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules('lname', 'Last Name', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules('address', 'Address', 'htmlspecialchars|trim|required');
            //$this->form_validation->set_rules('email','Email','htmlspecialchars|trim|required|valid_email');
            $this->form_validation->set_rules('contact', 'Contact', 'htmlspecialchars|trim|required');
            if ($this->form_validation->run() == FALSE) {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            } else {
                /* Check Email Exist */
                // $result=$this->users_model->getSearch(array("u.id_user !="=>$id_user,"u.user_email like"=>$_POST['email']),"","","",true);
                // if($result==0){
                $datas = array(
                    'user_fname' => $_POST['fname'],
                    'user_lname' => $_POST['lname'],
                    'user_mname' => $_POST['mname'],
                    'user_address' => $_POST['address'],
                    // 'user_email'=>$_POST['email'],
                    'user_contact' => $_POST['contact']
                );
                /* Update User Info */
                $this->users_model->update_table($datas, "id_user", $id_user);
                parent::save_log("update information", "user", $id_user);

                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                    'id' => 1
                );
                // }else{
                // $arr=array(
                // 'message_alert'=>$this->Misc->message_status('error',"Email already exist"),
                // 'id'=>0
                // );
                // }
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    function _method_upload_mypicture() {
        $id_user = $this->my_session->get('admin', 'user_id');
        $path = parent::upload_user_path($id_user);
        if (!empty($_POST['image_data'])) {
            /* Convert Base64 to Picture */
            $convert = parent::convert_image($_POST['image_data'], $path['profile'], $path['profile_thumb']);
            if ($convert[0]) {
                $datas = array(
                    'user_picture' => $convert['file_name'],
                    'updated_by' => $id_user,
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Update User Picture */
                $this->users_model->update_table($datas, "id_user", $id_user);
                parent::save_log("take a picture", "user", $id_user);
                $arr = array(
                    'id' => 1,
                    'message_alert' => $this->Misc->message_status('success', 'Successfully saved')
                );
            } else {
                $arr = array(
                    'id' => 0,
                    'message_alert' => $this->Misc->message_status('error', $convert['error'])
                );
            }
        } else if (!empty($_FILES)) {
            $filetype = 'gif|jpg|png';
            /* Upload Picture */
            $upload = parent::upload_file($path['profile'], $path['profile_thumb'], 'file', $filetype, '5000');
            if ($upload[0]) {
                $datas = array(
                    'user_picture' => $upload['orig_name'],
                    'updated_by' => $id_user,
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Update User Picture */
                $this->users_model->update_table($datas, "id_user", $id_user);
                parent::save_log("upload picture", "user", $id_user);
                $arr = array(
                    'id' => 1,
                    'message_alert' => $this->Misc->message_status('success', 'Successfully saved')
                );
            } else {
                $arr = array(
                    'id' => 0,
                    'message_alert' => $this->Misc->message_status('error', $upload['error'])
                );
            }
        } else {
            $arr = array(
                'id' => 0,
                'message_alert' => $this->Misc->message_status('error', 'No file')
            );
        }
        $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
    }

    function _method_edit_password() {
        if (IS_AJAX) {
            $id_user = $this->my_session->get('admin', 'user_id');
            $this->form_validation->set_rules('oldpassword', 'Old Password', 'htmlspecialchars|trim|required');
            $this->form_validation->set_rules('newpassword', 'New Password', 'htmlspecialchars|trim|required|matches[confirmpassword]');
            $this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'htmlspecialchars|trim|required');
            if ($this->form_validation->run() == FALSE) {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            } else {
                /* Check Old Password */
                $old_password = md5($_POST['oldpassword']);
                $result = $this->users_model->getSearch(array("u.id_user" => $id_user, "u.user_password like" => $old_password), "", "", "", true);
                if ($result) {
                    $datas = array(
                        'user_password' => md5($_POST['newpassword']),
                        'updated_by' => $id_user,
                        'updated_date' => date('Y-m-d H:i:s')
                    );
                    /* Save New Password */
                    $this->users_model->update_table($datas, "id_user", $id_user);
                    parent::save_log("change password", "user", $id_user);
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("success", "Successfully saved"),
                        'id' => $id_user
                    );
                } else {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", "Wrong Old Password"),
                        'id' => 0
                    );
                }
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /* ----------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Additional Module --------------------------------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------------------------------------------------------------------------------- */
}
