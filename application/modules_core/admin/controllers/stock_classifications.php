<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Stock Classifications Class
  |--------------------------------------------------------------------------
  |
  | Handles the Stock Classifications panel
  |
  | @category		Controller
  | @author		melin
 */

class Stock_Classifications extends Admin_Core {

    public $stock_classifications_model = '';
    public $list_content = array();
    public $contents = array();

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        //load models
        $this->load->model('admin/stock_classifications_model');

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        //initiate models
        $this->stock_classifications_model = new Stock_Classifications_Model();
        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_stock_classification',
            ),
            'stock_classification_name' => array(
                'label' => 'Stock Classification Name',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-4',
                'var-value' => 'stock_classification_name',
            ),
        );

        $this->contents = array('model_directory' => 'admin/stock_classifications_model',
            'model_name' => 'stock_classifications_model',
            'filters' => array(),
            'functionName' => 'Stock Classification'); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('stock_classifications/list_stock_classification'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_stock_classification() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'stock-classification-tools.js'))),
        );
        $this->load->view(admin_dir('stock_classifications/list_stock_classification'), $data);
    }

    function add_stock_classification() {
        $data = array(
            'template' => parent::main_template(),
        );
        $this->load->view(admin_dir('stock_classifications/add_stock_classification'), $data);
    }

    function edit_stock_classification() {
        $id_stock_classification = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Stock Classification if Exist */
        $row = $this->stock_classifications_model->getFields($id_stock_classification);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
            );
            $this->load->view(admin_dir('stock_classifications/edit_stock_classification'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function view_stock_classification() {
        $id_stock_classification = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Stock Classification if Exist */
        $row = $this->stock_classifications_model->getFields($id_stock_classification);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
            );
            $this->load->view(admin_dir('stock_classifications/view_stock_classification'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_stock_classification') { /* Method for Account */
            self::_method_list_stock_classification();
        } else if ($this->uri->rsegment(3) == 'add_stock_classification') {
            self::_method_add_stock_classification();
        } else if ($this->uri->rsegment(3) == 'edit_stock_classification') {
            self::_method_edit_stock_classification();
        } else if ($this->uri->rsegment(3) == 'delete_stock_classification') {
            self::_method_delete_stock_classification();
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Validation of the Form -------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    private function _validate() {
        $this->form_validation->set_rules('stock_classification_name', 'Stock Classification Name', 'htmlspecialchars|trim|required');
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_stock_classification() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }

        //set condition for the list
        $condition = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_stock_classification') {
                        $condition = array_merge($condition, array($var => $val));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'contents' => $this->contents
        );

        /* Get Stock Classification List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array('edit', 'view');

        $this->load->view(admin_dir('lists/list'), $data);
    }

    /*
     * 	Add New Stock Classification Method
     */

    function _method_add_stock_classification() {
        if (IS_AJAX) {
            self::_validate();

            //check form validation then save
            if ($this->form_validation->run() == TRUE) {
                $this->db->trans_start();
                //getPost data set to this model fields
                parent::copyFromPost($this->stock_classifications_model, 'id_stock_classification');

                /* Check Stock Classification Exist */
                $count = $this->stock_classifications_model->getSearch(array("stock_classification_name like" => $this->tools->getPost('stock_classification_name')), "", "", "", true);
                if ($count == 0) {
                    $this->my_session->get('admin', 'user_id');
                    $this->stock_classifications_model->add();
                    parent::save_log("add new commodity group " . $this->tools->getPost('stock_classification_name'), 'stock_classifications', $this->stock_classifications_model->id);
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                        'id' => $this->stock_classifications_model->id
                    );
                } else {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", "Stock Classification already exist"),
                        'id' => 0
                    );
                }
                $this->db->trans_complete();
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	Edit Stock Classification Type Method
     */

    function _method_edit_stock_classification() {
        if (IS_AJAX) {
            $id_stock_classification = $this->Misc->decode_id($this->tools->getPost('id'));

            /* Check Stock Classification Type if Exist */
            $row = $this->stock_classifications_model->getFields($id_stock_classification);
            if ($row) {
                self::_validate();
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    /* Check Stock Classification Exist */
                    $count = $this->stock_classifications_model->getSearch(array('id_stock_classification !=' => $id_stock_classification, "stock_classification_name like" => $this->tools->getPost('stock_classification_name')), "", "", "", true);
                    if ($count == 0) {
                        /* Update Stock Classification Info */
                        //getPost data set to this model fields
                        parent::copyFromPost($this->stock_classifications_model, 'id_stock_classification');
                        $this->stock_classifications_model->update(array('id_stock_classification' => $id_stock_classification));
                        parent::save_log("update commodity group " . $this->Misc->update_name_log($row->stock_classification_name, $this->tools->getPost('stock_classification_name')), 'stock_classifications', $id_stock_classification);
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                            'id' => $id_stock_classification
                        );
                    } else {
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('error', "Stock Classification already exist"),
                            'id' => 0
                        );
                    }
                    $this->db->trans_complete();
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Delete Stock Classification Method
     */

    function _method_delete_stock_classification() {
        if (IS_AJAX) {
            $id_stock_classification = $this->Misc->decode_id($this->tools->getPost('item'));
            /* Check Stock Classification if Exist */
            $row = $this->stock_classifications_model->getFields($id_stock_classification);
            if ($row) {
                $datas = array(
                    'enabled' => 0,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete Stock Classification */
                $this->stock_classifications_model->update_table($datas, "id_stock_classification", $id_stock_classification);
                parent::save_log("delete commodity group" . $row->stock_classification_name, 'stock_classifications', $id_stock_classification);
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Deleted'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

}
