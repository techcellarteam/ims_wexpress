<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Detailed PR Status Summary Class
  |--------------------------------------------------------------------------
  |
  | Handles the Detailed PR Status Summary panel
  |
  | @category		Controller
  | @author		melin
 */

class Report_PR_Summary extends Admin_Core {

    public $purchase_requisition = '';
    public $receiving_reports_model = '';
    public $prf_model = '';
    public $pr_summary_model = '';

    function __construct() {
        parent:: __construct();

        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);

        $this->load->model('admin/purchase_requisitions_model');
        $this->load->model('admin/purchase_requisition_items_model');
        $this->load->model('admin/purchase_orders_model');
        $this->load->model('admin/business_units_model');
        $this->load->model('admin/suppliers_model');
        $this->load->model('admin/general_group_model');
        $this->load->model('admin/receiving_reports_model');
        $this->load->model('admin/receiving_report_items_model');
        $this->load->model('admin/payment_requests_model');
        $this->load->model('admin/pr_summary_model');
        $this->load->model('admin/users_model');

        $this->purchase_requisition = new Purchase_Requisitions_Model();
        $this->purchase_requisition_items_model = new Purchase_Requisition_Items_Model();
        $this->rr_model = new Receiving_Reports_Model();
        $this->rri_model = new Receiving_Report_Items_Model();
        $this->prf_model = new Payment_Requests_Model();
        $this->pr_summary_model = new PR_Summary_Model();
        $this->users_model = new Users_Model();

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        //initialize PR code
        $this->pr_code = ($this->session->userdata['admin']['group_code'] == 'TFleet') ? 'TFL' : $this->session->userdata['admin']['group_code'];


        //for list view
        $this->list_content = array(
            'id' => array(
                'label' => 'ID',
                'type' => 'hidden',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => '',
                'var-value' => 'id_purchase_requisition',
            ),
            'pr_number' => array(
                'label' => 'PR No.',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'pr_number',
                'nowrap' => 1
            ),
            'acctg_unit' => array(
                'label' => 'Accounting Unit',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'acctg_unit',
            ),
            'plate_number' => array(
                'label' => 'Plate #',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'plate_number',
                'nowrap' => 1
            ),
            'approx_amount' => array(
                'label' => 'Amount',
                'type' => 'amount',
                'type-class' => '',
                'class' => 'col-lg-1',
                'var-value' => 'approx_amount',
                'nowrap' => 1
            ),
            'pr.added_date' => array(
                'label' => 'PR Date Prepared',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'added_date',
                'nowrap' => 1
            ),
            'pr_remarks' => array(
                'label' => 'Remarks',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-2 ',
                'var-value' => 'pr_remarks',
            ),
            'pr.status' => array(
                'label' => 'Status',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'status',
                'nowrap' => 1
            ),
            'pr_approved_date' => array(
                'label' => 'Date Approved',
                'type' => 'datepicker',
                'type-class' => '',
                'class' => 'col-lg-1',
                'var-value' => 'pr_approved_date',
                'nowrap' => 1
            ),
            'pr_approved_by' => array(
                'label' => 'Approved By',
                'type' => 'text',
                'type-class' => '',
                'class' => 'col-lg-1',
                'var-value' => 'pr_approved_by',
                'nowrap' => 1
            ),
            'added_approved' => array(
                'label' => 'No of Days (PR Prepared vs PR Approval)',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'added_approved',
            ),
            'po_number' => array(
                'label' => 'PO Number',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'po_number',
                'nowrap' => 1
            ),
            'supplier' => array(
                'label' => 'Supplier',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'supplier',
            ),
            'po_amount' => array(
                'label' => 'PO Amount',
                'type' => 'amount',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'po_amount',
            ),
            'po.added_date' => array(
                'label' => 'Date Prepared',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'po_added_date',
                'nowrap' => 1
            ),
            'po.status' => array(
                'label' => 'Status',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'po_status',
                'nowrap' => 1
            ),
            'po_approved_date' => array(
                'label' => 'Date Approved',
                'type' => 'datepicker',
                'type-class' => '',
                'class' => 'col-lg-1',
                'var-value' => 'po_approved_date',
                'nowrap' => 1
            ),
            'po_approved_by' => array(
                'label' => 'Appoved By',
                'type' => 'text',
                'type-class' => '',
                'class' => 'col-lg-1',
                'var-value' => 'po_approved_by',
                'nowrap' => 1
            ),
            'date_received' => array(
                'label' => 'Received Date',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'date_received',
                'nowrap' => 1
            ),
            'approved_received' => array(
                'label' => 'No of days ( PO Approved versus Items Receive)',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'approved_received',
            ),
            'prf_number' => array(
                'label' => 'RFP No.',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'prf_number',
                'nowrap' => 1
            ),
            'prf.total_ammount' => array(
                'label' => 'RFP Amount',
                'type' => 'amount',
                'type-class' => '',
                'class' => 'col-lg-1',
                'var-value' => 'prf_total_ammount',
                'nowrap' => 1
            ),
            'rfp.request_payment_payee' => array(
                'label' => 'Payee',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'request_payment_payee',
                'nowrap' => 1
            ),
            'prf.added_date' => array(
                'label' => 'Date Prepared',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'prf_added_date',
                'nowrap' => 1
            ),
            'invoice_number' => array(
                'label' => 'Invoice Number',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'invoice_number',
                'nowrap' => 1
            ),
            'prf_status' => array(
                'label' => 'Status',
                'type' => 'text',
                'type-class' => '',
                'class' => 'col-lg-1',
                'var-value' => 'prf_status',
                'nowrap' => 1
            ),
            'rfp.approved_date' => array(
                'label' => 'Date Approved',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'rfp_approval_date',
                'nowrap' => 1
            ),
            'rfp_approved_by' => array(
                'label' => 'Approved By',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'rfp_approved_by',
                'nowrap' => 1
            ),
            'received_invoiced' => array(
                'label' => 'No of days (Item received vs RFP turnover to accounting)',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'received_invoiced',
            ),
        );

        $this->contents = array('model_directory' => 'admin/pr_summary_model',
            'model_name' => 'pr_summary_model',
            'filters' => array(),
            'functionName' => 'Record',); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('reports/report_pr_summary'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_record() {
        $data = array(
            'template' => parent::main_template(),
        );

        $this->load->view(admin_dir('reports/report_pr_summary'), $data);
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if (($this->uri->rsegment(3) == 'list_record') || ($this->uri->rsegment(3) == 'pdf_report')) { /* Method for Account */
            self::_method_list_record();
        }
    }

    /* ---------- ENROLLMENT LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_record() {
        if (!IS_AJAX && ($this->uri->rsegment(3) == 'list_record')) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }

        //set condition for the list
        $condition = array();
        $condition_string = array();
        //limit transaction to be seen if user is not PEM, FM, CEO,OM or Super Admin
        if ((int) $this->session->userdata['admin']['user_type_id'] != 1) {
            $user_type_code = $this->session->userdata['admin']['user_type_code'];
            switch ($user_type_code) {
                case 'PU Head':
                    break;
                case 'Purchaser':
                    break;
                case 'PO Initiator':
                    break;
                case 'BU Head (OPS)':
                case 'BU Head (CSG)':
                case 'BU Head (TFleet)':
                    $group_id = $this->users_model->getValue($this->session->userdata['admin']['user_id'], 'general_group_id');
                    $condition = array('bu.general_group_id' => $group_id);
                    break;
                case 'Administrator':
                    break;
                case 'EVP': // 2nd level approved
                    $condition = array('po.status' => 'PO awaiting ELA approval');
                    break;
                case 'Chairman': //third level approved
                    $condition_string = array_merge($condition_string, array("(`po`.`status` = 'PO Approved (ELA)' OR (`po`.`status` = 'PO awaiting ELA approval' AND `group_code` = 'TFleet'))"));
                    break;
                default:
                    $group_id = $this->users_model->getValue($this->session->userdata['admin']['user_id'], 'general_group_id');
                    $condition = array('du.business_unit_id' => $this->session->userdata['admin']['business_unit_id'],
                        'bu.general_group_id' => $group_id);
                    break;
            }
        }
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '' && ($var != 'po_approved_date' || $var != 'po_approved_by' || $var != 'pr_approved_date' || $var != 'pr_approved_by')) {
                    $date_from = $this->tools->getPost('search', 'date_from');
                    $date_to = $this->tools->getPost('search', 'date_to');
                    if ($var == 'supplier') {
                        $condition_string = array_merge($condition_string, array("s`.`supplier_code` LIKE '%" . trim($val) . "%' OR `s`.`supplier_name` LIKE '%" . trim($val) . "%'"));
                    } elseif ($var == 'rfp_app_by') {
                        $condition_string = array_merge($condition_string, array("(`rfp_app_by`.`user_fname` LIKE '%" . $val . "%' OR `rfp_app_by`.`user_lname` LIKE '%" . $val . "%')"));
                    } elseif ($var == 'acctg_unit') {
                        $condition_string = array_merge($condition_string, array("(concat(`pl`.`plate_number`,SPACE(1),`pl`.`make`,SPACE(1),`pl`.`model`) LIKE '%" . $val . "%' OR concat(`bu`.`business_unit_name`,SPACE(1),`bu`.`business_unit_code`) LIKE '%" . $val . "%')"));
                    } elseif ($var == 'date_from') {
                        if (isset($date_to)) {
                            $condition_string = array_merge($condition_string, array("pr.date_needed BETWEEN '$val' AND '$date_to'"));
                        }
                    } elseif ($var == 'added_approved') {
                        $aging_where = '= ' . $val;
                        $test = (!((int) strpos($val, '>') < 0) || !((int) strpos($val, '>=') < 0) || !((int) strpos($val, '<') < 0) || !((int) strpos($val, '<=') < 0));
                        if ($test) {
                            $condition_string = array_merge($condition_string, array("(SELECT DATEDIFF(pr.top_level_approval_date,pr.added_date)) " . $aging_where));
                        }
                    } elseif ($var == 'approved_received') {
                        $aging_where = "";
                        $test = (!((int) strpos($val, '>') < 0) || !((int) strpos($val, '>=') < 0) || !((int) strpos($val, '<') < 0) || !((int) strpos($val, '<=') < 0));
                        if ($test) {
                            //for CSG and OPS - if >= 50k then last approver = chairman
                            $aging_where .= "((SELECT DATEDIFF(rr.date_received,po.third_approval_date ) = $val AND `po`.`third_approval_date` != '0000-00-00 00:00:00' AND `po`.`total_amount` >= 50000 AND `group_code` != 'TFleet')";
                            $aging_where .= " OR (SELECT DATEDIFF(rr.date_received,po.second_approval_date ) = $val AND `po`.`second_approval_date` != '0000-00-00 00:00:00' AND `po`.`total_amount` < 50000 AND `group_code` != 'TFleet')";
                            //if TFLEET
                            $aging_where .= " OR (SELECT DATEDIFF(rr.date_received,po.third_approval_date ) = $val AND `po`.`third_approval_date` != '0000-00-00 00:00:00' AND `group_code` = 'TFleet'))";
                            $condition_string = array_merge($condition_string, array($aging_where));
                        }
                    } elseif ($var == 'received_invoiced') {
                        $aging_where = '= ' . $val;
                        $test = (!((int) strpos($val, '>') < 0) || !((int) strpos($val, '>=') < 0) || !((int) strpos($val, '<') < 0) || !((int) strpos($val, '<=') < 0));
                        if ($test) {
                            $condition_string = array_merge($condition_string, array("(SELECT DATEDIFF(prf.invoice_received_date,rr.date_received )) " . $aging_where));
                        }
                    } elseif ($var == 'date_to') {
                        $condition_string = array_merge($condition_string, array("pr.date_needed BETWEEN '$date_from' AND '$val'"));
                    } elseif ($var == 'date_needed') {
                        $condition = array_merge($condition, array("pr.date_needed like" => "%" . date('Y-m-d', strtotime($val)) . "%"));
                    } elseif ($var == 'po_amount') {
                        $condition = array_merge($condition, array("po.total_amount like" => "%" . $val . "%"));
                    } elseif ($var == 'po_number') {
                        $condition = array_merge($condition, array("po.$var like" => "%" . $val . "%"));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        //show purchase requisitions requested by the business_unit only if user is not PEM, FM, CEO,OM or Super Admin
        if ((int) $this->session->userdata['admin']['user_type_id'] != 1) {
            $user_type_code = $this->session->userdata['admin']['user_type_code'];
            switch ($user_type_code) {
                case 'PU Head':
                case 'Purchaser':
                case 'EVP':
                case 'Chairman':
                case 'Administrator':
                    break;
                default:
                    $condition_string = array_merge($condition_string, array("`pr`.`business_unit_id` IN (" . $this->session->userdata['admin']['business_units'] . ")"));
                    break;
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get Detailed PR Status Summary List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array(); // to long to be poped-up
        foreach ($data['list']->result() as $q) {
            $q->approx_amount = $this->purchase_requisition_items_model->getApproxAmount(array('purchase_requisition_id' => $q->id_purchase_requisition));
            $q->prf_status = (($q->total_amount > 0) ? (($q->paid_amount / $q->total_amount) * 100) . "% paid" : '0%');
            $q->acctg_unit = ($q->business_unit_name == 'TFleet') ? $q->plate_number : $q->business_unit_name;
            $q->po_approved_by = '';
            $q->po_approved_date = '';
            $q->pr_approved_by = '';
            $q->pr_approved_date = '';
            $q->approved_received = '';
            $date1 = '';
            $date2 = '';
            if ($q->group_code == 'TFleet') {
                if ($q->po_third_approval_date != '0000-00-00 00:00:00') {
                    $date1 = $q->date_received;
                    $date2 = $q->po_third_approval_date;
                    $q->po_approved_by = $this->users_model->getName($q->po_third_level_id);
                    $q->po_approved_date = $q->po_third_approval_date;
                }
                if ($q->fourth_approval_date != '0000-00-00 00:00:00') {
                    $q->pr_approved_by = $this->users_model->getName($q->fourth_level_id);
                    $q->pr_approved_date = $q->fourth_approval_date;
                }
            } else {
                if ($q->total_amount >= 50000) {
                    if ($q->po_third_approval_date != '0000-00-00 00:00:00') {
                        $date1 = $q->date_received;
                        $date2 = $q->po_third_approval_date;
                        $q->po_approved_by = $this->users_model->getName($q->po_third_level_id);
                        $q->po_approved_date = $q->po_third_approval_date;
                    }
                    if ($q->fourth_approval_date != '0000-00-00 00:00:00') {
                        $q->pr_approved_by = $this->users_model->getName($q->third_level_id);
                        $q->pr_approved_date = $q->fourth_approval_date;
                    }
                } else {
                    if ($q->po_second_approval_date != '0000-00-00 00:00:00') {
                        $date1 = $q->date_received;
                        $date2 = $q->po_second_approval_date;
                        $q->po_approved_by = $this->users_model->getName($q->po_second_level_id);
                        $q->po_approved_date = $q->po_third_approval_date;
                    }
                    if ($q->third_approval_date != '0000-00-00 00:00:00') {
                        $q->pr_approved_by = $this->users_model->getName($q->second_level_id);
                        $q->pr_approved_date = $q->third_approval_date;
                    }
                }
            }
            if (strtotime($date1) > 0 && strtotime($date2) > 0) {
                $diff = date_diff(date_create($date1), date_create($date2));
                $q->approved_received = $diff->format("%a") + 1;
            }
        }
        if ($this->uri->rsegment(3) == 'pdf_report') {
            $data['title'] = 'Detailed PR Status Summary Report';
            parent::save_log('Detailed PR Status Summary PDF Report', 'report_pr_summary', '', 'report_pr_summary_pdf');
            $this->load->view(admin_dir('reports/report_pr_summary_pdf'), $data);
        } else {
            $this->load->view(admin_dir('lists/reports/report_pr_summary'), $data);
        }
    }

}
