<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | PR Status Summary Class
  |--------------------------------------------------------------------------
  |
  | Handles the PR Status Summary panel
  |
  | @category		Controller
  | @author		melin
 */

class Report_PR_Status_Summary extends Admin_Core {

    public $purchase_requisition = '';
    public $purchase_requisition_items_model = '';

    function __construct() {
        parent:: __construct();

        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);

        $this->load->model('admin/purchase_requisitions_model');
        $this->load->model('admin/purchase_requisition_items_model');
        $this->load->model('admin/users_model');

        $this->purchase_requisition = new Purchase_Requisitions_Model();
        $this->purchase_requisition_items_model = new Purchase_Requisition_Items_Model();
        $this->users_model = new Users_Model();

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        //for list view
        $this->list_content = array(
            'id' => array(
                'label' => 'ID',
                'type' => 'hidden',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => '',
                'var-value' => 'id_purchase_requisition',
            ),
            'pr_number' => array(
                'label' => 'PR No.',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'pr_number',
                'nowrap' => 1
            ),
            'acctg_unit' => array(
                'label' => 'Acctg Unit',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'acctg_unit',
            ),
            'pr.date_needed' => array(
                'label' => 'Date Expected',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'date_needed',
                'nowrap' => 1
            ),
            'pr_remarks' => array(
                'label' => 'Remarks',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1 ',
                'var-value' => 'pr_remarks',
            ),
            'approx_amount' => array(
                'label' => 'Amount',
                'type' => 'amount',
                'type-class' => '',
                'class' => 'col-lg-1',
                'var-value' => 'approx_amount',
                'nowrap' => 1
            ),
            'pr.status' => array(
                'label' => 'Status',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'status',
                'nowrap' => 1
            )
        );

        $this->contents = array('model_directory' => 'admin/purchase_requisitions_model',
            'model_name' => 'purchase_requisitions_model',
            'filters' => array(),
            'functionName' => 'Record',); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('reports/report_pr_status_summary'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_record() {
        $data = array(
            'template' => parent::main_template(),
        );

        $this->load->view(admin_dir('reports/report_pr_status_summary'), $data);
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if (($this->uri->rsegment(3) == 'list_record') || ($this->uri->rsegment(3) == 'pdf_report')) { /* Method for Account */
            self::_method_list_record();
        }
    }

    /* ---------- ENROLLMENT LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_record() {
        if (!IS_AJAX && ($this->uri->rsegment(3) == 'list_record')) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }

        //set condition for the list
        $condition = array();
        $condition_string = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    $date_from = $this->tools->getPost('search', 'date_from');
                    $date_to = $this->tools->getPost('search', 'date_to');
                    if ($var == 'supplier') {
                        $condition_string = array_merge($condition_string, array("s`.`supplier_code` LIKE '%" . trim($val) . "%' OR `s`.`supplier_name` LIKE '%" . trim($val) . "%'"));
                    } elseif ($var == 'acctg_unit') {
                        $condition_string = array_merge($condition_string, array("(concat(`pl`.`plate_number`,SPACE(1),`pl`.`make`,SPACE(1),`pl`.`model`) LIKE '%" . $val . "%' OR concat(`bu`.`business_unit_name`,SPACE(1),`bu`.`business_unit_code`) LIKE '%" . $val . "%')"));
                    } elseif ($var == 'date_from') {
                        if (isset($date_to)) {
                            $condition_string = array_merge($condition_string, array("pr.date_needed BETWEEN '$val' AND '$date_to'"));
                        }
                    } elseif ($var == 'date_to') {
                        $condition_string = array_merge($condition_string, array("pr.date_needed BETWEEN '$date_from' AND '$val'"));
                    } elseif ($var == 'date_needed') {
                        $condition = array_merge($condition, array("pr.date_needed like" => "%" . date('Y-m-d', strtotime($val)) . "%"));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get PR Status Summary List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array(); // to long to be poped-up
        foreach ($data['list']->result() as $q) {
            $q->approx_amount = $this->purchase_requisition_items_model->getApproxAmount(array('purchase_requisition_id' => $q->id_purchase_requisition));
            $q->acctg_unit = ($q->business_unit_name == 'TFleet') ? $q->plate_number : $q->business_unit_name;
            if ($q->first_level_id) {
                $first_level = $this->users_model->getFields($q->first_level_id);
                $q->first_level = ($first_level) ? "$first_level->user_fname $first_level->user_lname " : '---';
            }
            if ($q->branch_level_id) {
                $branch_level = $this->users_model->getFields($q->branch_level_id);
                $q->branch_level = ($branch_level) ? "$branch_level->user_fname $branch_level->user_lname " : '---';
            }
            if ($q->second_level_id) {
                $second_level = $this->users_model->getFields($q->second_level_id);
                $q->second_level = ($second_level) ? "$second_level->user_fname $second_level->user_lname " : '---';
            }
            if ($q->third_level_id) {
                $third_level = $this->users_model->getFields($q->third_level_id);
                $q->third_level = ($third_level) ? "$third_level->user_fname $third_level->user_lname " : '---';
            }
            if ($q->fourth_level_id) {
                $fourth_level = $this->users_model->getFields($q->fourth_level_id);
                $q->fourth_level = ($fourth_level) ? "$fourth_level->user_fname $fourth_level->user_lname " : '---';
            }
        }
        $purchase_requisitions = array();
        $purchase_requisition_items = $this->purchase_requisition_items_model->getList($condition, $condition_string)->result();
        foreach ($purchase_requisition_items as $pri) {
            $purchase_requisitions[$pri->purchase_requisition_id][] = $pri;
        }
        $data['purchase_requisitions'] = $purchase_requisitions;
        if ($this->uri->rsegment(3) == 'pdf_report') {
            $data['title'] = 'PR Status Summary Report';
            parent::save_log("PR Status Summary Report PDF generated", 'report_pr_status_summary', '', 'report_pr_status_summary_pdf');
            $this->load->view(admin_dir('reports/report_pr_status_summary_pdf'), $data);
        } else {
            $this->load->view(admin_dir('lists/reports/report_pr_status_summary'), $data);
        }
    }

}
