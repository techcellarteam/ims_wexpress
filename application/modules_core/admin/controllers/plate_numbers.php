<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Plate Numbers Class
  |--------------------------------------------------------------------------
  |
  | Handles the Plate Numbers panel
  |
  | @category		Controller
  | @author		melin
 */

class Plate_Numbers extends Admin_Core {

    public $plate_numbers_model = '';
    public $list_content = array();
    public $contents = array();

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        //load models
        $this->load->model('admin/plate_numbers_model');

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        //initiate models
        $this->plate_numbers_model = new Plate_Numbers_Model();
        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_plate_number',
            ),
            'plate_number' => array(
                'label' => 'Plate Number',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'plate_number',
            ),
            'make' => array(
                'label' => 'Make',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'make',
            ),
            'model' => array(
                'label' => 'Model',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'model',
            ),
            'year' => array(
                'label' => 'Year',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'year',
            )
        );

        $this->contents = array('model_directory' => 'admin/plate_numbers_model',
            'model_name' => 'plate_numbers_model',
            'filters' => array(),
            'functionName' => 'Plate Number'); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('plate_numbers/list_plate_number'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_plate_number() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'plate-number-tools.js'))),
        );
        $this->load->view(admin_dir('plate_numbers/list_plate_number'), $data);
    }

    function add_plate_number() {
        $data = array(
            'template' => parent::main_template(),
        );
        $this->load->view(admin_dir('plate_numbers/add_plate_number'), $data);
    }

    function edit_plate_number() {
        $id_plate_number = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Plate Number if Exist */
        $row = $this->plate_numbers_model->getFields($id_plate_number);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
            );
            $this->load->view(admin_dir('plate_numbers/edit_plate_number'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function view_plate_number() {
        $id_plate_number = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Plate Number if Exist */
        $row = $this->plate_numbers_model->getFields($id_plate_number);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
            );
            $this->load->view(admin_dir('plate_numbers/view_plate_number'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_plate_number') { /* Method for Account */
            self::_method_list_plate_number();
        } else if ($this->uri->rsegment(3) == 'add_plate_number') {
            self::_method_add_plate_number();
        } else if ($this->uri->rsegment(3) == 'edit_plate_number') {
            self::_method_edit_plate_number();
        } else if ($this->uri->rsegment(3) == 'delete_plate_number') {
            self::_method_delete_plate_number();
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Validation of the Form -------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    private function _validate() {
        if ($this->uri->rsegment(3) == 'add_plate_number') {
            $this->form_validation->set_rules('plate_number', 'Plate Number', 'htmlspecialchars|trim|required|is_unique[plate_numbers.plate_number]');
        } else if ($this->uri->rsegment(3) == 'edit_plate_number') {
            $this->form_validation->set_rules('plate_number', 'Plate Number', 'htmlspecialchars|trim|required');
        }
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_plate_number() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }

        //set condition for the list
        $condition = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_plate_number') {
                        $condition = array_merge($condition, array($var => $val));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'contents' => $this->contents
        );

        /* Get Plate Number List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array('edit', 'view');

        $this->load->view(admin_dir('lists/list'), $data);
    }

    /*
     * 	Add New Plate Number Method
     */

    function _method_add_plate_number() {
        if (IS_AJAX) {
            self::_validate();

            //check form validation then save
            if ($this->form_validation->run() == TRUE) {
                $this->db->trans_start();
                //getPost data set to this model fields
                parent::copyFromPost($this->plate_numbers_model, 'id_plate_number');

                /* Check Plate Number Code Plate Number Exist */
                $count = $this->plate_numbers_model->getSearch(array("plate_number like" => $this->tools->getPost('plate_number')), "", "", "", true);
                if ($count == 0) {
                    $this->my_session->get('admin', 'user_id');
                    $this->plate_numbers_model->add();
                    parent::save_log("add new plate number " . $this->tools->getPost('plate_number'), 'plate_numbers', $this->plate_numbers_model->id);
                    $arr = array(
                        'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                        'id' => $this->plate_numbers_model->id
                    );
                } else {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", "Plate Number already exist"),
                        'id' => 0
                    );
                }
                $this->db->trans_complete();
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	Edit Plate Number Type Method
     */

    function _method_edit_plate_number() {
        if (IS_AJAX) {
            $id_plate_number = $this->Misc->decode_id($this->tools->getPost('id'));

            /* Check Plate Number Type if Exist */
            $row = $this->plate_numbers_model->getFields($id_plate_number);
            if ($row) {
                self::_validate();
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    /* Check Plate Number Exist */
                    $count = $this->plate_numbers_model->getSearch(array('id_plate_number !=' => $id_plate_number, "plate_number like" => $this->tools->getPost('plate_number')), "", "", "", true);
                    if ($count == 0) {
                        /* Update Plate Number Info */
                        //getPost data set to this model fields
                        parent::copyFromPost($this->plate_numbers_model, 'id_plate_number');
                        $this->plate_numbers_model->update(array('id_plate_number' => $id_plate_number));
                        parent::save_log("update plate number " . $this->Misc->update_name_log($row->plate_number, $this->tools->getPost('plate_number')), 'plate_numbers', $id_plate_number);
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                            'id' => $id_plate_number
                        );
                    } else {
                        $arr = array(
                            'message_alert' => $this->Misc->message_status('error', "Plate Number already exist"),
                            'id' => 0
                        );
                    }
                    $this->db->trans_complete();
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Delete Plate Number Method
     */

    function _method_delete_plate_number() {
        if (IS_AJAX) {
            $id_plate_number = $this->Misc->decode_id($this->tools->getPost('item'));
            /* Check Plate Number if Exist */
            $row = $this->plate_numbers_model->getFields($id_plate_number);
            if ($row) {
                $datas = array(
                    'enabled' => 0,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete Plate Number */
                $this->plate_numbers_model->update_table($datas, "id_plate_number", $id_plate_number);
                parent::save_log("deleted plate number" . $row->plate_number, 'plate_numbers', $id_plate_number);
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Deleted'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    function get_plate_numbers() {
        $data = array('items' => $items,
            'label' => 'Select Item',
            'id' => 'id_item',
            'value' => 'description',
            'value2' => 'item_count');
        $this->load->view(admin_dir('template/select_template'), $data);
    }

}
