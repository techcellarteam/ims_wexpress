<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | KPI Report - Served PO Records Class
  |--------------------------------------------------------------------------
  |
  | Handles the KPI Report - Served PO Records panel
  |
  | @category		Controller
  | @author		melin
 */

class KPI_Served_PO extends Admin_Core {

    public $purchase_order;
    public $receiving_reports_model;

    function __construct() {
        parent:: __construct();

        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);

        $this->load->model('admin/purchase_orders_model');
        $this->load->model('admin/users_model');
        $this->load->model('admin/document_types_model');

        $this->purchase_order = new Purchase_orders_Model();
        $this->users_model = new Users_Model();
        $this->document_types_model = new Document_Types_Model();

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        //for list view
        $this->list_content = array(
            'id' => array(
                'label' => 'ID',
                'type' => 'hidden',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => '',
                'var-value' => 'id_purchase_order',
            ),
            'document_type_name' => array(
                'label' => 'Category',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'document_type_name',
            ),
            'po_number' => array(
                'label' => 'PO #',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'po_number',
                'nowrap' => 1
            ),
            'prepared_date' => array(
                'label' => 'Date PO Prepared',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'prepared_date',
                'nowrap' => 1
            ),
            'top_level_approval_date' => array(
                'label' => 'Date PO Approved',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'top_level_approval_date',
                'nowrap' => 1
            ),
            'top_level_approval_date' => array(
                'label' => 'Date PO Approved',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'top_level_approval_date',
                'nowrap' => 1
            ),
            'date_received' => array(
                'label' => 'Date Served',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'date_received',
                'nowrap' => 1
            ),
            'approved_served' => array(
                'label' => 'No. of days (Date PO Approved vs Date PO Served)',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'approved_served',
            ),
            'invoice_number' => array(
                'label' => 'Invoice Number',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'invoice_number',
                'nowrap' => 1
            ),
            'date_forwarded_acctg' => array(
                'label' => 'Date Forwarded to Accounting',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'date_forwarded_acctg',
                'nowrap' => 1
            ),
            'served_forwarded' => array(
                'label' => 'No. of days (Date PO  Served vs Date Forwarded to Accounting)',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'served_forwarded',
                'nowrap' => 1
            ),
        );

        $this->contents = array('model_directory' => 'admin/kpi_served_po_model',
            'model_name' => 'kpi_served_po_model',
            'filters' => array(),
            'functionName' => 'Record',); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('reports/kpi_served_po'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_record() {
        $where_string = array("`document_type_name` LIKE '%Regular%'");

        $data = array(
            'template' => parent::main_template(),
            'categories' => $this->document_types_model->getList(array('class_name' => 'purchase_requisitions'), $where_string)->result(),
        );

        $this->load->view(admin_dir('reports/kpi_served_po'), $data);
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if (($this->uri->rsegment(3) == 'list_record') || ($this->uri->rsegment(3) == 'pdf_report')) { /* Method for Account */
            self::_method_list_record();
        }
    }

    /* ---------- ENROLLMENT LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_record() {
        if (!IS_AJAX && ($this->uri->rsegment(3) == 'list_record')) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }

        //set condition for the list
        $condition = array();
        $condition_string = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    $date_from = $this->tools->getPost('search', 'date_from');
                    $date_to = $this->tools->getPost('search', 'date_to');
                    if ($var == 'user_name') {
                        $condition_string = array_merge($condition_string, array("du`.`user_fname` LIKE '%" . trim($val) . "%' OR `du`.`user_lname` LIKE '%" . trim($val) . "%'"));
                    } elseif ($var == 'po_number') {
                        $condition_string = array_merge($condition_string, array("`po`.`po_number` LIKE '%" . trim($val) . "%'"));
                    } elseif ($var == 'date_from') {
                        if (isset($date_to)) {
                            $condition_string = array_merge($condition_string, array("po.added_date BETWEEN '$val' AND '$date_to'"));
                        }
                    } elseif ($var == 'date_to') {
                        $condition_string = array_merge($condition_string, array("po.added_date BETWEEN '$date_from' AND '$val'"));
                    } elseif ($var == 'approved_served') {
                        $aging_where = "";
                        $test = (!((int) strpos($val, '>') < 0) || !((int) strpos($val, '>=') < 0) || !((int) strpos($val, '<') < 0) || !((int) strpos($val, '<=') < 0));
                        if ($test) {
                            $aging_where = "( CASE WHEN (`po`.`third_level_id` != 0) 
                                THEN (DATEDIFF(po.third_approval_date,po.added_date ) = $val)
                            WHEN (`po`.`second_level_id` != 0) 
                                THEN (DATEDIFF(po.second_approval_date,po.added_date ) = $val)
                                ELSE (DATEDIFF(po.approval_date,po.added_date ) = $val)
                            END )";
                            $condition_string = array_merge($condition_string, array($aging_where));
                        }
                    } elseif ($var == 'approved_received') {
                        $aging_where = "";
                        $test = (!((int) strpos($val, '>') < 0) || !((int) strpos($val, '>=') < 0) || !((int) strpos($val, '<') < 0) || !((int) strpos($val, '<=') < 0));
                        if ($test) {
                            $aging_where = "( CASE WHEN (`po`.`third_level_id` != 0) 
                                THEN (DATEDIFF(date_received,po.added_date ) = $val)
                            WHEN (`po`.`second_level_id` != 0) 
                                THEN (DATEDIFF(date_received,po.added_date ) = $val)
                                ELSE (DATEDIFF(date_received,po.added_date ) = $val)
                            END )";
                            $condition_string = array_merge($condition_string, array($aging_where));
                        }
                    } elseif ($var == 'prepared_date') {
                        $condition = array_merge($condition, array("po.added_date like" => "%" . date('Y-m-d', strtotime($val)) . "%"));
                    } elseif ($var == 'date_received') {
                        $condition = array_merge($condition, array("rr.date_received like" => "%" . date('Y-m-d', strtotime($val)) . "%"));
                    } elseif ($var == 'top_level_approval_date') {
                        $approval_where = "( CASE WHEN (`po`.`third_level_id` != 0) 
                                THEN `po`.`third_approval_date` LIKE '%" . date('Y-m-d', strtotime($val)) . "%'
                            WHEN (`po`.`second_level_id` != 0) 
                                THEN `po`.`second_approval_date` LIKE '%" . date('Y-m-d', strtotime($val)) . "%'
                            ELSE `po`.`approval_date` LIKE '%" . date('Y-m-d', strtotime($val)) . "%'
                            END )";
                        $condition_string = array_merge($condition_string, array($approval_where));
                    } elseif ($var == 'document_type_name') {
                        $condition = array_merge($condition, array("dty.document_type_name" => trim($val)));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . trim($val) . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get KPI Report - Served PO Records List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array(); // to long to be poped-up
        if ($this->uri->rsegment(3) == 'pdf_report') {
            $data['title'] = 'KPI Report - Served PO Records Report';
            parent::save_log("KPI Report - Served PO Records PDF Report Generated", 'report_rec_iss', '', 'kpi_served_po_pdf');
            $this->load->view(admin_dir('reports/kpi_served_po_pdf'), $data);
        } else {
            $this->load->view(admin_dir('lists/reports/kpi_served_po'), $data);
        }
    }

}
