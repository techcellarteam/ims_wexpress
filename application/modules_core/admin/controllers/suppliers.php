<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Suppliers Class
  |--------------------------------------------------------------------------
  |
  | Handles the Suppliers panel
  |
  | @category		Controller
  | @author		melin
 */

class Suppliers extends Admin_Core {

    public $suppliers_model = '';
    public $list_content = array();
    public $contents = array();

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);
        parent:: __construct();
        //load models
        $this->load->model('admin/suppliers_model');
        $this->load->model('admin/supplier_groups_model');
        $this->load->model('admin/countries_model');
        $this->load->model('admin/item_suppliers_model');
        $this->load->model('admin/general_group_model');
        $this->load->model('admin/stock_classifications_model');

        $this->suppliers_model = new Suppliers_Model();
        $this->supplier_groups_model = new Supplier_Groups_Model();
        $this->countries_model = new Countries_Model();
        $this->group_model = new General_Group_Model();
        $this->stock_classifications_model = new Stock_Classifications_Model();
        $this->items_suppliers = new Item_Suppliers_Model();

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        //for list view
        $this->list_content = array(
            'id' => array(
                'label' => '#',
                'type' => 'text',
                'type-class' => 'col-lg-1 uniform-input hidden',
                'class' => 'col-lg-1',
                'var-value' => 'id_supplier',
            ),
            'group_code' => array(
                'label' => 'Supplier Group',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'group_code',
            ),
            'stock_classification_name' => array(
                'label' => 'Stock Classification',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'stock_classification_name',
            ),
            'supplier_code' => array(
                'label' => 'Code',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'supplier_code',
            ),
            'supplier_name' => array(
                'label' => 'Supplier',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'supplier_name',
            ),
            'phone_no' => array(
                'label' => 'Contact No',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'phone_no',
            ),
            'email' => array(
                'label' => 'Email Address',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'email',
            ),
            'terms_payment' => array(
                'label' => 'Payment Terms',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'terms_payment',
            ),
            'tin_no' => array(
                'label' => 'Tin No',
                'type' => 'text',
                'type-class' => 'col-lg-8 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'tin_no',
            ),
            'c_name' => array(
                'label' => 'Contact Person',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'c_name',
            ),
        );

        $this->contents = array('model_directory' => 'admin/suppliers_model',
            'model_name' => 'suppliers_model',
            'filters' => array(),
            'functionName' => 'Supplier',); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('suppliers/list_supplier'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_supplier() {
        $data = array(
            'template' => parent::main_template()
        );
        $this->load->view(admin_dir('suppliers/list_supplier'), $data);
    }

    function add_supplier() {
        $data = array(
            'template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'supplier-tools.js'))),
            'countries' => $this->countries_model->getList(),
            'groups' => $this->group_model->getList()->result(),
            'stock_classifications' => $this->stock_classifications_model->getList()->result(),
        );
        $this->load->view(admin_dir('suppliers/add_supplier'), $data);
    }

    function add_supplier_group() {
        $data = array(
            'suppliers' => $this->suppliers_model->getList(array(), array(), array(), array('supplier_name'))->result()
        );
        $this->load->view(admin_dir('suppliers/extra/supplier_group'), $data);
    }

    function edit_supplier() {
        $id_supplier = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Supplier if Exist */
        $row = $this->suppliers_model->getFields($id_supplier);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
                'countries' => $this->countries_model->getList(),
                'groups' => $this->group_model->getList()->result(),
                'stock_classifications' => $this->stock_classifications_model->getList()->result(),
            );
            $this->load->view(admin_dir('suppliers/edit_supplier'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    function view_supplier() {
        $id_supplier = $this->Misc->decode_id($this->uri->rsegment(3));
        /* Check Supplier if Exist */
        $row = $this->suppliers_model->getFields($id_supplier);
        if ($row) {
            $data = array(
                'template' => parent::main_template(),
                'row' => $row,
                'js_files' => array('js_files' => array(js_dir('modules', 'supplier-tools.js'))),
                'countries' => $this->countries_model->getList(),
                'item_suppliers' => $this->items_suppliers->getList(array('supplier_id' => $id_supplier))->result(),
            );
            $this->load->view(admin_dir('suppliers/view_supplier'), $data);
        } else {
            redirect(admin_dir('profile/view_pagenotfound_page'));
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'list_supplier') { /* Method for Account */
            self::_method_list_supplier();
        } else if ($this->uri->rsegment(3) == 'add_supplier') {
            self::_method_add_supplier();
        } else if ($this->uri->rsegment(3) == 'edit_supplier') {
            self::_method_edit_supplier();
        } else if ($this->uri->rsegment(3) == 'delete_supplier') {
            self::_method_delete_supplier();
        } else if ($this->uri->rsegment(3) == 'supplier_select') {
            self::_method_supplier_select();
        } else if ($this->uri->rsegment(3) == 'supplier_group') {
            self::_method_supplier_group();
        }
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Validation of the Form -------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    private function _validate() {
        $this->form_validation->set_rules('supplier_name', 'Supplier Name', 'htmlspecialchars|trim|required|max_length[64]');
        $this->form_validation->set_rules('email', 'Email Address', 'htmlspecialchars|trim|max_length[64]|email');
        $this->form_validation->set_rules('tin_no', 'TIN No.', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('terms_payment', 'Terms of Payment', 'htmlspecialchars|trim');
        $this->form_validation->set_rules('phone_no', 'Phone No.', 'htmlspecialchars|trim||max_length[16]');
        $this->form_validation->set_rules('fax_no', 'Fax No', 'htmlspecialchars|trim||max_length[16]');
        $this->form_validation->set_rules('remarks', 'Remarks', 'htmlspecialchars|trim||max_length[128]');
        $this->form_validation->set_rules('accreditation_date', 'Accreditation Date', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('note', 'Note', 'htmlspecialchars|trim||max_length[128]');

        $this->form_validation->set_rules('c_name', 'Contact Person Name', 'htmlspecialchars|trim|required');
        $this->form_validation->set_rules('c_email', 'Contact Person Email Address', 'htmlspecialchars|trim|max_length[32]|email');
        $this->form_validation->set_rules('c_phone_no', 'Contact Person Phone No', 'htmlspecialchars|trim|');
        $this->form_validation->set_rules('c_address', 'Contact Person Address', 'htmlspecialchars|trim|max_length[64]');
        $this->form_validation->set_rules('c_department', 'Business Unit', 'htmlspecialchars|trim|max_length[16]');
        $this->form_validation->set_rules('c_job_title', 'Job Title', 'htmlspecialchars|trim|max_length[16]');

        $this->form_validation->set_rules('b_address', 'Billing Address', 'htmlspecialchars|trim|required|max_length[64]');
        $this->form_validation->set_rules('b_city', 'City', 'htmlspecialchars|trim|max_length[16]');
        $this->form_validation->set_rules('b_state', 'Province/State', 'htmlspecialchars|trim|max_length[16]');
        $this->form_validation->set_rules('b_zip_code', 'Postal/Zip Code', 'htmlspecialchars|trim');
        $this->form_validation->set_rules('b_country_code', 'Country', 'htmlspecialchars|trim');
    }

    /* ---------- ITEM BRAND LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_supplier() {
        if (!IS_AJAX) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }

        //set condition for the list
        $condition = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    if ($var == 'id_supplier') {
                        $condition = array_merge($condition, array($var => $val));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'contents' => $this->contents
        );

        /* Get Supplier List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array(); // to long to be poped-up

        $this->load->view(admin_dir('lists/list'), $data);
    }

    /*
     * 	Add New Supplier Method
     */

    function _method_add_supplier() {
        if (IS_AJAX) {
            self::_validate();
            //check form validation then save
            $arr = array();
            if ($this->form_validation->run() == TRUE) {
                $this->db->trans_start();
                //getPost data set to this model fields
                parent::copyFromPost($this->suppliers_model, 'id_supplier');

                /* Check Supplier Code Name Exist */
                $count = $this->suppliers_model->getSearch(array("supplier_name like" => $this->tools->getPost('supplier_name')), "", "", "", true);
                if ($count == 0) {
                    $this->my_session->get('admin', 'user_id');
                    $group_ids = $this->tools->getPost('general_group_id');
                    foreach ($group_ids as $idx => $general_group_id) {
                        $this->suppliers_model->general_group_id = $general_group_id;
                        if ($this->suppliers_model->add()) {
                            parent::save_log("add new supplier " . $this->tools->getPost('supplier_name'), 'suppliers', $this->suppliers_model->id);
                            $arr = array(
                                'message_alert' => $this->Misc->message_status('success', 'Successfully saved'),
                                'id' => $this->suppliers_model->id_supplier
                            );
                        }
                    }
                } else {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", "Supplier already exist"),
                        'id' => 1
                    );
                }
                $this->db->trans_complete();
            } else {
                $error = $this->Misc->oneline_string(validation_errors());
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", $error),
                    'id' => 0
                );
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	Edit Supplier Type Method
     */

    function _method_edit_supplier() {
        if (IS_AJAX) {
            $id_supplier = $this->Misc->decode_id($this->tools->getPost('id'));

            /* Check Supplier Type if Exist */
            $row = $this->suppliers_model->getFields($id_supplier);
            if ($row) {
                self::_validate();
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();

                    /* Update Supplier Info */
                    //getPost data set to this model fields
                    $general_group_id = $this->tools->getPost('general_group_id');
                    $count = $this->suppliers_model->getSearch(array('id_supplier !=' => $row->id_supplier,
                        "supplier_name like" => $this->tools->getPost('supplier_name'),
                        'general_group_id' => $general_group_id), "", "", "", true);
                    $message = '';
                    if ($count == 0) {
                        parent::copyFromPost($this->suppliers_model, 'id_supplier');
//                        if ($general_group_id != $row->general_group_id) {
//                            $group_model = $this->group_model->getFields($row->general_group_id);
//                            $message = "$row->supplier_name with group $group_model->group_name was deleted";
//                            $this->suppliers_model->update(array('id_supplier' => $id_supplier), array('enabled' => 0));
//                        }
                        if ($this->suppliers_model->update(array('id_supplier' => $id_supplier))) {
                            $message .= ". Updated supplier " . $this->Misc->update_name_log($row->supplier_name, $this->tools->getPost('supplier_name'));
                            parent::save_log($message, 'suppliers', $id_supplier);
                            $arr = array(
                                'message_alert' => $this->Misc->message_status('success', $message),
                                'id' => $id_supplier
                            );
                        }
                    } else {
                        $arr = array(
                            'message_alert' => $this->Misc->message_status("error", "$row->supplier_code with specified group already exist."),
                            'id' => 0
                        );
                    }
                    $this->db->trans_complete();
                } else {
                    $error = $this->Misc->oneline_string(validation_errors());
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0
                    );
                }
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    function _method_supplier_group() {
        if (IS_AJAX) {
            $group_ids = $this->tools->getPost('generalGroupId');
            $id_supplier = $this->tools->getPost('supplierId');
            /* Check Supplier if Exist */
            $row = $this->suppliers_model->getFields($id_supplier);
            $existing = $this->Misc->query_to_arr($this->supplier_groups_model->getList(array('sp.supplier_name' => $row->supplier_name))->result(), 'general_group_id', 'supplier_name');
            if ($group_ids && $row) {
                $this->db->trans_start();
                foreach ($group_ids as $idx => $general_group_id) {
                    $supplier_group_id = $this->supplier_groups_model->getFieldValue('id_supplier_group', array('sp.supplier_name' => $row->supplier_name,
                        'sp.general_group_id' => $general_group_id,
                        'sp.enabled' => 1));
                    if ($supplier_group_id == FALSE) {
                        $supplier_group_id = $this->supplier_groups_model->getFieldValue('id_supplier_group', array('sp.supplier_name' => $row->supplier_name,
                            'sp.general_group_id' => $general_group_id,
                            'sp.enabled' => 0));
                        if ($supplier_group_id) {
                            $this->supplier_groups_model->update(array('id_supplier_group' => $supplier_group_id), array('enabled' => 1));

                            $this->suppliers_model->update(array('supplier_name' => $row->supplier_name,
                                'general_group_id' => $general_group_id), array('enabled' => 1));
                        } else {
                            $supplier_groups_model = new Supplier_Groups_Model();
                            $supplier_group_data = array('general_group_id' => $general_group_id,
                                'sp.supplier_name' => $row->supplier_name,
                                'added_by' => $this->session->userdata['admin']['user_id'],
                                'added_date' => date('Y-m-d H:i:s'));
                            //unset
                            if ($supplier_groups_model->add($supplier_group_data)) {
                                $supplier_id = $this->suppliers_model->getFieldValue('id_supplier', array('supplier_name' => $row->supplier_name,
                                    'general_group_id' => $general_group_id,
                                    'enabled' => 0));
                                if ($supplier_id) {
                                    $this->suppliers_model->update(array('supplier_name' => $row->supplier_name,
                                        'general_group_id' => $general_group_id), array('enabled' => 1));
                                } else {
                                    parent::save_log("add new supplier " . $this->tools->getPost('supplier_name'), 'supplier_groups', $supplier_groups_model->id);
                                    //add to supplier list
                                    $supplier_model = new Suppliers_Model();
                                    $supplier_data = array();
                                    $supplier_data['general_group_id'] = $general_group_id;
                                    $supplier_data['stock_classification_id'] = $row->stock_classification_id;
                                    $supplier_data['accreditation_date'] = $row->accreditation_date;
                                    $supplier_data['supplier_type'] = $row->supplier_type;
                                    $supplier_data['supplier_name'] = $row->supplier_name;
                                    $supplier_data['supplier_code'] = $row->supplier_code;
                                    $supplier_data['email'] = $row->email;
                                    $supplier_data['terms_payment'] = $row->terms_payment;
                                    $supplier_data['tin_no'] = $row->tin_no;
                                    $supplier_data['vat'] = $row->vat;
                                    $supplier_data['phone_no'] = $row->phone_no;
                                    $supplier_data['fax_no'] = $row->fax_no;
                                    $supplier_data['c_name'] = $row->c_name;
                                    $supplier_data['c_email'] = $row->c_email;
                                    $supplier_data['c_phone_no'] = $row->c_phone_no;
                                    $supplier_data['c_address'] = $row->c_address;
                                    $supplier_data['c_department'] = $row->c_department;
                                    $supplier_data['c_job_title'] = $row->c_job_title;
                                    $supplier_data['b_address'] = $row->b_address;
                                    $supplier_data['b_city'] = $row->b_city;
                                    $supplier_data['b_state'] = $row->b_state;
                                    $supplier_data['b_zip_code'] = $row->b_zip_code;
                                    $supplier_data['b_country_code'] = $row->b_country_code;
                                    $supplier_data['remarks'] = $row->remarks;
                                    $supplier_data['note'] = $row->note;
                                    $supplier_data['nominated'] = $row->nominated;
                                    $supplier_data['added_date'] = date('Y-m-d H:i:s');
                                    $supplier_data['added_by'] = $this->session->userdata['admin']['user_id'];
                                    $supplier_model->add($supplier_data);
                                }
                            }
                        }
                        unset($existing[$general_group_id]);
                    } else {
                        unset($existing[$general_group_id]);
                    }
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("success", "Supplier groups were updated."),
                        'id' => 0
                    );
                }


                //remove supplier with this group if not included
                if ($existing) {
                    foreach ($existing as $general_group_id => $supplier_name) {
                        $supplier_group_id = $this->supplier_groups_model->getFieldValue('id_supplier_group', array('sp.supplier_name' => $supplier_name,
                            'sp.general_group_id' => $general_group_id));
                        if ($supplier_group_id) {
                            if ($this->supplier_groups_model->update(array('id_supplier_group' => $supplier_group_id), array('enabled' => 0))) {
                                $this->suppliers_model->update(array('supplier_name' => $supplier_name,
                                    'general_group_id' => $general_group_id), array('enabled' => 0));
                                parent::save_log("deleted supplier from grouping" . $this->tools->getPost('supplier_name'), 'suppliers', $id_supplier);
                                unset($group_ids[$idx]);
                            }
                            $arr = array(
                                'message_alert' => $this->Misc->message_status("success", "Other supplier in the group(s) were deleted."),
                                'id' => 0
                            );
                        }
                    }
                }
                $this->db->trans_complete();
            }
            $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
        }
    }

    /*
     * 	Delete Supplier Method
     */

    function _method_delete_supplier() {
        if (IS_AJAX) {
            $id_supplier = $this->Misc->decode_id($this->tools->getPost('item'));
            /* Check Supplier if Exist */
            $row = $this->suppliers_model->getFields($id_supplier);
            if ($row) {
                $datas = array(
                    'enabled' => 0,
                    'updated_by' => $this->my_session->get('admin', 'user_id'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                /* Delete Supplier */
                $this->suppliers_model->update_table($datas, "id_supplier", $id_supplier);
                parent::save_log("delete supplier" . $row->supplier_name, 'suppliers', $id_supplier);
                $arr = array(
                    'message_alert' => $this->Misc->message_status('success', 'Deleted'),
                    'id' => 1
                );
                $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
            }
        }
    }

    /*
     * 	Get Supplier List in select option
     */

    function _method_supplier_select() {
        if (IS_AJAX) {
            $where = $this->tools->getPost('where');
            if ($where) {
                $data = array('items' => $this->suppliers_model->getList($where)->result(),
                    'label' => 'Supplier',
                    'id' => 'id_supplier',
                    'value' => 'supplier_code',
                    'value2' => 'supplier_name'
                );
                $this->load->view(admin_dir('template/select_template'), $data);
            }
        }
    }

    /*
     * 	Add Select Supplier Groups
     */

    function show_supplier_group() {
        if (IS_AJAX) {
            $id_supplier = $this->tools->getPost('supplier_id');
            /* Check Supplier if Exist */
            $row = $this->suppliers_model->getFields($id_supplier);
            if ($row) {
                $general_group_ids = $this->Misc->query_to_string($this->supplier_groups_model->getList(array('supplier_name' => $row->supplier_name)), 'general_group_id');
                if ($general_group_ids) {
                    $existing_groups = $this->Misc->query_to_arr($this->group_model->getList(array(), array("id_general_group IN ($general_group_ids)"))->result(), 'id_general_group', 'id_general_group');
                }
                $data = array('items' => $this->group_model->getList()->result(),
                    'selected' => $existing_groups,
                    'id' => 'id_general_group',
                    'label' => 'Select Group',
                    'value' => 'group_code',
                    'value2' => 'group_name');
                $this->load->view(admin_dir('template/select_template_selected'), $data);
            }
        }
    }

    /*
     * 	Add Select Supplier Groups
     */

    function update_supplier_groups() {
        $suppliers = $this->suppliers_model->getList()->result();
        if ($suppliers) {
            foreach ($suppliers as $s) {
                $supplier_group_id = $this->supplier_groups_model->getFieldValue('id_supplier_group', array('sp.supplier_name' => $s->supplier_name,
                    'sp.general_group_id' => $s->general_group_id,
                    'sp.enabled' => 1));
                if (!$supplier_group_id) {
                    $supplier_groups_model = new Supplier_Groups_Model();
                    $supplier_group_data = array('general_group_id' => $s->general_group_id,
                        'supplier_name' => $s->supplier_name,
                        'added_by' => $this->session->userdata['admin']['user_id'],
                        'added_date' => date('Y-m-d H:i:s'));
                    $supplier_groups_model->add($supplier_group_data);
                }
            }
        }
    }

}
