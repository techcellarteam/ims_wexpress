<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Data Import Class
  |--------------------------------------------------------------------------
  |
  | Delegates functions for handling data import records
  |
  | @category     Controller
  | @author       melin
 */

class Data_Import extends Admin_Core {

    function __construct() {
        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);

        parent:: __construct();
        $this->load->model('admin/items_model');
        $this->load->model('admin/stock_classifications_model');
        $this->load->model('admin/classes_model');
        $this->load->model('admin/suppliers_model');
        $this->load->model('admin/item_suppliers_model');
        $this->load->model('admin/plate_numbers_model');
        $this->load->model('admin/general_group_model');
        $this->load->model('admin/unit_measurements_model');
        $this->load->model('admin/item_location_model');
        $this->load->model('admin/business_units_model');
        $this->load->model('admin/inventory_changes_model');

        $this->items_model = new Items_Model();
        $this->group_model = new General_Group_Model();
        $this->stock_classifications_model = new Stock_Classifications_Model();
        $this->suppliers_model = new Suppliers_Model();
        $this->item_suppliers_model = new Item_Suppliers_Model();
        $this->plate_numbers_model = new Plate_Numbers_Model();
        $this->um_model = new Unit_Measurements_Model();
        $this->item_location_model = new Item_Location_Model();
        $this->bu_model = new Business_Units_Model();
        $this->inventory_changes_model = new Inventory_Changes_Model();

        $this->classes_model = new Classes_Model();
        $this->class_id = $this->classes_model->getFieldValue('id_class', array('class_name' => $this->classname));

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        $this->contents = array(
            'filters' => array(),
            'functionName' => 'Data Import'); // use to call functions for access
    }

    // --------------------------------------------------------------------

    /*
     * Display Template
     *
     * @access      public
     * @return      void
     */
    public function view_import() {
        $data = array('template' => parent::main_template(),
            'js_files' => array('js_files' => array(js_dir('modules', 'data-import-tools.js'))),
            'business_units' => $this->bu_model->getList()->result(),
        );
        $this->load->view(admin_dir('data_import/view_import'), $data);
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if ($this->uri->rsegment(3) == 'import_file') {
            self::_import_file();
        }
    }

    // --------------------------------------------------------------------

    /*
     * Validate the form
     *
     * @access      public
     * @return      void
     */
    private function _validate() {
        $this->form_validation->set_rules("file", "Data to Import", "required|trim");
        if (strtolower($this->tools->getPost('data6')) == 'beginning_inventory') {
            $this->form_validation->set_rules("businessunitid", "Business Unit", "required|trim");
        }
    }

    // --------------------------------------------------------------------

    /*
     * Import File
     *
     * @access      public
     * @return      void
     */
    public function _import_file() {
        self::_validate();
        $arr = array();
        if ($this->form_validation->run() == TRUE) {
            if ($this->tools->getPost('data1')) {
                $uploading_data = strtolower($this->tools->getPost('data1'));
            } else if ($this->tools->getPost('data2')) {
                $uploading_data = strtolower($this->tools->getPost('data2'));
            } else if ($this->tools->getPost('data3')) {
                $uploading_data = strtolower($this->tools->getPost('data3'));
            } else if ($this->tools->getPost('data4')) {
                $uploading_data = strtolower($this->tools->getPost('data4'));
            } else if ($this->tools->getPost('data5')) {
                $uploading_data = strtolower($this->tools->getPost('data5'));
            } else {
                $uploading_data = strtolower($this->tools->getPost('data6'));
            }

            if ($uploading_data == 'items') {
                $arr = self::_save_items();
            }
            if ($uploading_data == 'suppliers') {
                $arr = self::_save_suppliers();
            }
            if ($uploading_data == 'stock_classifications') {
                $arr = self::_save_stock_classifications();
            }
            if ($uploading_data == 'unit_measurements') {
                $arr = self::_save_unit_measurements();
            }
            if ($uploading_data == 'plate_numbers') {
                $arr = self::_save_plate_numbers();
            }
            if ($uploading_data == 'beginning_inventory') {
                $arr = self::_save_beginning_inventory();
            }
        } else {
            $error = $this->Misc->oneline_string(validation_errors());
            $arr = array(
                'message_alert' => $this->Misc->message_status("error", $error),
                'id' => 0,
            );
        }
        $this->load->view(admin_dir('template/print'), array('print' => json_encode($arr)));
    }

    /*
     * Saving of Uploaded Items 
     */

    function _save_items() {
        $csv_mimetypes = array(
            'text/csv',
            'text/plain',
            'application/csv',
            'text/comma-separated-values',
            'application/excel',
            'application/vnd.ms-excel');
        $error = '';
        $error_exist = '';
        $success = FALSE;
        if ($_FILES && in_array($_FILES['file']['type'], $csv_mimetypes)) {
            $file = $_FILES["file"]["tmp_name"];
            if (($handle = fopen($file, "r")) !== FALSE) {
                $this->db->trans_start();
                while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) {
                    if (count($data) == 7) {
                        $group_code = $data[0];
                        $stock_classification_name = $data[1];
                        $sku = $data[2];
                        $description = $data[3];
                        $measurement_name = $data[4];
                        $with_supplier = (strtolower($data[5]) == 'yes') ? 1 : 0;
                        $enabled = (strtolower($data[6]) == 'active') ? 1 : 0;

                        //GET EXISTING DATA
                        $general_group_id = $this->group_model->getFieldValue('id_general_group', array(), array('group_code' => $group_code));
                        $stock_classification_id = $this->stock_classifications_model->getFieldValue('id_stock_classification', array(), array('stock_classification_name' => $stock_classification_name));
                        $unit_measurement_id = (int) $this->um_model->getFieldValue('id_unit_measurement', array(), array('measurement_name' => $measurement_name));

                        if ($stock_classification_id == FALSE) {
                            $stock_classification_id = $this->stock_classifications_model->add(array('stock_classification_name' => $stock_classification_name));
                        }
                        $where_data = array('stock_classification_id' => $stock_classification_id);
                        $like_data = array('sku' => $sku); //'description' => $description
                        $if_exist = $this->items_model->getFieldValue('id_item', $where_data, $like_data);
                        if ($if_exist == FALSE) {
                            $details = array('general_group_id' => $general_group_id,
                                'stock_classification_id' => $stock_classification_id,
                                'unit_measurement_id' => $unit_measurement_id,
                                'sku' => $sku,
                                'description' => $description,
                                'with_supplier' => $with_supplier,
                                'enabled' => $enabled,
                                'added_by' => $this->session->userdata['admin']['user_id'],
                                'added_date' => date('Y-m-d H:i:s'));

                            $items_id = $this->items_model->add($details);
                            //save Log
                            parent::save_log("Items added through upload: $sku", 'items', $items_id);
                            $success = TRUE;
                        } else {
                            $error_exist .= ($error_exist) ? ',' . $sku : $sku;
                        }
                    } else {
                        $arr = array(
                            'message_alert' => $this->Misc->message_status("error", 'Kindly check columns.'),
                            'id' => 0,
                        );
                    }
                }
                fclose($handle);
                $this->db->trans_complete();
                if ($error_exist) {
                    $error .= "The ff commodity(s) already exist: " . $error_exist;
                }

                if ($error) {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0,
                    );
                }
                if ($success) {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("success", 'Successfully saved all unique data.'),
                        'id' => 1,
                    );
                }
            } else {
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", 'Unhandled CSV file'),
                    'id' => 0,
                );
            }
        } else {
            $arr = array(
                'message_alert' => $this->Misc->message_status("error", 'Invalid File.'),
                'id' => 0,
            );
        }
        return $arr;
    }

    /*
     * Saving of Uploaded Suppliers 
     */

    function _save_suppliers() {
        $csv_mimetypes = array(
            'text/csv',
            'text/plain',
            'application/csv',
            'text/comma-separated-values',
            'application/excel',
            'application/vnd.ms-excel');
        $error = '';
        $error_exist = '';
        $success = FALSE;
        if ($_FILES && in_array($_FILES['file']['type'], $csv_mimetypes)) {
            $file = $_FILES["file"]["tmp_name"];
            if (($handle = fopen($file, "r")) !== FALSE) {
                $this->db->trans_start();
                while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) {
                    if (count($data) <= 16) {
                        $group_code = $data[0];
                        $supplier_type = $data[1];
                        $stock_classification_name = $data[2];
                        $supplier_code = $data[3];
                        $supplier_name = $data[4];
                        $email = $data[5];
                        $tin_no = $data[6];
                        $terms_payment = $data[7];
                        $vat = (strtolower($data[8]) == 'yes') ? 1 : 0;
                        $fax_no = $data[9];
                        $phone_no = $data[10];
                        $c_name = $data[11];
                        $c_job_title = $data[12];
                        $c_email = $data[13];
                        $c_phone_no = $data[14];
                        $remarks = $data[15];

                        //check EXISTING DATA
                        $general_group_id = $this->group_model->getFieldValue('id_general_group', array(), array('group_code' => $group_code));
                        $stock_classification_id = $this->stock_classifications_model->getFieldValue('id_stock_classification', array(), array('stock_classification_name' => $stock_classification_name));

                        if ($stock_classification_id == FALSE) {
                            $stock_classification_id = $this->stock_classifications_model->add(array('stock_classification_name' => $stock_classification_name));
                        }
                        $where_data = array('general_group_id' => $general_group_id,
                            'stock_classification_id' => $stock_classification_id);
                        $like_data = array('supplier_type' => $supplier_type,
                            'supplier_code' => $supplier_code,
                            'supplier_name' => $supplier_name);
                        $supplier_id = $this->suppliers_model->getFieldValue('id_supplier', $where_data, $like_data);
                        if ($supplier_id == FALSE) {
                            $details = array('general_group_id' => $general_group_id,
                                'stock_classification_id' => $stock_classification_id,
                                'supplier_type' => $supplier_type,
                                'supplier_code' => $supplier_code,
                                'supplier_name' => $supplier_name,
                                'email' => $email,
                                'terms_payment' => $terms_payment,
                                'tin_no' => $tin_no,
                                'vat' => $vat,
                                'phone_no' => $phone_no,
                                'fax_no' => $fax_no,
                                'c_name' => $c_name,
                                'c_email' => $c_email,
                                'c_phone_no' => $c_phone_no,
                                'c_job_title' => $c_job_title,
                                'added_date' => date('Y-m-d H:i:s'),
                                'added_by' => $this->session->userdata['admin']['user_id'],
                                'remarks' => $remarks);
                            $supplier_id = $this->suppliers_model->add($details);
                            //save Log
                            parent::save_log("Supplier data uploaded: $supplier_name", 'suppliers', $supplier_id);
                            $success = TRUE;
                        } else {
                            $error_exist .= ($error_exist) ? ', ' . $supplier_name : $supplier_name;
                        }
                    } else {
                        $error .= ($error) ? ', ' . $supplier_name : "The ff supplier(s)' details are not complete: " . $supplier_name;
                    }
                }
                fclose($handle);
                $this->db->trans_complete();

                if ($error_exist) {
                    $error .= "The ff suppliers(s) already exist: " . $error_exist;
                }

                if ($error) {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0,
                    );
                }
                if ($success) {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("success", 'Successfully saved unique data.'),
                        'id' => 1,
                    );
                }
            } else {
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", 'Unhandled CSV file'),
                    'id' => 0,
                );
            }
        } else {
            $arr = array(
                'message_alert' => $this->Misc->message_status("error", 'Invalid File.'),
                'id' => 0,
            );
        }
        return $arr;
    }

    /*
     * Saving of Uploaded  Stock Classifications 
     */

    function _save_stock_classifications() {
        $csv_mimetypes = array(
            'text/csv',
            'text/plain',
            'application/csv',
            'text/comma-separated-values',
            'application/excel',
            'application/vnd.ms-excel');
        $error = '';
        $error_exist = '';
        $success = FALSE;
        if ($_FILES && in_array($_FILES['file']['type'], $csv_mimetypes)) {
            $file = $_FILES["file"]["tmp_name"];
            if (($handle = fopen($file, "r")) !== FALSE) {
                $this->db->trans_start();
                while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) {
                    if (count($data) == 1) {
                        $stock_classification = $data[0];
                        $if_exist = $this->stock_classifications_model->getFieldValue('stock_classification_name', array(), array('stock_classification_name' => $stock_classification));
                        if ($if_exist == FALSE) {
                            $stock_classification_id = $this->stock_classifications_model->add(array('stock_classification_name' => htmlspecialchars($stock_classification),
                                'added_by' => $this->session->userdata['admin']['user_id'],
                                'added_date' => date('Y-m-d H:i:s')));
                            //save Log
                            parent::save_log("Stock Classification added through upload: $stock_classification", 'stock_classifications', $stock_classification_id);
                            $success = TRUE;
                        } else {
                            $error_exist .= ($error_exist) ? ',' . $stock_classification : $stock_classification;
                        }
                    }
                }
                fclose($handle);
                $this->db->trans_complete();
                if ($error_exist) {
                    $error .= "The ff stock classification(s) already exist: " . $error_exist;
                }

                if ($error) {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0,
                    );
                }
                if ($success) {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("success", 'Successfully saved all imported data.'),
                        'id' => 1,
                    );
                }
            } else {
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", 'Unhandled CSV file'),
                    'id' => 0,
                );
            }
        } else {
            $arr = array(
                'message_alert' => $this->Misc->message_status("error", 'Invalid File.'),
                'id' => 0,
            );
        }
        return $arr;
    }

    /*
     * Saving of Uploaded  Unit Measurements
     */

    function _save_unit_measurements() {
        $csv_mimetypes = array(
            'text/csv',
            'text/plain',
            'application/csv',
            'text/comma-separated-values',
            'application/excel',
            'application/vnd.ms-excel');
        $error = '';
        $error_exist = '';
        $success = FALSE;
        if ($_FILES && in_array($_FILES['file']['type'], $csv_mimetypes)) {
            $file = $_FILES["file"]["tmp_name"];
            if (($handle = fopen($file, "r")) !== FALSE) {
                $this->db->trans_start();
                while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) {
                    if (count($data) == 2) {
                        $measurement_name = $data[0];
                        $measurement_code = $data[1];
                        $if_exist = $this->um_model->getFieldValue('measurement_name', array(), array('measurement_name' => $measurement_name));
                        if ($if_exist == FALSE) {
                            $unit_measurement_id = $this->um_model->add(array('measurement_name' => htmlspecialchars($measurement_name),
                                'measurement_code' => $measurement_code,
                                'added_by' => $this->session->userdata['admin']['user_id'],
                                'added_date' => date('Y-m-d H:i:s')));
                            //save Log
                            parent::save_log("Measurement added through upload: $measurement_name", 'unit_measurements', $unit_measurement_id);
                            $success = TRUE;
                        } else {
                            $error_exist .= ($error_exist) ? ',' . $measurement_name : $measurement_name;
                        }
                    }
                }
                fclose($handle);
                $this->db->trans_complete();
                if ($error_exist) {
                    $error .= "The ff unit measurements(s) already exist: " . $error_exist;
                }

                if ($error) {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0,
                    );
                }
                if ($success) {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("success", 'Successfully saved all imported data.'),
                        'id' => 1,
                    );
                }
            } else {
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", 'Unhandled CSV file'),
                    'id' => 0,
                );
            }
        } else {
            $arr = array(
                'message_alert' => $this->Misc->message_status("error", 'Invalid File.'),
                'id' => 0,
            );
        }
        return $arr;
    }

    /*
     * Saving of Uploaded Plate Numbers
     */

    function _save_plate_numbers() {
        $csv_mimetypes = array(
            'text/csv',
            'text/plain',
            'application/csv',
            'text/comma-separated-values',
            'application/excel',
            'application/vnd.ms-excel');
        $error = '';
        $error_exist = '';
        $success = FALSE;
        if ($_FILES && in_array($_FILES['file']['type'], $csv_mimetypes)) {
            $file = $_FILES["file"]["tmp_name"];
            if (($handle = fopen($file, "r")) !== FALSE) {

                $this->db->trans_start();
                while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) {
                    if (count($data) == 4) {
                        $plate_number = $data[0];
                        $make = $data[1];
                        $model = $data[2];
                        $year = $data[3];
                        $if_exist = $this->plate_numbers_model->getFieldValue('plate_number', array(), array('plate_number' => $plate_number));
                        if ($if_exist == FALSE) {
                            $plate_number_id = $this->plate_numbers_model->add(array('plate_number' => htmlspecialchars($plate_number),
                                'make' => htmlspecialchars($make),
                                'model' => htmlspecialchars($model),
                                'year' => htmlspecialchars($year),
                                'added_by' => $this->session->userdata['admin']['user_id'],
                                'added_date' => date('Y-m-d H:i:s')));
                            //save Log
                            parent::save_log("Plate Number added through upload: $plate_number", 'plate_numbers', $plate_number_id);
                            $success = TRUE;
                        } else {
                            $error_exist .= ($error_exist) ? ',' . $plate_number : $plate_number;
                        }
                    }
                }
                fclose($handle);
                $this->db->trans_complete();

                if ($error_exist) {
                    $error .= "The ff plate number(s) already exist: " . $error_exist;
                }

                if ($error) {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0,
                    );
                }
                if ($success) {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("success", 'Successfully saved unique data.'),
                        'id' => 1,
                    );
                }
            } else {
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", 'Unhandled CSV file'),
                    'id' => 0,
                );
            }
        } else {
            $arr = array(
                'message_alert' => $this->Misc->message_status("error", 'Invalid File.'),
                'id' => 0,
            );
        }
        return $arr;
    }

    /*
     * Saving of Beginning Inventory
     * 
     */

    function _save_beginning_inventory() {
        $csv_mimetypes = array(
            'text/csv',
            'text/plain',
            'application/csv',
            'text/comma-separated-values',
            'application/excel',
            'application/vnd.ms-excel');
        $error = '';
        $error_exist = '';
        $success = FALSE;
        if ($_FILES && in_array($_FILES['file']['type'], $csv_mimetypes)) {
            $file = $_FILES["file"]["tmp_name"];
            if (($handle = fopen($file, "r")) !== FALSE) {
                $this->db->trans_start();
                $business_unit_id = $this->tools->getPost('businessunitid');
                while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) {
                    if (count($data) == 2) {

                        $sku = $data[0];
                        $quantity = (int) $data[1];

                        $item_id = (int) $this->items_model->getFieldValue('id_item', array('sku' => $sku));
                        if ($item_id == 0) {
                            $error_exist .= ($error_exist) ? ',' . $sku : $sku;
                        } else {
                            //check if with beginning inventory added
                            $il_id = $this->item_location_model->getFieldValue('id_item_location', array('po_item_id' => 0, 'location_id' => $business_unit_id, 'item_id' => $item_id));
                            if ($il_id) {
                                $item_loc = new Item_Location_Model($il_id);
                                $new_quantity = $item_loc->item_quantity + $quantity;
                                $new_item_count = $item_loc->item_count + $quantity;
                                $this->item_location_model->update(array('id_item_location' => $il_id), array('item_quantity' => $new_quantity, 'item_count' => $new_item_count));

                                //update data to inventory change
                                $inventory_data = array(
                                    'item_location_id' => $il_id,
                                    'administrator_id' => $this->session->userdata['admin']['user_id'],
                                    'stock_before' => $item_loc->item_count,
                                    'stock_quantity' => $quantity,
                                    'stock_after' => $new_item_count,
                                    'inventory_date' => date('Y-m-d H:i:s'),
                                    'transaction' => 'Beginning Inventory', //from purchase order
                                    'added_by' => $this->session->userdata['admin']['user_id'],
                                    'added_date' => date('Y-m-d H:i:s')
                                );
                                $this->inventory_changes_model->add($inventory_data);
                            } else {
                                $il_data = array(
                                    'location_id' => $business_unit_id,
                                    'item_id' => $item_id,
                                    'item_quantity' => $quantity,
                                    'item_count' => $quantity,
                                    'added_by' => $this->session->userdata['admin']['user_id'],
                                    'added_date' => date('Y-m-d H:i:s')
                                );
                                $il_id = $this->item_location_model->add($il_data);

                                //update data to inventory change
                                $inventory_data = array(
                                    'item_location_id' => $il_id,
                                    'administrator_id' => $this->session->userdata['admin']['user_id'],
                                    'stock_before' => 0,
                                    'stock_quantity' => $quantity,
                                    'stock_after' => $quantity,
                                    'inventory_date' => date('Y-m-d H:i:s'),
                                    'transaction' => 'Beginning Inventory', //from purchase order
                                    'added_by' => $this->session->userdata['admin']['user_id'],
                                    'added_date' => date('Y-m-d H:i:s')
                                );
                                $this->inventory_changes_model->add($inventory_data);
                            }

                            //save Log
                            parent::save_log("Beginning inventoy added through upload $quantity", 'item_location', $il_id);
                            $success = TRUE;
                        }
                    }
                }
                fclose($handle);
                $this->db->trans_complete();

                if ($error_exist) {
                    $error .= "The ff SKU(s) does not exist: " . $error_exist;
                }

                if ($error) {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("error", $error),
                        'id' => 0,
                    );
                }
                if ($success) {
                    $arr = array(
                        'message_alert' => $this->Misc->message_status("success", 'Successfully updated begining inventory of existing items under the Business Unit.'),
                        'id' => 1,
                    );
                }
            } else {
                $arr = array(
                    'message_alert' => $this->Misc->message_status("error", 'Unhandled CSV file'),
                    'id' => 0,
                );
            }
        } else {
            $arr = array(
                'message_alert' => $this->Misc->message_status("error", 'Invalid File.'),
                'id' => 0,
            );
        }
        return $arr;
    }

}
