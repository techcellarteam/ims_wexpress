<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Receiving Issuance Records Class
  |--------------------------------------------------------------------------
  |
  | Handles the Receiving Issuance Records panel
  |
  | @category		Controller
  | @author		melin
 */

class Report_Rec_Iss extends Admin_Core {

    public $purchase_requisition = '';
    public $receiving_reports_model = '';

    function __construct() {
        parent:: __construct();

        $this->classname = strtolower(get_class());
        $this->pagename = $this->uri->rsegment(2);
        $this->methodname = $this->uri->rsegment(3);

        $this->load->model('admin/purchase_requisitions_model');
        $this->load->model('admin/purchase_orders_model');
        $this->load->model('admin/business_units_model');
        $this->load->model('admin/suppliers_model');
        $this->load->model('admin/general_group_model');
        $this->load->model('admin/receiving_reports_model');
        $this->load->model('admin/receiving_report_items_model');

        $this->purchase_requisition = new Purchase_Requisitions_Model();
        $this->rr_model = new Receiving_Reports_Model();
        $this->rri_model = new Receiving_Report_Items_Model();

        //id
        $this->id = $this->Misc->decode_id($this->uri->rsegment(3));

        //for list view
        $this->list_content = array(
            'id' => array(
                'label' => 'ID',
                'type' => 'hidden',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => '',
                'var-value' => 'id_receiving_report',
            ),
            'date_needed' => array(
                'label' => 'Date',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'date_needed',
                'nowrap' => 1
            ),
            'pr_number' => array(
                'label' => 'PR #',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'pr_number',
                'nowrap' => 1
            ),
            'po_number' => array(
                'label' => 'PO #',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'po_number',
                'nowrap' => 1
            ),
            'supplier_name' => array(
                'label' => 'SUPPLIER',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'supplier_name',
            ),
            'invoice_number' => array(
                'label' => 'INVOICE #',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1 ',
                'var-value' => 'invoice_number',
                'nowrap' => 1
            ),
            'added_date' => array(
                'label' => 'INVOICE DATE',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'added_date',
                'nowrap' => 1
            ),
            'request_payment_amount' => array(
                'label' => 'AMOUNT',
                'type' => 'amount',
                'type-class' => '',
                'class' => 'col-lg-1',
                'var-value' => 'request_payment_amount',
                'nowrap' => 1
            ),
            'total_qty' => array(
                'label' => 'QTY',
                'type' => 'text',
                'type-class' => '',
                'class' => 'col-lg-1',
                'var-value' => 'total_qty',
                'nowrap' => 1
            ),
            'request_payment_particulars' => array(
                'label' => 'ITEM/s',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-4',
                'var-value' => 'request_payment_particulars',
            ),
            'user_name' => array(
                'label' => 'RECEIVED BY',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'user_name',
                'nowrap' => 1
            ),
            'business_unit_code' => array(
                'label' => 'DEPARTMENT',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'business_unit_code',
                'nowrap' => 1
            ),
            'date_received' => array(
                'label' => 'DATE',
                'type' => 'datepicker',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'date_received',
                'nowrap' => 1
            ),
            'prf_number' => array(
                'label' => 'RFP #',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-1',
                'var-value' => 'prf_number',
                'nowrap' => 1
            ),
            'invoice_received_by' => array(
                'label' => 'RECEIVED BY',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-2',
                'var-value' => 'invoice_received_by',
                'nowrap' => 1
            ),
            'invoice_received_date' => array(
                'label' => 'RECEIVED DATE',
                'type' => 'text',
                'type-class' => 'col-lg-12 uniform-input',
                'class' => 'col-lg-2',
                'var-value' => 'invoice_received_date',
                'nowrap' => 1
            ),
        );

        $this->contents = array('model_directory' => 'admin/receiving_reports_model',
            'model_name' => 'receiving_reports_model',
            'filters' => array(),
            'functionName' => 'Record',); // use to call functions for access
    }

    function index() {
        redirect(admin_dir('reports/report_receiving_issuance'));
    }

    /* ------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Page Function --------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------------------------------------------- */

    function list_record() {
        $data = array(
            'template' => parent::main_template(),
        );

        $this->load->view(admin_dir('reports/report_receiving_issuance'), $data);
    }

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------- Method Function --------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */

    function method() {
        if (($this->uri->rsegment(3) == 'list_record') || ($this->uri->rsegment(3) == 'pdf_report')) { /* Method for Account */
            self::_method_list_record();
        }
    }

    /* ---------- ENROLLMENT LIST ------------------------------------------------------------------------------------------------------------------ */

    function _method_list_record() {
        if (!IS_AJAX && ($this->uri->rsegment(3) == 'list_record')) {
            // Set confirmation message
            $this->session->set_flashdata('error', 'Direct access forbidden');
            redirect(admin_url($this->classname));
        }

        //set condition for the list
        $condition = array();
        $condition_string = array();
        if (!empty($this->tools->getPost('search'))) {
            foreach ($this->tools->getPost('search') as $var => $val) {
                if ($val != '') {
                    $date_from = $this->tools->getPost('search', 'date_from');
                    $date_to = $this->tools->getPost('search', 'date_to');
                    if ($var == 'user_name') {
                        $condition_string = array_merge($condition_string, array("du`.`user_fname` LIKE '%" . trim($val) . "%' OR `du`.`user_lname` LIKE '%" . trim($val) . "%'"));
                    } elseif ($var == 'po_number') {
                        $condition_string = array_merge($condition_string, array("`po`.`po_number` LIKE '%" . trim($val) . "%'"));
                    } elseif ($var == 'date_from') {
                        if (isset($date_to)) {
                            $condition_string = array_merge($condition_string, array("pr.date_needed BETWEEN '$val' AND '$date_to'"));
                        }
                    } elseif ($var == 'date_to') {
                        $condition_string = array_merge($condition_string, array("pr.date_needed BETWEEN '$date_from' AND '$val'"));
                    } elseif ($var == 'date_needed') {
                        $condition = array_merge($condition, array("pr.date_needed like" => "%" . date('Y-m-d', strtotime($val)) . "%"));
                    } else {
                        $condition = array_merge($condition, array($var . " like" => "%" . $val . "%"));
                    }
                }
            }
        }

        $datas = array(
            'search' => $this->tools->getPost('search'),
            'page' => $this->tools->getPost('page'),
            'sort' => $this->tools->getPost('sort'),
            'display' => $this->tools->getPost('display'),
            'num_button' => 5,
            'condition' => $condition,
            'stringcondition' => $condition_string,
            'contents' => $this->contents
        );

        /* Get Receiving Issuance Records List */
        $data = modules::run(admin_dir('lists/_request_data'), '_get_list', $datas);
        $data['search'] = $this->tools->getPost('search');
        $data['sort'] = $this->tools->getPost('sort');
        $data['list_content'] = $this->list_content;
        $data['pop_up'] = array(); // to long to be poped-up
        foreach ($data['list']->result() as $q) {
            $q->total_qty = $this->rri_model->getTotalQty(array('receiving_report_id' => $q->id_receiving_report));
        }
        if ($this->uri->rsegment(3) == 'pdf_report') {
            $data['title'] = 'Receiving Issuance Records Report';
            parent::save_log("Receiving Issuance Records PDF Report Generated", 'report_rec_iss', '', 'report_receiving_issuance_pdf');
            $this->load->view(admin_dir('reports/report_receiving_issuance_pdf'), $data);
        } else {
            $this->load->view(admin_dir('lists/reports/report_receiving_issuance'), $data);
        }
    }

}
