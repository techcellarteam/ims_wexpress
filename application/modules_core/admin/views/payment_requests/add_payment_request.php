<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>Add Payment Request</h3>
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Payment Request Information</span></h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Invoice Number:</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-receiving_report_id'>
                                                    <option value=''></option>
                                                    <?php foreach ($invoice_numbers as $q) { ?>
                                                        <option value='<?= $q->id_receiving_report; ?>'><?= $q->invoice_number; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <div id="custom_field"></div>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Due Date:</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata datepicker" id="formdata-due_date" value="<?= date('M d, Y'); ?>"/>
                                            </div>
                                        </div><!-- End .form-group  -->                                       
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Category:</label>
                                            <div class="col-lg-5">
                                                <input name="formdata-emergency" class="form-control formdata_radio" id="formdata-emergency" type="radio" value="1" /> <!-- value is 1 if within budget-->
                                                <i class="icon16 iconic-icon-checkmark">Emergency</i>
                                                <input name="formdata-emergency" class="form-control formdata_radio" id="formdata-emergency" type="radio" value="0" /> <!-- value is 2 if without budget-->
                                                <i class="icon16 iconic-icon-x">Non-Emergency</i>
                                            </div>
                                        </div><!-- End .form-group    -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Invoice Received Date (Supplier):</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata datepicker" id="formdata-invoice_received_date" value=""/>
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Invoice Received By (Supplier):</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-invoice_received_by" value=""/>
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks</label>
                                            <div class="col-lg-5">
                                                <textarea class="form-control formdata" id="formdata-remarks" rows="6" cols="50"></textarea>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div>

                            <!--Payment Request Items-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Particulars</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="po_items_info">
                                    </span>
                                </h4>
                            </div>
                            <div class="panel-body " id="po_items_info">
                                <div class="row form-horizontal">
                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">SKU</th>
                                                    <th class="col-lg-1">Description</th>
                                                    <th class="col-lg-1">UOM</th>
                                                    <th class="col-lg-1">Purchasing Price</th>
                                                    <th class="col-lg-1">Qty</th>
                                                    <th class="col-lg-1">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody id='particulars'>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--End of Payment Request Items-->
                            <!--Action Buttons-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Action Buttons</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="form-group">
                                    <div class="col-lg-8 center">
                                        <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-add_prf' type="button">Add Request of Payment</button>
                                    </div>
                                </div><!-- End .form-group  -->
                            </div>
                            <!--Action Buttons-->
                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->
            </div><!-- End .span12 -->
        </div><!-- End .row -->

        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->


<?php
$this->load->view(admin_dir('template/footer'));
