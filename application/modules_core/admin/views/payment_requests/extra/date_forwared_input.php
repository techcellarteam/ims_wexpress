
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="icon16 minia-icon-close"></span></button>
        <h3>Date Forwarded to Accounting:</h3>
    </div>
    <div class="modal-body">
        <div class="row popformdata_alert"></div>
        <form class="form-horizontal" action="#" role="form">
            <div class="form-group">
                <div class="col-lg-6">
                    <input type="text" class="form-control popformdata datepicker" id="popformdata-date_forwarded_acctg" value=""/>
                </div>
            </div><!-- End .form-group  -->
        </form>
    </div>
    <div class="modal-footer">
        <div class='right'>
            <button class="btn btn-success ui-wizard-content ui-formwizard-button" id="popformdata-save" type="button">Add</button>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#popformdata-save').on('click', {
            'action': "<?php echo admin_url("payment_requests/method/display_date_input"); ?>",
            'conMessage': "You are about save date when invoice was forwarded to accounting.",
            'redirect': "<?php echo admin_url("payment_requests/list_payment_request"); ?>",
            'id': "<?php echo $row->id_payment_request; ?>",
        }, save_form_v1);
        $(".datepicker").datepicker({
            maxDate: 1
        });
    });
</script>	