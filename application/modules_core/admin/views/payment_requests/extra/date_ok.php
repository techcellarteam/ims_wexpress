
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="icon16 minia-icon-close"></span></button>
        <h3>Date Forwarded to Accounting:</h3>
    </div>
    <div class="modal-body">
        <div class="row popformdata_alert"></div>
        <form class="form-horizontal" action="#" role="form">
            <div class="form-group">
                <div class="col-lg-6">
                    <?= ($row->date_forwarded_acctg != '0000-00-00') ? date('M d, Y', strtotime($row->date_forwarded_acctg)) : ''; ?>
                </div>
            </div><!-- End .form-group  -->
        </form>
    </div>
</div>	