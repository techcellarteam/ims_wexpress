<?php
$total_amt = 0;
$total_rcvd = 0;
$particulars = '';
if (!empty($rr_items)) {
    foreach ($rr_items as $q) {
        $total_amt += ($q->item_unit_cost * $q->qty_received);
        $total_rcvd += $q->qty_received;
        ?>
        <tr id="id_table">
            <td class='col-lg-1'>
                <?= $q->sku; ?>
            </td>
            <td class='col-lg-1'>
                <?= $q->description; ?>
                <?php
                if ($particulars) {
                    $particulars .= "," . $q->description;
                } else {
                    $particulars = $q->description;
                }
                ?>
            </td>
            <td class='col-lg-1'>
                <?= $q->measurement_code; ?>
            </td>
            <td class='col-lg-1'>
                <?= $q->item_unit_cost; ?>
            </td>
            <td class='col-lg-1'>
                <?= $q->qty_received; ?>
            </td>
            <td class='col-lg-1'>
        <?= number_format($q->item_unit_cost * $q->qty_received, 2); ?>
            </td>
        </tr>
    <?php }
    ?>
    <tr>
        <th colspan="4" class="col-lg-1 text-right">TOTAL</th>
        <th class="col-lg-1  text-right"><?= $total_rcvd; ?></th>
        <th class="col-lg-1 text-right">
    <?= number_format($total_amt, 2); ?>
            <textarea class="form-control formdata hidden" id="formdata-request_payment_particulars" rows="6" cols="50"><?= $particulars;?></textarea>
        </th>
    </tr>  
<?php } ?>
