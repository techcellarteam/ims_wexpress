<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>Edit Payment Request [<?= $row->prf_number; ?>]</h3>
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Payment Request Information</span>
                                    <input type="text" class="form-control formdata hidden" id="formdata-payment_request_id" value="<?= $this->misc->encode_id($row->id_payment_request); ?>"/>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Invoice Number:</label>
                                            <div class="col-lg-5">
                                                <?= $row->invoice_number; ?>
                                                <input type="text" class="form-control formdata hidden" id="formdata-receiving_report_id" value="<?= $this->misc->encode_id($row->receiving_report_id); ?>"/>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Supplier:</label>
                                            <div class="col-lg-5">
                                                <?= $row->request_payment_supplier; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Payee:</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-request_payment_payee" value="<?= $row->request_payment_payee; ?>"/>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Due Date:</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata datepicker" id="formdata-due_date" value="<?= date('M d, Y', strtotime($row->request_payment_due)); ?>"/>
                                            </div>
                                        </div><!-- End .form-group  -->                                     
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Category:</label>
                                            <div class="col-lg-5">
                                                <input name="formdata-emergency" class="form-control formdata_radio" id="formdata-emergency" type="radio" value="1" <?= ($row->emergency == 1) ? 'checked' : ''; ?> > <!-- value is 1 if within budget-->
                                                <i class="icon16 iconic-icon-checkmark">Emergency</i>
                                                <input name="formdata-emergency" class="form-control formdata_radio" id="formdata-emergency" type="radio" value="0" <?= ($row->emergency == 0) ? 'checked' : ''; ?>/> <!-- value is 2 if without budget-->
                                                <i class="icon16 iconic-icon-x">Non-Emergency</i>
                                            </div>
                                        </div><!-- End .form-group    -->
                                        
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Invoice Received Date (Supplier):</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata datepicker" id="formdata-invoice_received_date" value="<?= ($row->invoice_received_date != '0000-00-00') ? date('M d, Y', strtotime($row->invoice_received_date)) : date('M d, Y'); ?>"/>
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Invoice Received By (Supplier):</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-invoice_received_by" value="<?= $row->invoice_received_by; ?>"/>
                                            </div>
                                        </div> <!--End .form-group  -->
                                        
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks</label>
                                            <div class="col-lg-5">
                                                <textarea class="form-control formdata" id="formdata-remarks" rows="6" cols="50"><?= $row->remarks; ?></textarea>
                                            </div>
                                        </div><!-- End .form-group  -->                                        
                                    </div>
                                </div>
                            </div>

                            <!--Payment Request Items-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Particulars</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="po_items_info">
                                    </span>
                                </h4>
                            </div>
                            <div class="panel-body " id="po_items_info">
                                <div class="row form-horizontal">
                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">SKU</th>
                                                    <th class="col-lg-1">Description</th>
                                                    <th class="col-lg-1">UOM</th>
                                                    <th class="col-lg-1">Purchasing Price</th>
                                                    <th class="col-lg-1">Qty</th>
                                                    <th class="col-lg-1">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody id='particulars'>
                                                <?php
                                                $total_amt = 0;
                                                $total_rcvd = 0;
                                                $particulars = '';
                                                if (!empty($rr_items)) {
                                                    foreach ($rr_items as $q) {
                                                        $total_amt += ($q->item_unit_cost * $q->qty_received);
                                                        $total_rcvd += $q->qty_received;
                                                        ?>
                                                        <tr id="id_table">
                                                            <td class='col-lg-1'>
                                                                <?= $q->sku; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->description; ?>
                                                                <?php
                                                                if ($particulars) {
                                                                    $particulars .= "," . $q->description;
                                                                } else {
                                                                    $particulars = $q->description;
                                                                }
                                                                ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->measurement_code; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->item_unit_cost; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->qty_received; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= number_format($q->item_unit_cost * $q->qty_received, 2); ?>
                                                            </td>
                                                        </tr>
                                                    <?php }
                                                    ?>
                                                    <tr>
                                                        <th colspan="4" class="col-lg-1 text-right">TOTAL</th>
                                                        <th class="col-lg-1  text-right"><?= $total_rcvd; ?></th>
                                                        <th class="col-lg-1 text-right">
                                                            <?= number_format($total_amt, 2); ?>
                                                            <textarea class="form-control formdata hidden" id="formdata-request_payment_particulars" rows="6" cols="50"><?= $particulars; ?></textarea>
                                                        </th>
                                                    </tr>  
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--End of Payment Request Items-->
                            <!--Action Buttons-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Action Buttons</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="form-group">
                                    <div class="col-lg-8 center">
                                        <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-edit_prf' type="button">Save Request for Payment</button>
                                    </div>
                                </div><!-- End .form-group  -->
                            </div>
                            <!--Action Buttons-->
                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->
            </div><!-- End .span12 -->
        </div><!-- End .row -->

        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->


<?php
$this->load->view(admin_dir('template/footer'));
