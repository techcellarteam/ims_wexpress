<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>View Payment Request [<?= $row->prf_number; ?>]</h3>
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Payment Request Information</span>
                                    <input type="hidden" class="form-control formdata" id="formdata-purchase_order_id" value="<?= $this->Misc->encode_id($this->id); ?>"/>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> PRF Number:</label>
                                            <div class="col-lg-5">
                                                <?= $row->prf_number; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> PO Number:</label>
                                            <div class="col-lg-5">
                                                <a href="<?php echo admin_url("purchase_orders/view_purchase_order/" . $this->Misc->encode_id($row->id_purchase_order)); ?>" title="View <?= $this->contents['functionName']; ?>" class="tip view_purchase_order">
                                                    <?= $row->po_number; ?><span class="icon16 icomoon-icon-search-3"></span>
                                                </a>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> PR Number:</label>
                                            <div class="col-lg-5">
                                                <a href="<?php echo admin_url("purchase_requisitions/view_purchase_requisition/" . $this->Misc->encode_id($row->id_purchase_requisition)); ?>" title="View <?= $this->contents['functionName']; ?>" class="tip view_purchase_requisition">
                                                    <?= $row->pr_number; ?> <span class="icon16 icomoon-icon-search-3"></span>
                                                </a>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Due Date:</label>
                                            <div class="col-lg-5">
                                                <?= date('M d, Y', strtotime($row->request_payment_due)); ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Supplier:</label>
                                            <div class="col-lg-5">
                                                <?= $row->supplier_code . " | " . $row->supplier_name; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Payee:</label>
                                            <div class="col-lg-5">
                                                <?= ($row->supplier_contact_person) ? $row->supplier_contact_person : $row->supplier_name; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Category:</label>
                                            <div class="col-lg-5">
                                                <?= ($row->emergency) ? 'Emergency' : 'Non-emergency'; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Invoice Received Date:</label>
                                            <div class="col-lg-5">
                                                <?= ($row->invoice_received_date != '0000-00-00') ? date('M d, Y', strtotime($row->invoice_received_date)) : ''; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Invoice Received By:</label>
                                            <div class="col-lg-5">
                                                <?= $row->invoice_received_by; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Date Forwarded to Accounting:</label>
                                            <div class="col-lg-5">
                                                <?= ($row->date_forwarded_acctg != '0000-00-00') ? date('M d, Y', strtotime($row->date_forwarded_acctg)) : ''; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Total Amount:</label>
                                            <div class="col-lg-5">
                                                <?= "PHP " . $row->total_amount; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Paid Amount:</label>
                                            <div class="col-lg-5">
                                                <?= "PHP " . $row->paid_amount; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Status:</label>
                                            <div class="col-lg-5">
                                                <?= (($row->total_amount > 0) ? (($row->paid_amount / $row->total_amount) * 100) . "% paid" : '0%'); ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks:</label>
                                            <div class="col-lg-5">
                                                <?= $row->remarks; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div>

                            <!--Payment Request Items-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Particulars</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="po_items_info">
                                    </span>
                                </h4>
                            </div>
                            <div class="panel-body " id="po_items_info">
                                <div class="row form-horizontal">
                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">SKU</th>
                                                    <th class="col-lg-1">Description</th>
                                                    <th class="col-lg-1">UOM</th>
                                                    <th class="col-lg-1">Purchasing Price</th>
                                                    <th class="col-lg-1">Qty</th>
                                                    <th class="col-lg-1">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody id='po_items_body'>
                                                <?php
                                                $total_qty = 0;
                                                $total_amt = 0;
                                                $total_rcvd = 0;
                                                if ($rr_items) {
                                                    foreach ($rr_items as $q) {
                                                        $total_amt += ($q->item_unit_cost * $q->qty_received);
                                                        $total_rcvd += $q->qty_received;
                                                        ?>
                                                        <tr id="id_table">
                                                            <td class='col-lg-1'>
                                                                <?= $q->sku; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->description; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->measurement_code; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->item_unit_cost; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->qty_received; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= number_format($q->item_unit_cost * $q->qty_received, 2); ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                <tr>
                                                    <th colspan="3" class="col-lg-1 text-right">TOTAL</th>
                                                    <th class="col-lg-1 text-right"><?= $total_qty; ?></th>
                                                    <th class="col-lg-1  text-right"><?= $total_rcvd; ?></th>
                                                    <th class="col-lg-1 text-right"><?= number_format($total_amt, 2); ?></th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--End of Payment Request Items-->

                            <!--History-->
                            <div class="panel-heading">
                                <h4>
                                    <span>History</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="history" checked="checked">
                                    </span>
                                </h4>
                            </div>
                            <div class="panel-body hidden" id="history">
                                <div class="row form-horizontal">
                                    <div class="form-group">
                                        <label class="control-label col-lg-1">Comment:</label>
                                        <div class="col-lg-3">
                                            <textarea class="form-control formdata" id="formdata-comment" value="" rows="3" cols="5"></textarea>
                                            <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'add_comment')) { // check if permitted           ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-add_comment' type="button">Save</button>
                                            <?php } ?>
                                        </div>
                                    </div><!-- End .form-group  -->

                                    <?php foreach ($logs as $q) { ?>
                                        <div class="form-group">
                                            <label class="control-label col-lg-1"><?= date('M d, Y H:i:s', strtotime($q->user_log_date)); ?></label>
                                            <div class="col-lg-5>" >
                                                <?php
                                                switch (strtolower($q->user_log_detail)) {
                                                    case 'requested':
                                                        echo '<span class="icon16 icomoon-icon-new"></span>';
                                                        break;
                                                    case 'approved':
                                                        echo '<span class="icon16 icomoon-icon-thumbs-up-3"></span>';
                                                        break;
                                                    case 'declined':
                                                        echo '<span class="icon16 icomoon-icon-thumbs-up-4"></span>';
                                                        break;
                                                    case 'deleted attachment':
                                                    case 'uploaded attachment':
                                                        echo '<span class="icon16 icomoon-icon-file-6"></span>';
                                                        break;
                                                    default:
                                                        echo '<span class="icon16 icomoon-icon-bubble-2"></span>';
                                                        break;
                                                }
                                                ?>
                                                <span><strong><?= $q->user_log_detail; ?></strong></span>
                                                <i class=" icomoon-icon-arrow-right-5"> <?= $q->user_fname . ' ' . $q->user_lname . ' (' . $q->user_type_name . ')'; ?></i>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    <?php } ?>
                                </div>
                            </div>
                            <!--End of History-->

                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->
            </div><!-- End .span12 -->
        </div><!-- End .row -->

        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->


<?php
$this->load->view(admin_dir('template/footer'));
