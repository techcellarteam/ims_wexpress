<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->

        <div class="heading">
            <h3>Payment Requests</h3>                    
        </div><!-- End .heading-->
        <?= $this->session->flashdata('note'); ?>
        <?= $this->session->flashdata('confirm'); ?>
        <?= $this->session->flashdata('error'); ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <span>Payment Request List</span>
                            <form class="panel-form right" action="">
                                <!--Access Links--> 
                                <?php if ($this->Misc->accessible($this->access, 'payment_requests', 'method', 'add_payment_request')) { ?>
                                    <a href="<?php echo admin_url("payment_requests/add_payment_request"); ?>">
                                        <span class="icomoon-icon-plus"></span> Add New Entry
                                    </a>
                                <?php } ?>
                            </form>
                        </h4>
                    </div><!-- End .panel-heading -->
                    <div id='containerList'></div>	

                </div><!-- End .panel -->
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->       
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
    <?php
    $this->load->view(admin_dir('template/footer'));
    