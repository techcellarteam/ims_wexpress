<html>
    <head>
        <title>PDF Report</title>
        <style type="text/css">
            table, th, td{
                border: 1px solid #c4c4c4;
                border-collapse: collapse;
            }

            th{
                font-weight: bold;
            }
            
            th, td{
                padding: 10px;
            }
            
            .header div:first-child{
                float: left;
            }
            
            table{
                clear: both;
            }
            
            .header div h2{
                display: block;
            }
            
            .header div:last-child{
                margin-left: 250px;
            }
            
        </style>
    </head>
    <body>
        <div class="header">
            <div>
                <img src="<?= assets_dir('images/logo.png');?>">
            </div>
            <div>
                <h2><?=$title;?></h2>
            </div>
        </div>
        <table id = "checkAll">
            <thead>
                <tr>
                    <?php
                    foreach ($list_content as $col_name => $var) {
                        if ($var['type'] != 'hidden') {
                            ?>
                            <th class="<?= $var['class']; ?>"><?= $var['label']; ?></th>
                        <?php } ?>
                    <?php } ?>
                </tr>
            </thead>
            <tbody >
                <?php
                $i = 0;
                if (!empty($rowcount['start']))
                    $i = $rowcount['start'] - 1;
                foreach ($list->result() as $q) {
                    $i++;
                    ?>
                    <tr>
                        <?php
                        foreach ($list_content as $col_name => $val) {
                            $output = '';
                            $id = ($col_name == 'id') ? $q->{$val['var-value']} : $id;
                            if ($val['type'] == 'datepicker') {
                                $output .= date('M d, Y', strtotime($q->{$val['var-value']}));
                            } else {
                                $output .= $q->{$val['var-value']};
                            }
                            if ($val['type'] != 'hidden') {
                                ?>
                                <td>
                                    <?php echo $output; ?>
                                </td>
                                <?php
                            }
                        }
                        ?>
                    </tr>
                    <?php
                }
                if ($list->num_rows == 0) {
                    ?>
                    <tr>
                        <td valign="top" colspan="4" class="dataTables_empty">No matching records found</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </body>
</html>



