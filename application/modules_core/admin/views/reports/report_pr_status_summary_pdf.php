<html>
    <head>
        <title>PDF Report</title>
        <style type="text/css">
            table, th, td{
                border: 1px solid #c4c4c4;
                border-collapse: collapse;
            }

            th{
                font-weight: bold;
            }

            th, td{
                padding: 10px;
            }

            .header div:first-child{
                float: left;
            }

            table{
                clear: both;

            }

            .header div h2{
                display: block;
            }

            .header div:last-child{
                margin-left: 350px;
            }

        </style>
    </head>
    <body>
        <div>
            <div style="width:15%;float:left">
                <img src="<?= assets_dir('images/logo.png'); ?>" style="max-width:100%">
            </div>
        </div>
        <div class="header">
            <div style="text-align: center">
                <h2><?= $title; ?></h2>
            </div>
        </div>
        <table>
            <thead>
                <tr style="white-space: nowrap;">
                    <?php
                    foreach ($list_content as $col_name => $var) {
                        if ($var['type'] != 'hidden') {
                            ?>
                            <th class="<?= $var['class']; ?>"><?= $var['label']; ?></th>
                        <?php } ?>
                    <?php } ?>
                    <th class="col-lg-1">TRANSACTION SIGNATORIES</th>
                    <th class="col-lg-1">COMMODITY</th>
                    <th class="col-lg-1">QTY</th>
                    <th class="col-lg-1">UNIT</th>
                </tr>

            </thead>
            <tbody>
                <?php
                $i = 0;
                if (!empty($rowcount['start']))
                    $i = $rowcount['start'] - 1;
                foreach ($list->result() as $q) {
                    $i++;
                    ?>
                    <tr>
                        <?php
                        foreach ($list_content as $col_name => $val) {
                            $output = '';
                            $id = ($col_name == 'id') ? $q->{$val['var-value']} : $id;
                            if ($val['type'] == 'datepicker') {
                                if ($q->{$val['var-value']}) {
                                    if ($q->{$val['var-value']} != '0000-00-00' && $q->{$val['var-value']} != '0000-00-00 00:00:00') {
                                        $output .= date('M d, Y', strtotime($q->{$val['var-value']}));
                                    }
                                }
                            } else {
                                $output .= $q->{$val['var-value']};
                            }
                            if ($val['type'] != 'hidden') {
                                ?>
                                <td  <?= (isset($val['nowrap']) && $val['nowrap']) ? "style='white-space:nowrap'" : ''; ?>>
                                    <?php echo $output; ?>
                                </td>
                                <?php
                            }
                        }
                        ?>
                        <td  style='white-space:nowrap'>
                            <?php
                            if ($q->first_level_id) {
                                if ($q->first_approval_date != '0000-00-00 00:00:00') {
                                    ?>
                                    <b>1st Approval:</b>
                                    </br>
                                    <?= $q->first_level; ?>
                                    <?= date('M d, Y', strtotime($q->first_approval_date)); ?></br>
                                <?php } else {
                                    ?>
                                    <b> Declined (1st Level):</b>
                                    </br>
                                    <?= $q->first_level; ?>;
                                    <?= date('M d, Y', strtotime($q->first_decline_date)); ?></br>
                                    <?php
                                }
                            }
                            if ($q->branch_level_id) {
                                if ($q->branch_approval_date != '0000-00-00 00:00:00') {
                                    ?>
                                    <b> Branch Level Approval:</b>
                                    </br>
                                    <?= $q->branch_level; ?>;
                                    <?= date('M d, Y', strtotime($q->branch_approval_date)); ?></br>
                                <?php } else {
                                    ?>
                                    <b>Declined (Branch Level):</b>
                                    </br>
                                    <?= $q->branch_level; ?>;
                                    <?= date('M d, Y', strtotime($q->branch_decline_date)); ?></br>
                                    <?php
                                }
                            }

                            if ($q->second_level_id) {
                                if ($q->second_approval_date != '0000-00-00 00:00:00') {
                                    ?>
                                    <b>2nd Level Approval:</b>
                                    </br>
                                    <?= $q->second_level; ?>;
                                    <?= date('M d, Y', strtotime($q->second_approval_date)); ?></br>
                                <?php } else {
                                    ?>
                                    <b>Declined (2nd Level):</b>
                                    <?= $q->second_level; ?>;
                                    </br>
                                    <?= date('M d, Y', strtotime($q->second_decline_date)); ?></br>
                                    <?php
                                }
                            }
                            if ($q->third_level_id) {
                                if ($q->third_approval_date != '0000-00-00 00:00:00') {
                                    ?>
                                    <b>3rd Level Approval:</b>
                                    </br>
                                    <?= $q->third_level; ?>;
                                    <?= date('M d, Y', strtotime($q->third_approval_date)); ?></br>
                                <?php } else {
                                    ?>
                                    <b> Declined (3rd Level):</b>
                                    </br>
                                    <?= $q->third_level; ?>;
                                    <?= date('M d, Y', strtotime($q->third_decline_date)); ?></br>
                                    <?php
                                }
                            }
                            if ($q->fourth_level_id) {
                                if ($q->fourth_approval_date != '0000-00-00 00:00:00') {
                                    ?>
                                    <b> 4th Level Approval:</b>
                                    </br>
                                    <?= $q->fourth_level; ?>;
                                    <?= date('M d, Y', strtotime($q->fourth_approval_date)); ?></br>
                                <?php } else {
                                    ?>
                                    <b>Declined (4th Level):</b>
                                    </br>
                                    <?= $q->fourth_level; ?>;
                                    <?= date('M d, Y', strtotime($q->fourth_decline_date)); ?></br>
                                    <?php
                                }
                            }
                            if ($q->top_level_approval_date != '0000-00-00') {
                                ?>
                                <b>Top Level Approval Date:</b>
                                </br>
                                <?= date('M d, Y', strtotime($q->top_level_approval_date)); ?></br>
                            <?php } ?>
                        </td>
                        <?php
                        $count = 1;
                        if ($purchase_requisitions[$q->id_purchase_requisition]) {
                            foreach ($purchase_requisitions[$q->id_purchase_requisition] as $pri) {
                                if ($count == 1) {
                                    ?>
                                    <td><?= $pri->description; ?></td>
                                    <td><?= $pri->quantity; ?></td>
                                    <td><?= $pri->measurement_code; ?></td>
                                </tr>
                            <?php } else { ?>
                            <td colspan="7"><?= $q->pr_number; ?></td>                                
                            <td><?= $pri->description; ?></td>
                            <td><?= $pri->quantity; ?></td>
                            <td><?= $pri->measurement_code; ?></td>
                        </tr>
                        <?php
                    }
                    $count++;
                }
            }
            ?>
            <?php
        }
        if ($list->num_rows == 0) {
            ?>
            <tr>
                <td valign="top" colspan="4" class="dataTables_empty">No matching records found</td>
            </tr>
        <?php } ?>
    </tbody>
</table>
</body>
</html>



