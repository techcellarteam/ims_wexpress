<?php $this->load->view(admin_dir('template/header')); ?>
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->

        <div class="heading">
            <h3>Report</h3>                    
        </div><!-- End .heading-->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <span>PR Status Summary Report</span>
                            <form class="panel-form right" action="">
                                <?php if ($this->Misc->accessible($this->access, 'report_pr_summary', 'method', 'csv_export')) { ?>
                                    <a href=#><span class="icomoon-icon-file-excel csv_export" id="Receiving Issuance Records Report" style="pointer-events: cursor"> Export</span></a>
                                <?php } ?>
                                <?php if ($this->Misc->accessible($this->access, 'report_pr_summary', 'method', 'pdf_report')) { ?>
                                    <a href=#><span class="icomoon-icon-file-pdf pdf_report" id="PDF Format" style="pointer-events: cursor"> PDF Format</span></a>
                                <?php } ?>
                            </form>
                        </h4>
                    </div><!-- End .panel-heading -->
                    <div class="panel-body ">
                        <div class="row form-horizontal">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" style="padding-top: 20px;"> Date Parameter:</label>
                                    <div class="col-lg-2">
                                        <label>From</label>
                                        <input type="text" class="listsearch datepicker" id="date_from" value="" style="width: 100%;"/>
                                    </div>
                                    <div class="col-lg-2">
                                        <label>To</label>
                                        <input type="text" class="listsearch datepicker" id="date_to" value="" style="width: 100%;"/>
                                    </div>
                                </div><!-- End .form-group  -->
                            </div>
                        </div>
                    </div>
                    <div id='containerList'> </div>
                </div><!-- End .panel -->
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->       
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<script type="text/javascript">
    $(document).ready(function () {
        load_datalist({action: adminURL + 'report_pr_status_summary/method/list_record'});
        $(".listsearch").on('change', function () {
            var search = get_search();
            var sort = get_sort();
            var display = $('.listdisplay').val();
            load_list('', search, sort, display);
        });
    });

    jQuery(function ()
    {
        $('#date_from').datepicker({
            onSelect: function ()
            {
                $("#date_to").datepicker("option", {
                    maxDate: new Date(),
                    dateFormat: "yy-mm-dd"
                });
                var search = get_search();
                var sort = get_sort();
                var display = $('.listdisplay').val();
                load_list('', search, sort, display);
            },
            maxDate: new Date(),
            dateFormat: "yy-mm-dd"
        }
        );

        jQuery(document).on('click', '.pdf_report', generate_pdf);
    });

    function generate_pdf() {
        var search = get_search();
        var sort = get_sort();
        var display = $('.listdisplay').val();
        var page = $('a.listbutton.active').text();
        $.ajax({
            type: 'POST',
            url: adminURL + 'report_pr_status_summary/method/pdf_report',
            data: {'search': search,
                'sort': sort,
                'display': display,
                'page': page,
            },
            success: function (data)
            {
                //open a new window note:this is a popup so it may be blocked by your browser
                var newWindow = window.open("", "PR Status Summary PDF Report", );

                //write the data to the document of the newWindow
                newWindow.document.write(data);
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(xhr.status);
                alert(thrownError);
            },
            beforeSend: function ()
            {
                $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
            }
        });
    }
</script>
<?php
$this->load->view(admin_dir('template/footer'));
