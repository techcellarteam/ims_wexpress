<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="icon16 minia-icon-close"></span></button>
        <h3>Edit Unit Measurement</h3>
    </div>
    <div class="modal-body">
        <div class="row popformdata_alert"></div>
        <form class="form-horizontal" action="#" role="form">
            <div class="form-group">
                <label class="col-lg-4 control-label">* Unit Measurement Name</label>
                <div class="col-lg-8">
                    <input type="text" class='form-control popformdata' id='popformdata-measurement_name' value="<?php echo $row->measurement_name; ?>"/>
                </div>
            </div><!-- End .form-group  -->
            <div class="form-group">
                <label class="col-lg-4 control-label">* Unit Measurement Code</label>
                <div class="col-lg-8">
                    <input type="text" class='form-control popformdata' id='popformdata-measurement_code' value="<?php echo $row->measurement_code; ?>"/>
                </div>
            </div><!-- End .form-group  -->
        </form>
    </div>
    <div class="modal-footer">
        <div class='right'>
            <button class="btn btn-warning ui-wizard-content ui-formwizard-button" id="popformdata-save" type="button">Save</button>
        </div>
        <div class='right'>
            <button class="btn btn-warning ui-wizard-content ui-formwizard-button" id="popformdata-back" type="button">Back</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#popformdata-save').on('click', {
            'action': "<?php echo admin_url("unit_measurements/method/edit_unit_measurement"); ?>",
            'id': "<?php echo $this->Misc->encode_id($row->id_unit_measurement); ?>",
            'conMessage': "You are about to edit unit measurement."
        }, save_form_v1);
    });

    jQuery(function ()
    {
        jQuery(document).on('click', '#popformdata-back', backToList);
    });

    function backToList() {
        window.location.replace(adminURL + 'unit_measurements/list_unit_measurement');
    }
</script>