<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->

        <div class="heading">
            <h3>Unit Measurements</h3>                    
        </div><!-- End .heading-->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <span>Unit Measurement List</span>
                            <form class="panel-form right" action="">
                                <!--Access Links--> 
                                <?php if ($this->Misc->accessible($this->access, 'unit_measurements', 'method', 'add_unit_measurement')) { ?>
                                    <a data-toggle="modal" href="#dfltmodal" class="add_unit_measurement">
                                        <span class="icomoon-icon-plus"></span> Add New Entry
                                    </a>
                                <?php } ?>
                            </form>
                        </h4>
                    </div><!-- End .panel-heading -->
                    <div id='containerList'></div>	

                </div><!-- End .panel -->
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->       
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<?php $this->load->view(admin_dir('template/footer')); ?>