<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>Receive PO Items for PO# [<?= $row->po_number; ?>] </h3>
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Order Information</span>
                                    <input type="hidden" class="form-control formdata" id="formdata-purchase_order_id" value="<?= $this->Misc->encode_id($this->id); ?>"/>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> PO Number:</label>
                                            <div class="col-lg-5">
                                                <?= $row->po_number; ?>                                                
                                                <a href="<?php echo admin_url("purchase_orders/view_purchase_order/" . $this->Misc->encode_id($row->id_purchase_order)); ?>" title="View <?= $this->contents['functionName']; ?>" class="tip view_purchase_reqisition">
                                                    <span class="icon16 icomoon-icon-search-3"></span>
                                                </a>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> PR Number:</label>
                                            <div class="col-lg-5">
                                                <?= $row->pr_number; ?>
                                                <a href="<?php echo admin_url("purchase_requisitions/view_purchase_requisition/" . $this->Misc->encode_id($row->purchase_requisition_id)); ?>" title="View <?= $this->contents['functionName']; ?>" class="tip view_purchase_reqisition">
                                                    <span class="icon16 icomoon-icon-search-3"></span>
                                                </a>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Order Date:</label>
                                            <div class="col-lg-5">
                                                <?= date('M d, Y', strtotime($row->order_date)); ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Supplier:</label>
                                            <div class="col-lg-5">
                                                <?= $row->supplier_code . " | " . $row->supplier_name; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Supplier's Contact Person:</label>
                                            <div class="col-lg-5">
                                                <?= "$row->supplier_contact_person"; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Terms of Payment:</label>
                                            <div class="col-lg-5">
                                                <?= $row->terms_payment; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <?php if (isset($row->plate_number)) { ?>
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label"> Plate Number:</label>
                                                <div class="col-lg-5">
                                                    <?= $row->plate_number; ?>
                                                </div>
                                            </div><!-- End .form-group  -->
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks:</label>
                                            <div class="col-lg-5">
                                                <?= $row->po_remarks; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Status:</label>
                                            <div class="col-lg-5">
                                                <?= $row->status; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div>
                            <!--End of Purchase Order Info-->

                            <!--Purchase Order Items-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Order Items</span>
                                </h4>
                            </div>
                            <div class="panel-body " id="po_items_info">
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Date Received*:</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata datepicker" id="formdata-date_received" value="<?= date('M d, Y'); ?>"/>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Invoice Number*:</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-invoice_number" value=""/>
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <span style="color:rgb(36, 154, 184)">*Note: Correct item's quantity with</span> <span style="color:red">RED font</span>
                                    </div>
                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">SKU</th>
                                                    <th class="col-lg-1">Description</th>
                                                    <th class="col-lg-1">UOM</th>
                                                    <th class="col-lg-1">Initiators Price</th>
                                                    <th class="col-lg-1">Purchasing Price</th>
                                                    <th class="col-lg-1">Qty</th>
                                                    <th class="col-lg-1">Qty Received</th>
                                                    <th class="col-lg-1">Qty TO Received</th>
                                                    <th class="col-lg-1">Remarks</th>
                                                </tr>
                                            </thead>
                                            <tbody id='po_items_body'>
                                                <?php
                                                $total_qty = 0;
                                                $total_amt = 0;
                                                $total_rcvd = 0;
                                                $total_to_receive = 0;
                                                if ($po_items) {
                                                    foreach ($po_items as $q) {
                                                        $total_qty += $q->quantity;
                                                        $total_amt += ($q->item_unit_cost * $q->quantity);
                                                        $total_to_receive += ($q->quantity - $q->qty_rcvd);
                                                        $total_rcvd += $q->qty_rcvd;
                                                        $to_receive = ($q->quantity - $q->qty_rcvd);
                                                        if ($to_receive > 0) {
                                                            ?>
                                                            <tr id="id_table">
                                                                <td class='col-lg-1'>
                                                                    <?= $q->sku; ?>
                                                                </td>
                                                                <td class='col-lg-1'>
                                                                    <?= $q->description; ?>
                                                                </td>
                                                                <td class='col-lg-1'>
                                                                    <?= $q->measurement_code; ?>
                                                                </td>
                                                                <td class='col-lg-1'>
                                                                    <?= number_format($q->initiator_price, 2); ?>
                                                                </td>
                                                                <td class='col-lg-1'>
                                                                    <?= number_format($q->item_unit_cost, 2); ?>
                                                                </td>  
                                                                <td class='col-lg-1'>
                                                                    <?= $q->quantity; ?>
                                                                </td> 
                                                                <td class='col-lg-1'>
                                                                    <?= $q->qty_rcvd; ?>
                                                                </td> 
                                                                <td class='col-lg-1'>
                                                                    <input type="number" min="1" max="<?= ($q->quantity - $q->qty_rcvd); ?>" class="form-control formdata received" id="formdata-qty_rcvd[<?= $q->id_po_item; ?>]" value="<?= $to_receive; ?>"/>
                                                                    <input type="hidden" class="form-control formdata" id="formdata-item_id[<?= $q->id_po_item; ?>]" value="<?= $q->item_id; ?>"/>
                                                                </td>
                                                                <td class='col-lg-2'>
                                                                    <textarea class="form-control formdata" id="formdata-remarks[<?= $q->id_po_item; ?>]" rows="3" cols="50"></textarea>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                                <tr>
                                                    <th colspan="5" class="col-lg-1 text-right">TOTAL</th>
                                                    <th class="col-lg-1 text-right"><?= $total_qty; ?></th>
                                                    <th class="col-lg-1 text-right"><?= $total_rcvd; ?></th>
                                                    <th class="col-lg-1 text-right total_received"><?= $total_to_receive; ?></th>
                                                    <th class="col-lg-2"></th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--End of Purchase Order Items-->

                            <!--Action Buttons-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Action Buttons</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="form-group">
                                    <div class="col-lg-8 center">
                                        <?php
                                        if ($total_to_receive > 0) {
                                            if ($this->Misc->accessible($this->access, 'purchase_orders', 'method', 'receive_purchase_order')) {
                                                ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-receive' type="button">Receive Purchase Order Items</button>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <h3> ---- ALL ITEMS RECEIVED ----</h3>
                                        <?php } ?>
                                    </div>
                                </div><!-- End .form-group  -->
                            </div>
                            <!--Action Buttons-->

                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->
            </div><!-- End .span12 -->
        </div><!-- End .row -->

        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->


<?php
$this->load->view(admin_dir('template/footer'));
