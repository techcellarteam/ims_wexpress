<?php if (isset($items)): ?>
    <option value="">Select <?= $label; ?></option>
    <?php foreach ($items as $q): ?>
        <option value="<?= $q->$id; ?>" <?= (isset($selected[$q->$id]))?'selected':''; ?>>
            <?= $q->$value; ?> <?= isset($value2) ? " | " . $q->$value2 : ""; ?> <?= isset($value3) ? " | " . $q->$value3 : ""; ?>
        </option>
    <?php endforeach; ?>
<?php else: ?>
    <option value="">--- NO Entry</option>
<?php endif; ?>
