<?php
$this->load->view(admin_dir('template/header'));
$row = $result;
?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>Edit My Information</h3>     
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Upload Profile Picture</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formupload_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="well well-small progressbar_div display-none">
                                            &nbsp;
                                            <div class="inline middle blue bigger-110"> Your profile picture is <span class='progressbar_upload_label'></span> complete </div>
                                            &nbsp; &nbsp; &nbsp;
                                            <div id='progressbar_upload' style="width:100%;" data-percent="" class="inline middle no-margin progress progress-success progress-striped active">
                                                <div class="bar"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label" for="normalInput">* Upload Here</label>
                                            <div class="col-lg-5">
                                                <form id="upload_form" enctype="multipart/form-data" method="post">
                                                    <input type="file"  class='formupload' name="formupload_file" id="formupload_file" />
                                                </form>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <div class="col-lg-offset-4 col-lg-8">
                                                <button  class="btn btn-info ui-wizard-content ui-formwizard-button" id='formupload_save' type="button">Upload Photo</button>
                                                &nbsp;
                                                <a href="<?php echo admin_url('profile/take_picture_page'); ?>">
                                                    <button  class="btn btn-success ui-wizard-content ui-formwizard-button"  type="button"><span class="icomoon-icon-camera-3 white"></span> Take a Picture <span class="icomoon-icon-arrow-right-7 white"></span></button>
                                                </a>
                                            </div>
                                        </div><!-- End .form-group  --> 

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->  	
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Personal Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* First Name</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_fname" value='<?php echo $row->user_fname ?>' />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Middle Name</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_mname" value='<?php echo $row->user_mname ?>' />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Last Name</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_lname" value='<?php echo $row->user_lname ?>' />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Address</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_address" value='<?php echo $row->user_address ?>' />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <!--<div class="form-group">
                                                <label class="col-lg-4 control-label">* Email</label>
                                                <div class="col-lg-5">
                                                        <input type="text" class="form-control formdata" id="formdata_email" value='<?php echo $row->user_email ?>' />
                                                </div>
                                        </div> End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Contact No</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_contact" value='<?php echo $row->user_contact ?>' />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <div class="col-lg-offset-4 col-lg-8">
                                                <button  class="btn btn-warning ui-wizard-content ui-formwizard-button" id='formdata_save' type="button">Update</button>
                                            </div>
                                        </div><!-- End .form-group  --> 

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->  
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#formupload_save').on('click', {
            'action': "<?php echo admin_url("profile/method/upload_mypicture"); ?>",
            'conMessage': "You are about to change your profile picture."

        }, save_upload);

        $('#formdata_save').on('click', {
            'action': "<?php echo admin_url("profile/method/edit_myinfo"); ?>",
            'conMessage': "You are about to edit your information."
        }, save_form);
    });
</script>	
<?php $this->load->view(admin_dir('template/footer')); ?>