<?php $this->load->view(admin_dir('template/header'));
$row=$result; ?>
<!--Body content-->
<div id="content" class="clearfix">
	<div class="contentwrapper"><!--Content wrapper-->
		<div class="heading">
			<h3>Change Password</h3>     
		</div><!-- End .heading-->
			
		<!-- Build page from here: Usual with <div class="row-fluid"></div> -->

			<div class="row">
				<div class="col-lg-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4>
										<span class="icon16 icomoon-icon-key-2"></span>
										<span>Change Password</span>
										
									</h4>
								</div>
								<div class="panel-body ">
									<div class="row formdata_alert"></div>
									<div class="row form-horizontal">
										<div class="col-lg-10">
											<div class="form-group">
												<label class="col-lg-4 control-label" for="normalInput">* Email</label>
												<div class="col-lg-5">
													<input type="text" class="form-control" id="formdata_email" placeholder="Username" value='<?php echo $row->user_email ?>' disabled />
												</div>
											</div><!-- End .form-group  -->
											<div class="form-group">
												<label class="col-lg-4 control-label" for="normalInput">* Old Password</label>
												<div class="col-lg-5">
													<input type="password" class="form-control formdata" id="formdata_oldpassword" />
												</div>
											</div><!-- End .form-group  -->
											<div class="form-group">
												<label class="col-lg-4 control-label" for="normalInput">* New Password</label>
												<div class="col-lg-5">
													<input type="password" class="form-control formdata" id="formdata_newpassword" />
												</div>
											</div><!-- End .form-group  -->
											<div class="form-group">
												<label class="col-lg-4 control-label" for="normalInput">* Confirm Password</label>
												<div class="col-lg-5">
													<input type="password" class="form-control formdata" id="formdata_confirmpassword" />
												</div>
											</div><!-- End .form-group  -->
											<div class="form-group">
												<div class="col-lg-offset-4 col-lg-4">
													<button  class="btn btn-warning ui-wizard-content ui-formwizard-button" id='formdata_save' type="button">Update</button>
												</div>
											</div><!-- End .form-group  --> 
										</div>	
									</div>
								</div>
							</div>
						</div><!-- End .panel -->
					</div>	
				</div><!-- End .span12 -->  
			</div><!-- End .row -->  
	   
		<!-- Page end here -->
			
		
		
	</div><!-- End contentwrapper -->
</div><!-- End #content -->
<script type="text/javascript">
	$(document).ready(function(){	
		$('#formdata_save').on('click',{
			'action':"<?php echo admin_url("profile/method/edit_password"); ?>",
			'conMessage':"You are about to change your password.",
			'clear':true
		},save_form);
	});
</script>	
<?php $this->load->view(admin_dir('template/footer')); ?>