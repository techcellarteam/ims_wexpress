<?php // $this->load->view(admin_dir('template/header'), $js_files);  ?>
<link href="<?php echo assets_dir("css/chosen.css"); ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo assets_dir("js/chosen.jquery.js"); ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".chosen-select").chosen({no_results_text: "Oops, nothing found!", search_contains: true,width:"100%"});
    });
</script>
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="icon16 minia-icon-close"></span></button>
        <h3>Add New Business Unit</h3>
    </div>
    <div class="modal-body">
        <div class="row popformdata_alert"></div>
        <form class="form-horizontal" action="#" role="form">
            <div class="form-group">
                <label class="col-lg-4 control-label">* Business Unit Name</label>
                <div class="col-lg-8">
                    <input type="text" class='form-control popformdata' id='popformdata-business_unit_name' value=""/>
                </div>
            </div><!-- End .form-group  -->
            <div class="form-group">
                <label class="col-lg-4 control-label">* Business Unit Code</label>
                <div class="col-lg-8">
                    <input type="text" class='form-control popformdata' id='popformdata-business_unit_code' value=""/>
                </div>
            </div><!-- End .form-group  -->
            <div class="form-group">
                <label class="col-lg-4 control-label">* Location</label>
                <div class="col-lg-8">
                    <input type="text" class='form-control popformdata' id='popformdata-location' value=""/>
                </div>
            </div><!-- End .form-group  -->
            <div class="form-group">
                <label class="col-lg-4 control-label"> Group </label>
                <div class="col-lg-8">
                    <select class='form-control popformdata chosen-select' id='popformdata-general_group_id'>
                        <option value=''>Select Group</option>
                        <?php foreach ($groups as $q) { ?>
                            <option value='<?= $q->id_general_group; ?>'><?= $q->group_name; ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
            </div><!-- End .form-group  -->
            <div class="form-group">
                <label class="col-lg-4 control-label"> Business Unit Head</label>
                <div class="col-lg-8">
                    <select class='form-control popformdata chosen-select' id='popformdata-business_unit_head_id' >
                        <option value=''></option>
                        <?php foreach ($users as $q) { ?>
                            <option value='<?= $q->id_user; ?>'><?= $q->user_fname . " " . $q->user_lname; ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
            </div><!-- End .form-group  -->
            <div class="form-group">
                <label class="col-lg-4 control-label"> Purchasers</label>
                <div class="col-lg-8">
                    <select class='form-control popformdata chosen-select' multiple id='popformdata-purchasers' >
                        <option value=''></option>
                        <?php foreach ($purchasers as $q) { ?>
                            <option value='<?= $q->id_user; ?>'><?= $q->user_fname . " " . $q->user_lname; ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
            </div><!-- End .form-group  -->
        </form>
    </div>
    <div class="modal-footer">
        <div class='right'>
            <button class="btn btn-success ui-wizard-content ui-formwizard-button" id="popformdata-save" type="button">Add</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#popformdata-save').on('click', {
            'action': "<?php echo admin_url("business_units/method/add_business_unit"); ?>",
            'conMessage': "You are about to add new business unit.",
            'redirect': "<?php echo admin_url("business_units/list_business_unit"); ?>"
        }, save_form_v1);
    });
</script>	