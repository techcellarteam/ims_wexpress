<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="icon16 minia-icon-close"></span></button>
        <h3>Edit Business Unit</h3>
    </div>
    <div class="modal-body">
        <div class="row popformdata_alert"></div>
        <form class="form-horizontal" action="#" role="form">
            <dl class="dl-horizontal">
                <dt> Business Unit Name:</dt>
                <dd>
                    <b><?php echo $row->business_unit_name; ?></b>
                </dd>
            </dl><!-- End .form-group  -->
            <dl class="dl-horizontal">
                <dt> Business Unit Code:</dt>
                <dd>
                    <b><?php echo $row->business_unit_code; ?></b>
                </dd>
            </dl><!-- End .form-group  -->
            <dl class="dl-horizontal">
                <dt> Location:</dt>
                <dd>
                    <b><?php echo $row->location; ?></b>
                </dd>
            </dl><!-- End .form-group  -->
            <dl class="dl-horizontal">
                <dt> Group:</dt>
                <dd>
                    <b><?php echo $row->group_code . " | " . $row->group_name; ?></b>
               </dd>
            </dl><!-- End .form-group  -->
           <dl class="dl-horizontal">
                <dt> Business Unit Head:</dt>
                <dd>
                    <b><?php echo $row->user_fname . ' ' . $row->user_lname; ?></b>
                </dd>
            </dl><!-- End .form-group  -->
            <dl class="dl-horizontal">
                <dt> Purchasers:</dt>
                <?php foreach ($purchasers as $q) { ?>
                    <dd>
                        <b><?php echo $q->user_fname . ' ' . $q->user_lname; ?></b>
                    </dd>
                <?php } ?>
            </dl><!-- End .form-group  -->
        </form>
    </div>
    <div class="modal-footer">
        <div class='right'>
            <button class="btn btn-warning ui-wizard-content ui-formwizard-button" id="popformdata-back" type="button">Back</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function ()
    {
        jQuery(document).on('click', '#popformdata-back', backToList);
    });

    function backToList() {
        window.location.replace(adminURL + 'business_units/list_business_unit');
    }
</script>