<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>Edit Purchase Order [<?= $row->po_number; ?>]</h3>
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Order Information</span>
                                    <input type="hidden" class="form-control formdata" id="formdata-purchase_order_id" value="<?= $this->Misc->encode_id($this->id); ?>"/>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> PO Number:</label>
                                            <div class="col-lg-5">
                                                <?= $row->po_number; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> PR Number:</label>
                                            <div class="col-lg-5">
                                                <?= $row->pr_number; ?>
                                                <a href="<?php echo admin_url("purchase_requisitions/view_purchase_requisition/" . $this->Misc->encode_id($row->purchase_requisition_id)); ?>" title="View <?= $this->contents['functionName']; ?>" class="tip view_purchase_reqisition">
                                                    <span class="icon16 icomoon-icon-search-3"></span>
                                                </a>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Order Date:</label>
                                            <div class="col-lg-5">
                                                <?= date('M d, Y', strtotime($row->order_date)); ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Supplier:</label>
                                            <div class="col-lg-5">
                                                <?= $row->supplier_code . " | " . $row->supplier_name; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Supplier's Contact Person:</label>
                                            <div class="col-lg-5">
                                                <?= "$row->supplier_contact_person"; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Terms of Payment:</label>
                                            <div class="col-lg-5">
                                                <?= $row->terms_payment; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <?php if (isset($row->plate_number)) { ?>
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label"> Plate Number:</label>
                                                <div class="col-lg-5">
                                                    <?= $row->plate_number; ?>
                                                </div>
                                            </div><!-- End .form-group  -->
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks:</label>
                                            <div class="col-lg-5">
                                                <?= $row->po_remarks; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Status:</label>
                                            <div class="col-lg-5">
                                                <?= $row->status; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div>

                            <!--Purchase Order Items-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Order Items</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="po_items_info">
                                    </span>
                                </h4>
                            </div>
                            <div class="panel-body " id="po_items_info">
                                <div class="row form-horizontal">
                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">SKU</th>
                                                    <th class="col-lg-1">Description</th>
                                                    <th class="col-lg-1">UOM</th>
                                                    <th class="col-lg-1">Initiators Price</th>
                                                    <th class="col-lg-1">Purchasing Price</th>
                                                    <th class="col-lg-1">Qty</th>
                                                    <th class="col-lg-1">Qty Received</th>
                                                    <th class="col-lg-1">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody id='po_items_body'>
                                                <?php
                                                $total_qty = 0;
                                                $total_amt = 0;
                                                $total_rcvd = 0;
                                                if ($po_items) {
                                                    foreach ($po_items as $q) {
                                                        $total_qty += $q->quantity;
                                                        $total_amt += ($q->item_unit_cost * $q->quantity);
                                                        $total_rcvd += $q->qty_rcvd;
                                                        ?>
                                                        <tr id="id_table">
                                                            <td class='col-lg-1'>
                                                                <?= $q->sku; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->description; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->measurement_code; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->initiator_price; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->item_unit_cost; ?>
                                                            </td>    
                                                            <td class='col-lg-1'>
                                                                <?= $q->quantity; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->qty_rcvd; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= number_format($q->item_unit_cost * $q->quantity, 2); ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                <tr>
                                                    <th colspan="5" class="col-lg-1 text-right">TOTAL</th>
                                                    <th class="col-lg-1 text-right"><?= $total_qty; ?></th>
                                                    <th class="col-lg-1  text-right"><?= $total_rcvd; ?></th>
                                                    <th class="col-lg-1 text-right"><?= number_format($total_amt, 2); ?></th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--End of Purchase Order Items-->

                            <!--Receiving Report-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Receiving Report(s)</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="rr_items_info">
                                    </span>
                                </h4>
                            </div>
                            <div class="panel-body " id="rr_items_info">
                                <div class="row form-horizontal">
                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">RR Number</th>
                                                    <th class="col-lg-1">Invoice No.</th>
                                                    <th class="col-lg-1">RFP No.</th>
                                                    <th class="col-lg-1">Date Received</th>
                                                    <th class="col-lg-1">Received By</th>
                                                    <th class="col-lg-1">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id='rr_items_body'>
                                                <?php
                                                if (!empty($receiving_reports)) {
                                                    foreach ($receiving_reports as $q) {
                                                        ?>
                                                        <tr id="id_table">
                                                            <td class='col-lg-1'>
                                                                <b><?= sprintf('%05d', $q->rr_number); ?></b>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <input type="text" class="form-control formdata" id="formdata-invoice_number[<?= $q->id_receiving_report; ?>]" value="<?= $q->invoice_number; ?>"/>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->prf_number; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <input type="hidden" class="form-control formdata" id="formdata-date_received[<?= $q->id_receiving_report; ?>]" value="<?= $q->date_received; ?>"/>
                                                                <?= date('M d, Y', strtotime($q->date_received)); ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->user_name; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <i class="icon16 icomoon-icon-arrow-down-right-5 view_rri hidden" id="view_rri_<?= $q->id_receiving_report; ?>" onclick="view_rri('<?= $q->id_receiving_report; ?>')"></i>
                                                                <i class="icon16 icomoon-icon-arrow-up-left-5 close_rri "  id="close_rri_<?= $q->id_receiving_report; ?>" onclick="close_rri('<?= $q->id_receiving_report; ?>')" ></i>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="6">
                                                                <table class="table" id='table_<?= $q->id_receiving_report; ?>'>
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="col-lg-1">#</th>
                                                                            <th class="col-lg-1">SKU</th>
                                                                            <th class="col-lg-1">Description</th>
                                                                            <th class="col-lg-1">UOM</th>
                                                                            <th class="col-lg-1">Qty Receive</th>
                                                                            <th class="col-lg-1">Remarks</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody >
                                                                        <?php
                                                                        $idx = 0;
                                                                        $total_received = 0;
                                                                        foreach ($rr_items as $qi) {
                                                                            if ((int) $qi->receiving_report_id == (int) $q->id_receiving_report) {
                                                                                $idx +=1;
                                                                                $total_received += (int) $qi->qty_received;
                                                                                ?>
                                                                                <tr id="id_table">
                                                                                    <td class='col-lg-1'>
                                                                                        <?= $idx; ?>
                                                                                    </td>
                                                                                    <td class='col-lg-1'>
                                                                                        <?= $qi->sku; ?>
                                                                                    </td>
                                                                                    <td class='col-lg-1'>
                                                                                        <?= $qi->description; ?>
                                                                                    </td>
                                                                                    <td class='col-lg-1'>
                                                                                        <?= $qi->measurement_code; ?>
                                                                                    </td>
                                                                                    <td class='col-lg-1'>
                                                                                        <input type="number" min="1" max="<?= ($qi->quantity); ?>" class="form-control formdata received" id="formdata-qty_rcvd[<?= $qi->id_receiving_report_item; ?>]" value="<?= $qi->qty_received; ?>"/>
                                                                                    </td>
                                                                                    <td class='col-lg-1'>
                                                                                        <textarea class="form-control formdata" id="formdata-remarks[<?= $qi->id_receiving_report_item; ?>]" rows="2" cols="50"><?= $qi->remarks; ?></textarea>
                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                        <tr style="font-weight: bold">
                                                                            <th colspan="4" class='col-lg-1' style="text-align: right;">
                                                                                TOTAL
                                                                            </th>
                                                                            <th class='col-lg-1 total_received' >
                                                                                <?= $total_received; ?>
                                                                            </th>
                                                                            <th colspan="3" class='col-lg-1'></th>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--End of Receiving Report -->

                            <!--Action Buttons-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Action Buttons</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="form-group">
                                    <div class="col-lg-8 center">
                                        <?php
                                        if ($total_received > 0) {
                                            if ($this->Misc->accessible($this->access, 'full_deliveries', 'method', 'edit_full_delivery')) {
                                                ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-edit_receive' type="button">Edit Received PO Items</button>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <h3> ---- ALL ITEMS RECEIVED ----</h3>
                                        <?php } ?>
                                    </div>
                                </div><!-- End .form-group  -->
                            </div>
                            <!--Action Buttons-->

                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->
            </div><!-- End .span12 -->
        </div><!-- End .row -->

        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->


<?php
$this->load->view(admin_dir('template/footer'));
