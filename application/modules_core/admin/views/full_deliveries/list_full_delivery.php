<?php $this->load->view(admin_dir('template/header')); ?>
<script type="text/javascript">
    /*
     |--------------------------------------------------------------------------
     | jQuery Tools for purchase order tools
     |--------------------------------------------------------------------------
     */
    $(document).ready(function () {
        $(".datepicker").datepicker({changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", minDate: 0});
        load_datalist({action: adminURL + 'full_deliveries/method/list_full_delivery'});
    });
</script>
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->

        <div class="heading">
            <h3>Full Deliveries</h3>                    
        </div><!-- End .heading-->
        <?= $this->session->flashdata('note'); ?>
        <?= $this->session->flashdata('confirm'); ?>
        <?= $this->session->flashdata('error'); ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <span>Full Delivery List</span>
                            <form class="panel-form right" action="">
                                <!--Access Links--> 

                            </form>
                        </h4>
                    </div><!-- End .panel-heading -->
                    <div id='containerList'></div>	

                </div><!-- End .panel -->
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->       
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
    <?php
    $this->load->view(admin_dir('template/footer'));
    