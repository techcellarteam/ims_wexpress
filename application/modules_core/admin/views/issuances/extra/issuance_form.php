<!DOCTYPE html>
<html>
    <head>
        <title>ISSUANCE</title>
        <link rel="shortcut icon" href="<?= assets_dir('images/favicon.ico'); ?>" />
        <style type="text/css">
            @media print {
                .footerc {
                    background: green!important;
                }
            }
            td{
                padding: 0px;
                margin: 0px;
            }
            table{

            }
        </style>

    </head>
    <body>           
        <table style="width:100%;font-family:Arial!important;font-size:12px;">
            <tr>
                <td colspan="3" style="vertical-align:top;text-align:center;font-weight:bold;">
                    <h4  style="margin:0;font-size:18px;text-align:center;">WIDE WIDE WORLD EXPRESS CORPORATION</h4>
                    (Corporate Service Group)<br/>
                    WExpress Bldg., Pascor Drive, Sto. Nino, Paranaque<br/>
                    Telephone No.: (02) 793.1777
                </td>
            </tr>
            <tr>
                <td colspan="4" style="vertical-align:top;text-align:right;">
                    <span style="font-size:14px;">ISSUANCE NO:</span> <span style="font-size:14px;font-weight:bold;"><?= $row->issuance_no;?></span><br/>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="vertical-align:top;text-align:center;font-weight:bold;padding-bottom:15px;">
                    <h2 style="margin-top:10px;margin-bottom:10px;text-align:center;">ISSUANCE</h2>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;">
                    Company Name:
                </td>
                <td style="vertical-align:top;">
                    WIDE WIDE WORLD EXPRESS CORP
                </td>
                <td colspan="2" style="vertical-align:top;text-align: right">
                    Date: <?= date('M d, Y',strtotime($row->issuance_date));?>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;">
                    Address:
                </td>
                <td colspan="2" style="vertical-align:top;">
                    WWWE BLDG. PASCOR DRIVE STO NINO PARANAQUE CITY
                </td>
            </tr>
            
        </table>
        <!--head end-->
        <!--content-->
        <table style="width:100%;font-family:Arial!important;font-size:12px;">

            <tr>
                <td style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;width:120px">
                    ITEM
                </td>
                <td style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;width:80px">
                    QUANTITY
                </td>
                <td style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;width:80px">
                    UNIT
                </td>
                <td colspan="4" style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;">
                    COMPLETE DESCRIPTION OF ARTICLES
                </td>
            </tr>
            <?php foreach($issuance_items as $q){ ?>
            <tr>
                <td style="vertical-align:top;padding:5px;text-align:center;">
                    <?= $q->sku;?>
                </td>
                <td style="vertical-align:top;padding:5px;text-align:center;">
                    <?= $q->item_quantity;?>
                </td>
                <td style="vertical-align:top;padding:5px;text-align:center;">
                    <?= $q->measurement_code;?>
                </td>
                <td colspan="4" style="vertical-align:top;padding:5px;text-align:center;">
                    <?= $q->description;?>
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="4" style="vertical-align:top;padding:5px;text-align:center;">
                    **************** NOTHING FOLLOWS ****************
                </td>
            </tr>
            <tr>
                <td colspan="4" style="vertical-align:top;padding:5px;text-align:left;">
                    NOTE/REMARKS: <?= $row->remarks;?>
                </td>
            </tr>
        </table>
        <!--footer-->
        <table style="width:100%;font-family:Arial!important;font-size:12px;">
            <tr>
                <td style="vertical-align:top;padding:5px 0 12px 0px;text-align:left;">
                    SIGNATURE<br/>
                    _______________________
                </td>
                <td style="vertical-align:top;padding:5px 0 12px 0px;text-align:right;">
                    Wide Wide World Express Corporation<br/>
                    By: _______________________
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;padding:5px 0 12px 0px;text-align:left;">
                    Date Released:<br/>
                    <?= date('M d, Y',strtotime($row->date_released));?><br/>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center;vertical-align:top;padding:5px 0 12px 0px;">
                    <em>*** This document is system generated ***</em>
                </td>
            </tr>
        </table>
        <!--footer-->
    </body>
</html>