<?php
if (!empty($items)) {
    foreach ($items as $item) {
        ?>
        <tr id="id_table_<?= $item->id_item; ?>">
            <td class='col-lg-1'>
                <input type='hidden' class='form-control formdata items' id='formdata-items[<?= $item->id_item; ?>]' value='<?= $item->id_item; ?>' />
        <?= $item->sku; ?>
            </td>
            <td class='col-lg-1'>
        <?= $item->description; ?>
            </td>
            <td class='col-lg-1'>
        <?= $item->measurement_code; ?>
            </td>
            <td class='col-lg-1'>
        <?= $item->item_count; ?>
            </td>
            <td class='col-lg-1'>
        <?= ($qty_queued[$item->id_item]) ? $qty_queued[$item->id_item] : 0; ?>
            </td>
            <td class='col-lg-1'>
                <input type='number' min="0" max="<?= ($item->item_count - $qty_queued[$item->id_item]); ?>" class='form-control formdata quantities' id='formdata-qty_to_issue[<?= $item->id_item; ?>]' value='<?= ($item->item_count - $qty_queued[$item->id_item]); ?>' />
            </td>
            <td class='col-lg-1'>
                <textarea class='form-control formdata' id='formdata-remarks[<?= $item->id_item; ?>]'></textarea>
            </td> 
            <td class='col-lg-1'>
                <button class='icon16 icomoon-icon-remove formdata-remove_item' id='<?= $item->id_item; ?>'></button>
            </td>
        </tr>
        <?php
    }
}
?>