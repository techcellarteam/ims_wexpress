<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>Add Issuance</h3>
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Issuance Information</span></h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Issuance Date:</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata datepicker" id="formdata-issuance_date" value="<?= date('M d, Y'); ?>"/>
                                            </div>
                                        </div><!-- End .form-group  -->                                       
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Business Unit TO:</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-bu_id_to'>
                                                    <option value=''></option>
                                                    <?php foreach ($business_units as $q) { ?>
                                                        <option value='<?= $q->id_business_unit; ?>'><?= $q->business_unit_code . " | " . $q->business_unit_name; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group    -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks</label>
                                            <div class="col-lg-5">
                                                <textarea class="form-control formdata" id="formdata-issuance_remarks" rows="6" cols="50"></textarea>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div>

                            <!--Issuance Items-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Items</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="po_items_info">
                                    </span>
                                </h4>
                            </div>
                            <div class="panel-body " id="po_items_info">
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group " >
                                            <label class="col-lg-4 control-label">* Stock Classifications</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-stock_classification_id'>
                                                    <option value=''></option>
                                                    <?php foreach ($stock_classifications as $q) { ?>
                                                        <option value='<?= $q->id_stock_classification; ?>'><?= $q->stock_classification_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Items</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' multiple id='formdata-item' >
                                                    <option value="">Select product</option>
                                                </select>
                                            </div>
                                            <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-add_item' type="button">Add Item</button>
                                        </div><!-- End .form-group  -->
                                    </div><!-- End .col-lg-10    -->
                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">SKU</th>
                                                    <th class="col-lg-1">Description</th>
                                                    <th class="col-lg-1">UOM</th>
                                                    <th class="col-lg-1">Qty Available</th>
                                                    <th class="col-lg-1">Qty Queued</th>
                                                    <th class="col-lg-1">Qty to Issue</th>
                                                    <th class="col-lg-1">Remarks</th>
                                                    <th class="col-lg-1">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id='particulars'>

                                            </tbody>
                                            <tr style='font-weight: bold'>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1 text-right">Total QTY</td>
                                                <td class="col-lg-1 text-right" id='formdata-total_quantity'></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--End of Issuance Items-->
                            <!--Action Buttons-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Action Buttons</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="form-group">
                                    <div class="col-lg-8 center">
                                        <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-add_issuance' type="button">Add Issuance</button>
                                    </div>
                                </div><!-- End .form-group  -->
                            </div>
                            <!--Action Buttons-->
                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->
            </div><!-- End .span12 -->
        </div><!-- End .row -->

        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->


<?php
$this->load->view(admin_dir('template/footer'));
