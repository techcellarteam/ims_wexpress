<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>View Issuance [<?= $row->issuance_no; ?>]</h3>
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Issuance Information</span>
                                    <input type="hidden" class="form-control formdata" id="formdata-issuance_id" value="<?= $this->Misc->encode_id($this->id); ?>"/>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Issuance Number:</label>
                                            <div class="col-lg-5">
                                                <?= $row->issuance_no; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Issuance Date:</label>
                                            <div class="col-lg-5">
                                                <?= date('M d, Y', strtotime($row->issuance_date)); ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Business Unit TO:</label>
                                            <div class="col-lg-5">
                                                <?= $row->bu_name_to; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Status:</label>
                                            <div class="col-lg-5">
                                                <?= $row->status; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks:</label>
                                            <div class="col-lg-5">
                                                <?= $row->remarks; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div>

                            <!--Issuance Items-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Items</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="po_items_info">
                                    </span>
                                </h4>
                            </div>
                            <div class="panel-body " id="po_items_info">
                                <div class="row form-horizontal">
                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">SKU</th>
                                                    <th class="col-lg-1">Description</th>
                                                    <th class="col-lg-1">UOM</th>
                                                    <th class="col-lg-1">Qty</th>
                                                    <th class="col-lg-1">Qty Released</th>
                                                </tr>
                                            </thead>
                                            <tbody id='po_items_body'>
                                                <?php
                                                $total_qty = 0;
                                                $total_released = 0;
//                                                $total_amt = 0;
                                                if ($issuance_items) {
                                                    foreach ($issuance_items as $q) {
                                                        $total_qty += $q->item_quantity;
                                                        $total_released += $q->item_released;
                                                        ?>
                                                        <tr id="id_table">
                                                            <td class='col-lg-1'>
                                                                <?= $q->sku; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->description; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->measurement_code; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->item_quantity; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->item_released; ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                <tr>
                                                    <th colspan="3" class="col-lg-1 text-right">TOTAL</th>
                                                    <th class="col-lg-1 text-center"><?= $total_qty; ?></th>
                                                    <th class="col-lg-1 text-center"><?= $total_released; ?></th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--End of Issuance Items-->

                            <!--Action Buttons-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Action Buttons</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="form-group">
                                    <div class="col-lg-10 center">
                                        <div class="form-group">
                                            <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'print_issuance')) { ?>
                                                <a target="blank" href="<?php echo admin_url("$this->classname/method/print_issuance/" . $this->Misc->encode_id($row->id_issuance)); ?>" title="Print Issuance Form" >
                                                    <span class="icon32 icomoon-icon-print"> PRINT</span>
                                                </a>
                                            <?php } ?>
                                        </div>
                                        <?php if ($row->status != 'Released' && $this->Misc->accessible($this->access, $this->classname, 'method', 'release_issuance')) { ?>
                                            <div class="form-group">
                                                <label class="col-lg-offset-2 col-lg-2 control-label"> Release Date:</label>
                                                <div class="col-lg-2">
                                                    <input type="text" class="form-control formdata datepicker" id="formdata-date_released" value="<?= date('M d, Y'); ?>"/>
                                                </div>
                                            </div><!-- End .form-group  -->      
                                            <div class="form-group">    
                                                <div class="col-lg-2">
                                                    <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-release_issuance' type="button" value="Release">Release Issuance</button>
                                                </div>
                                            </div>
                                        <?php } ?>

                                    </div>
                                </div><!-- End .form-group  -->
                            </div>
                            <!--Action Buttons-->

                            <!--History-->
                            <div class="panel-heading">
                                <h4>
                                    <span>History</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="history" checked="checked">
                                    </span>
                                </h4>
                            </div>
                            <div class="panel-body hidden" id="history">
                                <div class="row form-horizontal">
                                    <div class="form-group">
                                        <label class="control-label col-lg-1">Comment:</label>
                                        <div class="col-lg-3">
                                            <textarea class="form-control formdata" id="formdata-comment" value="" rows="3" cols="5"></textarea>
                                            <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'add_comment')) { // check if permitted           ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-add_comment' type="button">Save</button>
                                            <?php } ?>
                                        </div>
                                    </div><!-- End .form-group  -->

                                    <?php foreach ($logs as $q) { ?>
                                        <div class="form-group">
                                            <label class="control-label col-lg-1"><?= date('M d, Y H:i:s', strtotime($q->user_log_date)); ?></label>
                                            <div class="col-lg-5>" >
                                                <?php
                                                switch (strtolower($q->user_log_detail)) {
                                                    case 'requested':
                                                        echo '<span class="icon16 icomoon-icon-new"></span>';
                                                        break;
                                                    case 'approved':
                                                        echo '<span class="icon16 icomoon-icon-thumbs-up-3"></span>';
                                                        break;
                                                    case 'declined':
                                                        echo '<span class="icon16 icomoon-icon-thumbs-up-4"></span>';
                                                        break;
                                                    case 'deleted attachment':
                                                    case 'uploaded attachment':
                                                        echo '<span class="icon16 icomoon-icon-file-6"></span>';
                                                        break;
                                                    default:
                                                        echo '<span class="icon16 icomoon-icon-bubble-2"></span>';
                                                        break;
                                                }
                                                ?>
                                                <span><strong><?= $q->user_log_detail; ?></strong></span>
                                                <i class=" icomoon-icon-arrow-right-5"> <?= $q->user_fname . ' ' . $q->user_lname . ' (' . $q->user_type_name . ')'; ?></i>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    <?php } ?>
                                </div>
                            </div>
                            <!--End of History-->

                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->
            </div><!-- End .span12 -->
        </div><!-- End .row -->

        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->


<?php
$this->load->view(admin_dir('template/footer'));
