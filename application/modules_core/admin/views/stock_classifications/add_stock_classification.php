<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="icon16 minia-icon-close"></span></button>
        <h3>Add New Stock Classification</h3>
    </div>
    <div class="modal-body">
        <div class="row popformdata_alert"></div>
        <form class="form-horizontal" action="#" role="form">
            <div class="form-group">
                <label class="col-lg-4 control-label">* Stock Classification Name</label>
                <div class="col-lg-8">
                    <input type="text" class='form-control popformdata' id='popformdata-stock_classification_name' value=""/>
                </div>
            </div><!-- End .form-group  -->
        </form>
    </div>
    <div class="modal-footer">
        <div class='right'>
            <button class="btn btn-success ui-wizard-content ui-formwizard-button" id="popformdata-save" type="button">Add</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#popformdata-save').on('click', {
            'action': "<?php echo admin_url("stock_classifications/method/add_stock_classification"); ?>",
            'conMessage': "You are about to add new commodity group.",
//            'redirect': "<?php echo admin_url("stock_classifications/list_stock_classification"); ?>"
        }, save_form_v1);
    });
</script>	