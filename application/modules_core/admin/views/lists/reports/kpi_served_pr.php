<div class="panel-body noPad ">
    <div class='dataTables_wrapper form-inline'>
        <div class="row">
            <div class="col-lg-4">
                <div class="dataTables_length">
                    <select class="col-lg-6 listdisplay">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <input type='hidden' class='listsort'>
                </div>
            </div>
        </div>
    </div>
    <div class='dataTables_wrapper form-inline' style="overflow:auto">
        <table class="table generate">
            <thead>
                <tr>
                    <?php
                    foreach ($list_content as $col_name => $var) {
                        if ($var['type'] != 'hidden') {
                            ?>
                            <th class="<?= $var['class']; ?>"><?= $var['label']; ?></th>
                            <?php
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <?php foreach ($list_content as $col_name => $var) { ?>
                        <?php if ($var['type-class'] != '') { ?>
                            <?php if (($var['type'] == 'text' || $var['type'] == 'amount')) { ?>
                                <th>
                                    <input type="<?= $var['type']; ?>" class="listsearch uniform-input text <?= $var['type-class']; ?>" id="<?= $col_name; ?>" placeholder="<?= $var['label']; ?>" />
                                </th>
                            <?php } ?>
                            <?php if (($var['type'] == 'datepicker') && ($var['type-class'])) { ?>
                                <th>
                                    <input type="text" class="datepicker listsearch uniform-input text <?= $var['type-class']; ?>" id="<?= $col_name; ?>" placeholder="<?= $var['label']; ?>" />
                                </th>
                            <?php } ?>
                        <?php } else { ?>
                            <th></th>
                        <?php } ?>
                    <?php } ?>
                </tr>
            </thead>
            <tbody >
                <?php
                $i = 0;
                if (!empty($rowcount['start']))
                    $i = $rowcount['start'] - 1;
                foreach ($list->result() as $q) {
                    $i++;
                    ?>
                    <tr>
                        <?php
                        foreach ($list_content as $col_name => $val) {
                            $output = '';
                            if ($val['type'] == 'datepicker') {
                                if ($q->{$val['var-value']}) {
                                    if ($q->{$val['var-value']} != '0000-00-00' && $q->{$val['var-value']} != '0000-00-00 00:00:00') {
                                        $output .= date('M d, Y', strtotime($q->{$val['var-value']}));
                                    }
                                }
                            } else {
                                    if ($col_name == 'id') {
                                        $id = $q->{$val['var-value']};
                                        $output .= $i;
                                    } else if ($val['type'] == 'amount') {
                                        $output .= number_format($q->{$val['var-value']}, 2);
                                    } else {
                                        $output .= $q->{$val['var-value']};
                                    }
                                }
                                if ($val['type'] != 'hidden') {
                                    if ($val['var-value'] == 'pr_number') {
                                        if ($this->Misc->accessible($this->access, 'purchase_requisitions', 'method', 'view_purchase_requisition')) {
                                            ?>                                
                                            <td  <?= (isset($val['nowrap']) && $val['nowrap']) ? "style='white-space:nowrap'" : ''; ?>>
                                                <a target="blank" href="<?php echo admin_url("purchase_requisitions/view_purchase_requisition/" . $this->Misc->encode_id($q->id_purchase_requisition)); ?>" style="cursor: pointer;" title="View PR" class="tip view_purchase_requisition>">
                                                    <?php echo $output; ?>
                                                </a>
                                            </td>
                                            <?php
                                        }
                                    } else if ($val['var-value'] == 'po_number') {
                                        ?>
                                        <td <?= (isset($val['nowrap']) && $val['nowrap']) ? "style='white-space:nowrap'" : ''; ?>>
                                            <a target="blank" href="<?php echo admin_url("purchase_orders/view_purchase_order/" . $this->Misc->encode_id($q->id_purchase_order)); ?>" style="cursor: pointer;" title="View PR" class="tip view_purchase_order>">
                                                <?php echo $output; ?>
                                            </a>
                                        </td>
                                    <?php } else {
                                        ?>
                                        <td <?= (isset($val['nowrap']) && $val['nowrap']) ? "style='white-space:nowrap'" : ''; ?>>
                                            <?php echo $output; ?>
                                        </td>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </tr>
                        <?php
                    }
                    if ($list->num_rows == 0) {
                        ?>
                        <tr>
                            <td valign="top" colspan="12" class="dataTables_empty">No matching records found</td>
                        </tr>
                    <?php }
                    ?>
                </tbody>
            </table>
        </div>
        <?php $this->load->view(admin_dir('template/pagination')); ?>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".datepicker").datepicker({changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", maxDate: '+0d'});
            $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
            function tooltip_placement(context, source) {
                var $source = $(source);
                var $parent = $source.closest('table')
                var off1 = $parent.offset();
                var w1 = $parent.width();

                var off2 = $source.offset();
                var w2 = $source.width();

                if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2))
                    return 'right';
                return 'left';
            }

    <?php
    if (!empty($search)) {
        foreach ($search as $var => $val) {
            ?>
                    $('#<?php echo $var; ?>').val('<?php echo $val; ?>');
            <?php
        }
    }
    ?>
    <?php if (!empty($sort)) { ?>
                $('.listsort').addClass('list<?php echo $sort['sort_type']; ?>');
                $('.listsort').attr('name', '<?php echo $sort['sort_by']; ?>');
    <?php } ?>
    <?php if (!empty($display)) { ?>
                $('.listdisplay').val('<?php echo $display; ?>');
    <?php } ?>
        });
    </script>
    <script type="text/javascript" src="<?php echo assets_dir("js/main.js"); ?>"></script><!-- Core js functions -->