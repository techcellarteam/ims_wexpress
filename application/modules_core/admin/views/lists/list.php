<div class="panel-body noPad ">
    <div class='dataTables_wrapper form-inline'>
        <div class="row">
            <div class="col-lg-4">
                <div class="dataTables_length">
                    <select class="col-lg-6 listdisplay">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <input type='hidden' class='listsort'>
                </div>
            </div>
        </div>
    </div>
    <div class='dataTables_wrapper form-inline responsive'>
        <table class="table " id="checkAll">
            <thead>
                <tr>
                    <?php
                    foreach ($list_content as $col_name => $var) {
                        if ($var['type'] != 'hidden') {
                            ?>                    
                            <th class="<?= $var['class']; ?>"><?= $var['label']; ?>
                                <img class='listasc' name='<?= $var['var-value']; ?>' title='Ascending' src='<?php echo images_dir('up.gif'); ?>'>&nbsp;
                                <img class='listdesc' name='<?= $var['var-value']; ?>' title='Descending' src='<?php echo images_dir('down.gif'); ?>'>
                            </th>
                            <?php
                        }
                    }
                    ?>
                    <?php if (!empty($this->access[$this->classname]['method'])) { ?>
                        <th class="col-lg-1">Actions</th>
                    <?php } ?>
                </tr>
                <tr>
                    <?php foreach ($list_content as $col_name => $var) { ?>
                        <?php if ($var['type'] == 'text') { ?>
                            <th>
                                <input type="<?= $var['type']; ?>" class="listsearch uniform-input text <?= $var['type-class']; ?>" id="<?= $var['var-value']; ?>" placeholder="<?= $var['label']; ?>" />
                            </th>
                        <?php } ?>
                        <?php if ($var['type'] == 'datepicker') { ?>
                            <th>
                                <input type="text" class="datepicker listsearch uniform-input text <?= $var['type-class']; ?>" id="<?= $var['var-value']; ?>" placeholder="<?= $var['label']; ?>" />
                            </th>
                        <?php } ?>
                        <?php if ($var['type'] == 'decimal' || $var['type'] == 'number') { ?>
                            <th>
                                <input type="number" class=" listsearch uniform-input text <?= $var['type-class']; ?>" id="<?= $var['var-value']; ?>" placeholder="<?= $var['label']; ?>" />
                            </th>
                        <?php } ?>
                    <?php } ?>
                    <?php if (!empty($this->access[$this->classname]['method'])) { ?>
                        <th>&nbsp;</th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody >
                <?php
                $i = 0;
                if (!empty($rowcount['start']))
                    $i = $rowcount['start'] - 1;
                foreach ($list->result() as $q) {
                    $i++;
                    ?>
                    <tr>
                        <?php
                        foreach ($list_content as $col_name => $val) {
                            $output = '';

                            if ($val['type'] == 'datepicker') {
                                if ($q->{$val['var-value']}) {
                                    if (strtotime($q->{$val['var-value']}) != 0) {
                                        $output .= date('M d, Y', strtotime($q->{$val['var-value']}));
                                    } else {
                                        $output = '';
                                    }
                                } else {
                                    $output = '';
                                }
                            } else if ($val['type'] == 'decimal') {
                                $output .= number_format($q->{$val['var-value']}, 2);
                            } else if ($val['var-value'] == 'enabled' || $val['var-value'] == 'with_supplier') {
                                if ($q->{$val['var-value']}) {
                                    $output .= "<span class='icon8 icomoon-icon-cancel-circle-2'></span>";
                                } else {
                                    $output .= "<span class='icon8  icomoon-icon-checkmark-circle-2'></span>";
                                }
                            } else if ($col_name == 'id') {
                                $id = $q->{$val['var-value']};
                                $output .= $i;
                            } else {
                                $output .= $q->{$val['var-value']};
                            }
                            ?>
                            <td>
                                <?php echo $output; ?>
                            </td>
                        <?php } ?>
                        <?php if (!empty($this->access[$this->classname]['method'])) { ?>
                            <td>
                                <div class="controls center">
                                    <!-- Access Links -->
                                    <?php
                                    foreach ($this->access as $classname => $val) {
                                        if (!empty($val['method'])) {
                                            foreach ($val['method'] as $method_name => $v) {
                                                if ($this->Misc->accessible($this->access, $this->classname, 'method', $method_name)) { // check if permitted
                                                    //output would be classname in singular form
                                                    $format_function = '_' . strtolower(trim(str_replace(' ', '_', $this->contents['functionName'])));
                                                    //get function to differentiate if view, edit, delete, print etc.
                                                    $function = strtolower(str_replace($format_function, '', $method_name));
                                                    ?>
                                                    <?php if (($function == 'view') && ($classname == $this->classname)) { ?>
                                                        <?php if (in_array('view', $pop_up)) { ?>
                                                            <a data-toggle="modal" href="#dfltmodal" class="<?= $method_name; ?>" id="<?= $this->Misc->encode_id($id); ?>" title="View <?= $this->contents['functionName']; ?>">
                                                                <span class="icomoon-icon-search-3"></span>
                                                            </a>
                                                        <?php } else { ?>
                                                            <a href="<?php echo admin_url("$this->classname/$method_name/" . $this->Misc->encode_id($id)); ?>" title="View <?= $this->contents['functionName']; ?>" class="tip <?= $method_name; ?>">
                                                                <span class="icon16 icomoon-icon-search-3"></span>
                                                            </a>
                                                            <?php
                                                        }
                                                    }
                                                    ?>

                                                    <?php if (($function == 'edit') && ($classname == $this->classname)) { ?>
                                                        <?php if (in_array('edit', $pop_up)) { ?>
                                                            <a data-toggle="modal" href="#dfltmodal" data-backdrop="static" class="<?= $method_name; ?>" id="<?= $this->Misc->encode_id($id); ?>" title="Edit <?= $this->contents['functionName']; ?>">
                                                                <span class="icomoon-icon-pencil"></span>
                                                            </a>
                                                        <?php } else { ?>
                                                            <a href="<?php echo admin_url("$this->classname/$method_name/" . $this->Misc->encode_id($id)); ?>" title="Edit <?= $this->contents['functionName']; ?>" class="tip <?= $method_name; ?>" >
                                                                <span class="con16 icomoon-icon-pencil"></span>
                                                            </a>
                                                            <?php
                                                        }
                                                    }
                                                    ?>

                                                    <?php if (($function == 'add_supplier') && ($classname == $this->classname)) { ?>
                                                        <a data-toggle="modal" href="#dfltmodal" class="add_item_suppliers" id="<?= $this->Misc->encode_id($id); ?>" title="Add Supplier">
                                                            <span class="icomoon-icon-user-plus"></span>
                                                        </a>
                                                    <?php } ?>

                                                    <?php if (($function == 'add_receiving_report') && ($classname == $this->classname)) { ?>
                                                        <a href="<?php echo admin_url("$this->classname/add_receiving_report/" . $this->Misc->encode_id($id)); ?>" title="Add Receiving Report" class="tip <?= $method_name; ?>" >
                                                            <span class="con16 icomoon-icon-file-plus"></span>
                                                        </a>
                                                    <?php } ?>

                                                    <?php if (($function == 'add_date_forwarded') && ($classname == $this->classname)) { ?>
                                                        <a data-toggle="modal" href="#dfltmodal"  class="<?= $method_name; ?>" id="<?= $this->Misc->encode_id($id); ?>" title="Add Date Forwared to Accounting">
                                                            <span class="icomoon-icon-file-plus"></span>
                                                        </a>
                                                    <?php } ?>

                                                    <?php if (($function == 'delete') && ($classname == $this->classname)) { ?>
                                                        <a href="#" title="<?= $method_name; ?>" class="tip delete_<?= strtolower(trim(str_replace(' ', '_', $this->contents['functionName']))); ?>" value='<?php echo $this->Misc->encode_id($id); ?>' title="Delete <?= $this->contents['functionName']; ?>">
                                                            <span class="icon16 icomoon-icon-remove"></span>
                                                        </a>
                                                    <?php } ?>
                                                    <?php
                                                }
                                            }
                                        }
                                        if (!empty($val['page'])) {
                                            foreach ($val['page'] as $method_name => $v) {
                                                if ($this->Misc->accessible($this->access, $this->classname, 'page', $method_name)) {
                                                    //output would be classname in singular form
                                                    $format_function = '_' . strtolower(trim(str_replace(' ', '_', $this->contents['functionName'])));
                                                    //get function to differentiate if view, edit, delete, print etc.
                                                    $function = strtolower(str_replace($format_function, '', $method_name));
                                                    if (($function == 'print') && ($classname == $this->classname)) {
                                                        ?>
                                                        <a target="blank" href="<?php echo admin_url("$this->classname/$method_name/" . $this->Misc->encode_id($id)); ?>" title="Print <?= $this->contents['functionName']; ?>" class="tip <?= $method_name; ?>" >
                                                            <span class="con16 icomoon-icon-print"></span>
                                                        </a>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </td>
                        <?php } ?>
                    </tr>
                    <?php
                }
                if ($list->num_rows == 0) {
                    ?>
                    <tr>
                        <td valign="top" colspan="12" class="dataTables_empty">No matching records found</td>
                    </tr>
                <?php }
                ?>
            </tbody>
        </table>
    </div>
    <?php $this->load->view(admin_dir('template/pagination')); ?>
</div>
<table class="table hidden generate">
    <thead>
        <tr>
            <?php
            foreach ($list_content as $col_name => $var) {
                if ($var['type'] != 'hidden') {
                    ?>                    
                    <th class="<?= $var['class']; ?>"><?= $var['label']; ?></th>
                    <?php
                }
            }
            ?>
            <?php if (!empty($this->access[$this->classname]['method'])) { ?>
            <?php } ?>
        </tr>
        <tr>
    </thead>
    <tbody >
        <?php
        $i = 0;
        if (!empty($rowcount['start']))
            $i = $rowcount['start'] - 1;
        foreach ($list->result() as $q) {
            $i++;
            ?>
            <tr>
                <?php
                foreach ($list_content as $col_name => $val) {
                    $output = '';

                    if ($val['type'] == 'datepicker') {
                        if ($q->{$val['var-value']}) {
                            if (strtotime($q->{$val['var-value']}) != 0) {
                                $output .= date('M d, Y', strtotime($q->{$val['var-value']}));
                            } else {
                                $output = '';
                            }
                        } else {
                            $output = '';
                        }
                    } else if ($val['type'] == 'decimal') {
                        $output .= number_format($q->{$val['var-value']}, 2);
                    } else if ($val['var-value'] == 'enabled' || $val['var-value'] == 'with_supplier') {
                        if ($q->{$val['var-value']}) {
                            $output .= "<span class='icon8 icomoon-icon-cancel-circle-2'></span>";
                        } else {
                            $output .= "<span class='icon8  icomoon-icon-checkmark-circle-2'></span>";
                        }
                    } else if ($col_name == 'id') {
                        $id = $q->{$val['var-value']};
                        $output .= $i;
                    } else {
                        $output .= $q->{$val['var-value']};
                    }
                    ?>
                    <td>
                        <?php echo $output; ?>
                    </td>
                <?php } ?>
            </tr>
            <?php
        }
        if ($list->num_rows == 0) {
            ?>
            <tr>
                <td valign="top" colspan="12" class="dataTables_empty">No matching records found</td>
            </tr>
        <?php }
        ?>
    </tbody>
</table>
<script type="text/javascript">
    $(document).ready(function () {
        $(".datepicker").datepicker({changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", maxDate: '+0d'});
        $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
        function tooltip_placement(context, source) {
            var $source = $(source);
            var $parent = $source.closest('table')
            var off1 = $parent.offset();
            var w1 = $parent.width();

            var off2 = $source.offset();
            var w2 = $source.width();

            if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2))
                return 'right';
            return 'left';
        }

<?php if (!empty($search)) foreach ($search as $var => $val) { ?>
                $('#<?php echo $var; ?>').val('<?php echo $val; ?>');
    <?php } ?>
<?php if (!empty($sort)) { ?>
            $('.listsort').addClass('list<?php echo $sort['sort_type']; ?>');
            $('.listsort').attr('name', '<?php echo $sort['sort_by']; ?>');
<?php } ?>
<?php if (!empty($display)) { ?>
            $('.listdisplay').val('<?php echo $display; ?>');
<?php } ?>
    });
</script>
<script type="text/javascript" src="<?php echo assets_dir("js/main.js"); ?>"></script><!-- Core js functions -->