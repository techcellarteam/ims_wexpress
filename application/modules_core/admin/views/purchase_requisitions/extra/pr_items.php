<?php if ($items): ?>
    <option value="">Select product</option>
    <?php foreach ($items as $q): ?>
        <option value="<?= $q->item_id; ?>" >
            <?= $q->sku . ' | ' . $q->description; ?>
        </option>
    <?php endforeach; ?>
<?php else: ?>
    <option value="">--- Select Item</option>
<?php endif; ?>
