<tr id="id_table_<?= $item->item_id; ?>">
    <td class='col-lg-1'>
        <?= $item->stock_classification_name; ?>
    </td>
    <td class='col-lg-2'>
        <input type='hidden' class='form-control formdata pr_items' id='formdata-pr_item_id[<?= $item->item_id; ?>]' value='<?= $item->id_pr_item; ?>' />
        <input type='hidden' class='form-control formdata items' id='formdata-items[<?= $item->item_id; ?>]' value='<?= $item->item_id; ?>' />
        <?= $item->sku . " | " . $item->description; ?>
    </td>
    <td class='col-lg-1'>
        <?= ($item->measurement_code); ?>
    </td>
    <td class='col-lg-1'>
        <?= ($item->quantity); ?>
    </td>
    <td class='col-lg-1'>
        <?= ($item->last_ordered_price); ?>
    </td>
    <td class='col-lg-1'>
        <?= ($item->initiator_price); ?>
    </td> 
    <td class='col-lg-1 pu_price'>
        <input type='text' class='form-control formdata purchase_price' id='formdata-item_unit_cost[<?= $item->item_id; ?>]' value='' />
    </td>
    <td class='col-lg-1 po_qty'>
        <input type='number' min="1" max="<?= $item->quantity; ?>" class='form-control formdata quantity' id='formdata-quantity[<?= $item->item_id; ?>]' value='<?= $item->quantity; ?>' />
        <span class="alert" style="color:red"></span>
    </td>
    <td class='col-lg-1 po_amount'>

    </td>
    <td class='col-lg-1 pu_price'>
        <?= $item->remarks; ?>
    </td> 
    <td class='col-lg-1'>
        <button class='icon16 icomoon-icon-remove formdata-remove_item' id='<?= $item->item_id; ?>'></button>
    </td>
</tr>   