<!DOCTYPE html>
<html>
    <head>
        <title>PURCHASE REQUISITION</title>
        <link rel="shortcut icon" href="<?= assets_dir('images/favicon.ico'); ?>" />
        <style type="text/css">
            @media print {
                .footerc {
                    background: green!important;
                }
            }
            td{
                padding: 0px;
                margin: 0px;
            }
            table{

            }
        </style>

    </head>
    <body>           
        <table style="width:100%;font-family:Arial!important;font-size:12px;">
            <tr>
                <td colspan="3" style="vertical-align:top;text-align:center;font-weight:bold;">
                    <h4  style="margin:0;font-size:18px;text-align:center;">WIDE WIDE WORLD EXPRESS CORPORATION</h4>
                    (Corporate Service Group)<br/>
                    WExpress Bldg., Pascor Drive, Sto. Nino, Paranaque<br/>
                    Telephone No.: (02) 793.1777
                </td>
            </tr>
            <tr>
                <td colspan="3" style="vertical-align:top;text-align:center;font-weight:bold;padding-bottom:15px;">
                    <h2 style="margin-top:10px;margin-bottom:10px;text-align:center;">PURCHASE REQUISITION</h2>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="vertical-align:top;text-align:right;">
                    <span style="font-size:14px;">PR NO:</span> <span style="font-size:14px;font-weight:bold;"><?= $row->pr_number; ?></span><br/>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="vertical-align:top;text-align:right;">
                    <span style="font-size:14px;">Date Prepared:</span> <span style="font-size:14px;font-weight:bold;"><?= date('M d, Y', strtotime($row->added_date)); ?></span><br/>
                </td>
            </tr>            
            <tr>
                <td colspan="3" style="vertical-align:top;text-align:right;">
                    <span style="font-size:14px;">Approx Amount:</span> <span style="font-size:14px;font-weight:bold;"><?php number_format($approx_amount, 2); ?></span><br/>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="vertical-align:top;width:80%;">
                    <strong>Remarks:</strong> <?= $row->pr_remarks; ?>
                </td>
                <td style="vertical-align:top;">
                </td>
            </tr>
            <tr>
                <td colspan="3" style="vertical-align:top;">
                    <strong>Status: </strong> <?= $row->status; ?>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="vertical-align:top;text-align: left">
                    Please Procure the following articles for <strong><?= $row->business_unit_name; ?> <?= ($row->plate_number) ? " | " . $row->plate_number : ''; ?></strong>
                </td>
            </tr>
        </table>
        <!--head end-->
        <!--content-->
        <table style="width:100%;font-family:Arial!important;font-size:12px;">

            <tr>
                <td style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;width:120px">
                    QTY
                </td>
                <td style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;width:120px">
                    UNIT
                </td>
                <td colspan="4" style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;">
                    COMPLETE DESCRIPTION OF ARTICLES
                </td>
                <td style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;width:120px">
                    UNIT COST
                </td>
                <td style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;width:100px">
                    TOTAL COST
                </td>
            </tr>
            <?php foreach ($pr_items as $q) { ?>
                <tr>
                    <td style="vertical-align:top;padding:5px;text-align:center;">
                        <?= $q->quantity; ?>
                    </td>
                    <td style="vertical-align:top;padding:5px;text-align:center;">
                        <?= $q->measurement_name; ?>
                    </td>
                    <td  colspan="4"  style="vertical-align:top;padding:5px;text-align:center;">
                        <?= $q->description; ?>
                    </td>
                    <td style="vertical-align:top;padding:5px;text-align:center;">
                        <?= $q->initiator_price; ?>
                    </td>
                    <td style="vertical-align:top;padding:5px;text-align:center;">
                        <?= number_format($q->quantity * $q->initiator_price, 2); ?>
                    </td>
                </tr>
            <?php } ?>

            <tr>
                <td style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td colspan="4" style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td style="vertical-align:top;padding:5px;text-align:center;">
                    <strong>GRAND TOTAL
                </td>
                <td style="vertical-align:top;padding:5px;text-align:center;">
                    <strong><?php echo number_format($approx_amount, 2); ?></strong>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td colspan="4" style="vertical-align:top;padding:5px;text-align:center;">
                    **************** NOTHING FOLLOWS ****************
                </td>
                <td style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td style="vertical-align:top;padding:5px;text-align:right;">

                </td>
            </tr>
            <tr>
                <td colspan="3" style="vertical-align:top;padding:5px;text-align:center;">
                    REQUISITION BY:<br/><br/>
                    <?= $row->added_by; ?>
                </td>
                <td colspan="3" style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td colspan="2" style="vertical-align:top;padding:5px;text-align:right;">

                </td>
            </tr>
            <tr>
                <td colspan="3" style="vertical-align:top;padding:5px;text-align:center;">
                    Requisitioner:
                </td>
                <td colspan="3" style="vertical-align:top;padding:5px;text-align:center;">
                    Property Officer
                </td>
                <td colspan="2" style="vertical-align:top;padding:5px;text-align:right;">

                </td>
            </tr>
            <tr>
                <td colspan="3" style="vertical-align:top;padding:5px;text-align:center;">
                    PRICES CANVASSED:
                </td>
                <td colspan="3" style="vertical-align:top;padding:5px;text-align:center;">
                    P.O. No.<br/>
                    Date:
                </td>
                <td colspan="2" style="vertical-align:top;padding:5px;text-align:right;">
                    Approved:<br/>
                    <?= $row->first_level; ?>
                </td>
            </tr>
<!--            <tr>
                <td colspan="3" style="vertical-align:top;padding:5px;text-align:center;">
                    Procurement Officer
                </td>
                <td colspan="3" style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td colspan="2" style="vertical-align:top;padding:5px;text-align:right;">
                    President Executive Vice-President
                </td>
            </tr>-->
        </table>
        <!--footer-->
        <table style="width:100%;font-family:Arial!important;font-size:12px;">
            <tr>
                <td style="text-align:center;vertical-align:top;padding:5px 0 12px 0px;">
                    COPY FOR THE PROCUREMENT OFFICER
                </td>
            </tr>
            <tr>
                <td style="text-align:left;vertical-align:top;padding:5px 0 12px 0px;">
                    Approvals:<br/>
                    DEPARTMENT: <?= ($row->first_level) ? $row->first_level . '(' . date('Y-m-d', strtotime($row->first_approval_date)) . ')' : ''; ?><br/>
                    PURCHASING: <?= ($row->second_level) ? $row->second_level . '(' . date('Y-m-d', strtotime($row->second_approval_date)) . ')' : ''; ?><br/>
                    <?= ($row->top_level_approval_date) ? 'TOP: DJF ' . '(' . date('Y-m-d', strtotime($row->top_level_approval_date)) . ')' : ''; ?><br/>
                    <?= ($row->third_level) ? 'Executive Vice President: ' . $row->third_level . '(' . date('Y-m-d', strtotime($row->third_approval_date)) . ')' : ''; ?><br/>
                    <?= ($row->fourth_level) ? 'Chairman and President: ' . $row->fourth_level . '(' . date('Y-m-d', strtotime($row->fourth_approval_date)) . ')' : ''; ?><br/>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center;vertical-align:top;padding:5px 0 12px 0px;">
                    <em>*** This document is system generated ***</em>
                </td>
            </tr>
        </table>
        <!--footer-->
    </body>
</html>