<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>Edit New Purchase Requisition</h3>     
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->

        <div class="row">
            <div class="col-lg-12">	
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Requisition Information (<?= $row->pr_number; ?>)</span>
                                    <input type="hidden" class="form-control formdata" id="formdata-purchase_requisition_id" value="<?= $this->Misc->encode_id($this->id); ?>"/>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* PR Type</label>
                                            <div class="col-lg-5">
                                                <?= $row->document_type_name; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Initiator</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-requestor" value="<?= $row->user_name; ?>" readonly/>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Business Unit</label> <!--Business Unit Name-->
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-department" value="<?= $row->business_unit_name; ?>" readonly/>
                                                <input type="hidden" class='form-control formdata' id='formdata-business_unit_id' value="<?php echo $row->business_unit_id; ?>"/>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Date Needed</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata datepicker" id="formdata-date_needed" value="<?= date('M d, Y', strtotime($row->date_needed)); ?>"/>
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Category</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-category_id'>
                                                    <option value=''></option>
                                                    <?php foreach ($categories as $q) { ?>
                                                        <option value='<?= $q->id_document_type; ?>' <?= ($q->id_document_type == $row->category_id) ? 'selected' : ''; ?> >
                                                            <?= $q->document_type_name; ?>
                                                        </option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <?php if (!empty($plate_numbers)) { ?>
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label">* SRF #</label>
                                                <div class="col-lg-5">
                                                    <input type="text" class="form-control formdata" id="formdata-srf_number" value="<?= $row->srf_number; ?>"/>
                                                </div>
                                            </div><!-- End .form-group  -->
                                            <div class="form-group " >
                                                <label class="col-lg-4 control-label">* Plate Number</label>
                                                <div class="col-lg-5">
                                                    <select class='form-control formdata chosen-select' id='formdata-plate_number_id'>
                                                        <option value=''></option>
                                                        <?php foreach ($plate_numbers as $q) { ?>
                                                            <option value='<?= $q->id_plate_number; ?>' <?= ($q->id_plate_number == $row->plate_number_id) ? 'selected' : ''; ?>><?= $q->plate_number; ?></option>
                                                        <?php }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div><!-- End .form-group  -->                                        
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Check if for stocking:</label>
                                            <div class="col-lg-5">
                                                <label class="checkbox-inline">
                                                    <div class="checkbox">
                                                        <label>
                                                            <div class="checker">
                                                                <span class="checked">
                                                                    <input type="checkbox" name="formdata-for_stocking" class="form-control formdata_radio" id="formdata-for_stocking" value="1" <?= ((int) $row->for_stocking == 1) ? 'checked' : ''; ?>>
                                                                </span>
                                                            </div>
                                                            For Stocking
                                                        </label>
                                                    </div>
                                                </label>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks</label>
                                            <div class="col-lg-5">
                                                <textarea class="form-control formdata" id="formdata-pr_remarks" rows="6" cols="50" ><?= $row->pr_remarks; ?></textarea>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div>
                            <!--Adding Items-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Requisition Items</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert_item"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group " >
                                            <label class="col-lg-4 control-label">* Stock Classifications</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-stock_classification_id'>
                                                    <option value=''>Select Option</option>
                                                    <?php foreach ($stock_classifications as $q) { ?>
                                                        <option value='<?= $q->id_stock_classification; ?>'><?= $q->stock_classification_name; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Items</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-item' >
                                                    <option value="">Select product</option>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->                                   
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks</label> <!--Business Unit Name-->
                                            <div class="col-lg-4">
                                                <textarea class="form-control formdata" id="formdata-init_remarks" placeholder="Enter Remarks for this item." ></textarea>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Quantity</label> <!--Business Unit Name-->
                                            <div class="col-lg-3">
                                                <input type="number" min="1" class="form-control formdata" id="formdata-qty" value="1"/>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Initiators Price</label> <!--Business Unit Name-->
                                            <div class="col-lg-3">
                                                <input type="text" class="form-control formdata" id="formdata-initiators_input_price" placeholder="Enter Initiators Price" />
                                            </div>
                                            <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-add_item' type="button">Add Item</button>
                                        </div><!-- End .form-group  -->
                                    </div>

                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">Stock Classification</th>
                                                    <th class="col-lg-1">SKU</th>
                                                    <th class="col-lg-1">Title/Description</th>
                                                    <th class="col-lg-1">UOM</th>
                                                    <th class="col-lg-1">Qty</th>
                                                    <th class="col-lg-1">Last Ordered Price</th>
                                                    <th class="col-lg-1">Initiators Price</th>
                                                    <th class="col-lg-1">Total Amount</th>
                                                    <th class="col-lg-1">Remarks</th>
                                                    <th class="col-lg-1">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id='pr_items_body'>
                                                <?php
                                                $total_qty = 0;
                                                $total_amt = 0;
                                                foreach ($pr_items as $q) {
                                                    $total_qty += $q->quantity;
                                                    $total_amt += ($q->initiator_price * $q->quantity);
                                                    ?>
                                                    <tr id="id_table_<?= $q->id_pr_item; ?>">
                                                        <td class='col-lg-1'>
                                                            <input type = 'hidden' class = 'form-control formdata items' id = 'formdata-pr_items[<?= $q->id_pr_item; ?>]' value = '<?= $q->item_id; ?>' />
                                                            <?= $q->stock_classification_name; ?>
                                                        </td>
                                                        <td class='col-lg-1'>
                                                            <?= $q->sku; ?>
                                                        </td>
                                                        <td class='col-lg-1'>
                                                            <?= $q->description; ?>
                                                        </td> 
                                                        <td class='col-lg-1'>
                                                            <?= $q->measurement_code; ?>
                                                        </td>
                                                        <td class='col-lg-1'>
                                                            <input type='number' min="1" class='form-control formdata' id='formdata-pr_quantity[<?= $q->id_pr_item; ?>]' value='<?= $q->quantity; ?>' />
                                                        </td>
                                                        <td class='col-lg-1'>
                                                            <input type='hidden' class='form-control formdata last_ordered_price' id='formdata-last_ordered_price[<?= $q->id_pr_item; ?>]' value="<?= $q->last_ordered_price; ?>"/>
                                                            <?= $q->last_ordered_price; ?>
                                                        </td>
                                                        <td class='col-lg-1'>
                                                            <input type='text' class='form-control formdata initiators_price' id='formdata-initiators_price[<?= $q->id_pr_item; ?>]' value="<?= $q->initiator_price; ?>"/>
                                                        </td>
                                                        <td class='col-lg-1 total'>
                                                            <?= number_format(($q->initiator_price * $q->quantity), 2); ?>
                                                        </td>
                                                        <td class='col-lg-1'>
                                                            <textarea class='form-control formdata' id='formdata-remarks[<?= $q->id_pr_item; ?>]' ><?= $q->remarks; ?></textarea>
                                                        </td>
                                                        <td class='col-lg-1'>
                                                            <button class='icon16 icomoon-icon-remove formdata-remove_item' id='<?= $q->id_pr_item; ?>'></button>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                            <tr style='font-weight: bold'>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1 text-right">Total QTY</td>
                                                <td class="col-lg-1 text-right" id='formdata-total_quantity'><?= $total_qty; ?></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1 text-right">Total Amount</td>
                                                <td class="col-lg-1 text-right" id='formdata-total_amount'><?= number_format($total_amt, 2); ?></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-body ">
                                <div class="row form-horizontal">
                                    <div class="form-group">
                                        <div class="col-lg-offset-4 col-lg-8">
                                            <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-edit' type="button">Submit</button>
                                        </div>
                                    </div><!-- End .form-group  --> 
                                </div>
                            </div>

                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->  
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<?php
$this->load->view(admin_dir('template/footer'));
