<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>Add New Purchase Requisition</h3>     
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->

        <div class="row">
            <div class="col-lg-12">	
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Requisition Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Business Unit</label>
                                            <div class="col-lg-5">
                                                <select class="form-control formdata chosen-select" id="formdata-business_unit_id" >
                                                    <option value=''>Select Business Unit</option>
                                                    <?php foreach ($user_bu as $q) { ?>
                                                        <option value='<?php echo $q->business_unit_id; ?>' <?= (count($user_bu) == 1) ? 'selected' : ''; ?>><?php echo "$q->group_code | $q->business_unit_name"; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Date Needed</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata datepicker" id="formdata-date_needed" value="<?= date('M d, Y'); ?>"/>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Category</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-category_id'>
                                                    <option value=''></option>
                                                    <?php foreach ($categories as $q) { ?>
                                                        <option value='<?= $q->id_document_type; ?>'><?= $q->document_type_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="tfleet_code hidden">
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label">* SRF #</label>
                                                <div class="col-lg-5">
                                                    <input type="text" class="form-control formdata" id="formdata-srf_number" value=""/>
                                                </div>
                                            </div><!-- End .form-group  -->
                                            <div class="form-group " >
                                                <label class="col-lg-4 control-label">* Plate Number</label>
                                                <div class="col-lg-5">
                                                    <select class='form-control formdata chosen-select' id='formdata-plate_number_id'>
                                                        <option value=''></option>
                                                        <?php foreach ($plate_numbers as $q) { ?>
                                                            <option value='<?= $q->id_plate_number; ?>'><?= $q->plate_number; ?></option>
                                                        <?php }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div><!-- End .form-group  -->   
                                        </div><!-- End .tfleet_code  -->   
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Check if for stocking:</label>
                                            <div class="col-lg-5">
                                                <label class="checkbox-inline">
                                                    <div class="checkbox">
                                                        <label>
                                                            <div class="checker">
                                                                <span class="checked">
                                                                    <input type="checkbox" name="formdata-for_stocking" class="form-control formdata_radio" id="formdata-for_stocking" value="1">
                                                                </span>
                                                            </div>
                                                            For Stocking
                                                        </label>
                                                    </div>
                                                </label>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks</label>
                                            <div class="col-lg-5">
                                                <textarea class="form-control formdata" id="formdata-pr_remarks" rows="6" cols="50"></textarea>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div>
                            <!--Adding Items-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Requisition Items</span>
                                </h4>

                            </div>
                            <div class="panel-body ">
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group " >
                                            <label class="col-lg-4 control-label">* Stock Classifications</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-stock_classification_id'>
                                                    <option value=''></option>
                                                    <?php foreach ($stock_classifications as $q) { ?>
                                                        <option value='<?= $q->id_stock_classification; ?>'><?= $q->stock_classification_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Items</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-item' >
                                                    <option value="">Select product</option>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks</label> <!--Business Unit Name-->
                                            <div class="col-lg-4">
                                                <textarea class="form-control formdata" id="formdata-init_remarks" placeholder="Enter Remarks for this item." ></textarea>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Quantity</label> <!--Business Unit Name-->
                                            <div class="col-lg-3">
                                                <input type="number" min="1" class="form-control formdata" id="formdata-qty" value="1"/>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group center">
                                            <label class="col-lg-4 control-label">* Initiators Price</label> <!--Business Unit Name-->
                                            <div class="col-lg-3">
                                                <input type="text" class="form-control formdata" id="formdata-initiators_input_price" placeholder="Enter Initiators Price" />
                                            </div>
                                            <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-add_item' type="button">Add Item</button>
                                        </div><!-- End .form-group  -->
                                    </div>

                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">Stock Classification</th>
                                                    <th class="col-lg-1">SKU</th>
                                                    <th class="col-lg-1">Title/Description</th>
                                                    <th class="col-lg-1">UOM</th>
                                                    <th class="col-lg-1">Qty</th>
                                                    <th class="col-lg-1">Last Ordered Price</th>
                                                    <th class="col-lg-1">Initiators Price</th>
                                                    <th class="col-lg-1">Total Amount</th>
                                                    <th class="col-lg-1">Remarks</th>
                                                    <th class="col-lg-1">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id='pr_items_body'>

                                            </tbody>
                                            <tr style='font-weight: bold'>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1 text-right">Total QTY</td>
                                                <td class="col-lg-1 text-right" id='formdata-total_quantity'></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1 text-right">Total Amount</td>
                                                <td class="col-lg-1 text-right" id='formdata-total_amount'></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-body ">
                                <div class="row form-horizontal">
                                    <div class="form-group">
                                        <div class="col-lg-8 center">
                                            <?php if ($this->session->userdata['admin']['business_unit_head_id'] == $this->session->userdata['admin']['user_id']) { ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-auto_approve' type="button" value="">Submit (Auto Approve)</button>
                                            <?php } else { ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-save' type="button">Submit as Draft</button>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-add_submit' type="button" value="Submit Draft">Submit</button>
                                            <?php } ?>
                                        </div>
                                    </div><!-- End .form-group  --> 
                                </div>
                            </div>

                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->  
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<?php
$this->load->view(admin_dir('template/footer'));
