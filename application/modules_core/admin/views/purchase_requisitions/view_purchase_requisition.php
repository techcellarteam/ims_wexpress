<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>View Purchase Requisition [<?= $row->pr_number; ?>]</h3>
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Requisition Information</span>
                                    <input type="hidden" class="form-control formdata" id="formdata-purchase_requisition_id" value="<?= $this->Misc->encode_id($this->id); ?>"/>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Category:</label>
                                            <div class="col-lg-5">
                                                <?= $row->category_name; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> PR Number:</label>
                                            <div class="col-lg-5">
                                                <?= $row->pr_number; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> PR Type:</label>
                                            <div class="col-lg-5">
                                                <?= $row->document_type_name; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Initiator:</label>
                                            <div class="col-lg-5">
                                                <?= "$row->user_name"; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Business Unit:</label> <!--Business Unit Name-->
                                            <div class="col-lg-5">
                                                <?= $row->business_unit_name; ?>
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <?php if ($row->plate_number) { ?>
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label">* SRF #</label>
                                                <div class="col-lg-5">
                                                    <?= $row->srf_number; ?>
                                                </div>
                                            </div><!-- End .form-group  -->
                                            <div class="form-group " >
                                                <label class="col-lg-4 control-label">* Plate Number</label>
                                                <div class="col-lg-5">
                                                    <?= $row->plate_number; ?>
                                                </div>
                                            </div><!-- End .form-group  -->
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Date Created:</label>
                                            <div class="col-lg-5">
                                                <?= date('M d, Y', strtotime($row->added_date)); ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Date Needed:</label>
                                            <div class="col-lg-5">
                                                <?= date('M d, Y', strtotime($row->date_needed)); ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks:</label>
                                            <div class="col-lg-5">
                                                <?= $row->pr_remarks; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> For Stocking? </label>
                                            <div class="col-lg-5">
                                                <?= ($row->for_stocking) ? 'Yes' : 'NO'; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Status:</label>
                                            <div class="col-lg-5">
                                                <?= $row->status; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div>

                            <!--Transaction Signatories-->
                            <?php if ($row->first_level_id || $row->gtwy_level_id || $row->branch_level_id) { ?>
                                <div class="panel-heading">
                                    <h4>
                                        <span>Transaction Signatories</span>
                                        <span class="text-toggle-button right">
                                            <input type="checkbox" class="nostyle selected" id="transaction_signatories">
                                        </span>
                                    </h4>
                                </div>
                                <div class="panel-body " id="transaction_signatories">
                                    <div class="row form-horizontal">
                                        <div class="col-lg-10">
                                            <?php
                                            if ($row->first_level_id) {
                                                if ($row->first_approval_date != '0000-00-00 00:00:00') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> First Level Approved By:</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->first_level; ?>
                                                            <?= date('M d, Y', strtotime($row->first_approval_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                <?php } else {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Declined By (1st Level):</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->first_level; ?>
                                                            <?= date('M d, Y', strtotime($row->first_decline_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                    <?php
                                                }
                                            }
                                            if ($row->branch_level_id) {
                                                if ($row->branch_approval_date != '0000-00-00 00:00:00') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Branch Level Approved By:</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->branch_level; ?>
                                                            <?= date('M d, Y', strtotime($row->branch_approval_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                <?php } else {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Declined By (Branch Level):</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->branch_level; ?>
                                                            <?= date('M d, Y', strtotime($row->branch_decline_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                    <?php
                                                }
                                            }

                                            if ($row->gtwy_level_id) {
                                                if ($row->gtwy_approval_date != '0000-00-00 00:00:00') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Gateway Approver:</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->gateway_level; ?>
                                                            <?= date('M d, Y', strtotime($row->gtwy_approval_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                <?php } else {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Declined By (Gateway Level):</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->gateway_level; ?>
                                                            <?= date('M d, Y', strtotime($row->branch_decline_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                    <?php
                                                }
                                            }


                                            if ($row->svp_level_id) {
                                                if ($row->svp_approval_date != '0000-00-00 00:00:00') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> SVP Level Approved By:</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->svp_level; ?>
                                                            <?= date('M d, Y', strtotime($row->svp_approval_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                <?php } else {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Declined By (SVP Level):</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->svp_level; ?>
                                                            <?= date('M d, Y', strtotime($row->svp_decline_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                    <?php
                                                }
                                            }

                                            if ($row->second_level_id) {
                                                if ($row->second_approval_date != '0000-00-00 00:00:00') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Second Level Approved By:</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->second_level; ?>
                                                            <?= date('M d, Y', strtotime($row->second_approval_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                <?php } else {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Declined By (2nd Level):</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->second_level; ?>
                                                            <?= date('M d, Y', strtotime($row->second_decline_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                    <?php
                                                }
                                            }
                                            if ($row->third_level_id) {
                                                if ($row->third_approval_date != '0000-00-00 00:00:00') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Third Level Approved By:</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->third_level; ?>
                                                            <?= date('M d, Y', strtotime($row->third_approval_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                <?php } else {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Declined By (3rd Level):</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->third_level; ?>
                                                            <?= date('M d, Y', strtotime($row->third_decline_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                    <?php
                                                }
                                            }
                                            if ($row->fourth_level_id) {
                                                if ($row->fourth_approval_date != '0000-00-00 00:00:00') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Fourth Level Approved By:</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->fourth_level; ?>
                                                            <?= date('M d, Y', strtotime($row->fourth_approval_date)); ?>
                                                            <?= ($row->group_code == 'TFleet' && $row->total_amount <= 50000) ? '<b>(Auto-approved)</b>' : ''; ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                <?php } else {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Declined By (4th Level):</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->fourth_level; ?>
                                                            <?= date('M d, Y', strtotime($row->fourth_decline_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                    <?php
                                                }
                                            }
                                            if ($row->top_level_approval_date) {
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-lg-4 control-label"> Top Level Approval Date:</label>
                                                    <div class="row-lg-5">
                                                        <?= date('M d, Y', strtotime($row->top_level_approval_date)); ?>
                                                    </div>
                                                </div><!-- End .form-group  -->
                                            <?php } ?>
                                        </div> <!-- glfgfg1 -->
                                    </div> <!-- glfgfg2 -->
                                </div> <!-- glfgfg3 -->
                            <?php } ?>
                            <!--End of Transaction Signatories-->

                            <!--Purchase Requisition Items-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Requisition Items</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="pr_items_info">
                                    </span>
                                </h4>

                            </div>
                            <div class="panel-body " id="pr_items_info">
                                <div class="row form-horizontal">
                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">SKU</th>
                                                    <th class="col-lg-1">Description</th>
                                                    <th class="col-lg-1">UOM</th>
                                                    <th class="col-lg-1">Qty</th>
                                                    <th class="col-lg-1">Last Ordered Price</th>
                                                    <th class="col-lg-1">Initiators Price</th>
                                                    <th class="col-lg-1">Total</th>
                                                    <th class="col-lg-1">Purchasing Price</th>
                                                    <th class="col-lg-1">PO'ed</th>
                                                    <th class="col-lg-3">Remarks</th>
                                                </tr>
                                            </thead>
                                            <tbody id='pr_items_body'>
                                                <?php
                                                $total_qty = 0;
                                                $total_amt = 0;
                                                $poed = 0;
                                                if ($pr_items) {
                                                    foreach ($pr_items as $q) {
                                                        $total_qty += $q->quantity;
                                                        $total_amt += ($q->initiator_price * $q->quantity);
                                                        ?>
                                                        <tr id="id_table">
                                                            <td class='col-lg-1'>
                                                                <?= $q->sku; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->description; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->measurement_code; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->quantity; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= number_format($q->last_ordered_price, 2); ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= number_format($q->initiator_price, 2); ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= number_format($q->initiator_price * $q->quantity, 2); ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= number_format($q->purchasing_price, 2); ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?php
                                                                if ($q->poed) {
                                                                    $poed ++;
                                                                    ?>
                                                                    <i class="icon16 iconic-icon-checkmark"></i>
                                                                <?php } else { ?>
                                                                    <i class="icon16 iconic-icon-x"></i>
                                                                <?php } ?>
                                                            </td>
                                                            <td class='col-lg-3'>
                                                                <?= $q->remarks; ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                <tr>
                                                    <th colspan="3" class="col-lg-1 text-right">TOTAL</th>
                                                    <th class="col-lg-1 text-right"><?= $total_qty; ?></th>
                                                    <th class="col-lg-1"></th>
                                                    <th class="col-lg-1"></th>
                                                    <th class="col-lg-1 text-right">
                                                        <input type="hidden"  class='formdata' id="formdata-total_amount" value="<?= $total_amt; ?>"/>
                                                        <?= number_format($total_amt, 2); ?>
                                                    </th>
                                                    <th class="col-lg-1 "></th>
                                                    <th class="col-lg-1 text-center"><?= $poed . '/' . count($pr_items); ?></th>
                                                    <th class="col-lg-1 "></th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--End of Purchase Requisition Items-->

                            <!--Attaching Documents-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Attach Documents</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="pr_attachments_info">
                                    </span>
                                </h4>
                            </div>
                            <div class="panel-body " id="pr_attachments_info">
                                <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'add_attachment')) { // check if allowed to attach document        ?>
                                    <div class="row formupload_alert"></div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label"> Documents to Attach:</label>
                                        <form id="upload_form" enctype="multipart/form-data" method="post">
                                            <div class="col-lg-5">
                                                <input type="file"  class='formupload' name="formupload_file" id="formupload_file" />
                                                <input type="text" class="formupload" id="formupload_upload_title" value=""/>
                                                <button  class="btn btn-primary ui-wizard-content ui-formwizard-button" id='formupload_save' type="button">Upload File</button>
                                            </div>
                                        </form>
                                    </div><!-- End .form-group  -->
                                <?php } ?>
                                <div class="dataTables_wrapper form-inline responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="col-lg-1">Title</th>
                                                <th class="col-lg-1">Date Upload</th>
                                                <th class="col-lg-1">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($module_attachments as $q): ?>
                                                <tr>
                                                    <td class='col-lg-1'><?= $q->upload_title; ?></td>
                                                    <td class='col-lg-1'><?= date("M d,Y", strtotime($q->added_date)); ?></td>
                                                    <td class='col-lg-1'>
                                                        <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'view_attachment')) { // check if permitted        ?>
                                                            <a target="_blank" href="<?= base_url(); ?>upload/Purchase Requisitions/<?= $row->pr_number; ?>/<?= $q->upload_file_name; ?>" title="View">
                                                                <span class="icomoon-icon-search-3" ></span>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'delete_attachment')) { // check if permitted        ?>
                                                            <a href="#" title="Delete" class="tip delete_attachment" value='<?php echo $this->Misc->encode_id($q->id_module_attachment); ?>'>
                                                                <span class="icon16 icomoon-icon-remove"></span>
                                                            </a>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--End of Attachments-->

                            <!--Action Buttons-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Action Buttons</span>
                                </h4>
                            </div>
                            <div class="panel-body">
                                <div class="row form-horizontal">
                                    <?php if (($row->status == 'Submitted' && $row->group_code == 'TFleet') || ($row->status == 'Queued to Purchasing for Approval' || $row->status == 'ReQueued to Purchasing')) { ?>
                                        <div class="form-group col-lg-8">
                                            <label class="col-lg-3  col-lg-offset-4 control-label">* Top Level Approval Date</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata datepicker" id="formdata-top_level_approval_date" value="<?= date('M d, Y'); ?>"/>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    <?php } ?>
                                    <div class="form-group">
                                        <div class="col-lg-8 center">
                                            <?php if ($total_amt > 50000) { ?>
                                                <div class='col-lg-12'><div class='alert-info'>
                                                        <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                                        <strong>Info!</strong> This is more than PHP 50,000.00
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if ($row->status == 'Draft' && $this->Misc->accessible($this->access, $this->classname, 'method', 'submit_purchase_requisition')) { ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-submit' type="button" value="Submit Draft">Submit Draft</button>
                                            <?php } ?>
                                            <?php if ($row->status == 'Submitted' && $this->Misc->accessible($this->access, $this->classname, 'method', 'first_level_approval')) { ?>
                                                <?php if ($row->group_code != 'TFleet') { ?>
                                                    <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-first_level_approval' type="button" value="APPROVED (BU Head)"><?= "APPROVE (BU Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                    <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (BU Head)"><?= "DECLINE (BU Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                <?php } else { ?>
                                                    <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-first_level_approval' type="button" value="APPROVED (Tfleet Head)"><?= "APPROVE (Tfleet Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                    <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (Tfleet Head)"><?= "DECLINE (Tfleet Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php if ($row->status == 'ReQueued to Tfleet Head') { ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-first_level_approval' type="button" value="APPROVED (Tfleet Head)"><?= "APPROVE (Tfleet Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (Tfleet Head)"><?= "DECLINE (Tfleet Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                            <?php } ?>
                                            <?php
                                            if ($row->status == 'Queued to Purchasing for Approval' || $row->status == 'ReQueued to Purchasing') {
                                                if ($this->Misc->accessible($this->access, $this->classname, 'method', 'second_level_approval')) {
                                                    ?>
                                                    <div class="col-lg-8 center">
                                                        <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-second_level_approval' type="button" value="Queued to ELA"><?= "APPROVE (Purchasing Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                        <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (Purchasing Head)"><?= "DECLINE (Purchasing Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>

                                            <?php if (($row->status == 'Queued to Branch Head') && $this->Misc->accessible($this->access, $this->classname, 'method', 'branch_level_approval')) { ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-branch_level_approval' type="button" value="APPROVED (Branch Head)"><?= "APPROVE (Branch Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (Branch Head)"><?= "DECLINE (Branch Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                            <?php } ?>

                                            <?php if (($row->status == 'Queued to Gateway Approver') && $this->Misc->accessible($this->access, $this->classname, 'method', 'approve_gateway_pr')) { ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-gtwy_level_approval' type="button" value="APPROVED (Gateway Approver)"><?= "APPROVE [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (Gateway Approver)"><?= "DECLINE [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                            <?php } ?>

                                            <?php if (($row->status == 'Queued to SVP') && $this->Misc->accessible($this->access, $this->classname, 'method', 'svp_level_approval')) { ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-svp_level_approval' type="button" value="APPROVED (SVP)"><?= "APPROVE (SVP) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (SVP)"><?= "DECLINE (SVP) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                            <?php } ?>

                                            <?php if (($row->status == 'Queued to ELA' || $row->status == 'On-Hold (ELA)') && $this->Misc->accessible($this->access, $this->classname, 'method', 'third_level_approval')) { ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-third_level_approval' type="button" value="Queued to DJF"><?= "APPROVE (Exec VP) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (Exec VP)"><?= "DECLINE (Exec VP) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>

                                                <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'requeue_pr')) { ?>
                                                    <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-reQueue' type="button" value="ReQueued to Purchasing"><?= "ReQueue to Purchasing (Exec VP) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                <?php } ?>
                                            <?php } ?>

                                            <?php if ($row->status == 'Queued to ELA' && $this->Misc->accessible($this->access, $this->classname, 'method', 'on_hold_pr')) { ?>
                                                <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-on_hold' type="button" value="On-Hold (ELA)"><?= "On-Hold (Exec VP) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                            <?php } ?>

                                            <?php if (($row->status == 'Queued to DJF' || $row->status == 'On-Hold (DJF)') && $this->Misc->accessible($this->access, $this->classname, 'method', 'fourth_level_approval')) { ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-fourth_level_approval' type="button" value="PR Approved (DJF)"><?= "APPROVE (Chairman & Pres.) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (Chairman & Pres.)"><?= "DECLINE (Chairman & Pres.) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>

                                                <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'requeue_pr')) { ?>
                                                    <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-reQueue' type="button" value="ReQueued to Purchasing"><?= "ReQueue to Purchasing (Chairman & Pres.) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                <?php } ?>
                                            <?php } ?>

                                            <?php if ($row->status == 'Queued to DJF' && $this->Misc->accessible($this->access, $this->classname, 'method', 'on_hold_pr')) { ?>
                                                <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-on_hold' type="button" value="On-Hold (DJF)"><?= "On-Hold (Chairman & Pres.) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                            <?php } ?>

                                            <?php if (($row->status == 'APPROVED (Tfleet Head)' || $row->status == 'On-Hold (DJF)') && $this->Misc->accessible($this->access, $this->classname, 'method', 'fourth_level_approval')) { ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-fourth_level_approval' type="button" value="PR Approved (DJF)"><?= "APPROVE (Chairman & Pres.) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (Chairman & Pres.)"><?= "DECLINE (Chairman & Pres.) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>

                                                <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'requeue_pr')) { ?>
                                                    <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-reQueue' type="button" value="ReQueued to Tfleet Head"><?= "ReQueue to Tfleet Head (Chairman & Pres.) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                <?php } ?>
                                            <?php } ?>

                                            <?php if ($row->status == 'APPROVED (Tfleet Head)' && $this->Misc->accessible($this->access, $this->classname, 'method', 'on_hold_pr')) { ?>
                                                <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-on_hold' type="button" value="On-Hold (DJF)"><?= "On-Hold (Chairman & Pres.) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                            <?php } ?>

                                            <?php if ($row->status != 'CLOSED' && $row->poed == 0 && $this->Misc->accessible($this->access, $this->classname, 'method', 'close_purchase_requisition')) { ?>
                                                <button  class="btn btn-primary ui-wizard-content ui-formwizard-button formdata" id='formdata-close' type="button" value="CLOSED">CLOSE PR</button>
                                            <?php } ?>

                                            <div class="col-lg-8 center">
                                                <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'print_purchase_requisition')) { ?>
                                                    <a target="blank" href="<?php echo admin_url("$this->classname/print_purchase_requisition/" . $this->Misc->encode_id($row->id_purchase_requisition)); ?>" title="Print PR Form" >
                                                        <span class="icon32 icomoon-icon-print"> PRINT</span>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div><!-- End .form-group  -->
                                </div>
                            </div>
                            <!--Action Buttons-->

                            <!--History-->
                            <div class="panel-heading">
                                <h4>
                                    <span>History</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="history">
                                    </span>
                                </h4>
                            </div>
                            <div class="panel-body" id="history">
                                <div class="row form-horizontal">
                                    <?php if ($this->session->userdata['admin']['user_type_code'] == 'Chairman' || $this->session->userdata['admin']['user_type_code'] == 'EVP') { ?>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">* Who to send:</label>
                                            <div class="col-lg-5">
                                                <select class="form-control formdata chosen-select" id="formdata-comment_user_id" >
                                                    <option value=''>ALL</option>
                                                    <?php foreach ($pr_users as $q) { ?>
                                                        <option value='<?php echo $q->id_user; ?>' ><?php echo "$q->user_fname $q->user_lname | $q->user_type_code"; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    <?php } ?>
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Comment:</label>                                       
                                        <div class="col-lg-5">
                                            <textarea class="form-control formdata" id="formdata-comment" rows="3" cols="5"></textarea>
                                            <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'add_comment')) { // check if permitted             ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-add_comment' type="button">Save</button>
                                            <?php } ?>
                                        </div>
                                    </div><!-- End .form-group  -->

                                    <?php foreach ($logs->result() as $q) { ?>
                                        <div class="form-group">
                                            <label class="control-label col-lg-1"><?= date('M d, Y H:i:s', strtotime($q->user_log_date)); ?></label>
                                            <div class="col-lg-5>" >
                                                <?php
                                                switch (strtolower($q->user_log_detail)) {
                                                    case 'requested':
                                                        echo '<span class="icon16 icomoon-icon-new"></span>';
                                                        break;
                                                    case 'APPROVED (BU Head)':
                                                    case 'Queued to ELA':
                                                    case 'Queued to DJF':
                                                        echo '<span class="icon16 icomoon-icon-thumbs-up-3"></span>';
                                                        break;
                                                    case 'DECLINED (BU Head)':
                                                    case 'DECLINED (Purchasing Head)':
                                                    case 'DECLINED (Exec VP)':
                                                        echo '<span class="icon16 icomoon-icon-thumbs-up-4"></span>';
                                                        break;
                                                    case 'deleted attachment':
                                                    case 'uploaded attachment':
                                                        echo '<span class="icon16 icomoon-icon-file-6"></span>';
                                                        break;
                                                    default:
                                                        echo '<span class="icon16 icomoon-icon-bubble-2"></span>';
                                                        break;
                                                }
                                                ?>
                                                <span><strong><?= $q->user_log_detail; ?></strong></span>
                                                <i class=" icomoon-icon-arrow-right-5"> <?= $q->user_fname . ' ' . $q->user_lname . ' (' . $q->user_type_name . ')'; ?></i>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    <?php } ?>
                                </div>
                            </div>
                            <!--End of History-->

                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->
            </div><!-- End .span12 -->
        </div><!-- End .row -->

        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->


<?php
$this->load->view(admin_dir('template/footer'));
