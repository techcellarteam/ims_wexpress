<?php $this->load->view(admin_dir('template/header')); ?>
<div id="content" class="clearfix">
	<div class="contentwrapper"><!--Content wrapper-->

		<div class="heading">
			<h3>Menu</h3>                    
		</div><!-- End .heading-->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>
							<span>User Type List</span>
							<form class="panel-form right" action="">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
									<span class="icon16 icomoon-icon-cog-2"></span>
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu" style='width:250px'>
									<!-- Access Links -->
									<?php
									if($this->Misc->accessible($this->access,'menu','method','copy_link')){ ?>
										<li><a data-toggle="modal" href="#dfltmodal" class="copylink"><span class="icomoon-icon-copy"></span> Copy Link</a></li>
									<?php
									} ?>
								</ul>
							</form>
						</h4>
					</div><!-- End .panel-heading -->
					<div id='containerList'></div>	

				</div><!-- End .panel -->
			</div><!-- End .span12 -->  
		</div><!-- End .row -->  
	 <!-- Page end here -->       
	</div><!-- End contentwrapper -->
</div><!-- End #content -->
<script type="text/javascript">
	$(document).ready(function(){	
		load_datalist({action: "<?php echo admin_url('menu/method/list_usertype'); ?>"});
		
		//Link
		$('.copylink').on('click',{
			'action':"<?php echo admin_url("menu/popupform_link"); ?>",
			'type' : 3,
			'redirect' :"<?php echo current_url(); ?>"
		},load_dfltpopform);
	});
</script>
<?php $this->load->view(admin_dir('template/footer')); ?>