<?php $this->load->view(admin_dir('template/header')); ?>
<div id="content" class="clearfix">
	<div class="contentwrapper"><!--Content wrapper-->

		<div class="heading">
			<h3>Classes and Functions</h3>                    
		</div><!-- End .heading-->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>
							<span>Classes and Functions</span>
							<form class="panel-form right" action="">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
									<span class="icon16 icomoon-icon-cog-2"></span>
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu" style='width:250px'>
									<!-- Access Links -->
									<?php
									if($this->Misc->accessible($this->access,'myclass','method','add_class')){ ?>
										<li><a data-toggle="modal" href="#dfltmodal" class="addclass"><span class="icomoon-icon-plus"></span> Add New Class</a></li>
									<?php
									}
									if($this->Misc->accessible($this->access,'myclass','method','add_function')){ ?>	
										<li><a data-toggle="modal" href="#dfltmodal" class="addfunction"><span class="icomoon-icon-plus"></span> Add New Function</a></li>
									<?php
									} ?>
								</ul>
							</form>
							
						</h4>
					</div><!-- End .panel-heading -->
					<div class="panel-body noPad ">
						<table class="table">
							<thead>
								<tr>
									<th class="col-lg-1">#</th>
									<th class="col-lg-1">Class</th>
									<th class="col-lg-4">Function</th>
									<th class="col-lg-3">Name</th>
									<th class="col-lg-3">Actions</th>
								</tr>
							</thead>
							<tbody >
								<?php
								$i=0;
								foreach($classes as $q){ $i++; ?>
									<tr class='warning'>
										<td><?php echo $i; ?></td>
										<td colspan=3 style='text-align: left'><?php echo $q->class_title; ?> <small>(<?php echo $q->class_name; ?>)</small></td>
										<td>
											<div class="controls center">
												<!-- Access Links -->
												<?php
												if($this->Misc->accessible($this->access,'myclass','method','edit_class')){ ?>
												<a data-toggle="modal" href="#dfltmodal" title="Edit Class" class="tip editclass" value='<?php echo $this->Misc->encode_id($q->id_class); ?>'><span class="icon16 icomoon-icon-pencil"></span></a>
												<?php
												}
												if($this->Misc->accessible($this->access,'myclass','method','delete_class')){ ?>
												<a href="#" title="Delete Class" class="tip deleteclass" value='<?php echo $this->Misc->encode_id($q->id_class); ?>'><span class="icon16 icomoon-icon-remove"></span></a>
												<?php
												} ?>
											</div>
										</td>
									</tr>
									<?php
									if(!empty($class_functions[$q->id_class][1])){ ?>
										<tr class='success'>
											<td>&nbsp;</td>
											<td style='text-align: left'><span class='icon16 icomoon-icon-file-5'></span></td>
											<td style='text-align: left' colspan='3'>Page</td>
										</tr>	
										<?php
										$pagedata=$class_functions[$q->id_class][1];
										foreach($pagedata as $var=>$val){ ?>
											<tr>
												<td colspan='2'>&nbsp;</td>
												<td style='text-align: left'><span class='icon16 typ-icon-arrow-right'></span> <?php echo $val->class_function_title; ?></td>
												<td><?php echo $val->class_function_name; ?></td>
												<td>
													<!-- Access Links -->
													<?php
													if($this->Misc->accessible($this->access,'myclass','method','edit_function')){ ?>
													<a data-toggle="modal" href="#dfltmodal" title="Edit Function" class="tip editfunction" value='<?php echo $this->Misc->encode_id($val->id_class_function); ?>'><span class="icon16 icomoon-icon-pencil"></span></a>
													<?php
													}
													if($this->Misc->accessible($this->access,'myclass','method','delete_function')){ ?>
													<a href="#" title="Delete Function" class="tip deletefunction" value='<?php echo $this->Misc->encode_id($val->id_class_function); ?>'><span class="icon16 icomoon-icon-remove"></span></a>
													<?php
													} ?>
												</td>
											</tr>
										<?php	
										}
									}
									
									if(!empty($class_functions[$q->id_class][2])){ ?>
										<tr class='success'>
											<td>&nbsp;</td>
											<td style='text-align: left'><span class='icon16 minia-icon-bolt'></span></td>
											<td style='text-align: left' colspan='3'>Method</td>
										</tr>	
										<?php
										$methoddata=$class_functions[$q->id_class][2];
										foreach($methoddata as $var=>$val){ ?>
											<tr>
												<td colspan='2'>&nbsp;</td>
												<td style='text-align: left'><span class='icon16 typ-icon-arrow-right'></span> <?php echo $val->class_function_title; ?></td>
												<td><?php echo $val->class_function_name; ?></td>
												<td>
													<!-- Access Links -->
													<?php
													if($this->Misc->accessible($this->access,'myclass','method','edit_function')){ ?>
													<a data-toggle="modal" href="#dfltmodal" title="Edit Function" class="tip editfunction" value='<?php echo $this->Misc->encode_id($val->id_class_function); ?>'><span class="icon16 icomoon-icon-pencil"></span></a>
													<?php
													}
													if($this->Misc->accessible($this->access,'myclass','method','delete_function')){ ?>
													<a href="#" title="Delete Function" class="tip deletefunction" value='<?php echo $this->Misc->encode_id($val->id_class_function); ?>'><span class="icon16 icomoon-icon-remove"></span></a>
													<?php
													} ?>
												</td>
											</tr>
										<?php	
										}
									} ?>
								<?php
								} ?>
								
							</tbody>
						</table>	
					</div><!-- End .panel-body -->

				</div><!-- End .panel -->
			</div><!-- End .span12 -->  
		</div><!-- End .row -->  
	 <!-- Page end here -->       
	</div><!-- End contentwrapper -->
</div><!-- End #content -->
<script type="text/javascript">
	$(document).ready(function(){	
		//Class
		$('.addclass').on('click',{
			'action':"<?php echo admin_url("myclass/popupform_class"); ?>",
			'type' : 1,
			'redirect' :"<?php echo current_url(); ?>"
		},load_dfltpopform);
		
		$('.editclass').on('click',{
			'action':"<?php echo admin_url("myclass/popupform_class"); ?>",
			'type' : 2,
			'redirect' :"<?php echo current_url(); ?>"
		},load_dfltpopform);
		
		$('.deleteclass').on('click',{
			'action':"<?php echo admin_url("myclass/method/delete_class"); ?>",
			'conMessage':"You are about to delete this class.",
			'redirect' :"<?php echo current_url(); ?>"
		},dfltaction_item);
		
		//Function
		$('.addfunction').on('click',{
			'action':"<?php echo admin_url("myclass/popupform_function"); ?>",
			'type' : 1,
			'redirect' :"<?php echo current_url(); ?>"
		},load_dfltpopform);
		
		$('.editfunction').on('click',{
			'action':"<?php echo admin_url("myclass/popupform_function"); ?>",
			'type' : 2,
			'redirect' :"<?php echo current_url(); ?>"
		},load_dfltpopform);
		
		$('.deletefunction').on('click',{
			'action':"<?php echo admin_url("myclass/method/delete_function"); ?>",
			'conMessage':"You are about to delete this function.",
			'redirect' :"<?php echo current_url(); ?>"
		},dfltaction_item);
	});
</script>
<?php $this->load->view(admin_dir('template/footer')); ?>