<?php $this->load->view(admin_dir('template/header')); ?>
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->

        <div class="heading">
            <h3>Trace Inventory Changes</h3>                    
        </div><!-- End .heading-->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <span>Items Inventory Changes</span>
                            <form class="panel-form right" action="">
                                <!-- Access Links -->
                            </form>
                        </h4>
                    </div><!-- End .panel-heading -->
                    <div id='containerList'></div>	

                </div><!-- End .panel -->
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->       
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<?php $this->load->view(admin_dir('template/footer')); ?>
<script type="text/javascript">
    $(document).ready(function () {
        load_datalist({action: adminURL + 'inventory_changes/method/list_inventory_change'});
    });
</script>