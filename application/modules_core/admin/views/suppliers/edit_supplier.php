<?php $this->load->view(admin_dir('template/header')); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>Edit Item Brand</h3>     
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->

        <div class="row">
            <div class="col-lg-12">	
                <div class='clearfix'>
                    <div class="right">
                        <a href="<?php echo admin_url('suppliers/list_supplier'); ?>"><span class='icon16 icomoon-icon-arrow-left-5'></span> Back</a>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span><?= $row->supplier_name; ?>'s Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Group</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-general_group_id' >
                                                    <option value=''></option>
                                                    <?php foreach ($groups as $q) { ?>
                                                        <option value='<?= $q->id_general_group; ?>' <?= ($q->id_general_group == $row->general_group_id) ? 'Selected' : ''; ?> ><?= $q->group_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Supplier Type</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-supplier_type" value="<?= $row->supplier_type; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Stock Classification </label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-stock_classification_id' >
                                                    <option value=''></option>
                                                    <?php foreach ($stock_classifications as $q) { ?>
                                                        <option value='<?= $q->id_stock_classification; ?>' <?= ($row->stock_classification_id == $q->id_stock_classification) ? 'Selected' : ''; ?> ><?= $q->stock_classification_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Supplier's Name</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-supplier_name" value="<?= $row->supplier_name; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Supplier's Code</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-supplier_code" value="<?= $row->supplier_code; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Email Address</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-email" value="<?= $row->email; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* TIN No.</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-tin_no" value="<?= $row->tin_no; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> VAT Status:</label>
                                            <div class="col-lg-5">
                                                <input name="formdata-vat" class="form-control formdata_radio" id="formdata-vat" type="radio" value="1" <?= ((int) $row->vat == 1) ? 'checked' : ''; ?> /> <!-- value is 1 if within budget-->
                                                <i class="icon16 iconic-icon-checkmark">VAT</i>
                                                <input name="formdata-vat" class="form-control formdata_radio" id="formdata-vat" type="radio" value="0" <?= ((int) $row->vat == 0) ? 'checked' : ''; ?>/> <!-- value is 2 if without budget-->
                                                <i class="icon16 iconic-icon-x">Non-VAT</i>
                                            </div>
                                        </div><!-- End .form-group    -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Terms of Payment</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-terms_payment" value="<?= $row->terms_payment; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Phone No.</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-phone_no" value="<?= $row->phone_no; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Fax No.</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-fax_no" value="<?= $row->fax_no; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Nominated:</label>
                                            <div class="col-lg-5">
                                                <input name="formdata-nominated" class="form-control formdata_radio" id="formdata-nominated" type="radio" value="1" <?= ((int) $row->nominated == 1) ? 'checked' : ''; ?> /> <!-- value is 1 if within budget-->
                                                <i class="icon16 iconic-icon-checkmark">YES</i>
                                                <input name="formdata-nominated" class="form-control formdata_radio" id="formdata-nominated" type="radio" value="0" <?= ((int) $row->nominated == 0) ? 'checked' : ''; ?>/> <!-- value is 2 if without budget-->
                                                <i class="icon16 iconic-icon-x">NO</i>
                                            </div>
                                        </div><!-- End .form-group    -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Accreditation Date</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata datepicker" id="formdata-accreditation_date" value="<?= date('Y-m-d', strtotime($row->accreditation_date)); ?>"/>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks </label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-remarks" value="<?= $row->fax_no; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Note </label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-note" value="<?= $row->note; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div><!-- End .body-panel  --> 

                            <div class="panel-heading">
                                <h4>
                                    <span>Contact Person Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Name</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-c_name" value="<?= $row->c_name; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Email Address</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-c_email" value="<?= $row->c_email; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Phone No.</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-c_phone_no" value="<?= $row->c_phone_no; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Address</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-c_address" value="<?= $row->c_address; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Business Unit</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-c_department" value="<?= $row->c_department; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Job Title</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-c_job_title" value="<?= $row->c_job_title; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->

                                    </div>
                                </div>
                            </div><!-- End .body-panel  --> 
                            <div class="panel-heading">
                                <h4>
                                    <span>Billing Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Address</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-b_address" value="<?= $row->b_address; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">City</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-b_city" value="<?= $row->b_city; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Province/State.</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-b_state" value="<?= $row->b_state; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Postal/Zip Code</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-b_zip_code" value="<?= $row->b_zip_code; ?>" />
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Country</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata' id='formdata-b_country_code' >
                                                    <option value=''></option>
                                                    <?php foreach ($countries as $q) { ?>
                                                        <option value="<?= $q->ccode; ?>" <?= ($row->b_country_code == $q->ccode) ? 'Selected' : ''; ?> ><?= $q->country; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->

                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-lg-offset-4 col-lg-8">
                                        <button  class="btn btn-warning ui-wizard-content ui-formwizard-button" id='formdata-save' type="button">Edit</button>
                                    </div>
                                </div><!-- End .form-group  --> 
                            </div><!-- End .body-heading  -->
                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->  
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#formdata-save').on('click', {
            'action': "<?php echo admin_url("suppliers/method/edit_supplier"); ?>",
            'id': "<?php echo $this->Misc->encode_id($row->id_supplier); ?>",
            'conMessage': "You are about to edit supplier.",
            'redirect': "<?php echo admin_url("suppliers/view_supplier/" . $this->Misc->encode_id($row->id_supplier)); ?>",
        }, save_form_v1);
    });
</script>	
<?php $this->load->view(admin_dir('template/footer')); ?>