<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="icon16 minia-icon-close"></span></button>
        <h3>Group Supplier</h3>
    </div>
    <div class="modal-body">
        <div class="row popformdata_alert"></div>
        <form class="form-horizontal" action="#" role="form">
            <div class="form-group">
                <label class="col-lg-3 control-label">* Supplier</label>
                <div class="col-lg-9">
                    <select class='form-control popformdata chosen-select' id='popformdata_supplierId' >
                        <option value=''></option>
                        <?php foreach ($suppliers as $q) { ?>
                            <option value='<?php echo $q->id_supplier; ?>'><?php echo "$q->supplier_code | $q->supplier_name"; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div><!-- End .form-group  -->
            <div class="form-group">
                <label class="col-lg-3 control-label">* Group</label>
                <div class="col-lg-9">
                    <select class='form-control popformdata chosen-select' multiple id='popformdata_generalGroupId' >
                    </select>
                </div>
            </div><!-- End .form-group  -->
        </form>
    </div>
    <div class="modal-footer">
        <div class='right'>
            <button class="btn btn-success ui-wizard-content ui-formwizard-button" id="popformdata_save" type="button">Submit</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".chosen-select").chosen({no_results_text: "Oops, nothing found!", search_contains: true});

        $('#popformdata_save').on('click', {
            'action': "<?php echo admin_url("suppliers/method/supplier_group"); ?>",
            'conMessage': "You are about to add group to supplier.",
            'clear': true,
        }, save_form);
    });

    jQuery(function () {
        jQuery(document).on('change', '#popformdata_supplierId', getSupplierGroups);//check received items
    });

    function getSupplierGroups() {
        $.ajax({
            type: 'POST',
            url: adminURL + 'suppliers/show_supplier_group',
            data: {'supplier_id': $(this).val()},
            success: function (data)
            {
                $('select#popformdata_generalGroupId').html(data).trigger('chosen:updated');
                $('div#custom_field').html('');
            },
            error: function (xhr, ajaxOptions, thrownError)
            {
                alert(xhr.status);
                alert(thrownError);
            },
            beforeSend: function ()
            {
                $('div#custom_field').html('<img src="' + baseURL + 'images/loaders/circular/059.gif">');
            }
        });
    }
</script>
<script type="text/javascript" src="<?php echo assets_dir("js/main.js"); ?>"></script><!-- Core js functions -->