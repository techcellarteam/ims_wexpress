<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>View Supplier</h3>     
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->

        <div class="row">
            <div class="col-lg-12">	
                <div class='clearfix'>
                    <div class="right">
                        <a href="<?php echo admin_url('suppliers/list_supplier'); ?>"><span class='icon16 icomoon-icon-arrow-left-5'></span> Back</a>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span><?= $row->supplier_name; ?>'s Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Group:</label>
                                            <div class="col-lg-5">
                                                <b><?= $row->group_code . " | " . $row->group_name; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Supplier Type:</label>
                                            <div class="col-lg-5">
                                                <b><?= $row->supplier_type; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Stock Classification:</label>
                                            <div class="col-lg-5">
                                                <b><?= $row->stock_classification_name; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Supplier's Name:</label>
                                            <div class="col-lg-5">
                                                <b><?= $row->supplier_name; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Supplier's Code:</label>
                                            <div class="col-lg-5">
                                                <b><?= $row->supplier_code; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Email Address:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->email; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">TIN No.:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->tin_no; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Terms of Payment:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->terms_payment; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> VAT Status:</label>
                                            <div class="col-lg-5">
                                                <b><?= ((int) $row->vat > 0) ? 'VAT' : 'Non-VAT'; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Phone No.:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->phone_no; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Fax No.:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->fax_no; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Nominated:</label>
                                            <div class="col-lg-5">
                                                <b><?= ((int) $row->nominated > 0) ? 'YES' : 'NO'; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Accreditation Date:</label>
                                            <div class="col-lg-5">
                                                <b><?= date('M d, Y', strtotime($row->accreditation_date)); ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks :</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->fax_no; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Note :</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->note; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div><!-- End .body-panel  --> 

                            <div class="panel-heading">
                                <h4>
                                    <span>Contact Person Information</span>
                                </h4>
                                <a href="#" class="minimize" style="display: block;">Minimize</a>
                            </div>
                            <div class="panel-body" id="contact_person">
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Name:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->c_name; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Email Address:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->c_email; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Phone No.:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->c_phone_no; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Address:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->c_address; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Business Unit:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->c_department; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Job Title:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->c_job_title; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->

                                    </div>
                                </div>
                            </div><!-- End .body-panel  --> 
                            <div class="panel-heading">
                                <h4>
                                    <span>Billing Information</span>
                                </h4>
                                <a href="#" class="minimize" style="display: block;">Minimize</a>
                            </div>
                            <div class="panel-body " id="billing_info">
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Address:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->b_address; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">City:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->b_city; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Province/State.:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->b_state; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Postal/Zip Code:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->b_zip_code; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Country:</label>
                                            <div class="col-lg-5">
                                                <b> <?= $row->country; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->

                                    </div>
                                </div>
                            </div><!-- End .panel -->

                            <div class="panel-heading">
                                <h4>
                                    <span>Item Suppliers' Information</span>
                                </h4>
                                <a href="#" class="minimize" style="display: block;">Minimize</a>
                            </div>
                            <div class="panel-body " id="item_supplier_info">
                                <div class="row form-horizontal">
                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">Item</th>
                                                    <th class="col-lg-1">Price</th>
                                                    <th class="col-lg-1">View</th>
                                                </tr>
                                            </thead>
                                            <tbody id='supplier_body'>
                                                <?php foreach ($item_suppliers as $q) { ?>
                                                    <tr>
                                                        <td class="col-lg-1">
                                                            <b><?= $q->description . " - " . $q->sku; ?></b>
                                                        </td>
                                                        <td class="col-lg-1">
                                                            <b><?= $q->item_unit_cost; ?></b>
                                                        </td>
                                                        <td class="col-lg-1">
                                                            <a href="<?php echo admin_url("items/view_item/" . $this->Misc->encode_id($q->item_id)); ?>" title="<?= $this->methodname; ?>" class="tip view_item">
                                                                <span class="icon16 icomoon-icon-search-3"></span>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div><!-- End .panel-body -->

                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->  
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->	
<?php $this->load->view(admin_dir('template/footer')); ?>