<?php $this->load->view(admin_dir('template/header')); ?>
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->

        <div class="heading">
            <h3>Suppliers</h3>                    
        </div><!-- End .heading-->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <span>Suppliers List</span>
                            <form class="panel-form right" action="">
                                <!--Access Links--> 
                                <?php if ($this->Misc->accessible($this->access, 'suppliers', 'method', 'add_supplier')) { ?>
                                    <a href="<?php echo admin_url("suppliers/add_supplier"); ?>">
                                        <span class="icomoon-icon-plus"></span> Add New Entry
                                    </a>
                                <?php } ?>
                                <?php if ($this->Misc->accessible($this->access, 'suppliers', 'method', 'add_supplier_group')) { ?>
                                    <a data-toggle="modal" href="#dfltmodal" href="#" class="add_supplier_group">
                                        <span class="icomoon-icon-plus"></span> Supplier Grouping
                                    </a>
                                <?php } ?>
                                <?php if ($this->Misc->accessible($this->access, 'suppliers', 'method', 'csv_export')) { ?>
                                    <a href=#><span class="icon16 icomoon-icon-file-excel csv_export" id="Suppliers List" style="pointer-events: cursor"> Export</span></a>
                                <?php } ?>
                            </form>
                        </h4>
                    </div><!-- End .panel-heading -->
                    <div id='containerList'></div>	

                </div><!-- End .panel -->
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->       
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<script type="text/javascript">
    $(document).ready(function () {
        load_datalist({action: "<?php echo admin_url('suppliers/method/list_supplier'); ?>"});
        //Class
        $('.add_supplier_group').on('click', {
            'action': "<?php echo admin_url("suppliers/add_supplier_group"); ?>",
            'type': 1,
            'redirect': "<?php echo current_url(); ?>"
        }, load_dfltpopform);

        //Link
        $('#containerList').on('click', '.delete_supplier', {
            'action': "<?php echo admin_url("suppliers/method/delete_supplier"); ?>",
            'conMessage': "You are about to delete this supplier.",
            'redirect': "<?php echo current_url(); ?>"
        }, dfltaction_item);
    });
</script>
<?php $this->load->view(admin_dir('template/footer')); ?>