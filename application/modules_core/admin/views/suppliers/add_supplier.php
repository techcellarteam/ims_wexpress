
<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>Add New Item Brand</h3>     
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->

        <div class="row">
            <div class="col-lg-12">	
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Supplier Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Group </label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' multiple id='formdata-general_group_id' >
                                                    <option value=''></option>
                                                    <?php foreach ($groups as $q) { ?>
                                                        <option value='<?= $q->id_general_group; ?>'><?= $q->group_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Supplier Type</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-supplier_type" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Stock Classification </label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-stock_classification_id' >
                                                    <option value=''></option>
                                                    <?php foreach ($stock_classifications as $q) { ?>
                                                        <option value='<?= $q->id_stock_classification; ?>'><?= $q->stock_classification_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Supplier's Name</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-supplier_name" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Supplier's Code</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-supplier_code" />
                                            </div>
                                        </div><!-- End .form-group  -->                                        
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Email Address</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-email" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* TIN No.</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-tin_no" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> VAT Status:</label>
                                            <div class="col-lg-5">
                                                <input name="formdata-vat" class="form-control formdata_radio" id="formdata-vat" type="radio" value="1" /> <!-- value is 1 if within vat-->
                                                <i class="icon16 iconic-icon-checkmark">VAT</i>
                                                <input name="formdata-vat" class="form-control formdata_radio" id="formdata-vat" type="radio" value="0" /> <!-- value is 0 if without non-vat-->
                                                <i class="icon16 iconic-icon-x">Non-VAT</i>
                                            </div>
                                        </div><!-- End .form-group    -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Terms of Payment</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-terms_payment" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Phone No.</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-phone_no" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Fax No.</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-fax_no" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Nominated:</label>
                                            <div class="col-lg-5">
                                                <input name="formdata-nominated" class="form-control formdata_radio" id="formdata-nominated" type="radio" value="1" /> <!-- value is 1 if within vat-->
                                                <i class="icon16 iconic-icon-checkmark">YES</i>
                                                <input name="formdata-nominated" class="form-control formdata_radio" id="formdata-nominated" type="radio" value="0" /> <!-- value is 0 if without non-vat-->
                                                <i class="icon16 iconic-icon-x">NO</i>
                                            </div>
                                        </div><!-- End .form-group    -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Accreditation Date</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata datepicker" id="formdata-accreditation_date" value="<?= date('Y-m-d'); ?>"/>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks </label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-remarks" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Note </label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-note" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div><!-- End .body-panel  --> 

                            <div class="panel-heading">
                                <h4>
                                    <span>Contact Person Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Name</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-c_name" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Email Address</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-c_email" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Phone No.</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-c_phone_no" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Address</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-c_address" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Business Unit</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-c_department" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Job Title</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-c_job_title" />
                                            </div>
                                        </div><!-- End .form-group  -->

                                    </div>
                                </div>
                            </div><!-- End .body-panel  --> 
                            <div class="panel-heading">
                                <h4>
                                    <span>Billing Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Address</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-b_address" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">City</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-b_city" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Province/State.</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-b_state" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Postal/Zip Code</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-b_zip_code" />
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Country</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata' id='formdata-b_country_code' >
                                                    <option value=''></option>
                                                    <?php foreach ($countries as $q) { ?>
                                                        <option value='<?= $q->ccode; ?>'><?= $q->country; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->

                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-lg-offset-4 col-lg-8">
                                        <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-save' type="button">Add</button>
                                    </div>
                                </div><!-- End .form-group  --> 
                            </div><!-- End .body-panel  -->
                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->  
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<?php $this->load->view(admin_dir('template/footer')); ?>