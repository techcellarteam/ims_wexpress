<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="icon16 minia-icon-close"></span></button>
        <h3>View Plate Number</h3>
    </div>
    <div class="modal-body">
        <div class="row popformdata_alert"></div>
        <form class="form-horizontal" action="#" role="form">
            <dl class="dl-horizontal">
                <dt> Plate Number:</dt>
                <dd>
                    <b><?php echo $row->plate_number; ?></b>
                </dd>
            </dl><!-- End .form-group  -->
            <dl class="dl-horizontal">
                <dt> Make:</dt>
                <dd>
                    <b><?php echo $row->make; ?></b>
                </dd>
            </dl><!-- End .form-group  -->
            <dl class="dl-horizontal">
                <dt> Model:</dt>
                <dd>
                    <b><?php echo $row->model; ?></b>
                </dd>
            </dl><!-- End .form-group  -->
            <dl class="dl-horizontal">
                <dt> Year:</dt>
                <dd>
                    <b><?php echo $row->year; ?></b>
                </dd>
            </dl><!-- End .form-group  -->

        </form>
    </div>
    <div class="modal-footer">
        <div class='right'>
            <button class="btn btn-warning ui-wizard-content ui-formwizard-button" id="popformdata-back" type="button">Back</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function ()
    {
        jQuery(document).on('click', '#popformdata-back', backToList);
    });

    function backToList() {
        window.location.replace(adminURL + 'plate_numbers/list_plate_number');
    }
</script>