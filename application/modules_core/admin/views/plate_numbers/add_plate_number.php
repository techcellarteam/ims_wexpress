<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="icon16 minia-icon-close"></span></button>
        <h3>Add New Plate Number</h3>
    </div>
    <div class="modal-body">
        <div class="row popformdata_alert"></div>
        <form class="form-horizontal" action="#" role="form">
            <div class="form-group">
                <label class="col-lg-4 control-label">* Plate Number</label>
                <div class="col-lg-8">
                    <input type="text" class='form-control popformdata' id='popformdata-plate_number' value=""/>
                </div>
            </div><!-- End .form-group  -->
            <div class="form-group">
                <label class="col-lg-4 control-label">* Make</label>
                <div class="col-lg-8">
                    <input type="text" class='form-control popformdata' id='popformdata-make' value=""/>
                </div>
            </div><!-- End .form-group  -->
            <div class="form-group">
                <label class="col-lg-4 control-label">* Model</label>
                <div class="col-lg-8">
                    <input type="text" class='form-control popformdata' id='popformdata-model' value=""/>
                </div>
            </div><!-- End .form-group  -->
            <div class="form-group">
                <label class="col-lg-4 control-label">* Year</label>
                <div class="col-lg-8">
                    <input type="text" class='form-control popformdata' id='popformdata-year' value=""/>
                </div>
            </div><!-- End .form-group  -->
        </form>
    </div>
    <div class="modal-footer">
        <div class='right'>
            <button class="btn btn-success ui-wizard-content ui-formwizard-button" id="popformdata-save" type="button">Add</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#popformdata-save').on('click', {
            'action': "<?php echo admin_url("plate_numbers/method/add_plate_number"); ?>",
            'conMessage': "You are about to add new plate number.",
//            'redirect': "<?php echo admin_url("plate_numbers/list_plate_number"); ?>"
        }, save_form_v1);
    });
</script>	