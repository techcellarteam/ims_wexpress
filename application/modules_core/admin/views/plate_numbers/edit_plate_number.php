<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="icon16 minia-icon-close"></span></button>
        <h3>Edit Plate Number</h3>
    </div>
    <div class="modal-body">
        <div class="row popformdata_alert"></div>
        <form class="form-horizontal" action="#" role="form">
            <div class="form-group">
                <label class="col-lg-4 control-label">* Plate Number</label>
                <div class="col-lg-8">
                    <input type="text" class='form-control popformdata' id='popformdata-plate_number' value="<?php echo $row->plate_number; ?>"/>
                </div>
            </div><!-- End .form-group  -->
            <div class="form-group">
                <label class="col-lg-4 control-label">* Make</label>
                <div class="col-lg-8">
                    <input type="text" class='form-control popformdata' id='popformdata-make' value="<?php echo $row->make; ?>"/>
                </div>
            </div><!-- End .form-group  -->
            <div class="form-group">
                <label class="col-lg-4 control-label">* Model</label>
                <div class="col-lg-8">
                    <input type="text" class='form-control popformdata' id='popformdata-model' value="<?php echo $row->model; ?>"/>
                </div>
            </div><!-- End .form-group  -->
            <div class="form-group">
                <label class="col-lg-4 control-label">* Year</label>
                <div class="col-lg-8">
                    <input type="text" class='form-control popformdata' id='popformdata-year' value="<?php echo $row->year; ?>"/>
                </div>
            </div><!-- End .form-group  -->
        </form>
    </div>
    <div class="modal-footer">
        <div class='right'>
            <button class="btn btn-success ui-wizard-content ui-formwizard-button" id="popformdata-save" type="button">Save</button>
        </div>
        <div class='right'>
            <button class="btn btn-warning ui-wizard-content ui-formwizard-button" id="popformdata-back" type="button">Back</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#popformdata-save').on('click', {
            'action': "<?php echo admin_url("plate_numbers/method/edit_plate_number"); ?>",
            'id': "<?php echo $this->Misc->encode_id($row->id_plate_number); ?>",
            'conMessage': "You are about to edit plate number."
        }, save_form_v1);
    });

    jQuery(function ()
    {
        jQuery(document).on('click', '#popformdata-back', backToList);
    });

    function backToList() {
        window.location.replace(adminURL + 'plate_numbers/list_plate_number');
    }
</script>