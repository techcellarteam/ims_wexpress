<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->

        <div class="heading">
            <h3>Data Import</h3>
        </div><!-- End .heading-->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <span>Import Items</span>
                        </h4>
                    </div><!-- End .panel-heading -->
                    <div id='containerList'>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel-body ">
                                    <div class="row formupload_alert"></div>
                                    <div class="row form-horizontal">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Choose Data to Import:</label>
                                            <div class="col-lg-8">
                                                <input name="formupload_data" class="form-control formupload_radio formupload items_radio" id="formupload_data1" type="radio" value="items"  />
                                                <i class="icon16 icomoon-icon-file-4">Items</i>
                                                <input name="formupload_data" class="form-control formupload_radio formupload stock_classifications_radio" id="formupload_data2" type="radio" value="stock_classifications" />
                                                <i class="icon16 icomoon-icon-user-plus">Stock Classifications </i>
                                                <input name="formupload_data" class="form-control formupload_radio formupload unit_measurements_radio" id="formupload_data3" type="radio" value="unit_measurements" />
                                                <i class="icon16 icomoon-icon-user-plus">Unit Measurement </i>
                                                <input name="formupload_data" class="form-control formupload_radio formupload plate_numbers_radio" id="formupload_data4" type="radio" value="plate_numbers" />
                                                <i class="icon16 icomoon-icon-user-plus">Plate Numbers</i>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"></label>
                                            <div class="col-lg-8">
                                                <input name="formupload_data" class="form-control formupload_radio formupload suppliers_radio" id="formupload_data5" type="radio" value="suppliers" />
                                                <i class="icon16 icomoon-icon-truck">Suppliers</i>
                                                <input name="formupload_data" class="form-control formupload_radio formupload beg_inv_radio" id="formupload_data6" type="radio" value="beginning_inventory" />
                                                <i class="icon16 icomoon-icon-truck">Beginning Inventory</i>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Template:</label>
                                            <div class="col-lg-8">
                                                <a href="<?= base_url(); ?>assets/templates/Items.xlsx" title="View" class="item_template hidden">
                                                    <span class="icon32 icomoon-icon-file-excel" ></span>
                                                    Click the excel icon to download the Items template.</br>
                                                    Fill it up with your data, save it as a .csv or unicode text (in case your text is in another language).
                                                </a>
                                                <a href="<?= base_url(); ?>assets/templates/Stock Classifications.xlsx" title="View" class="cg_template hidden">
                                                    <span class="icon32 icomoon-icon-file-excel" ></span>
                                                    Click the excel icon to download the Stock Classifications template.</br>
                                                    Fill it up with your data, save it as a .csv or unicode text (in case your text is in another language).
                                                </a>
                                                <a href="<?= base_url(); ?>assets/templates/Unit Measurements.xlsx" title="View" class="um_template hidden">
                                                    <span class="icon32 icomoon-icon-file-excel" ></span>
                                                    Click the excel icon to download the Unit Measurements template.</br>
                                                    Fill it up with your data, save it as a .csv or unicode text (in case your text is in another language).
                                                </a>
                                                <a href="<?= base_url(); ?>assets/templates/Plate Numbers.xlsx" title="View" class="pn_template hidden">
                                                    <span class="icon32 icomoon-icon-file-excel" ></span>
                                                    Click the excel icon to download the Plate Numbers template.</br>
                                                    Fill it up with your data, save it as a .csv or unicode text (in case your text is in another language).
                                                </a>
                                                <a href="<?= base_url(); ?>assets/templates/Suppliers.xlsx" title="View" class="supplier_template hidden">
                                                    <span class="icon32 icomoon-icon-file-excel" ></span>
                                                    Click the excel icon to download the Suppliers template.</br>
                                                    Fill it up with your data, save it as a .csv or unicode text (in case your text is in another language).
                                                </a>
                                                <a href="<?= base_url(); ?>assets/templates/Beginning Inventory.xlsx" title="View" class="beg_inv_template hidden">
                                                    <span class="icon32 icomoon-icon-file-excel" ></span>
                                                    Click the excel icon to download the Beginning Inventory template.</br>
                                                    Fill it up with your data, save it as a .csv or unicode text (in case your text is in another language).
                                                </a>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group beg_inv_template" >
                                            <label class="col-lg-4 control-label">* Business Unit:</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formupload chosen-select' id='formupload_businessunitid'>
                                                    <option value=''>----</option>
                                                    <?php foreach ($business_units as $q) { ?>
                                                        <option value='<?= $q->id_business_unit; ?>'><?= $q->business_unit_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                                <span class="required">Business unit to where to account inventory.</span>
                                            </div>
                                            <div class="row center" id="custom_field"></div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Documents to Attach:</label>
                                            <form id="upload_form" enctype="multipart/form-data" method="post">
                                                <div class="col-lg-8">
                                                    <input type="file"  class='formupload' name="formupload_file" id="formupload_file" />
                                                    <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formupload_save' type="button">Upload CSV</button>
                                                </div>
                                            </form>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Notes:</label>
                                            <span class="col-lg-8">
                                                (1) Upload Correct CSV File,
                                                (2) No headers
                                            </span>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div> <!--End .panel -->
                                <!--End of Delivery Items-->

                            </div>
                        </div><!-- End .row -->
                    </div><!-- End #containerList -->
                </div><!-- End content wrapper -->

            </div><!-- End #content -->
        </div><!-- End .row -->


    </div><!-- End .panel -->

</div><!-- End #content -->
<!-- Page end here -->

<?php $this->load->view(admin_dir('template/footer')); ?>