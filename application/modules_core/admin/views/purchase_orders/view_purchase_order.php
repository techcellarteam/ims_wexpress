<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>View Purchase Order [<?= $row->po_number; ?>]</h3>
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Order Information</span>
                                    <input type="hidden" class="form-control formdata" id="formdata-purchase_order_id" value="<?= $this->Misc->encode_id($this->id); ?>"/>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> PO Number:</label>
                                            <div class="col-lg-5">
                                                <?= $row->po_number; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> PR Number:</label>
                                            <div class="col-lg-5">                                                
                                                <a href="<?php echo admin_url("purchase_requisitions/view_purchase_requisition/" . $this->Misc->encode_id($row->purchase_requisition_id)); ?>" title="View <?= $this->contents['functionName']; ?>" class="tip view_purchase_reqisition">
                                                    <?= $row->pr_number; ?><span class="icon16 icomoon-icon-search-3"></span>
                                                </a>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <?php if ($row->repo) { ?>
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label"> RE-PO'ed PO #:</label>
                                                <div class="col-lg-5">                                                
                                                    <a href="<?php echo admin_url("purchase_orders/view_purchase_order/" . $this->Misc->encode_id($row->repo->id_purchase_order)); ?>" title="View <?= $this->contents['functionName']; ?>" class="tip view_purchase_order">
                                                        <?= $row->repo->po_number; ?>
                                                        <span class="icon16 icomoon-icon-search-3"></span>
                                                    </a>
                                                </div>
                                            </div><!-- End .form-group  -->
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Order Date:</label>
                                            <div class="col-lg-5">
                                                <?= date('M d, Y', strtotime($row->order_date)); ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Supplier:</label>
                                            <div class="col-lg-5">
                                                <?= $row->supplier_code . " | " . $row->supplier_name; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Supplier's Contact Person:</label>
                                            <div class="col-lg-5">
                                                <?= "$row->supplier_contact_person"; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Terms of Payment:</label>
                                            <div class="col-lg-5">
                                                <?= $row->terms_payment; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <?php if (isset($row->plate_number)) { ?>
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label"> Plate Number:</label>
                                                <div class="col-lg-5">
                                                    <?= $row->plate_number; ?>
                                                </div>
                                            </div><!-- End .form-group  -->
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks:</label>
                                            <div class="col-lg-5">
                                                <?= $row->po_remarks; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Status:</label>
                                            <div class="col-lg-5">
                                                <?= $row->status; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div>

                            <!--Transaction Signatories-->
                            <?php if (isset($row->approved_by)) { ?>
                                <div class="panel-heading">
                                    <h4>
                                        <span>Transaction Signatories</span>
                                        <span class="text-toggle-button right">
                                            <input type="checkbox" class="nostyle selected" id="transaction_signatories">
                                        </span>
                                    </h4>
                                </div>
                                <div class="panel-body " id="transaction_signatories">
                                    <div class="row form-horizontal">
                                        <div class="col-lg-10">
                                            <?php
                                            if ($row->first_level_id) {
                                                if ($row->first_approval_date != '0000-00-00 00:00:00') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> First Level Approved By:</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->first_level; ?>
                                                            <?= date('M d, Y', strtotime($row->first_approval_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                <?php } else {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Declined By (1st Level):</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->first_level; ?>
                                                            <?= date('M d, Y', strtotime($row->first_decline_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                    <?php
                                                }
                                            }

                                            if ($row->approved_by) {
                                                if ($row->approval_date != '0000-00-00 00:00:00') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label">Purchasing Approved By:</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->approved; ?>
                                                            --><?= date('M d, Y', strtotime($row->approval_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                <?php } else {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Declined By:</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->declined; ?>
                                                            --><?= date('M d, Y', strtotime($row->declined_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                    <?php
                                                }
                                            }

                                            if ($row->second_level_id) {
                                                if ($row->second_approval_date != '0000-00-00 00:00:00') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Second Level Approved By:</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->second_level; ?>
                                                            <?= date('M d, Y', strtotime($row->second_approval_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                <?php } else {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Declined By (2nd Level):</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->second_level; ?>
                                                            <?= date('M d, Y', strtotime($row->second_decline_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                    <?php
                                                }
                                            }
                                            if ($row->third_level_id) {
                                                if ($row->third_approval_date != '0000-00-00 00:00:00') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Third Level Approved By:</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->third_level; ?>
                                                            <?= date('M d, Y', strtotime($row->third_approval_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                <?php } else {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-lg-4 control-label"> Declined By (3rd Level):</label>
                                                        <div class="row-lg-5">
                                                            <?= $row->third_level; ?>
                                                            <?= date('M d, Y', strtotime($row->third_decline_date)); ?>
                                                        </div>
                                                    </div><!-- End .form-group  -->
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <!--End of Transaction Signatories-->

                            <!--Purchase Order Items-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Order Items</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="po_items_info">
                                    </span>
                                </h4>
                            </div>
                            <div class="panel-body " id="po_items_info">
                                <div class="row form-horizontal">
                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">SKU</th>
                                                    <th class="col-lg-1">Description</th>
                                                    <th class="col-lg-1">UOM</th>
                                                    <th class="col-lg-1">Initiators Price</th>
                                                    <th class="col-lg-1">Purchasing Price</th>
                                                    <th class="col-lg-1">Qty</th>
                                                    <th class="col-lg-1">Qty Receive</th>
                                                    <th class="col-lg-1">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody id='po_items_body'>
                                                <?php
                                                $total_qty = 0;
                                                $total_amt = 0;
                                                $total_rcvd = 0;
                                                if ($po_items) {
                                                    foreach ($po_items as $q) {
                                                        $total_qty += $q->quantity;
                                                        $total_amt += ($q->item_unit_cost * $q->quantity);
                                                        $total_rcvd += $q->qty_rcvd;
                                                        ?>
                                                        <tr id="id_table">
                                                            <td class='col-lg-1'>
                                                                <?= $q->sku; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->description; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->measurement_code; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->initiator_price; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->item_unit_cost; ?>
                                                            </td>    
                                                            <td class='col-lg-1'>
                                                                <?= $q->quantity; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= $q->qty_rcvd; ?>
                                                            </td>
                                                            <td class='col-lg-1'>
                                                                <?= number_format($q->item_unit_cost * $q->quantity, 2); ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                <tr>
                                                    <th colspan="5" class="col-lg-1 text-right">TOTAL</th>
                                                    <th class="col-lg-1 text-right"><?= $total_qty; ?></th>
                                                    <th class="col-lg-1  text-right"><?= $total_rcvd; ?></th>
                                                    <th class="col-lg-1 text-right"><?= number_format($total_amt, 2); ?></th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--End of Purchase Order Items-->

                            <!--Attaching Documents-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Attach Documents</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="po_attachments_info">
                                    </span>
                                </h4>
                            </div>
                            <div class="panel-body " id="po_attachments_info">
                                <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'add_attachment')) { // check if allowed to attach document           ?>
                                    <div class="row formupload_alert"></div>
                                    <div class="row form-horizontal">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Documents to Attach:</label>
                                            <form id="upload_form" enctype="multipart/form-data" method="post">
                                                <div class="col-lg-5">
                                                    <input type="file"  class='formupload' name="formupload_file" id="formupload_file" />
                                                    <input type="text" class="formupload" id="formupload_upload_title" value=""/>
                                                    <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formupload_save' type="button">Upload File</button>
                                                </div>
                                            </form>
                                        </div><!-- End .form-group  -->
                                    <?php } ?>
                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">Title</th>
                                                    <th class="col-lg-1">Date Upload</th>
                                                    <th class="col-lg-1">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($module_attachments as $q): ?>
                                                    <tr>
                                                        <td class='col-lg-1'><?= $q->upload_title; ?></td>
                                                        <td class='col-lg-1'><?= date("M d,Y", strtotime($q->added_date)); ?></td>
                                                        <td class='col-lg-1'>
                                                            <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'view_attachment')) { // check if permitted           ?>
                                                                <a target="_blank" href="<?= base_url(); ?>upload/Purchase Orders/<?= $row->po_number; ?>/<?= $q->upload_file_name; ?>" title="View">
                                                                    <span class="icomoon-icon-search-3" ></span>
                                                                </a>
                                                            <?php } ?>
                                                            <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'delete_attachment')) { // check if permitted          ?>
                                                                <a href="#" title="Delete" class="tip delete_attachment" value='<?php echo $this->Misc->encode_id($q->id_module_attachment); ?>'>
                                                                    <span class="icon16 icomoon-icon-remove"></span>
                                                                </a>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--End of Attachments-->

                            <!--Action Buttons-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Action Buttons</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="form-group">
                                    <div class="col-lg-8 center">
                                        <?php if ($total_amt > 50000) { ?>
                                            <div class='col-lg-12'><div class='alert-info'>
                                                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                                    <strong>Info!</strong> This is more than PHP 50,000.00
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if (trim($row->status) == trim('Queued to BU Head') && $row->group_code == 'TFleet' && $this->Misc->accessible($this->access, $this->classname, 'method', 'first_level_approval')) { ?>
                                            <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-approve' type="button" value="Queued to Purchasing Head"><?= "APPROVE (BU Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                            <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (BU Head)"><?= "DECLINE (BU Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                        <?php } ?>
                                        <?php if (trim($row->status) == trim('Queued to Purchasing Head') && $row->group_code == 'TFleet' && $this->Misc->accessible($this->access, $this->classname, 'method', 'approve_purchase_order')) { ?>
                                            <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-approve' type="button" value="Queued to DJF"><?= "APPROVE (Purchasing Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                            <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (Purchasing Head)"><?= "DECLINE (Purchasing Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                        <?php } ?>
                                        <?php if (trim($row->status) == trim('Queued to Purchasing Head') && $row->group_code != 'TFleet' && $this->Misc->accessible($this->access, $this->classname, 'method', 'approve_purchase_order')) { ?>
                                            <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-approve' type="button" value="Queued to ELA"><?= "APPROVE (Purchasing Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                            <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (Purchasing Head)"><?= "DECLINE (Purchasing Head) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                        <?php } ?>
                                        <?php if (trim($row->status) == trim('Queued to ELA') && $this->Misc->accessible($this->access, $this->classname, 'method', 'second_level_approval')) { ?>
                                            <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-approve' type="button" value="PO Approved (ELA)"><?= "APPROVE (Exec VP) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                            <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (Exec VP)"><?= "DECLINE (Exec VP) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                        <?php } ?>
                                        <?php if ((trim($row->status) == trim('Queued to DJF')) && $this->Misc->accessible($this->access, $this->classname, 'method', 'third_level_approval')) { ?>
                                            <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-approve' type="button" value="PO Approved (DJF)"><?= "APPROVE (Chairman & Pres.) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                            <button  class="btn btn-warning ui-wizard-content ui-formwizard-button formdata" id='formdata-decline' type="button" value="DECLINED (Chairman & Pres.)"><?= "DECLINE (Chairman & Pres.) [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                        <?php } ?>

                                        <?php
                                        if ((trim($row->status) == 'Queued to ELA' || trim($row->status) == "Queued to DJF" || trim($row->status) == "For Receiving")) {
                                            //REPO
                                            if ($this->Misc->accessible($this->access, $this->classname, 'method', 're_po')) {
                                                ?>
                                                <button  class="btn btn-primary ui-wizard-content ui-formwizard-button formdata" id='formdata-re_po' type="button" value="Re-Po'ed"><?= "RE-PO [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                <?php
                                            }
                                            // CANCEL
                                            if ($this->Misc->accessible($this->access, $this->classname, 'method', 'cancel_purchase_order')) {
                                                ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-cancel' type="button" value="Cancelled PO"><?= "CANCEL PO [" . $this->session->userdata['admin']['user_type_code'] . "]"; ?></button>
                                                <?php
                                            }
                                        }
                                        ?>



                                        <!-- PRINT -->
                                        <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'print_purchase_order')) { ?>
                                            <a target="blank" href="<?php echo admin_url("$this->classname/print_purchase_order/" . $this->Misc->encode_id($row->id_purchase_order)); ?>" title="Print PO Form" >
                                                <span class="icon32 icomoon-icon-print"> PRINT</span>
                                            </a>
                                        <?php } ?>
                                    </div>

                                </div> <!-- End .form-group  -->
                            </div>
                            <!--Action Buttons

                            <!--History-->
                            <div class="panel-heading">
                                <h4>
                                    <span>History</span>
                                    <span class="text-toggle-button right">
                                        <input type="checkbox" class="nostyle selected" id="history" checked="checked">
                                    </span>
                                </h4>
                            </div>
                            <div class="panel-body hidden" id="history">
                                <div class="row form-horizontal">
                                    <div class="form-group">
                                        <label class="control-label col-lg-1">Comment:</label>
                                        <div class="col-lg-3">
                                            <textarea class="form-control formdata" id="formdata-comment" value="" rows="3" cols="5"></textarea>
                                            <?php if ($this->Misc->accessible($this->access, $this->classname, 'method', 'add_comment')) { // check if permitted               ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-add_comment' type="button">Save</button>
                                            <?php } ?>
                                        </div>
                                    </div><!-- End .form-group  -->

                                    <?php foreach ($logs as $q) { ?>
                                        <div class="form-group">
                                            <label class="control-label col-lg-1"><?= date('M d, Y H:i:s', strtotime($q->user_log_date)); ?></label>
                                            <div class="col-lg-5>" >
                                                <?php
                                                switch (strtolower($q->user_log_detail)) {
                                                    case 'requested':
                                                        echo '<span class="icon16 icomoon-icon-new"></span>';
                                                        break;
                                                    case 'approved':
                                                        echo '<span class="icon16 icomoon-icon-thumbs-up-3"></span>';
                                                        break;
                                                    case 'declined':
                                                        echo '<span class="icon16 icomoon-icon-thumbs-up-4"></span>';
                                                        break;
                                                    case 'deleted attachment':
                                                    case 'uploaded attachment':
                                                        echo '<span class="icon16 icomoon-icon-file-6"></span>';
                                                        break;
                                                    default:
                                                        echo '<span class="icon16 icomoon-icon-bubble-2"></span>';
                                                        break;
                                                }
                                                ?>
                                                <span><strong><?= $q->user_log_detail; ?></strong></span>
                                                <i class=" icomoon-icon-arrow-right-5"> <?= $q->user_fname . ' ' . $q->user_lname . ' (' . $q->user_type_name . ')'; ?></i>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    <?php } ?>
                                </div>
                            </div>
                            <!--End of History-->

                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->
            </div><!-- End .span12 -->
        </div><!-- End .row -->

        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->


<?php
$this->load->view(admin_dir('template/footer'));
