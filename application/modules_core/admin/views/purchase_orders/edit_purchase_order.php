<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>Edit New Purchase Order</h3>     
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->

        <div class="row">
            <div class="col-lg-12">	
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Order Information</span>
                                    <input type="hidden" class="form-control formdata" id="formdata-purchase_order_id" value="<?= $this->Misc->encode_id($this->id); ?>"/>
                                    <input type="hidden" class="form-control formdata" id="formdata-purchase_requisition_id" value="<?= $row->purchase_requisition_id; ?>"/>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Order Date</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata datepicker" id="formdata-order_date" value="<?= date('M d, Y'); ?>"/>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks</label>
                                            <div class="col-lg-5">
                                                <textarea class="form-control formdata" id="formdata-po_remarks" rows="6" cols="50">
                                                    <?= $row->po_remarks; ?>
                                                </textarea>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group " >
                                            <label class="col-lg-4 control-label">* PR #</label>
                                            <div class="col-lg-5">
                                                <?= $row->pr_number; ?>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group " >
                                            <label class="col-lg-4 control-label">* Group</label>
                                            <div class="col-lg-5">
                                                <?= $row->group_code; ?>
                                            </div>
                                            <div class="row center" id="custom_field"></div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group " >
                                            <label class="col-lg-4 control-label">* Suppliers</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-supplier_id'>
                                                    <?php foreach ($suppliers as $q) { ?>
                                                        <option value="<?= $q->id_supplier; ?>" <?php echo ($q->id_supplier == $row->supplier_id) ? 'selected' : ''; ?>><?= $q->supplier_code . ' | ' . $q->supplier_name; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Supplier Contact Person</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-supplier_contact_person" value=""/>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Terms of Payment</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-terms_payment" value=""/>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div>
                            <!--Adding Items-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Order Items</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert_item"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Stock Classification</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-edit_stock_classification_id'>
                                                    <option value=''></option>
                                                    <?php foreach ($stock_classifications as $q) { ?>
                                                        <option value='<?= $q->id_stock_classification; ?>'><?= $q->stock_classification_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                                <div class="row center" id="custom_field"></div>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Items</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-item' >
                                                    <option value="">Select commodity</option>
                                                </select>
                                            </div>
                                            <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-add_item' type="button">Add Item</button>
                                        </div><!-- End .form-group  -->     
                                    </div>

                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead> 
                                                <tr>
                                                    <th class="col-lg-1">Stock Classification</th>
                                                    <th class="col-lg-2">Item</th>
                                                    <th class="col-lg-1">UOM</th>
                                                    <th class="col-lg-1">PR Qty</th>
                                                    <th class="col-lg-1">Last Ordered Price</th>
                                                    <th class="col-lg-1">Initiators Price</th>
                                                    <th class="col-lg-1">Purchasing Price</th>
                                                    <th class="col-lg-1">PO QTY</th>
                                                    <th class="col-lg-1">Total Amount</th>
                                                    <th class="col-lg-1">PR Remarks</th>
                                                    <th class="col-lg-1">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id='po_items_body'>
                                                <?php
                                                $total_quantity = 0;
                                                foreach ($po_items as $q) {
                                                    $total_quantity += $q->quantity;
                                                    ?>
                                                    <tr id="id_table_<?= $q->item_id; ?>">
                                                        <td class='col-lg-1'>
                                                            <?= $q->stock_classification_name; ?>
                                                        </td>
                                                        <td class='col-lg-2'>
                                                            <input type='hidden' class='form-control formdata po_items' id='formdata-po_items[<?= $q->item_id; ?>]' value='<?= $q->id_po_item; ?>' />
                                                            <?= $q->sku . " | " . $q->description; ?>
                                                        </td>
                                                        <td class='col-lg-1'>
                                                            <?= ($q->measurement_code); ?>
                                                        </td>
                                                        <td class='col-lg-1'>
                                                            <?= ($q->quantity); ?>
                                                        </td>
                                                        <td class='col-lg-1'>
                                                            <?= ($q->last_ordered_price); ?>
                                                        </td>
                                                        <td class='col-lg-1'>
                                                            <?= ($q->initiator_price); ?>
                                                        </td> 
                                                        <td class='col-lg-1 pu_price'>
                                                            <input type='text' class='form-control formdata purchase_price' id='formdata-item_unit_cost[<?= $q->item_id; ?>]' value='<?= $q->item_unit_cost; ?>' />
                                                        </td>
                                                        <td class='col-lg-1 po_qty'>
                                                            <input type='number' min="1" max="<?= $q->quantity; ?>" class='form-control formdata quantity' id='formdata-po_quantity[<?= $q->item_id; ?>]' value='<?= $q->quantity; ?>' />
                                                            <span class="alert" style="color:red"></span>
                                                        </td>
                                                        <td class='col-lg-1 po_amount'>
                                                            <?= number_format($q->item_unit_cost * $q->quantity, 2); ?>
                                                        </td>
                                                        <td class='col-lg-1 pu_price'>
                                                            <?= $q->remarks; ?>
                                                        </td> 
                                                        <td class='col-lg-1'>
                                                            <button class='icon16 icomoon-icon-remove formdata-remove_item' id='<?= $q->item_id; ?>'></button>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                <tr style='font-weight: bold'>
                                                    <td class="col-lg-1"></td>
                                                    <td class="col-lg-1"></td>
                                                    <td class="col-lg-1"></td>
                                                    <td class="col-lg-1"></td>
                                                    <td class="col-lg-1"></td>
                                                    <td class="col-lg-1"></td>
                                                    <td class="col-lg-1 text-right">Total</td>
                                                    <td class="col-lg-1 text-right" id='total_quantity'><?php echo $total_quantity; ?></td>
                                                    <td class="col-lg-1 text-right" id='total_amount'><?php echo number_format($row->total_amount, 2); ?></td>
                                                    <td class="col-lg-1">
                                                        <input type="hidden" class="formdata" id="formdata-total_amount" value="<?php echo $row->total_amount; ?>"/>
                                                    </td>
                                                    <td class="col-lg-1"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-body ">
                                <div class="row form-horizontal">
                                    <div class="form-group">
                                        <div class="col-lg-offset-4 col-lg-8">
                                            <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-edit' type="button">Submit</button>
                                        </div>
                                    </div><!-- End .form-group  --> 
                                </div>
                            </div>

                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->  
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<?php
$this->load->view(admin_dir('template/footer'));
