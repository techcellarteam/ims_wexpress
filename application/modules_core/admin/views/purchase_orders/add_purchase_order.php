<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>Add New Purchase Order</h3>
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Order Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Remarks</label>
                                            <div class="col-lg-5">
                                                <textarea class="form-control formdata" id="formdata-po_remarks" rows="6" cols="50"></textarea>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group " >
                                            <label class="col-lg-4 control-label">* Group</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-group_id'>
                                                    <option value=''></option>
                                                    <?php foreach ($groups as $q) { ?>
                                                        <option value='<?= $q->id_general_group; ?>'><?= $q->group_code; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="row center" id="custom_field"></div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group " >
                                            <label class="col-lg-4 control-label">* Suppliers</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-supplier_id'>
                                                    <option value=''></option>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group " >
                                            <label class="col-lg-4 control-label">* Purchase Requisition</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-purchase_requisition_id'>
                                                    <option value=''></option>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div>
                            <!--Adding Items-->
                            <div class="panel-heading">
                                <h4>
                                    <span>Purchase Order Items</span>
                                </h4>

                            </div>
                            <div class="panel-body ">
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group " >
                                            <label class="col-lg-4 control-label">* Stock Classifications</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-stock_classification_id'>
                                                    <option value=''></option>
                                                    <?php foreach ($stock_classifications as $q) { ?>
                                                        <option value='<?= $q->id_stock_classification; ?>'><?= $q->stock_classification_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Items</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-item' >
                                                    <option value="">Select commodity</option>
                                                </select>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-add_item' type="button">Add Item</button>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>

                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">Stock Classification</th>
                                                    <th class="col-lg-2">Item</th>
                                                    <th class="col-lg-1">UOM</th>
                                                    <th class="col-lg-1">PR Qty</th>
                                                    <th class="col-lg-1">Last Ordered Price</th>
                                                    <th class="col-lg-1">Initiators Price</th>
                                                    <th class="col-lg-1">Purchasing Price</th>
                                                    <th class="col-lg-1">PO QTY</th>
                                                    <th class="col-lg-1">Total Amount</th>
                                                    <th class="col-lg-1">PR Remarks</th>
                                                    <th class="col-lg-1">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id='po_items_body'>

                                            </tbody>
                                            <tr style='font-weight: bold'>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1"></td>
                                                <td class="col-lg-1 text-right">Total</td>
                                                <td class="col-lg-1 text-right" id='total_quantity'></td>
                                                <td class="col-lg-1 text-right" id='total_amount'></td>
                                                <td class="col-lg-1">
                                                    <input type="hidden" class="formdata" id="formdata-total_amount" />
                                                </td>
                                                <td class="col-lg-1"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-body ">
                                <div class="row form-horizontal">
                                    <div class="form-group">
                                        <div class="col-lg-8 center">
                                            <?php if ($this->session->userdata['admin']['business_unit_head_id'] == $this->session->userdata['admin']['user_id']) { ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-auto_approve' type="button" value="">Submit (Auto Approve)</button>
                                            <?php } else { ?>
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-save' type="button">Submit</button>
                                            <?php } ?>
                                        </div>
                                    </div><!-- End .form-group  -->
                                </div>
                            </div>

                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->
            </div><!-- End .span12 -->
        </div><!-- End .row -->
        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<?php
$this->load->view(admin_dir('template/footer'));
