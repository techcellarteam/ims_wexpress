<!DOCTYPE html>
<html>
    <head>
        <title>PURCHASE ORDER</title>
        <link rel="shortcut icon" href="<?= assets_dir('images/favicon.ico'); ?>" />
        <style type="text/css">
            @media print {
                .footerc {
                    background: green!important;
                }
            }
            td{
                padding: 0px;
                margin: 0px;
            }
            table{

            }
        </style>

    </head>
    <body>           
        <table style="width:100%;font-family:Arial!important;font-size:12px;">
            <tr>
                <td colspan="3" style="vertical-align:top;text-align:center;font-weight:bold;">
                    <h4  style="margin:0;font-size:18px;text-align:center;">WIDE WIDE WORLD EXPRESS CORPORATION</h4>
                    (Corporate Service Group)<br/>
                    WExpress Bldg., Pascor Drive, Sto. Nino, Paranaque<br/>
                    Telephone No.: (02) 793.1777
                </td>
            </tr>
            <tr>
                <td colspan="4" style="vertical-align:top;text-align:right;">
                    <span style="font-size:14px;">PO NO:</span> <span style="font-size:14px;font-weight:bold;"><?= $row->po_number; ?></span><br/>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="vertical-align:top;text-align:center;font-weight:bold;padding-bottom:15px;">
                    <h2 style="margin-top:10px;margin-bottom:10px;text-align:center;">PURCHASE ORDER</h2>
                    <em>Supplier's Copy</em>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;">
                    Company Name:
                </td>
                <td style="vertical-align:top;">
                    <?= $row->supplier_name; ?>
                </td>
                <td colspan="2" style="vertical-align:top;text-align: right">
                    Date: <?= date('M d, Y', strtotime($row->order_date)); ?>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;">
                    Address:
                </td>
                <td colspan="2" style="vertical-align:top;">
                    <?= $row->b_address; ?>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;">
                    Contact Person:
                </td>
                <td colspan="2" style="vertical-align:top;">
                    <?= $row->supplier_contact_person ?>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;">
                    Accounting Unit:
                </td>
                <td colspan="2" style="vertical-align:top;">
                    <?= $row->business_unit_name ?>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;">
                    PR#:
                </td>
                <td colspan="2" style="vertical-align:top;">
                    <?= $row->pr_number ?><?= ($row->plate_number) ? " | " . $row->plate_number : ''; ?>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="vertical-align:top;font-weight:bold;padding:15px 0px;">
                    Please deliver for the account of Wide Wide World Express Corporation.
                </td>
            </tr>
        </table>
        <!--head end-->
        <!--content-->
        <table style="width:100%;font-family:Arial!important;font-size:12px;">

            <tr>
                <td style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;width:120px">
                    ITEM
                </td>
                <td style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;width:80px">
                    QUANTITY
                </td>
                <td style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;width:80px">
                    UNIT
                </td>
                <td colspan="4" style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;">
                    COMPLETE DESCRIPTION OF ARTICLES
                </td>
                <td style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;width:80px">
                    UNIT PRICE
                </td>
                <td style="vertical-align:top;padding:5px;border:1px solid #666;text-align:center;width:80px">
                    AMOUNT
                </td>
            </tr>
            <?php foreach ($po_items as $q) { ?>
                <tr>
                    <td style="vertical-align:top;padding:5px;text-align:center;">
                        <?= $q->sku; ?>
                    </td>
                    <td style="vertical-align:top;padding:5px;text-align:center;">
                        <?= $q->quantity; ?>
                    </td>
                    <td style="vertical-align:top;padding:5px;text-align:center;">
                        <?= $q->measurement_code; ?>
                    </td>
                    <td colspan="4" style="vertical-align:top;padding:5px;text-align:center;">
                        <?= $q->description; ?>
                    </td>
                    <td style="vertical-align:top;padding:5px;text-align:center;">
                        <?= $q->item_unit_cost; ?>
                    </td>
                    <td style="vertical-align:top;padding:5px;text-align:right;">
                        <?= ($q->item_unit_cost * $q->quantity); ?>
                    </td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="5" style="vertical-align:top;padding:5px;text-align:center;">
                    **************** NOTHING FOLLOWS ****************
                </td>
                <td style="vertical-align:top;padding:5px;text-align:right;">
                    - - - - - -
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td colspan="4" style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td style="vertical-align:top;padding:5px;text-align:right;">
                    <?= $row->total_amount; ?>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td colspan="4" style="vertical-align:top;padding:5px;text-align:center;">
                    NOTE/REMARKS: <?= $row->po_remarks; ?>
                </td>
                <td style="vertical-align:top;padding:5px;text-align:center;">

                </td>
                <td style="vertical-align:top;padding:5px;text-align:right;">

                </td>
            </tr>
            <tr style="font-weight: bold;">
                <td style="vertical-align:top;padding:5px;text-align:center;border:1px solid #666;">
                    TERMS:
                </td>
                <td colspan="6" style="vertical-align:top;padding:5px;text-align:left;border:1px solid #666;">
                    <?= $row->terms_payment; ?>
                </td>
                <td style="vertical-align:top;padding:5px;text-align:center;border:1px solid #666;">
                    TOTAL
                </td>
                <td style="vertical-align:top;padding:5px;text-align:right;border:1px solid #666;">
                    <?= $row->total_amount; ?>
                </td>
            </tr>
        </table>
        <!--footer-->
        <table style="width:100%;font-family:Arial!important;font-size:12px;">
            <tr>
                <td style="vertical-align:top;padding:5px 0 12px 0px;text-align:left;">
                    SIGNATURE<br/>
                    _______________________
                </td>
                <td style="vertical-align:top;padding:5px 0 12px 0px;text-align:right;">
                    Wide Wide World Express Corporation<br/>
                    By: _______________________
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;padding:5px 0 12px 0px;text-align:left;">
                    Approvals:<br/>

                    <?php
                    if ($row->first_level_id && $row->first_approval_date != '0000-00-00 00:00:00') {
                        echo $row->first_level;
                        echo date('M d, Y', strtotime($row->first_approval_date))."</br>";
                    }
                    if ($row->approved_by && $row->approval_date != '0000-00-00 00:00:00') {
                        echo $row->approved;
                        echo date('M d, Y', strtotime($row->approval_date))."</br>";
                    }
                    if ($row->second_level_id && $row->second_approval_date != '0000-00-00 00:00:00') {
                        echo $row->second_level;
                        echo date('M d, Y', strtotime($row->second_approval_date))."</br>";
                    }
                    if ($row->third_level_id && $row->third_approval_date != '0000-00-00 00:00:00') {
                        echo $row->third_level;
                        echo date('M d, Y', strtotime($row->third_approval_date))."</br>";
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center;vertical-align:top;padding:5px 0 12px 0px;">
                    <em>*** This document is system generated ***</em>
                </td>
            </tr>
        </table>
        <!--footer-->
    </body>
</html>