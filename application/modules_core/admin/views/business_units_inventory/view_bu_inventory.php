<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3><?= $row->business_unit_name; ?> Items' Inventory</h3>
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default gradient">
                    <div class="panel-heading">
                        <h4>
                            <span>Inventory List</span>
                        </h4>
                    </div>
                    <div class="panel-body noPad clearfix">
                        <table cellpadding="0" cellspacing="0" border="0" class="tableTools display table table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <th class="col-lg-1">Stocking</th>
                                    <th class="col-lg-1">Classification</th>
                                    <th class="col-lg-1">SKU</th>
                                    <th class="col-lg-1">Description</th>
                                    <th class="col-lg-1">UOM</th>
                                    <th class="col-lg-1">Purchasing Price</th>
                                    <th class="col-lg-1">Qty</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total = 0;
                                foreach ($il_items as $q) {
                                    $total += $q->item_count;
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class='col-lg-1'>
                                            <?= ($q->for_stocking)?"<span class='icon16 icomoon-icon-checkmark-3'></span>":"<span class='icon16 icomoon-icon-close'></span>"; ?>
                                        </td>
                                        <td class='col-lg-1'>
                                            <?= $q->stock_classification_name; ?>
                                        </td>
                                         <td class='col-lg-1'>
                                            <?= $q->sku; ?>
                                        </td>
                                        <td class='col-lg-1'>
                                            <?= $q->description; ?>
                                        </td>
                                        <td class='col-lg-1'>
                                            <?= $q->measurement_code; ?>
                                        </td>
                                        <td class='col-lg-1'>
                                            <?= number_format($q->item_unit_cost, 2); ?>
                                        </td>    
                                        <td class='col-lg-1'>
                                            <?= $q->item_count; ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr style="font-weight: bold">
                                    <td class='col-lg-1'>
                                        ---
                                    </td>
                                    <td class='col-lg-1'>
                                        ---
                                    </td>
                                    <td class='col-lg-1'>
                                        ---
                                    </td>
                                    <td class='col-lg-1'>
                                        ---
                                    </td>
                                    <td class='col-lg-1'>
                                        ---
                                    </td>
                                    <td class='col-lg-1'>
                                        GRAND TOTAL
                                    </td>    
                                    <td class='col-lg-1' style="text-align: center">
                                        <?= $total; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="col-lg-1">Stocking</th>
                                    <th class="col-lg-1">Classification</th>
                                    <th class="col-lg-1">SKU</th>
                                    <th class="col-lg-1">Description</th>
                                    <th class="col-lg-1">UOM</th>
                                    <th class="col-lg-1">Purchasing Price</th>
                                    <th class="col-lg-1">Qty</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div><!-- End .panel -->

            </div><!-- End .span12 -->

        </div><!-- End .row -->
        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->

<?php $this->load->view(admin_dir('template/footer')); ?>
