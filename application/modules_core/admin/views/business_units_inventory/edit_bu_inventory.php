<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3><?= $row->business_unit_name; ?> Items' Inventory</h3>
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default gradient">
                    <div class="panel-heading">
                        <h4>
                            <span>
                                Manual Update of Inventory List
                                <input type="hidden" class="form-control formdata" id="formdata-business_unit_id" value="<?= $this->Misc->encode_id($this->id); ?>"/>
                            </span>
                        </h4>
                    </div>
                    <div class="panel-body noPad clearfix">
                                                    <div class="row formdata_alert"></div>
                        <table cellpadding="0" cellspacing="0" border="0" class="dynamicTable display table table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <th class="col-lg-1">SKU</th>
                                    <th class="col-lg-1">Description</th>
                                    <th class="col-lg-1">UOM</th>
                                    <th class="col-lg-1">Purchasing Price</th>
                                    <th class="col-lg-1">Qty</th>
                                    <th class="col-lg-1">Manual Adjustment</th>
                                    <th class="col-lg-2">Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total = 0;
                                foreach ($il_items as $q) {
                                    $total += $q->item_count;
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class='col-lg-1'>
                                            <?= $q->sku; ?>
                                        </td>
                                        <td class='col-lg-1'>
                                            <?= $q->description; ?>
                                        </td>
                                        <td class='col-lg-1'>
                                            <?= $q->measurement_code; ?>
                                        </td>
                                        <td class='col-lg-1'>
                                            <?= number_format($q->item_unit_cost, 2); ?>
                                        </td>
                                        <td class='col-lg-1'>
                                            <?= $q->item_count; ?>
                                        </td>
                                        <td class='col-lg-1 '>
                                            <input type="number" max="<?= $q->item_count; ?>" class="form-control formdata manual_edit" id="formdata-qty_edit[<?= $q->id_item_location; ?>]" value="<?= $q->item_count; ?>">
                                        </td>
                                        <td class='col-lg-2'>
                                            <textarea class='form-control formdata' id='formdata-remarks[<?= $q->id_item_location; ?>]'></textarea>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="col-lg-1">SKU</th>
                                    <th class="col-lg-1">Description</th>
                                    <th class="col-lg-1">UOM</th>
                                    <th class="col-lg-1">Purchasing Price</th>
                                    <th class="col-lg-1"><?= $total; ?></th>
                                    <th class="col-lg-1 total_qty_edit">0</th>
                                    <th class=""></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <!--Action Buttons-->
                    <div class="panel-heading">
                        <h4>
                            <span>Action Buttons</span>
                        </h4>
                    </div>
                    <div class="panel-body ">
                        <div class="row form-horizontal">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">* Manual Adjustment Date</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control formdata datepicker" id="formdata-adjustment_date" value="<?= date('M d, Y'); ?>"/>
                                    </div>
                                </div><!-- End .form-group  -->

                                <div class="form-group">
                                    <div class="col-lg-4 center">
                                        <button  class="btn btn-success ui-wizard-content ui-formwizard-button formdata" id='formdata-edi_bu_inventory' type="button" value="Save"> Save Manual Inventory</button>
                                    </div>
                                </div><!-- End .form-group  -->
                            </div>
                        </div>
                    </div>
                    <!--Action Buttons-->

                </div><!-- End .panel -->

            </div><!-- End .span12 -->

        </div><!-- End .row -->
        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->

<?php $this->load->view(admin_dir('template/footer')); ?>
