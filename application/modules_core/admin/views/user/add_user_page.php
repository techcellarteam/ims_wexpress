<?php $this->load->view(admin_dir('template/header')); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>Add New User</h3>     
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->

        <div class="row">
            <div class="col-lg-12">	
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>User Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* ID No.</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_code" />
                                                <span class="help-block blue span8">You cannot change this once added.</span>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* First Name</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_fname" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Middle Name</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_mname" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Last Name</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_lname" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Address</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_address" />
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Contact No</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_contact" />
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* User Type</label>
                                            <div class="col-lg-5">
                                                <select class="form-control formdata" id="formdata_usertype" >
                                                    <option value=''></option>
                                                    <?php foreach ($user_types as $q) { ?>
                                                        <option value='<?php echo $q->id_user_type; ?>'><?php echo $q->user_type_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Business Unit</label>
                                            <div class="col-lg-5">
                                                <select class="form-control formdata chosen-select" id="formdata_businessUnitID" >
                                                    <option value=''></option>
                                                    <?php foreach ($departments as $q) { ?>
                                                        <option value='<?php echo $q->id_business_unit; ?>'><?php echo "$q->group_code | $q->business_unit_name"; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Business Units for PR</label>
                                            <div class="col-lg-5">
                                                <select class="form-control formdata chosen-select" multiple id="formdata_businessIDs[]" >
                                                    <option value=''></option>
                                                    <?php foreach ($departments as $q) { ?>
                                                        <option value='<?php echo $q->id_business_unit; ?>'><?php echo "$q->group_code | $q->business_unit_name"; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Email</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_email"  />
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Password</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_password"  />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <div class="col-lg-offset-4 col-lg-8">
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata_save' type="button">Add</button>
                                            </div>
                                        </div><!-- End .form-group  --> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->  
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#formdata_save').on('click', {
            'action': "<?php echo admin_url("user/method/add_user"); ?>",
            'conMessage': "You are about to add new user. Check if the ID No. is correct, you cannot change it once added.",
            'clear': true
        }, save_form);
    });
</script>	
<?php $this->load->view(admin_dir('template/footer')); ?>