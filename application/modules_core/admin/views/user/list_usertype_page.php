<?php $this->load->view(admin_dir('template/header')); ?>
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->

        <div class="heading">
            <h3>User Type</h3>                    
        </div><!-- End .heading-->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <span>User Type List</span>
                            <form class="panel-form right" action="">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <span class="icon16 icomoon-icon-cog-2"></span>
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" style='width:250px'>
                                    <!-- Access Links -->
                                    <?php if ($this->Misc->accessible($this->access, 'user', 'method', 'add_usertype')) { ?>
                                        <li>
                                            <a data-toggle="modal" href="#dfltmodal" class="addusertype">
                                                <span class="icomoon-icon-plus"></span> Add New User Type</a>
                                        </li>
                                        <?php }
                                    ?>
                                </ul>
                            </form>
                        </h4>
                    </div><!-- End .panel-heading -->
                    <div id='containerList'></div>	

                </div><!-- End .panel -->
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->       
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<script type="text/javascript">
    $(document).ready(function () {
        load_datalist({action: "<?php echo admin_url('user/method/list_usertype'); ?>"});

        //Link
        $('.addusertype').on('click', {
            'action': "<?php echo admin_url("user/popupform_usertype"); ?>",
            'type': 1,
            'redirect': "<?php echo current_url(); ?>"
        }, load_dfltpopform);

        $('#containerList').on('click', '.editusertype', {
            'action': "<?php echo admin_url("user/popupform_usertype"); ?>",
            'type': 2,
            'redirect': "<?php echo current_url(); ?>"
        }, load_dfltpopform);

        $('#containerList').on('click', '.deleteusertype', {
            'action': "<?php echo admin_url("user/method/delete_usertype"); ?>",
            'conMessage': "You are about to delete this user type.",
            'redirect': "<?php echo current_url(); ?>"
        }, dfltaction_item);
    });
</script>
<?php $this->load->view(admin_dir('template/footer')); ?>