<?php $this->load->view(admin_dir('template/header')); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>Edit User</h3>     
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->

        <div class="row">
            <div class="col-lg-12">	
                <div class='clearfix'>
                    <div class="right">
                        <a href="<?php echo admin_url('user/list_user_page'); ?>"><span class='icon16 icomoon-icon-arrow-left-5'></span> Back</a>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span><?php echo $this->Misc->display_name($row->user_fname, $row->user_mname, $row->user_lname); ?>'s Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">ID No.</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control" disabled value='<?php echo $row->user_code; ?>' />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* First Name</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_fname" value='<?php echo $row->user_fname; ?>' />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Middle Name</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_mname" value='<?php echo $row->user_mname; ?>' />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Last Name</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_lname"  value='<?php echo $row->user_lname; ?>' />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Address</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_address" value='<?php echo $row->user_address; ?>' />
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Contact No</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_contact" value='<?php echo $row->user_contact; ?>' />
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* User Type</label>
                                            <div class="col-lg-5">
                                                <select class="form-control formdata" id="formdata_usertype" >
                                                    <option value=''></option>
                                                    <?php foreach ($user_types as $q) { ?>
                                                        <option value='<?php echo $q->id_user_type; ?>' <?php echo ($q->id_user_type == $row->id_user_type) ? "Selected" : ""; ?> ><?php echo $q->user_type_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Business Unit</label>
                                            <div class="col-lg-5">
                                                <select class="form-control formdata chosen-select" id="formdata_businessUnitID" >
                                                    <option value='0'>-</option>
                                                    <?php foreach ($departments as $q) { ?>
                                                        <option value='<?php echo $q->id_business_unit; ?>' <?= ($q->id_business_unit == $row->business_unit_id) ? 'selected' : ''; ?>><?php echo "$q->group_code | $q->business_unit_name"; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Business Unit for PR</label>
                                            <div class="col-lg-5">
                                                <select class="form-control formdata chosen-select" multiple id="formdata_businessIDs[]" >
                                                    <option value=''></option>
                                                    <?php foreach ($departments as $q) { ?>
                                                        <option value='<?php echo $q->id_business_unit; ?>' <?= (in_array($q->id_business_unit, $user_bu)) ? 'selected' : ''; ?>><?php echo "$q->group_code | $q->business_unit_name"; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Email</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata_email" value='<?php echo $row->user_email; ?>' />
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Password</label>
                                            <div class="col-lg-5">
                                                <input type="password" class="form-control formdata" id="formdata_password"  />
                                            </div>
                                        </div><!-- End .form-group  -->

                                        <div class="form-group">
                                            <div class="col-lg-offset-4 col-lg-8">
                                                <button  class="btn btn-warning ui-wizard-content ui-formwizard-button" id='formdata_save' type="button">Save</button>
                                            </div>
                                        </div><!-- End .form-group  --> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->  
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#formdata_save').on('click', {
            'action': "<?php echo admin_url("user/method/edit_user"); ?>",
            'id': "<?php echo $this->Misc->encode_id($row->id_user); ?>",
            'conMessage': "You are about to edit user."
        }, save_form);
    });
</script>	
<?php $this->load->view(admin_dir('template/footer')); ?>