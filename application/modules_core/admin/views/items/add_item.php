<?php $this->load->view(admin_dir('template/header'), $js_files); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span>Item Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Group </label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' multiple id='formdata-general_group_id[]' >
                                                    <option value=''></option>
                                                    <?php foreach ($groups as $q) { ?>
                                                        <option value='<?= $q->id_general_group; ?>'><?= $q->group_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> Stock Classification </label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-stock_classification_id' >
                                                    <option value=''></option>
                                                    <?php foreach ($stock_classifications as $q) { ?>
                                                        <option value='<?= $q->id_stock_classification; ?>'><?= $q->stock_classification_name; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* SKU</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-sku" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Description</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-description" />
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Unit Measurement</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata chosen-select' id='formdata-unit_measurement_id' >
                                                    <option value=''></option>
                                                    <?php foreach ($unit_measurements as $q) { ?>
                                                        <option value='<?= $q->id_unit_measurement; ?>'><?= $q->measurement_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                                <span id="custom_field"></span>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"> With Nominated Supplier:</label>
                                            <div class="col-lg-5">
                                                <input name="formdata-budget_status" class="form-control formdata_radio" id="formdata-with_supplier" type="radio" value="1"  /> <!-- value is 1 if within budget-->
                                                <i class="icon16 iconic-icon-checkmark">With</i>
                                                <input name="formdata-budget_status" class="form-control formdata_radio" id="formdata-with_supplier" type="radio" value="0" /> <!-- value is 2 if without budget-->
                                                <i class="icon16 iconic-icon-x">Without</i>
                                            </div>
                                        </div><!-- End .form-group    -->
                                    </div>
                                </div>
                            </div><!-- End .panel-body  -->
                            <div class="panel-heading">
                                <h4>
                                    <span>Suppliers' Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Supplier</label>
                                            <div class="col-lg-5">
                                                <select class='form-control formdata' id='formdata-supplier' >
                                                    <option value=''></option>
                                                    <?php foreach ($suppliers as $q) { ?>
                                                        <option value='<?= $q->id_supplier; ?>' supplier_name='<?= $q->supplier_name; ?>' ><?= $q->supplier_name; ?></option>
                                                    <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">* Item Unit Cost</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control formdata" id="formdata-unit_cost" />
                                                <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-add_supplier' type="button">Add Supplier</button>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">Supplier</th>
                                                    <th class="col-lg-1">Price</th>
                                                    <th class="col-lg-1">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id='supplier_body'>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div><!-- End .panel-body -->

                            <div class="panel-body ">
                                <div class="form-group">
                                    <div class="col-lg-offset-4 col-lg-8">
                                        <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='formdata-save' type="button">Add Item</button>
                                    </div>
                                </div><!-- End .form-group  -->
                            </div>

                        </div><!-- End .panel -->
                    </div>
                </div><!-- End .row -->
            </div><!-- End .span12 -->
        </div><!-- End .row -->
        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<?php $this->load->view(admin_dir('template/footer')); ?>