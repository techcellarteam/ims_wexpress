<?php foreach ($commodity as $q) { ?>
    <tr id="id_table_<?= $q->id_item; ?>">
        <td class='col-lg-1'>
            <input type='hidden' class='form-control formdata items' id='formdata-items[<?= $q->id_item; ?>]' value='<?= $q->id_item; ?>' />
            <?= $q->sku; ?>
        </td>
        <td class='col-lg-1'>
            <?= $q->description; ?>
        </td>
        <td class='col-lg-1'>
            <?= $q->measurement_code; ?>
        </td>
        <td class='col-lg-1'>
            <input type='number' min="1" class='form-control formdata' id='formdata-item_unit_cost[<?= $q->id_item; ?>]' value='<?= $q->item_unit_cost; ?>' />
            <?= $q->item_unit_cost; ?>
        </td>
        <td class='col-lg-1'>
            <input type='number' min="1" class='form-control formdata' id='formdata-quantity[<?= $q->id_item; ?>]' value='<?= $quantity; ?>' />
        </td>
        <td class='col-lg-1'>
            <button class='icon16 icomoon-icon-remove formdata-remove_item' id='<?= $q->id_item; ?>'></button>
        </td>
    </tr>
    <?php
}