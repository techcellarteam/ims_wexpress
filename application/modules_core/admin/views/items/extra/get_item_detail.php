<tr id="id_table_<?= $item->id_item; ?>">
    <td class='col-lg-1'>
        <?= $item->stock_classification_name; ?>
    </td>
    <td class='col-lg-1'>
        <input type='hidden' class='form-control formdata items' id='formdata-items[<?= $item->id_item; ?>]' value='<?= $item->id_item; ?>' />
        <?= $item->sku; ?>
    </td>
    <td class='col-lg-1'>
        <?= $item->description; ?>
    </td>
    <td class='col-lg-1'>
        <?= $item->measurement_code; ?>
    </td>
    <td class='col-lg-1'>
        <input type='hidden' class='form-control formdata quantities' id='formdata-quantity[<?= $item->id_item; ?>]' value='<?= $item->quantity; ?>' />
        <?= $item->quantity; ?>
    </td>
    <td class='col-lg-1'>
        <input type='hidden' class='form-control formdata last_ordered_price' id='formdata-last_ordered_price[<?= $item->id_item; ?>]' value='<?= number_format($item->last_ordered_price,2); ?>' />
        <?= number_format($item->last_ordered_price,2); ?>
    </td>
    <td class='col-lg-1'>
        <input type='hidden' class='form-control formdata initiators_prices' id='formdata-initiators_price[<?= $item->id_item; ?>]' value='<?= number_format($item->initiators_price,2); ?>' />
        <?= number_format($item->initiators_price,2); ?>
    </td>
    <td class='col-lg-1 total'>
        <?= number_format(($item->initiators_price * $item->quantity),2); ?>
    </td>
    <td class='col-lg-1'>
        <textarea class='form-control formdata hidden' id='formdata-remarks[<?= $item->id_item; ?>]'><?= $item->remarks; ?></textarea>
        <?= $item->remarks; ?>
    </td> 
    <td class='col-lg-1'>
        <button class='icon16 icomoon-icon-remove formdata-remove_item' id='<?= $item->id_item; ?>'></button>
    </td>
</tr>