<?php $this->load->view(admin_dir('template/header')); ?>
<!--Body content-->
<div id="content" class="clearfix">
    <div class="contentwrapper"><!--Content wrapper-->
        <div class="heading">
            <h3>View Item</h3>     
        </div><!-- End .heading-->

        <!-- Build page from here: Usual with <div class="row-fluid"></div> -->

        <div class="row">
            <div class="col-lg-12">	
                <div class='clearfix'>
                    <div class="right">
                        <a href="<?php echo admin_url('Items/list_item'); ?>"><span class='icon16 icomoon-icon-arrow-left-5'></span> Back</a>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <input type="hidden" class="form-control formdata" id="formdata-item_id" value='<?= $this->Misc->encode_id($this->id); ?>' />
                                    <span><?= $row->description; ?>'s Information</span>
                                </h4>
                            </div>
                            <div class="panel-body ">
                                <div class="row formdata_alert"></div>
                                <div class="row form-horizontal">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Group:</label>
                                            <div class="col-lg-5">
                                                <b><?= $row->group_code . " | " . $row->group_name; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Stock Classification:</label>
                                            <div class="col-lg-5">
                                                <b><?= $row->stock_classification_name; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">SKU: </label>
                                            <div class="col-lg-5">
                                                <b><?= $row->sku; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Description: </label>
                                            <div class="col-lg-5">
                                                <b><?= $row->description; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">Unit Measurement: </label>
                                            <div class="col-lg-5">
                                                <b><?= $row->measurement_code.' | '.$row->measurement_name; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label">With Supplier: </label>
                                            <div class="col-lg-5">
                                                <b><?= ($row->with_supplier)?'with Nominated Supplier':'none'; ?></b>
                                            </div>
                                        </div><!-- End .form-group  -->
                                    </div>
                                </div>
                            </div><!-- End .panel-body  -->

                            <div class="panel-heading">
                                <h4>
                                    <span>Item Suppliers' Information</span>
                                </h4>
                                <a href="#" class="minimize" style="display: block;">Minimize</a>
                            </div>
                            <div class="panel-body " id="item_supplier_info">
                                <div class="row form-horizontal">
                                    <div class="dataTables_wrapper form-inline responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-lg-1">Supplier</th>
                                                    <th class="col-lg-1">Price</th>
                                                    <th class="col-lg-1">View</th>
                                                </tr>
                                            </thead>
                                            <tbody id='supplier_body'>
                                                <?php foreach ($item_suppliers as $q) { ?>
                                                    <tr>
                                                        <td class="col-lg-1">
                                                            <b><?= $q->supplier_name; ?></b>
                                                        </td>
                                                        <td class="col-lg-1">
                                                            <b><?= $q->item_unit_cost; ?></b>
                                                        </td>
                                                        <td class="col-lg-1">
                                                            <a href="<?php echo admin_url("suppliers/view_supplier/" . $this->Misc->encode_id($q->supplier_id)); ?>" title="<?= $this->methodname; ?>" class="tip view_supplier">
                                                                <span class="icon16 icomoon-icon-search-3"></span>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div><!-- End .panel-body -->
                        </div>
                    </div><!-- End .panel -->
                </div><!-- End .row -->  
            </div><!-- End .span12 -->  
        </div><!-- End .row -->  
        <!-- Page end here -->
    </div><!-- End contentwrapper -->
</div><!-- End #content -->
<?php $this->load->view(admin_dir('template/footer')); ?>