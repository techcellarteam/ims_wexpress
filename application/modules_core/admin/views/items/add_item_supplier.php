<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="icon16 minia-icon-close"></span></button>
        <h3>Supplier Information</h3>
    </div>
    <div class="modal-body">
        <div class="row popformdata_alert"></div>
        <form class="form-horizontal" action="#" role="form">
            <div class="form-group">
                <label class="col-lg-4 control-label">* Item Supplier</label>
                <div class="col-lg-8">
                    <select class='form-control popformdata' id='popformdata-supplier' >
                        <option value=''></option>
                        <?php foreach ($suppliers as $q) { ?>
                            <option value='<?= $q->id_supplier; ?>' supplier_name='<?= $q->supplier_name; ?>' ><?= $q->supplier_name; ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
            </div><!-- End .form-group  -->
            <div class="form-group">
                <label class="col-lg-4 control-label">* Item Unit Cost</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control popformdata" id="popformdata-unit_cost" />
                    <button  class="btn btn-success ui-wizard-content ui-formwizard-button" id='popformdata-add_supplier' type="button">Add Supplier</button>
                </div>
            </div><!-- End .form-group  -->
            <div class="dataTables_wrapper form-inline responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="col-lg-1">Supplier</th>
                            <th class="col-lg-1">Price</th>
                            <th class="col-lg-1">Action</th>
                        </tr>
                    </thead>
                    <tbody id='supplier_body'>

                    </tbody>
                </table>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <div class='right'>
            <input type="hidden" class="form-control popformdata" id="popformdata-item_id" value='<?= $row->id_item; ?>'/>
            <button class="btn btn-success ui-wizard-content ui-formwizard-button" id="popformdata-save" type="button">Add</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#popformdata-save').on('click', {
        'action': adminURL + 'Items/method/add_item_suppliers',
        'id': "<?php echo $this->Misc->encode_id($row->id_item);?>",
        'conMessage': 'You are about to add new item supplier.',
        'redirect': currentURL,
    }, save_form_v1);
</script>