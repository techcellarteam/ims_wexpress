<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Purchase Order</title>
		<style>
			body {margin:5px;padding:0px;font-family: Verdana, sans-serif;font-size: 13px;}
			
			#wrapper {width:600px;}
			#logo {margin-bottom: 10px;}
			#header {padding-top: 2px; padding-bottom: 2px;}
			
			#main{margin: 10px 0 10px 0;}
			
			.banner{background-color: #9C231B; padding: 5px 2px 5px 10px;color:#fff}
			.mainInfo{margin-top: 15px; margin-bottom: 15px;}
			
			.line{border-top: 2px solid gray}
			.center{text-align: center;}
			.left-align{text-align: left;}
			.right-align{text-align: right;}
			.bold{font-weight: bold;}
			.left{float:left;}
			.right{float:right;}
			.clear{clear:both;}
			.w50{width:50%;}
			.w100{width:100%;}
			
			.table-header{background-color: #ABABAB}
			.table-body{background-color: #EBEBEB;padding-right:5px;}
			.table-product{background-color: #CCCCCC;padding-right:5px;}
			.table-others{background-color: #F4F4F4;padding-right:5px;}
			.table-total{background-color: #FF6666;padding-right:5px;}
                        
                        td, th {padding-top: 2px;padding-bottom:2px;}
			
		</style>
	</head>
	<body>
		<div id="wrapper">
			<div id="logo"><img src="<?php echo assets_dir("images/logo.png"); ?>"/></div>
			<span id="header">Hi <b><?=$user;?></b></span>
			<div id="main">
				<!--Purchase Order -->
				<div class="banner"><b>Purchase Order <?=$purchase_order->po_number;?></b></div>
				<div class="mainInfo">
                                        <span><b>Your purchase order request was successfully approved by <?=$administrator;?>.</b></span><br/>
                                        <span>
                                            Click <a href="<?=admin_url($this->classname, 'view', $purchase_order->id_purchase_order);?>">here</a> to view transaction details.
                                        </span>
				</div>
				<!--end purchase order-->
			</div>
			<div class="line"></div>
			<div class="center">Inventory Management System</div>
		</div>
	</body>
</html>